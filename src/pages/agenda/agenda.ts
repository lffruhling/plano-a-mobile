import {Component} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Calendar} from "@ionic-native/calendar";
import * as moment from "moment";
import {AcoesProvider} from "../../providers/acoes/acoes";
import {ChecklistProvider} from "../../providers/checklist/checklist";
import {EmpresaProvider} from "../../providers/empresa/empresa";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AuthProvider} from "../../providers/auth/auth";
import {ModalBaixaPage} from "../acoes/modal-baixa/modal-baixa";
import {PerguntasPage} from "../checklist/perguntas/perguntas";
import {EN_TAB_PAGES} from "../../config";
import {Push, PushObject, PushOptions} from "@ionic-native/push";
import {BackbuttonService} from "../../services/backbutton.service";
import {AppVersion} from "@ionic-native/app-version";
import {TecnicoProvider} from "../../providers/tecnico/tecnico";
import {VersoesProvider} from "../../providers/versoes/versoes";
import {Market} from "@ionic-native/market";
import {DbExceptionProvider} from "../../providers/db-exception/db-exception";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {FirestoreDatabaseProvider} from "../../providers/firestore-database/firestore-database";
import {GeolocationProvider} from "../../providers/geolocation/geolocation";
import {PropriedadesProvider} from "../../providers/produtores/propriedades";
import {IonicSelectableComponent} from "ionic-selectable";
import {RecontagemPlantelProvider} from "../../providers/recontagem-plantel/recontagem-plantel";
import {VisitasProvider} from "../../providers/visitas/visitas";
import {PlantelContagemPage} from "../produtores/recontagem-plantel/plantel-contagem/plantel-contagem";
import {VisitaLotePage} from "../visita-lote/visita-lote";

@IonicPage()
@Component({
	selector: 'page-agenda',
	templateUrl: 'agenda.html',
})
export class AgendaPage {

	date: any;
	daysInThisMonth: any;
	daysInLastMonth: any;
	daysInNextMonth: any;
	monthNames: string[];
	currentMonth: any;
	currentMonthNumber: any;
	nowMonthNumber: any;
	currentYear: any;
	currentDate: any;

	eventListCalendar: Array<any> = [];
	eventListCalendarOriginal: Array<any> = [];


	eventListDay: Array<any> = [];
	eventListDayOriginal: Array<any> = [];

	eventListWeek: Array<any> = [];
	eventListWeekOriginal: Array<any> = [];
	eventListMonth: Array<any> = [];
	eventListMonthOriginal: Array<any> = [];


	isSelected: any = false;
	selectedDay: any = null;
	currentDateSelected: any;
	dateNow = moment().format('DD');
	protected chk_periodo = false;
	aba = 'dia';
	mes_consulta = moment().format('M');
	tipo_consulta = 0;
	propriedade;
	protected propriedades: any[] = [];

	constructor(private navCtrl: NavController,
				private navParams: NavParams,
				private calendar: Calendar,
				private acaoProvider: AcoesProvider,
				private checklistProvider: ChecklistProvider,
				private empresaProvider: EmpresaProvider,
				private networkService: CheckNetworkProvider,
				private authProvider: AuthProvider,
				public modalCtrl: ModalController,
				private backbuttonService: BackbuttonService,
				private appVersion: AppVersion,
				private tecnicoProvider: TecnicoProvider,
				private versaoProvider: VersoesProvider,
				private market: Market,
				private exceptionProvider: DbExceptionProvider,
				private push: Push,
				public events: Events,
				private messagesService: AlertMessagesProvider,
				private fbDbProvider: FirestoreDatabaseProvider,
				private geolocationProvider: GeolocationProvider,
				private propriedadesProvider: PropriedadesProvider,
				private recontagesProvider: RecontagemPlantelProvider,
				private visitasLotesProvider: VisitasProvider,
	) {
		this.empresaProvider.getParamChkObrigaCapturaGeolocalizacao().then(data => {

			if (data) {
				let geolocation = this.geolocationProvider;
				setTimeout(() => this.geolocationProvider.currentLocation()
					.subscribe((resp) => {
						if (resp) {
							geolocation.saveGeolocation();
						}

					}, err => {
						console.error('falhou aqui dentro do time out tmb ', err)
					}), 60000);
			}
		});

		let day = new Date().getDay();
		this.currentDateSelected = `${this.currentYear}-${this.currentMonth}-${day}`
		this.empresaProvider.getParamChkPeriodo().then((chk_periodo: boolean) => {
			this.chk_periodo = chk_periodo;
		});

	}

	private ordenaEventos() {

		this.eventListMonth.sort(function (a, b) {
			if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
				return -1;
			} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
				return 1;
			} else {
				return 0;
			}
		});

		this.eventListWeek.sort(function (a, b) {
			if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
				return -1;
			} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
				return 1;
			} else {
				return 0;
			}
		});

		this.eventListDay.sort(function (a, b) {
			if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
				return -1;
			} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
				return 1;
			} else {
				return 0;
			}
		});

	}

	ionViewWillEnter() {
		this.date = new Date();

		this.propriedadesProvider.getPropriedadesLocal(true).then((data: any[]) => {
			this.propriedades = data;
			this.propriedades.unshift({'id': 0, 'propriedade_concat_produtor': 'Todos'});
			this.propriedade = {'id': 0, 'propriedade_concat_produtor': 'Todos'};
		});

		let day = new Date().getDay();
		this.currentDateSelected = `${this.currentYear}-${this.currentMonth}-${day}`
		this.empresaProvider.getParamChkPeriodo().then((chk_periodo: boolean) => {
			this.chk_periodo = chk_periodo;
		});

		this.carregaEventosMes().then(() => {
			this.ordenaEventos();
		});

		this.carregaEventosSemana().then(() => {
			this.ordenaEventos();
		});

		this.carregaEventosDia().then(() => {
			this.ordenaEventos();
		});

		/*Configurações de inicialização do app*/
		this.appVersion.getVersionNumber().then((versionNumber) => {
			this.events.publish('version_app:changed', versionNumber);
			this.versaoProvider.apiGetVersion().subscribe((response: any) => {
				let versao_web = response.versao.split(".");
				let versao_local = versionNumber.split(".");
				if (parseInt(versao_web[0]) > parseInt(versao_local[0])) {
					this.messagesService.showAlert('Atualização!', 'Exise uma atualização para o aplicativo. Você será redirecionado para atualizar a versão.').then(() => {
						this.market.open('br.net.agritec.appplanoa');
					});
				} else {
					if (parseInt(versao_web[0]) == parseInt(versao_local[0]) && parseInt(versao_web[1]) > parseInt(versao_local[1])) {
						this.messagesService.showAlert('Atualização!', 'Exise uma atualização para o aplicativo. Você será redirecionado para atualizar a versão.').then(() => {
							this.market.open('br.net.agritec.appplanoa');
						});
					} else {
						if (parseInt(versao_web[0]) == parseInt(versao_local[0]) && parseInt(versao_web[1]) == parseInt(versao_local[1]) && parseInt(versao_web[2]) > parseInt(versao_local[2])) {
							this.messagesService.showAlert('Atualização!', 'Exise uma atualização para o aplicativo. Você será redirecionado para atualizar a versão.').then(() => {
								this.market.open('br.net.agritec.appplanoa');
							});
						}
					}
				}
			}, err => {
				this.exceptionProvider.insert(err, 'agenda.ts', 'ionViewWillEnter');
				this.fbDbProvider.addExceptionApp(err, 'agenda.ts', 'ionViewWillEnter');
				console.error('erro ao buscar versão', err);
			})
		}, err => {
			this.exceptionProvider.insert(err, 'agenda.ts', 'ionViewWillEnter');
			this.fbDbProvider.addExceptionApp(err, 'agenda.ts', 'ionViewWillEnter');
			console.error(err, 'Erro ao pegar a versão do app');
		});

		this.backbuttonService.pushPage(EN_TAB_PAGES.EN_TP_ACAO, this.navCtrl);

		this.tecnicoProvider.get().then((result: any) => {
			this.events.publish('tecnico:changed', result)
		});

		this.pushSetup();

		this.nowMonthNumber = this.date.getMonth() + 1;
		this.monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
		this.getDaysOfMonth();

		this.loadEventThisMonth().then(() => {
			this.eventListCalendar.sort(function (a, b) {
				if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
					return -1;
				} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
					return 1;
				} else {
					return 0;
				}
			});
		});

	}

	private carregaEventosMes() {
		this.eventListMonth = [];
		this.eventListMonthOriginal = [];
		var startDate = moment(new Date(this.date.getFullYear(), parseInt(this.mes_consulta) - 1, 1)).format('YYYY-MM-DD');
		var endDate = moment(new Date(this.date.getFullYear(), parseInt(this.mes_consulta), 0)).format('YYYY-MM-DD');

		let promise = new Promise(resolve => {

			this.acaoProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
				events.forEach(event => {
					this.eventListMonth.push(event);
					this.eventListMonthOriginal.push(event);
				});

				this.recontagesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
					events.forEach(event => {
						this.eventListMonth.push(event);
						this.eventListMonthOriginal.push(event);
					});

					this.visitasLotesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
						events.forEach(event => {
							this.eventListMonth.push(event);
							this.eventListMonthOriginal.push(event);
						});

						this.empresaProvider.getParamChkPeriodo().then(chk_periodo => {
							if (chk_periodo) {
								this.checklistProvider.getByPeriodoAndDate(startDate, endDate).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListMonth.push(event);
										this.eventListMonthOriginal.push(event);
									});

									resolve();

								});
							} else {
								this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 3).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListMonth.push(event);
										this.eventListMonthOriginal.push(event);
									});

									this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 4).then((events: Array<any>) => {
										events.forEach(event => {
											this.eventListMonth.push(event);
											this.eventListMonthOriginal.push(event);
										});
										resolve();
									});
								});
							}
						});
					});

				});

			});
		});

		return promise;

	}

	private carregaEventosSemana() {
		this.eventListWeek = [];
		this.eventListWeekOriginal = [];
		let currentDate = moment();
		let startDate = currentDate.clone().startOf('week').format('YYYY-MM-DD');
		let endDate = currentDate.clone().endOf('week').format('YYYY-MM-DD');


		let promise = new Promise(resolve => {

			this.acaoProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
				events.forEach(event => {
					this.eventListWeek.push(event);
					this.eventListWeekOriginal.push(event);
				});

				this.recontagesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
					events.forEach(event => {
						this.eventListWeek.push(event);
						this.eventListWeekOriginal.push(event);
					});

					this.visitasLotesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
						events.forEach(event => {
							this.eventListWeek.push(event);
							this.eventListWeekOriginal.push(event);
						});

						this.empresaProvider.getParamChkPeriodo().then(chk_periodo => {
							if (chk_periodo) {
								this.checklistProvider.getByPeriodoAndDate(startDate, endDate).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListWeek.push(event);
										this.eventListWeekOriginal.push(event);
									});

									resolve();

								});
							} else {
								this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 3).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListWeek.push(event);
										this.eventListWeekOriginal.push(event);
									});

									this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 4).then((events: Array<any>) => {
										events.forEach(event => {
											this.eventListWeek.push(event);
											this.eventListWeekOriginal.push(event);
										});
										resolve();

									});
								});
							}
						});
					});

				});

			});
		});

		return promise;

	}

	private carregaEventosDia() {
		this.eventListDay = [];
		this.eventListDayOriginal = [];
		let currentDate = moment();
		let startDate = currentDate.format('YYYY-MM-DD');
		let endDate = currentDate.format('YYYY-MM-DD');

		let promise = new Promise(resolve => {

			this.acaoProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
				events.forEach(event => {
					this.eventListDay.push(event);
					this.eventListDayOriginal.push(event);
				});

				this.recontagesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
					events.forEach(event => {
						this.eventListDay.push(event);
						this.eventListDayOriginal.push(event);
					});

					this.visitasLotesProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
						events.forEach(event => {
							this.eventListDay.push(event);
							this.eventListDayOriginal.push(event);
						});

						this.empresaProvider.getParamChkPeriodo().then(chk_periodo => {
							if (chk_periodo) {
								this.checklistProvider.getByPeriodoAndDate(startDate, endDate).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListDay.push(event);
										this.eventListDayOriginal.push(event);
									});

									resolve();

								});
							} else {
								this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 3).then((events: Array<any>) => {
									events.forEach(event => {
										this.eventListDay.push(event);
										this.eventListDayOriginal.push(event);
									});

									this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 4).then((events: Array<any>) => {
										events.forEach(event => {
											this.eventListDay.push(event);
											this.eventListDayOriginal.push(event);
										});
										resolve();

									});
								});
							}
						});
					});

				});

			});
		});

		return promise;

	}


	/*
	* Funções utilizadas para o filtro
	* */
	getFilter() {
		this.eventListMonth = JSON.parse(JSON.stringify(this.eventListMonthOriginal));
		this.eventListWeek = JSON.parse(JSON.stringify(this.eventListWeekOriginal));
		this.eventListDay = JSON.parse(JSON.stringify(this.eventListDayOriginal));

		if (this.propriedade.id != 0) {
			this.eventListMonth = this.eventListMonth.filter(event => {
				return event.propriedade_id == this.propriedade.id;
			});

			this.eventListWeek = this.eventListWeek.filter(event => {
				return event.propriedade_id == this.propriedade.id;
			});

			this.eventListDay = this.eventListDay.filter(event => {
				return event.propriedade_id == this.propriedade.id;
			});
		} else {
			this.ordenaEventos();
		}

		if (this.tipo_consulta > 0) {
			this.eventListMonth = this.eventListMonth.filter(event => {
				return event.type == this.tipo_consulta;
			});

			this.eventListWeek = this.eventListWeek.filter(event => {
				return event.type == this.tipo_consulta;
			});

			this.eventListDay = this.eventListDay.filter(event => {
				return event.type == this.tipo_consulta;
			});
		} else {
			this.ordenaEventos();
		}
	}

	changeMesConsulta() {
		this.carregaEventosMes().then((_) => {
			this.ordenaEventos();

			this.propriedade = {'id': 0, 'propriedade_concat_produtor': 'Todos'};
			this.tipo_consulta = 0;

		})
	}

	/*
	* Funções utilizadas para o calendário
	* */

	compareDate(date_limit) {
		let d1 = new Date(date_limit);
		let d2 = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);

		return d1 < d2;
	}

	abrirModal(evento) {
		this.criaModal(evento).then((modal: any) => {
			modal.onDidDismiss((data) => {
				switch (parseInt(evento.type)) {
					case 1 : {
						let index = this.eventListMonth.indexOf(evento);
						if (data.remover_item) {
							if (index > -1) {
								this.eventListMonth.splice(index, 1);
								this.eventListMonthOriginal.splice(index, 1);
							}
						} else {
							/*Se o Mês de retorno da renovação é dirente do Mês atual, remove ela da lista e carrega no proximo mês*/
							if (moment().month() != moment(data.result.data_prevista).month()) {
								if (index > -1) {
									this.eventListMonth.splice(index, 1);
									this.eventListMonthOriginal.splice(index, 1);
								}
							} else {
								this.eventListMonth[index].date_limit = data.result.data_prevista;
								this.eventListMonthOriginal[index].date_limit = data.result.data_prevista;
							}

						}
					}
						break;
					case 2 : {
						if (data) {
							let index = this.eventListMonth.indexOf(evento);
							if (index > -1) {
								this.eventListMonth.splice(index, 1);
								this.eventListMonthOriginal.splice(index, 1);
							}
						}
					}
						break;
					case 3 : {
						if (data) {
							let index = this.eventListMonth.indexOf(evento);
							if (index > -1) {
								this.eventListMonth.splice(index, 1);
								this.eventListMonthOriginal.splice(index, 1);
							}
						}
					}
						break;
					case 4 : {
						if (data) {
							let index = this.eventListMonth.indexOf(evento);
							if (index > -1) {
								this.eventListMonth.splice(index, 1);
								this.eventListMonthOriginal.splice(index, 1);
							}
						}
					}
						break;
				}
			});
			modal.present();
		}).catch(err => {
			console.error('falha ao criar modal', err)
		});
	}

	criaModal(evento) {
		let promise = new Promise((resolve, reject) => {
			switch (parseInt(evento.type)) {
				case 1 : {
					this.acaoProvider.get(evento.id).then((acao: any) => {
						let modal = this.modalCtrl.create(ModalBaixaPage, {acao: acao});
						resolve(modal);
					}).catch((err) => {
						reject(err);
					});
				}
					break;
				case 2 : {
					if (evento.lote_id) {
						this.empresaProvider.getParamOrdenaAplicacaoChk().then((data: boolean) => {
							if (data) {
								this.checklistProvider.find(evento.id).then((ordem_aplicacao: any) => {
									this.checklistProvider.findChkAplicacaoAtual(evento.lote_id).then((chk_aplicavel) => {
										if (ordem_aplicacao > chk_aplicavel) {
											this.messagesService.showAlert('Oops! Checklist fora da Ordem!', 'Existe outro checklist que deve ser aplicado antes deste!');
										} else {
											let modal = this.modalCtrl.create(PerguntasPage, {
												checklist_id: evento.id,
												descricao: evento.message,
												lote_id: evento.lote_id,
												periodo_id: evento.periodo_id,
												consultaAux: true
											});
											resolve(modal);
										}
									})
								})
							} else {
								let modal = this.modalCtrl.create(PerguntasPage, {
									checklist_id: evento.id,
									descricao: evento.message,
									lote_id: evento.lote_id,
									periodo_id: evento.periodo_id,
									consultaAux: true
								});
								resolve(modal);
							}
						});
					} else {

						let modal = this.modalCtrl.create(PerguntasPage, {
							checklist_id: evento.id,
							descricao: evento.message,
							propriedade_id: evento.propriedade_id,
							intervalo_id: evento.intervalo_id,
							lote_id: evento.lote_id,
							periodo_id: evento.periodo_id,
							consultaAux: true
						});
						resolve(modal);

					}
				}
					break;
				case 3 : {
					this.recontagesProvider.getCelasUteis().then((data: any[]) => {
						let modal = this.modalCtrl.create(PlantelContagemPage, {
							recontagem_id: evento.id,
							data_prevista: evento.data_prevista,
							propriedade_id: evento.propriedade_id,
							galpao_id: evento.galpao_id,
							intervalo_id: evento.intervalo_id,
							produtor: evento.produtor,
							galpao: evento.galpao,
							celas_uteis: data
						});

						resolve(modal);
					});
				}
					break;
				case 4 : {
					let modal = this.modalCtrl.create(VisitaLotePage, {
						visita_lancamento_id: evento.id,
						lote_id: evento.lote_id,
						propriedade_id: evento.propriedade_id,
						galpao_id: evento.galpao_id,
						checklist_id: evento.checklist_id,
						produtor: evento.location,
						galpao: evento.galpao,
						aplica_chk_gp: evento.aplica_chk_gp,
						lote: evento.lote,
					});

					resolve(modal);
				}
					break;
			}
		});

		return promise;
	}


	/*Atualização pushToDown*/

	swipe(event) {
		if (event.direction === 4) {
			this.navCtrl.parent.select(3);
		} else if (event.direction === 2) {
			this.navCtrl.parent.select(1);
		}
	}

	doRefresh(refresher) {
		let count = 0;

		this.propriedadesProvider.getPropriedadesLocal(true).then((data: any[]) => {
			this.propriedades = data;
			this.propriedades.unshift({'id': 0, 'propriedade_concat_produtor': 'Todos'});
			this.propriedade = {'id': 0, 'propriedade_concat_produtor': 'Todos'};
		});

		this.carregaEventosMes().then(() => {
			this.ordenaEventos();
			count += 1;
			if (count == 3) {
				refresher.complete();
			}
		});

		this.carregaEventosSemana().then(() => {
			this.ordenaEventos();
			count += 1;
			if (count == 3) {
				refresher.complete();
			}
		});

		this.carregaEventosDia().then(() => {
			this.ordenaEventos();
			count += 1;
			if (count == 3) {
				refresher.complete();
			}
		});
	}

	/*
	* Métodos utilizados para o calendário
	* */
	getDaysOfMonth() {
		this.daysInThisMonth = new Array();
		this.daysInLastMonth = new Array();
		this.daysInNextMonth = new Array();
		this.currentMonth = this.monthNames[this.date.getMonth()];
		this.currentMonthNumber = this.date.getMonth() + 1;
		this.currentYear = this.date.getFullYear();
		if (this.date.getMonth() === new Date().getMonth()) {
			this.currentDate = new Date().getDate();
		} else {
			this.currentDate = 999;
		}

		var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
		var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
		for (var a = prevNumOfDays - (firstDayThisMonth - 1); a <= prevNumOfDays; a++) {
			this.daysInLastMonth.push(a);
		}

		var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
		for (var b = 0; b < thisNumOfDays; b++) {
			this.daysInThisMonth.push(b + 1);
		}

		var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
		var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
		for (var c = 0; c < (6 - lastDayThisMonth); c++) {
			this.daysInNextMonth.push(c + 1);
		}
		var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
		if (totalDays < 36) {
			for (var d = (7 - lastDayThisMonth); d < ((7 - lastDayThisMonth) + 7); d++) {
				this.daysInNextMonth.push(d);
			}
		}

	}

	goToLastMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
		this.getDaysOfMonth();
		this.loadEventThisMonth().then(() => {
			this.eventListCalendar.sort(function (a, b) {
				if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
					return -1;
				} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
					return 1;
				} else {
					return 0;
				}
			});
		});
	}

	goToNextMonth() {
		this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
		this.getDaysOfMonth();
		this.loadEventThisMonth().then(() => {
			this.eventListCalendar.sort(function (a, b) {
				if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
					return -1;
				} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
					return 1;
				} else {
					return 0;
				}
			});
		});
	}

	loadEventThisMonth() {
		let promise = new Promise(resolve => {
			this.eventListCalendar = [];
			this.eventListCalendarOriginal = [];
			var startDate = moment(new Date(this.date.getFullYear(), this.date.getMonth(), 1)).format('YYYY-MM-DD');
			var endDate = moment(new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0)).format('YYYY-MM-DD');

			this.acaoProvider.getByDate(startDate, endDate).then((events: Array<any>) => {
				events.forEach(event => {
					this.eventListCalendar.push(event);
					this.eventListCalendarOriginal.push(event);
				});

				this.empresaProvider.getParamChkPeriodo().then(chk_periodo => {
					if (chk_periodo) {
						this.checklistProvider.getByPeriodoAndDate(startDate, endDate).then((events: Array<any>) => {
							events.forEach(event => {
								this.eventListCalendar.push(event);
								this.eventListCalendarOriginal.push(event);
							});

							resolve();

						});
					} else {
						this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 3).then((events: Array<any>) => {
							events.forEach(event => {
								this.eventListCalendar.push(event);
								this.eventListCalendarOriginal.push(event);
							});

							this.checklistProvider.getByLoteAndDateAndFase(startDate, endDate, 4).then((events: Array<any>) => {
								events.forEach(event => {
									this.eventListCalendar.push(event);
									this.eventListCalendarOriginal.push(event);
								});

								resolve();

							});
						});
					}
				});

			});
		});

		return promise;
	}

	checkEvent(day) {
		var hasEvent = false;
		var thisDate1 = this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).slice(-2) + "-" + ("0" + (day)).slice(-2);

		this.eventListCalendarOriginal.forEach(event => {
			if (event.date_limit == thisDate1) {
				hasEvent = true;
			}
		});

		return hasEvent;
	}

	selectDate(day) {
		var thisDate1 = this.date.getFullYear() + "-" + ("0" + (this.date.getMonth() + 1)).slice(-2) + "-" + ("0" + (day)).slice(-2);

		if (this.currentDate != day) {
			this.selectedDay = day;
			this.isSelected = !this.isSelected;

			if (this.isSelected) {
				this.eventListCalendar = this.eventListCalendar.filter(event => {
					return event.date_limit == thisDate1
				});
			} else {
				this.selectedDay = null;
				this.eventListCalendar = JSON.parse(JSON.stringify(this.eventListCalendarOriginal));
				this.eventListCalendar.sort(function (a, b) {
					if (moment(a.date_limit).isBefore(moment(b.date_limit))) {
						return -1;
					} else if (moment(a.date_limit).isAfter(moment(b.date_limit))) {
						return 1;
					} else {
						return 0;
					}
				});
			}
		} else {
			this.eventListCalendar = this.eventListCalendar.filter(event => {
				return event.date_limit == thisDate1
			});
		}

	}

	/*Push Message*/
	private pushSetup() {
		this.tecnicoProvider.getTokenExists().then(data => {
			if (data == null) {
				const options: PushOptions = {
					android: {
						senderID: '15881055492',
						sound: true,
						vibrate: true,
						forceShow: true,
					},
					ios: {
						alert: true,
						badge: true,
						sound: true
					}
				};

				const pushObject: PushObject = this.push.init(options);

				pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

				pushObject.on('registration').subscribe((registration: any) => {
					this.tecnicoProvider.saveTokenFCM(registration.registrationId);
					if (this.networkService.isConnected()) {
						this.tecnicoProvider.get().then((tecnico) => {
							this.tecnicoProvider.apiUpdate(tecnico)
						})
					}
				});

				pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
			}
		})

	}

}
