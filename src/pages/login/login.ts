import {Component} from '@angular/core';
import {Events, Nav} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {finalize} from 'rxjs/operators';
import {DatabaseProvider} from "../../providers/database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from '@ionic/storage';
import {TecnicoProvider} from "../../providers/tecnico/tecnico";
import {SERVERS} from "../../config";
import {SlideProvider} from "../../providers/slide/slide";
import {IntroPage} from "../intro/intro";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {SincronizacaoPage} from "../sincronizacao/sincronizacao";
import {DbExceptionProvider} from "../../providers/db-exception/db-exception";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  protected mostraServers = true;

  constructor(private readonly authProvider: AuthProvider,
              private readonly dbProvider: DatabaseProvider,
              private readonly storage: Storage,
              private messageCtrl: AlertMessagesProvider,
              private readonly tecnicoProvider: TecnicoProvider,
              private readonly networkService: CheckNetworkProvider,
              public nav: Nav,
              private slideProvider: SlideProvider,
              private mesageService: AlertMessagesProvider,
              public events: Events,
              private exceptionProvider:DbExceptionProvider,
  ) {
    this.dbProvider.createDatabase();
    this.dbProvider.updateDatabase();
    this.dbProvider.createDatabaseCrash();
    this.dbProvider.createDatabaseGeo();

    if (slideProvider.getConfigSlide() != 'false') {
      this.nav.setRoot(IntroPage);
    } else {
      this.storage.get('SERVER_URL').then((data: any) => {
        this.mostraServers = data == null;
      })
    }
  }

  login(value: any) {
    let server: any = SERVERS.find(server => server.codigo === value.codigo.toUpperCase().trim());

    if (!server) {
      this.mesageService.showToastError('Ops!, Esse código da empresa não existe!');
    } else {
      this.storage.set('SERVER_URL', server.url);
      this.storage.set('NOME_EMPRESA', server.nome);
      this.storage.set('COD_EMPRESA', server.codigo);
      this.events.publish('empresa:changed', server.nome);
      this.events.publish('url:changed', server.url);

      let loading = this.messageCtrl.showLoading('Autenticando ...', 'bubbles');

      loading.present();
      if (this.networkService.isConnected()) {
          this.authProvider
            .login(value, server.url)
            .pipe(finalize(() => {
              loading.dismiss();
            }))
            .subscribe((response:any) => {
            	console.log('#############login###########', value);
              this.storage.set('login', value);
              this.nav.setRoot('TabsPage');
              this.dbProvider.getDB().then((db: SQLiteObject) => {
                  let sql = 'SELECT ultima_sincronizacao FROM tecnicos';
                  db.executeSql(sql,[]).then((data)=>{

                    if(data.rows.item(0) == null){
                      this.messageCtrl.showConfirmationAlert('Sincronizar?',
                        'Aparentemente faz um tempo que a aplicação não é sincronizada, deseja sincronizar agora?',
                        'Sim, sincronizar', 'Cancelar').then((data)=>{

                          if(data){
                            this.nav.push(SincronizacaoPage, {origin: ['PlanoA', 'Produtores', 'Checklist', 'All'], automatico:true});
                          }
                      })
                    }
                  }).catch(err=>{
                    this.exceptionProvider.insert(err, 'login.ts', 'login consulta lastSync', sql, null);
                    console.log('falha ao consultar ultima_sincronizacao');
                  })

              });
            }, err => {
              this.handleError(err)
            });

      } else {
        this.tecnicoProvider.checkLogin(value.email, value.password).then((data: any) => {
          loading.dismiss();
          if (data) {
            this.nav.setRoot('TabsPage')
          } else {
            this.messageCtrl.showToast('O login falhou. Tente novamente.');
          }
        })
      }

    }
  }

  handleError(response: any) {
    let message: any;
    if (response.status && response.status === 401) {
      message = 'O login falhou. Tente novamente.';
    } else {
      message = JSON.parse(response.error);
      message = message.error;
      // message = 'O login falhou. Tente novamente.';
    }

    this.messageCtrl.showToast(message);
  }

}
