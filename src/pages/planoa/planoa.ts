import {Component} from '@angular/core';
import {Events, IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {AcoesIndexPage} from "../acoes/acoes/index/acoes-index";
import {SlideProvider} from "../../providers/slide/slide";
import {IntroPage} from "../intro/intro";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {SincronizacaoPage} from "../sincronizacao/sincronizacao";

export interface PageInterface {
    title: string;
    pageName: string;
    icon: string;
    modal?: boolean;
}


@IonicPage()
@Component({
  selector: 'page-planoa',
  templateUrl: 'planoa.html',
})
export class PlanoaPage {
  pages: PageInterface[];

  rootPage: any;
  dadosEnvio:any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public nav: Nav,
              private networkService: CheckNetworkProvider,
              private authProvider: AuthProvider,
              private slideProvider: SlideProvider,
              private messagesService: AlertMessagesProvider,
              public events: Events,
  ) {

    if(slideProvider.getConfigSlide() != 'false'){
      this.nav.setRoot(IntroPage);
    }else {
      this.pages = [
        { title: 'Ações', pageName: "AcoesIndexPage", icon: 'add-circle'},
        { title: 'Avaliação Inicial', pageName: "AvaliacaoInicialPage", icon:'clipboard'},
        { title: 'Ações baixadas', pageName: "BaixadasPage", icon: 'checkmark-circle'},
      ];
    }
  }


  openPage(page) {
    this.nav.push(page.pageName)
  }

  swipe(event) {
    if(event.direction === 4) {
      this.navCtrl.parent.select(0);
    }else if(event.direction === 2){
      this.navCtrl.parent.select(2);
    }
  }

  doRefresh(refresher) {

    if(this.networkService.isConnected()) {
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.nav.push(SincronizacaoPage, {automatico:true});
        }
        refresher.complete();
      });
    }else{
      refresher.complete();
      this.messagesService.showToast('Ops, não há uma conexão com a internet no momento!')
    }
  }

}
