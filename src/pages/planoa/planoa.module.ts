import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanoaPage } from './planoa';

@NgModule({
  declarations: [
    PlanoaPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanoaPage),
  ],
})
export class PlanoaPageModule {}
