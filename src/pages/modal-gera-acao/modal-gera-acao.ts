import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import * as moment from "moment";
import {FormControl, FormGroup} from "@angular/forms";
import {UniqueIdProvider} from "../../providers/unique-id/unique-id";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AcoesProvider} from "../../providers/acoes/acoes";
import {PropriedadesProvider} from "../../providers/produtores/propriedades";
import {GrupoAcoesProvider} from "../../providers/acoes/grupoAcoes";
import {PontoAcoesProvider} from "../../providers/acoes/pontoAcoes";
import {ParametroAcoesProvider} from "../../providers/acoes/parametroAcoes";
import {CameraProvider} from "../../providers/camera/camera";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {Propriedade} from "../../class/Propriedade";
import {UtilsService} from "../../services/utils.service";
import {AuthProvider} from "../../providers/auth/auth";
import {DbExceptionProvider} from "../../providers/db-exception/db-exception";
import {FirestoreDatabaseProvider} from "../../providers/firestore-database/firestore-database";
import {ModalSignPage} from "../modal-sign/modal-sign";
import {IntervalosProvider} from "../../providers/intervalos/intervalos";
import {CalendarComponentOptions} from "ion2-calendar";

@IonicPage()
@Component({
  selector: 'page-modal-gera-acao',
  templateUrl: 'modal-gera-acao.html',
})
export class ModalGeraAcaoPage implements OnInit{

  protected propriedade_id;
  protected tipo_integracao_id;
  protected pergunta_id;
  protected parametro_id;
  protected grupo_id;
  protected ponto_id;
  protected acao_id;
  protected propriedade;
  protected grupos;
  protected pontos;
  protected parametros;
  protected valor_padrao;
  protected now = moment().format('YYYY-MM-DD');
  acaoForm: FormGroup;
  protected mobile_key;
  protected retornoModal = false;
  protected informacao_padrao;
  protected fotos: any[] = [];
	protected isIos = false;
	protected assinautraProdutor = null;
	protected maxDate;
	protected options;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private viewCtrl: ViewController,
              private uniqueKeyService: UniqueIdProvider,
              private networkService: CheckNetworkProvider,
              private acaoService: AcoesProvider,
              private propriedadeService: PropriedadesProvider,
              private grupoAcoesProvider: GrupoAcoesProvider,
              private pontoAcoesProvider: PontoAcoesProvider,
              private parametroAcoesProvider: ParametroAcoesProvider,
              private cameraService: CameraProvider,
              private messageService: AlertMessagesProvider,
              private authProvider: AuthProvider,
              private dbException: DbExceptionProvider,
              private utils: UtilsService,
              private fbDbProvider: FirestoreDatabaseProvider,
			  private modalCtrl: ModalController,
			  private plt: Platform,
			  private intervalosProvider: IntervalosProvider,
  ) {
    this.propriedade_id = navParams.get('propriedade_id');
    this.tipo_integracao_id = navParams.get('tipo_integracao_id');
    this.pergunta_id = navParams.get('pergunta_id');
    this.grupo_id = navParams.get('grupo_id');
    this.ponto_id = navParams.get('ponto_id');
    this.parametro_id = navParams.get('parametro_id');

    this.mobile_key = this.uniqueKeyService.generateKey();

    this.propriedadeService.get(this.propriedade_id).then((propriedade:Propriedade) =>{
      this.propriedade = propriedade;
      this.acaoForm.controls['tipo_integracao_id'].setValue(propriedade.tipo_integracao_id);
    }).catch((err) => { console.error('falha ao consultar propriedade no modal de avaliação', err)});
  }

  ngOnInit() {
	  this.intervalosProvider.getDiasIntervaloAplicaChk().then(data => {
		  if(data != null){
			  this.maxDate = moment().add(data, 'day');
		  }else{
			  this.maxDate = moment().add(1, 'year');
		  }

		  const opt: CalendarComponentOptions = {
			  from: moment().toDate(),
			  to: this.maxDate.toDate(),
			  weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
			  monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
		  }
		  this.options = opt;
	  });
	  this.isIos = this.plt.is('ios');

    this.acaoForm = new FormGroup({
      'propriedade_id': new FormControl(this.propriedade_id),
      'tipo_integracao_id': new FormControl(this.tipo_integracao_id),
      'situacao_id': new FormControl(1),
      'grupo_acao_id': new FormControl(null),
      'ponto_acao_id': new FormControl(this.ponto_id),
      'parametro_acao_id': new FormControl(this.parametro_id),
      'valor_padrao': new FormControl(null),
      'o_que': new FormControl(''),
      'como': new FormControl(''),
      'quem': new FormControl(''),
      'quando': new FormControl(''),
      'foto': new FormControl('assets/imgs/camera_800X300.png'),
      'com_foto': new FormControl(false),
      'considerar_calculos': new FormControl(false),
      'mobile_key': new FormControl(this.mobile_key),
      'pergunta_id': new FormControl(this.pergunta_id),
      'data_criacao': new FormControl(this.now),
    });

    this.grupoAcoesProvider.find(this.grupo_id).then(grupo=>{
      this.acaoForm.controls['grupo_acao_id'].setValue(grupo);

      this.pontoAcoesProvider.find(this.ponto_id).then(ponto=>{
        this.acaoForm.controls['ponto_acao_id'].setValue(ponto);

        this.parametroAcoesProvider.find(this.parametro_id).then(parametro=>{
          this.acaoForm.controls['parametro_acao_id'].setValue(parametro);

          this.valorPadrao(null, parametro);

        })

      })
    }).catch(err=>{
      console.error('falha ao consultar/preencher grupo acao', err);
    });

  }

  /* Atualiza dados base antes de exibir a página o modal e caso haja conexão*/
  ionViewWillEnter(){
    this.carregarGrupos(this.tipo_integracao_id);
  }

  carregarGrupos(tipo_integracao_id) {
    this.acaoForm.controls['tipo_integracao_id'].setValue(tipo_integracao_id);
    this.grupoAcoesProvider.getGruposPorIntegracaoLocal(tipo_integracao_id).then((data:any) => {
      this.grupos = data;
    })

  }

  carregarPontos(event) {
    let grupo_acao_id;
    if(event){
      grupo_acao_id = event.value.id;

    }else{
      grupo_acao_id = this.grupo_id;
    }
    this.pontoAcoesProvider.getPontosPorGrupoLocal(grupo_acao_id).then((data:any) => {
      this.pontos = data;

    })
  }

  carregarParametros(event) {
    let ponto_id;
    if(event){
      ponto_id = event.value.id;

    }else{
      ponto_id = this.ponto_id;
    }
    this.parametroAcoesProvider.getParametrosPorPontoLocal(ponto_id).then((data:any) => {
      this.parametros = data;
    });
  }

  /* Adiciona valor padrão do parametro da ação*/
  valorPadrao(event, parametro = null) {
    let valor_padrao;
    if(event == null && parametro != null){
      valor_padrao = parametro.informacao_padrao
    }else{
      valor_padrao = event.value.informacao_padrao
    }

    if (valor_padrao == "true") {
      this.informacao_padrao = "Sim";

    } else if (valor_padrao == "false") {
      this.informacao_padrao = "Não";
    } else {
      this.informacao_padrao = valor_padrao
    }

    this.acaoForm.controls['valor_padrao'].setValue(valor_padrao);
  }

  /* Gera/Salva a ação */
  gerarAcao(){
    let loading = this.messageService.showLoading('Salvando dados...');
    this.acaoForm.controls['grupo_acao_id'].setValue(this.acaoForm.value.grupo_acao_id.id);
    this.acaoForm.controls['parametro_acao_id'].setValue(this.acaoForm.value.parametro_acao_id.id);
    this.acaoForm.controls['ponto_acao_id'].setValue(this.acaoForm.value.ponto_acao_id.id);

    if (this.utils.validaAcao(this.acaoForm.value)) {
      this.acaoService.insert(this.acaoForm.value, this.assinautraProdutor).then(acao=>{

        this.messageService.showToastSuccess('Ação gerada com sucesso!');
        this.acao_id = acao.insertId;
        let last_id = acao.insertId;
        this.acaoService.ligaFotos(this.mobile_key, last_id);

        this.retornoModal = true;
        this.dismiss();
        loading.dismiss();

        this.authProvider.checkLogin().then((data: any) => {

          if (data == "success") {
            this.utils.retornaUUID().then((uuid: any) => {
              let dados = this.acaoForm.value;
              dados.id_mobile = last_id;
              dados.device_uuid = uuid;

              this.acaoService.apiStore(dados).subscribe((response:any) => {

                this.acaoService.updateSinc(response.id, response.id_mobile);

                this.acaoService.enviaFotos(last_id, response.id);

              });
            });
          }
        });
      });


    } else{
      this.messageService.showAlert('Ops', 'Houve uma falha ao preencher os dados da ação, tente novamente!');
      this.dbException.insert('falha ao salvar ação', 'modal-avaliacao.ts','gerarAcao', 'sql de insert ação', this.acaoForm.value);
      this.fbDbProvider.addExceptionApp('falha ao salvar ação', 'modal-avaliacao.ts','gerarAcao', 'sql de insert ação', this.acaoForm.value);
      loading.dismiss();
    }

    this.fbDbProvider.addAcao(this.acaoForm.value);

  }

  dismiss() {
    this.viewCtrl.dismiss({
      retornoModal:this.retornoModal,
      acao_id: this.acao_id
    });
  }

  newFoto() {
    let mobile_key = this.mobile_key;
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.acaoService.insertFoto({
            mobile_key: mobile_key,
            img: foto
          }).then((data)=>{
            this.fotos.push({
              id: data.insertId,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err=>{
            console.error('falha salvar foto acao - tela', err);
          });


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        this.acaoService.deleteFoto(foto_id).then(()=>{
          this.fotos = this.fotos.filter((_foto)=>{
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

	assinarAcao() {
		let modal = this.modalCtrl.create(ModalSignPage);
		modal.onDidDismiss((data) => {
			if (data.retornoModal) {
				this.assinautraProdutor = data.assinatura;
			}
		});
		modal.present();
	}
}
