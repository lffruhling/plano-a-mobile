import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalGeraAcaoPage } from './modal-gera-acao';

@NgModule({
  imports: [
    IonicPageModule.forChild(ModalGeraAcaoPage),
  ],
})
export class ModalGeraAcaoPageModule {}
