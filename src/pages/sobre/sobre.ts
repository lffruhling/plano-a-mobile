import {Component, OnInit} from '@angular/core';
import { IonicPage} from 'ionic-angular';
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {VersoesProvider} from "../../providers/versoes/versoes";

/**
 * Generated class for the SobrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sobre',
  templateUrl: 'sobre.html',
})
export class SobrePage implements OnInit{
  private readonly tawkChatLink : string = 'https://tawk.to/chat/5d152cc953d10a56bd7c399f/1dfc4cggv';
  protected versoes:any[];

  constructor(
    private networkService: CheckNetworkProvider,
    private iab: InAppBrowser,
    private messageService: AlertMessagesProvider,
    private versaoProvider: VersoesProvider) {
  }

  ngOnInit(){
    this.versaoProvider.getAll().then((data:any[])=>{
      this.versoes = data;
      console.log(data)
    }).catch((err)=>{
      console.error('falha ao buscar versoes', err);
    })
  }

  onClickChat() {
    if(this.networkService.isConnected()){
      this.iab.create(this.tawkChatLink,'_blank',{location:'yes'})
    }else{
      this.messageService.showToast('Você precisa estar conectado na internet para utilizar o chat!')
    }
  }

}
