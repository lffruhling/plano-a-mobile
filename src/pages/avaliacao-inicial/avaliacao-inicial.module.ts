import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvaliacaoInicialPage } from './avaliacao-inicial';
import {IonicSelectableModule} from "ionic-selectable";

@NgModule({
  declarations: [
    AvaliacaoInicialPage,
  ],
  imports: [
    IonicPageModule.forChild(AvaliacaoInicialPage),
    IonicSelectableModule
  ],
})
export class AvaliacaoInicialPageModule {}
