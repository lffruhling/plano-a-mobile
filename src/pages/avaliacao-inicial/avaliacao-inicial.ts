import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, FabContainer} from 'ionic-angular';
import { PropriedadesProvider } from '../../providers/produtores/propriedades';
import { AvaliacoesProvider } from '../../providers/acoes/avaliacoes';
import { ModalAvaliacaoPage } from "../acoes/modal-avaliacao/modal-avaliacao";
import {ParametroAcoesProvider} from "../../providers/acoes/parametroAcoes";
import {TecnicoProvider} from "../../providers/tecnico/tecnico";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {CameraProvider} from "../../providers/camera/camera";
import {Storage} from "@ionic/storage";
import {UniqueIdProvider} from "../../providers/unique-id/unique-id";
import {UtilsService} from "../../services/utils.service";
import {AuthProvider} from "../../providers/auth/auth";
import {FirestoreDatabaseProvider} from "../../providers/firestore-database/firestore-database";
import {FotosPlanoAcaoPage} from "../fotos-plano-acao/fotos-plano-acao";

@IonicPage()
@Component({
  selector: 'page-avaliacao-inicial',
  templateUrl: 'avaliacao-inicial.html',
})
export class AvaliacaoInicialPage {
  public propriedades_av:Array<any>;
  protected parametros;
  protected parametrosOriginais;
  public propriedade_id;
  public tipo_integracao_id;
  protected avaliacao:any;
  protected isSearchbarOpened = false;
  protected busca:string = null;
  protected mobile_key = null;

  constructor(
  	private navCtrl: NavController,
  	private navParams: NavParams,
    private propriedadesProvider: PropriedadesProvider,
    private parametrosProvider: ParametroAcoesProvider,
  	private avaliacaoProvider: AvaliacoesProvider,
    private modalCtrl: ModalController,
    private networkService: CheckNetworkProvider,
    private zone: NgZone,
    private providerTecnico: TecnicoProvider,
    private messageService: AlertMessagesProvider,
    private cameraService: CameraProvider,
    private storage: Storage,
    private authProvider: AuthProvider,
    private uniqueKeyService: UniqueIdProvider,
    private utils: UtilsService,
    private fbDbProvider: FirestoreDatabaseProvider
  ) {
    this.propriedadesProvider.getPropriedadesLocal().then((data:any) => {
      this.propriedades_av = data;
    });

    this.mobile_key = this.uniqueKeyService.generateKey();
  }

  getParametros(event) {
    let propriedade_id = event.value.id;
    let integracao_id = event.value.tipo_integracao_id;
    let loading = this.messageService.showLoading('Buscando parâmetros...');

    this.parametrosProvider.getAll(propriedade_id, integracao_id).then((data:any) => {
        this.parametros = data;
        this.parametrosOriginais = data;
        this.propriedade_id = propriedade_id;
        this.propriedadesProvider.get(propriedade_id).then((propriedade:any)=>{
          this.tipo_integracao_id = propriedade.tipo_integracao_id;
        });
        loading.dismiss();
      }, error => {
        console.error('erro', error);
        loading.dismiss();
      });
  }

  abrirModal(parametro, propriedade_id){

    let modal = this.modalCtrl.create(ModalAvaliacaoPage, {
      parametro: parametro,
      parametro_id   : parametro.id,
      propriedade_id : propriedade_id,
      tipo_integracao_id : this.tipo_integracao_id,
      grupo_acao_id : parametro.grupo_acao_id,
      ponto_acao_id : parametro.ponto_acao_id,
    });

    modal.onDidDismiss((data)=>{
      if(data){
        let index = this.parametros.indexOf(parametro);
        if( index > -1){
          this.parametros.splice(index, 1);
        }
      }
    });

    modal.present();
  }

  resolver(parametro ,propriedade_id, parametro_id){
    this.messageService.showConfirmationAlert('Avaliar', 'Deseja marcar este parâmetro como avaliado?', 'Sim, Avaliar!', 'Não').then((res)=>{
      if(res){
        let dateNow = new Date ();
        let data_formatada = dateNow.getFullYear () + "-" + (dateNow.getMonth () + 1) + "-" + dateNow.getDate ();

        this.avaliacao = {
          propriedade_id: propriedade_id,
          parametro_id:parametro_id,
          data_visita:data_formatada,
          gerou_acao:false,
          com_acao:false,
          mobile_key: this.mobile_key
        };

        this.avaliacaoProvider.insert(this.avaliacao).then((data) => {
          let last_id = data.insertId;
          this.messageService.showConfirmationAlert('Fotografar?', 'Deseja capturar uma foto do parâmetro avaliado?', 'Sim, Tirar Foto!', 'Não')
            .then((res)=> {

              if (res) {
                this.navCtrl.push(FotosPlanoAcaoPage, {
                  origin: 'avaliacao-inicial',
                  mobile_key: this.mobile_key,
                  title: 'da Avaliação',
                  id_avi: last_id
                });

                let index = this.parametros.indexOf(parametro);
                if( index > -1){
                  this.parametros.splice(index, 1);
                }
              }else{

                let index = this.parametros.indexOf(parametro);
                if( index > -1){
                  this.parametros.splice(index, 1);
                }

                this.messageService.showToastSuccess('Resolvido com sucesso!');

                if(this.networkService.isConnected()) {
                  this.authProvider.checkLogin().then((data: any) => {
                    if (data == "success") {
                      this.utils.retornaUUID().then((uuid: any) => {
                          let dados = this.avaliacao;
                          dados.id_mobile = last_id;
                          dados.device_uuid = uuid;
                          dados.com_acao = false;
                          this.avaliacaoProvider.apiStore(dados).subscribe((response:any)=>{
                            let avaliacao_up:any = {
                              propriedade_id: dados.propriedade_id,
                              parametro_id: dados.parametro_id,
                              tecnico_id: dados.tecnico_id,
                              data_visita: dados.data_visita,
                              gerou_acao:dados.gerou_acao,
                              mobile_key:dados.mobile_key,
                              id_web: response.id,
                              id: dados.id,
                            };

                            this.avaliacaoProvider.update(avaliacao_up);

                          })
                      })

                    }
                  });
                }
              }
            });
        });
        this.fbDbProvider.addAvaliacaoInicial(this.avaliacao);
      }
    });

  }

  mostraCampo(fab: FabContainer){
    this.isSearchbarOpened=true;
    fab.close();
  }

  buscarParametros(ev: any) {
    /*pega valor a ser pesquisado*/
    this.busca = ev.target.value;

    /*se o valor pesquisado for vazio, não filtra*/
    if (this.busca && this.busca.trim() != '') {
      this.parametros = this.parametros.filter((parametro) => {
        return ((parametro.descricao.toLowerCase().indexOf(this.busca.toLowerCase()) > -1) ||
          (parametro.ponto_descricao.toLowerCase().indexOf(this.busca.toLowerCase()) > -1) ||
          (parametro.grupo_descricao.toLowerCase().indexOf(this.busca.toLowerCase()) > -1));
      })
    } else {
      /*Voltar para todos os itens*/
      this.parametros = JSON.parse(JSON.stringify(this.parametrosOriginais));
    }
  }

  limpaFiltros(fab: FabContainer){
    let loading = this.messageService.showLoading('Buscando Ações...');
    /*Voltar para todos os itens*/
    this.parametros = JSON.parse(JSON.stringify(this.parametrosOriginais));
    this.isSearchbarOpened = false;
    loading.dismiss();
    fab.close();
  }

}
