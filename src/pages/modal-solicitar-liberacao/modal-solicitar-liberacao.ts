import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormControl, FormGroup} from "@angular/forms";
import {UniqueIdProvider} from "../../providers/unique-id/unique-id";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {LiberacaoChkProvider} from "../../providers/liberacao-chk/liberacao-chk";
import * as moment from "moment";
import {AuthProvider} from "../../providers/auth/auth";
import {PeriodosProvider} from "../../providers/periodos/periodos";
import {UtilsService} from "../../services/utils.service";

@IonicPage()
@Component({
  selector: 'page-modal-solicitar-liberacao',
  templateUrl: 'modal-solicitar-liberacao.html',
})
export class ModalSolicitarLiberacaoPage implements OnInit{

  protected solicitacaoForm: FormGroup;
  protected mobile_key;
  protected retornoModal = false;
  protected periodo_id;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private uniqueKeyService: UniqueIdProvider,
              private messageService: AlertMessagesProvider,
              private networkService: CheckNetworkProvider,
              private liberacaoChkProvider: LiberacaoChkProvider,
              private viewCtrl: ViewController,
              private authProvider: AuthProvider,
              private periodoProvider: PeriodosProvider,
              private utilsService: UtilsService,
  ) {
    this.mobile_key = this.uniqueKeyService.generateKey();
  }

  ngOnInit(){
    this.periodo_id = this.navParams.get('periodo_id');
    let now = moment().format('YYYY-MM-DD');
    this.solicitacaoForm = new FormGroup({
      'periodo_id': new FormControl(this.periodo_id),
      'justificativa': new FormControl(''),
      'mobile_key': new FormControl(this.mobile_key),
      'liberada': new FormControl(0),
      'data_solicitada': new FormControl(now),
      'descricao_propriedade': new FormControl(''),
      'descricao_checklist': new FormControl(''),
      'periodo_vigente': new FormControl(''),
    });

    this.periodoProvider.getPeriodoComChecklist(this.periodo_id).then((data)=>{
      this.solicitacaoForm.controls['descricao_propriedade'].setValue(data.propriedade_descricao);
      this.solicitacaoForm.controls['descricao_checklist'].setValue(data.checklist_descricao);
      this.solicitacaoForm.controls['periodo_vigente'].setValue(data.periodo_vigente);
    })
  }

  store(){
    let loading = this.messageService.showLoading('Salvando Solicitação...');

    this.liberacaoChkProvider.insert(this.solicitacaoForm.value).then(()=>{
      if(this.networkService.isConnected()){
        this.authProvider.checkLogin().then((data)=>{

          this.liberacaoChkProvider.getLatestId().then((last_id:any)=>{
            this.utilsService.retornaUUID().then((uuid:any)=>{
              let dados = this.solicitacaoForm.value;
              dados.id_mobile = last_id;
              dados.device_uuid = uuid;
              this.liberacaoChkProvider.apiStore(dados).timeout(10000).subscribe((data:any)=>{
                console.log('retorno do uplod da solicitação', data);
                this.messageService.showToastSuccess('Liberação solicitada com sucesso!');
                this.liberacaoChkProvider.updateSinc(data.id, data.id_mobile,data.mobile_key);
                loading.dismiss();
                this.retornoModal = true;
                this.dismiss();
              }, err =>{
                if(err.name == "TimeoutError"){
                  this.messageService.showAlert('Opss!', 'Sua conexão parece estar lenta, a liberação solicitada foi salva, mas não foi sincronizada!').then(()=>{
                    this.retornoModal = true;
                    this.dismiss();
                    this.messageService.showToastSuccess('Liberação solicitada com sucesso!');
                  });
                }else{
                  this.messageService.showToastError('Falha ao solicitar liberação! Tente novamente mais tarde.');
                }
                console.log('erro ao enviar solicitação', err);
                loading.dismiss();
              })
            });
          });
        });
      }else{
        this.messageService.showToastSuccess('Liberação solicitada com sucesso!');
        loading.dismiss();
        this.retornoModal = true;
        this.dismiss();
      }
    })
  }

  dismiss() {
    this.viewCtrl.dismiss({
      retornoModal:this.retornoModal,
      periodo_id: this.periodo_id
    });
  }
}
