import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSolicitarLiberacaoPage } from './modal-solicitar-liberacao';

@NgModule({
  imports: [
    IonicPageModule.forChild(ModalSolicitarLiberacaoPage),
  ],
})
export class ModalSolicitarLiberacaoPageModule {}
