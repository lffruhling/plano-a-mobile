import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BaixaAcoesProvider } from '../../providers/acoes/baixaAcoes';
import {BaixaAcao} from "../../class/BaixaAcao";
import {EditarBaixasPage} from "./editar-baixas/editar-baixas";

@IonicPage()
@Component({
  selector: 'page-baixadas',
  templateUrl: 'baixadas.html',
})
export class BaixadasPage {
  public baixas;
  public baixasSincronizadas;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public provider: BaixaAcoesProvider,
    ) {

      this.provider.getAll().then(
        (data:any) => {
          this.baixas = data;
        }, err => {
          console.error('erro', err);
        }
      );

    this.provider.getAllSincronizadas().then(
        (data:any) => {
          this.baixasSincronizadas = data;
        }, err => {
          console.error('erro', err);
        }
      );
  }

  editarBaixa(baixa: BaixaAcao){
    this.navCtrl.push(EditarBaixasPage, {
      baixa: baixa,
    })
  }
}
