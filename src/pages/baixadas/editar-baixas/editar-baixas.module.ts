import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarBaixasPage } from './editar-baixas';

@NgModule({
  imports: [
    IonicPageModule.forChild(EditarBaixasPage),
  ],
})
export class EditarBaixasPageModule {}
