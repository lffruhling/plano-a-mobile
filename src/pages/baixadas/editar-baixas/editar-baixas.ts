import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormControl, FormGroup} from "@angular/forms";
import {BaixaAcoesProvider} from "../../../providers/acoes/baixaAcoes";
import {UtilsService} from "../../../services/utils.service";
import {CameraProvider} from "../../../providers/camera/camera";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {AcoesIndexPage} from "../../acoes/acoes/index/acoes-index";
import {BaixadasPage} from "../baixadas";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {AuthProvider} from "../../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-editar-baixas',
  templateUrl: 'editar-baixas.html',
})
export class EditarBaixasPage implements OnInit {
  protected baixa;
  baixaForm: FormGroup;
  protected fotos: any[] = [];

  constructor(
    protected navCtrl: NavController,
    protected navParams: NavParams,
    protected baixaProvider: BaixaAcoesProvider,
    private messageService: AlertMessagesProvider,
    private utils: UtilsService,
    private cameraService: CameraProvider,
    private networkService: CheckNetworkProvider,
    private authProvider: AuthProvider,
  ) {
    this.baixa = navParams.get('baixa');
  }

  ngOnInit() {
    this.baixaForm = new FormGroup({
      'id': new FormControl(this.baixa.id),
      'id_web': new FormControl(this.baixa.id_web),
      'mobile_key': new FormControl(this.baixa.mobile_key),
      'acao_id': new FormControl(this.baixa.acao_id),
      'acao_id_web': new FormControl(this.baixa.acao_id_web),
      'resolvido': new FormControl(this.baixa.resolvido),
      'data': new FormControl(this.baixa.data),
      'resposta': new FormControl(this.baixa.resposta),
      'sincronizado': new FormControl(false),
    });

    this.baixaProvider.find(this.baixa.id).then((baixa)=>{
      this.baixa = baixa;
    });

    this.baixaProvider.getFotos(this.baixa.id).then((fotos: any[]) => {
      this.fotos = fotos;
    }).catch((err) => {
      console.error('falha ao carregar fotos', err);
    });
  }

  newFoto() {

    let mobile_key = this.baixa.mobile_key;
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.baixaProvider.insertFoto({
            mobile_key: mobile_key,
            img: foto,
            baixa_acao_id: this.baixa.id
          }).then((data)=>{
            this.fotos.push({
              id: data.insertId,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err=>{
            console.error('falha salvar foto acao - tela', err);
          });


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        this.baixaProvider.deleteFoto(foto_id).then(()=>{
          this.fotos = this.fotos.filter((_foto)=>{
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

  update(){

    let loading = this.messageService.showLoading('Salvando dados...');

    this.baixaProvider.editaBaixa(this.baixaForm.value).then(()=>{

      this.navCtrl.pop();
      loading.dismiss();

      if (this.networkService.isConnected()) {
        this.authProvider.checkLogin().then((data: any) => {
          if (data == "success") {
            if(this.baixa.id_web){
              this.baixaProvider.apiUpdate(this.baixa.id_web, this.baixaForm.value).subscribe(()=>{
                /*Envia Fotos das ações*/
                this.baixaProvider.enviaFotos(this.baixa.id, this.baixa.id_web);
                this.baixaProvider.updateSinc(this.baixa.id_web, this.baixa.id, this.baixa.acao_id_web);
              }, err=>{
                console.error('falha a atualizar baixa', err);
              })
            }else{
              this.baixaProvider.apiStore(this.baixaForm.value).subscribe((data:any)=>{
                /*Envia Fotos das ações*/
                this.baixaProvider.enviaFotos(this.baixa.id, data.id);
                this.baixaProvider.updateSinc(this.baixa.id, data.id, this.baixa.acao_id_web);
              }, err=>{
                console.error('falha a enviar acao', err);
              })
            }
          }
        });
      }

    }).catch(err => {
      loading.dismiss();
      console.error('erro update', err);
    })
  }

}
