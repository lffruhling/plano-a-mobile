import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BaixadasPage } from './baixadas';

@NgModule({
  declarations: [
    BaixadasPage,
  ],
  imports: [
    IonicPageModule.forChild(BaixadasPage),
  ],
})
export class BaixadasPageModule {}
