import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Tabs} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {LoginPage} from "../login/login";
import { Globals } from "../../config";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild("tabs") tabs: Tabs;

  tab1Root = 'AgendaPage';
  tab2Root = 'PlanoaPage';
  tab3Root = 'ChecklistPage';
  tab4Root = 'ProdutoresPage';

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.storage.get('login').then(function (data) {
      if(!data){
        navCtrl.setRoot(LoginPage);
      }
    });

    this.storage.get('SERVER_URL').then(function (data) {
      if(!data){
        navCtrl.setRoot(LoginPage);
      }
    })
  }

  ionViewDidEnter() {
    Globals.tabs = this.tabs;
  }
}
