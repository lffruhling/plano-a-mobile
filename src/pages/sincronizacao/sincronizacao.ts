import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {EmpresaProvider} from "../../providers/empresa/empresa";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../../providers/database/database";
import {TecnicoProvider} from "../../providers/tecnico/tecnico";
import {AuthProvider} from "../../providers/auth/auth";
import {PerguntasProvider} from "../../providers/checklist/perguntas";
import {AvaliacoesChecklistsProvider} from "../../providers/checklist/avaliacoes-checklists";
import {AcoesProvider} from "../../providers/acoes/acoes";
import {AvaliacoesProvider} from "../../providers/acoes/avaliacoes";
import {BaixaAcoesProvider} from "../../providers/acoes/baixaAcoes";
import {ProdutoresProvider} from "../../providers/produtores/produtores";
import {PropriedadesProvider} from "../../providers/produtores/propriedades";
import {GalpoesProvider} from "../../providers/produtores/galpoes";
import {LotesProvider} from "../../providers/produtores/lotes";
import {TipoIntegracaoProvider} from "../../providers/base/tipoIntegracao";
import {SubtipoIntegracoesProvider} from "../../providers/base/SubtipoIntegracoes";
import {ComedourosProvider} from "../../providers/comedouros/comedouros";
import {IntegradoraProvider} from "../../providers/base/integradora";
import {TipoInformacaoAcaoProvider} from "../../providers/base/TipoInformacaoAcao";
import {SituacaoAcaoProvider} from "../../providers/base/SituacaoAcao";
import {GrupoAcoesProvider} from "../../providers/acoes/grupoAcoes";
import {PontoAcoesProvider} from "../../providers/acoes/pontoAcoes";
import {ParametroAcoesProvider} from "../../providers/acoes/parametroAcoes";
import {ParametroPagamentoAcoesProvider} from "../../providers/acoes/parametroPagamentoAcoes";
import {ChecklistProvider} from "../../providers/checklist/checklist";
import {CotacaoProvider} from "../../providers/cotacao/cotacao";
import {IntervalosProvider} from "../../providers/intervalos/intervalos";
import {PeriodosProvider} from "../../providers/periodos/periodos";
import {LiberacaoChkProvider} from "../../providers/liberacao-chk/liberacao-chk";
import {RespostasProvider} from "../../providers/checklist/respostas";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {Storage} from "@ionic/storage";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {Device} from "@ionic-native/device";
import {DbExceptionProvider} from "../../providers/db-exception/db-exception";
import * as moment from "moment";
import {UtilsService} from "../../services/utils.service";
import {AppVersion} from "@ionic-native/app-version";
import {RecontagemPlantelProvider} from "../../providers/recontagem-plantel/recontagem-plantel";
import {GeolocationProvider} from "../../providers/geolocation/geolocation";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {VisitasProvider} from "../../providers/visitas/visitas";

@IonicPage()
@Component({
  selector: 'page-sincronizacao',
  templateUrl: 'sincronizacao.html',
})
export class SincronizacaoPage {

  protected aba = 'enviar';
  protected listSincDown = [];
  protected listSincUp = [];
  protected inSync = false;
  protected showMsg = false;
  protected typeMsg = '';
  protected texto = '';
  protected automatico = false;
  protected perguntasId = [];
  protected totalFotos = 0;

  constructor(
    private dbProvider: DatabaseProvider,
    private networkProvider: CheckNetworkProvider,
    private navParams: NavParams,
    private storage: Storage,
    private fileTransfer: FileTransfer,
    private device: Device,
    private appVersion: AppVersion,
    private exceptionProvider: DbExceptionProvider,
    private acoesProvider: AcoesProvider,
    private authProvider: AuthProvider,
    private avaliacoesProvider: AvaliacoesProvider,
    private avaliacoesChecklistProvider: AvaliacoesChecklistsProvider,
    private baixasProvider: BaixaAcoesProvider,
    private checklistProvider: ChecklistProvider,
    private comedouroProvider: ComedourosProvider,
    private cotacaoProvider: CotacaoProvider,
    private empresaProvider: EmpresaProvider,
    private integradorasProvider: IntegradoraProvider,
    private intervalosProvider: IntervalosProvider,
    private liberacaoProvider: LiberacaoChkProvider,
    private lotesProvider: LotesProvider,
    private galpoesProvider: GalpoesProvider,
    private grupoAcoesProvider: GrupoAcoesProvider,
    private paramAcoesProvider: ParametroAcoesProvider,
    private paramPgtoProvider: ParametroPagamentoAcoesProvider,
    private periodosProvider: PeriodosProvider,
    private perguntasProvider: PerguntasProvider,
    private pontoAcoesProvider: PontoAcoesProvider,
    private produtoresProvider: ProdutoresProvider,
    private propriedadesProvider: PropriedadesProvider,
    private respostasProvider: RespostasProvider,
    private situacoesAcoesProvider: SituacaoAcaoProvider,
    private stpIntegracaoProvider: SubtipoIntegracoesProvider,
    private tecnicoProvider: TecnicoProvider,
    private tpIntegracaoProvider: TipoIntegracaoProvider,
    private tpInfoAcoesProvider: TipoInformacaoAcaoProvider,
    private utilsService: UtilsService,
    private recontagemPlantelProvider: RecontagemPlantelProvider,
    private geolocationProvider: GeolocationProvider,
    private messagesService: AlertMessagesProvider,
    private visitasProvider: VisitasProvider,
  ) {
    this.automatico = this.navParams.get('automatico');

    if (this.automatico) {
      this.sincronizarApp();
    }

  }

  protected sincronizarApp() {
    this.listSincDown = [];
    this.listSincUp = [];
    this.aba = 'enviar';
    this.showMsg = false;
    this.typeMsg = '';
    this.texto = '';
    this.inSync = true;

    this.dbProvider.getDB().then((db: SQLiteObject) => {
      this.messagesService.showToast('Otimizando dados para enviar...');

      this.dbProvider.compactDatabase(db).then(() => {
        this.startSinc();
      }).catch(e => {
        this.startSinc();
        console.error('Erro ao criar as tabelas', e);
      });


    }).catch(e => console.error('erro na base aqui', e));

  }

  private startSinc() {
    if (this.networkProvider.isConnected()) {
      this.networkProvider.speed().catch(err => {

        if (err.name == "TimeoutError") {
          this.showMsg = true;
          this.typeMsg = 'warning';
          this.texto = "A internet está um pouco lenta, isto pode demorar um pouco!";
        } else {
          this.showMsg = true;
          this.typeMsg = 'warning';
          this.texto = "A algo de errado com a internet, isto pode demorar um pouco!";
        }
      });

      this.authProvider.checkLogin().then((data: any) => {
      	console.log('this.authProvider.checkLogin' + data)
        if (data == "success") {

          this.adicionaItemToListSync('acoes', 'Ações', 'U');
          this.adicionaItemToListSync('baixas', 'Ações Baixadas', 'U');
          this.adicionaItemToListSync('renovadas', 'Ações Renovadas', 'U');
          this.adicionaItemToListSync('avaliacoes_iniciais', 'Avaliações Iniciais', 'U');
          this.adicionaItemToListSync('perguntas_acoes', 'Perguntas que Geraram Ações', 'U');

          this.adicionaItemToListSync('acoes', 'Ações', 'D');
          this.adicionaItemToListSync('baixas_acoes', 'Ações Baixadas', 'D');
          this.adicionaItemToListSync('renovadas', 'Ações Renovadas', 'D');
          this.adicionaItemToListSync('avaliacoes_iniciais', 'Avaliações Iniciais', 'D');
          this.adicionaItemToListSync('base_plano_acoes', 'Dados Base Plano de Ação', 'D');


          this.adicionaItemToListSync('produtores', 'Produtores', 'U');
          this.adicionaItemToListSync('propriedades', 'Propriedades', 'U');
          this.adicionaItemToListSync('galpoes', 'Galpões', 'U');
          this.adicionaItemToListSync('lotes', 'Lotes', 'U');

          this.adicionaItemToListSync('produtores', 'Produtores', 'D');
          this.adicionaItemToListSync('propriedades', 'Propriedades', 'D');
          this.adicionaItemToListSync('galpoes', 'Galpões', 'D');
          this.adicionaItemToListSync('lotes', 'Lotes', 'D');
          this.adicionaItemToListSync('base_produtores', 'Dados Base dos Produtores', 'D');

          this.adicionaItemToListSync('avaliacoes', 'Checklists Avaliados', 'U');
          this.adicionaItemToListSync('respostas', 'Respostas dos Checklists', 'U');

          this.adicionaItemToListSync('base_checklist', 'Dados Base do Checklists', 'D');
          this.adicionaItemToListSync('avaliacoes_com_respostas', 'Respostas Checklists', 'D');

          this.adicionaItemToListSync('tecnico', 'Dados do Usuário', 'U');

          this.adicionaItemToListSync('empresa', 'Dados da Empresa', 'D');
          this.adicionaItemToListSync('tecnico', 'Dados do Usuário', 'D');

          this.adicionaItemToListSync('celas_uteis', 'Celas Úteis', 'D');

          this.adicionaItemToListSync('recontagem_plantel', 'Recontagem de Plantel', 'U');

          this.adicionaItemToListSync('recontagem_plantel', 'Recontagem de Plantel', 'D');

          this.adicionaItemToListSync('base_visitas', 'Dados das Visitas', 'D');
          this.adicionaItemToListSync('visitas_lotes', 'Visitas dos Lotes', 'U');


          this.tecnicoProvider.getLastSync().then((lastSync) => {
            this.enviaDados().then(() => {
              this.aba = 'baixar';
              this.baixaDados(lastSync);

            }).catch(() => {
              this.baixaDados(lastSync);
            });
          })
        }
      });
    } else {
      this.showMsg = true;
      this.typeMsg = 'danger';
      this.texto = "Você está desconectado da internet!";
    }
  }

  private enviaDados() {
    let promise = new Promise(resolve => {
      /*Envia dados do Técnico*/
      this.tecnicoProvider.get().then((result: any) => {
        if (result) {
          this.appVersion.getVersionNumber().then((versionNumber) => {
            let data = {
              'id': result.id,
              'mobile_info': `DEVICE: ${this.device.model} - ANDROID: ${this.device.version} - APP: ${versionNumber}`
            };

            this.tecnicoProvider.enviaDados(data).subscribe((_) => {
            }, err => {
              console.error('falhou tec envio', err);
            });

          });
          for (let i = 0; i < this.listSincUp.length; i++) {
            this.listSincUp[i].total = 1;
            if (this.listSincUp[i].id == 'tecnico') {
              this.marcaSincronizado(i, 'U');
              this.listSincUp[i].progress = 100;
              i = this.listSincUp.length;
            }
          }

        } else {
          for (let i = 0; i < this.listSincUp.length; i++) {
            this.listSincUp[i].total = 1;
            if (this.listSincUp[i].id == 'tecnico') {
              this.marcaSincronizado(i, 'U');
              this.listSincUp[i].progress = 100;
              i = this.listSincUp.length;
            }
          }
        }

      });


      this.utilsService.retornaUUID().then((uuid: any) => {

        this.upChecklists(uuid).then(() => {
          this.upPlanoDeAcao(uuid).then(() => {
            this.upProdutores().then(() => {
              this.upRecontagensPlantel(uuid).then(() => {
                this.upVisitasLotes(uuid).then(() => {
                  this.enviaErros();
                  resolve();
                });
              });
            }).catch(err => {
              console.error('Falha ao enviar produtores', err);
            });
          }).catch(err => {
            console.error('Falha ao enviar plano de ação', err);
          });
        }).catch(err => {
          console.error('falha no metodo de enviaAvaliacoesComRespostas', err);
        });

        this.storage.get('bases_to_export').then((result) => {

          if (result) {
            this.enviaBase();
          }
        }).catch(err => console.error('falha ao consulta dados da base export', err));

      });

    });

    return promise;
  }

  private baixaDados(lastSync) {
    /*Atualiza Tecnico*/
    this.tecnicoProvider.getDadosTecnicoApi().subscribe((response: any) => {
      this.tecnicoProvider.atualizaDadosTecnicoDB(response).then(() => {
        for (let i = 0; i < this.listSincDown.length; i++) {
          if (this.listSincDown[i].id == 'tecnico') {
            this.listSincDown[i].progress = 100;
            this.marcaSincronizado(i, 'D');
            i = this.listSincDown.length;
          }
        }
      });
    });

    /*Atualiza Empresa*/
    this.empresaProvider.getDadosEmpresaApi().subscribe((response) => {
      this.empresaProvider.atualizaDadosEmpresaDB(response).then(() => {
        for (let i = 0; i < this.listSincDown.length; i++) {
          if (this.listSincDown[i].id == 'empresa') {
            this.listSincDown[i].progress = 100;
            this.marcaSincronizado(i, 'D');
            i = this.listSincDown.length;
          }
        }
      });
    });

    this.downProdutores(lastSync).then(() => {
      this.downPlanoAcao(lastSync).then(() => {
        this.downRecontagem(lastSync).then(() => {
          this.downVisitas(lastSync).then(() => {
            this.downChecklist(lastSync).then(() => {
              /*Atualiza flag que finalizou sincronização*/
              this.inSync = false;
              this.showMsg = true;
              this.typeMsg = 'success';
              this.texto = "Sincronização Finalizada com Sucesso!";
              this.tecnicoProvider.updateLastSync(moment().format('YYYY-MM-DD H:mm'));
              this.exceptionProvider.deleteAfterSync();
              this.dbProvider.getDB().then((db: SQLiteObject) => {
                this.enviaFotos(db, this.perguntasId.length, 0);
              })
              this.enviaLocalizacoes();
              this.baixaThumbsPerguntas(lastSync);
            });
          });
        });
      });
    });

    this.empresaProvider.getTotalEstados().then((baixaEstados) => {
      if (baixaEstados) {
        this.baixaEstados(lastSync).then(() => {
          this.empresaProvider.getTotalMunicipios().then((baixaMunicipios) => {
            if (baixaMunicipios) {
              this.baixaMunicipios(lastSync);
            }
          });
        });
      } else {
        this.empresaProvider.getTotalMunicipios().then((baixaMunicipios) => {
          if (baixaMunicipios) {
            this.baixaMunicipios(lastSync);
          }
        });
      }
    })
    this.baixaVersoes(lastSync);
  }

  /*
  * update checklists_avaliacoes set id_web = null where data_aplicacao = '2021-03-17' and deleted_at is null
  * update checklists_respostas set id_web = null where data_resposta = '2021-03-17' and deleted_at is null
  * */


// função recursiva, continua chamando a si mesma até que a condição seja atendida
  private enviaFotos(db: SQLiteObject, until, previous) {
    if (this.perguntasId.length > 0) {
      this.messagesService.showAlert('Enviando Fotos', 'O Aplicativo pode ficar lento durante o envio das fotos...');

      this.perguntasId.forEach((idResposta) => {
        this.perguntasProvider.uploadFoto(db, idResposta);
      })
    }
  }

  /*
  * #############
  * Métodos para Gerenciar Lista de Downloads/Uploads
  * #############
  * */
  private adicionaItemToListSync(id, title, type) {
    if (type == 'D') {
      this.listSincDown.push({
        'id': id,
        'icon': 'cloud-download',
        'color_icon': 'warning',
        'title': title,
        'msg': null,
        'progress': 0,
        'total': 0,
        'showSpinner': true,
      });
    } else if (type == 'U') {
      this.listSincUp.push({
        'id': id,
        'icon': 'cloud-upload',
        'color_icon': 'warning',
        'title': title,
        'msg': null,
        'progress': 0,
        'total': 0,
        'showSpinner': true,
      });
    }
  }

  private marcaSincronizado(index, type) {
    if (type == 'D') {
      this.listSincDown[index].msg = 'Atualizado agora mesmo';
      this.listSincDown[index].icon = 'cloud-done';
      this.listSincDown[index].color_icon = 'secondary';
      this.listSincDown[index].showSpinner = false;
      this.listSincDown.sort((a, b) => a.progress - b.progress);
    } else if (type == 'U') {
      this.listSincUp[index].msg = 'Atualizado agora mesmo';
      this.listSincUp[index].icon = 'cloud-done';
      this.listSincUp[index].color_icon = 'secondary';
      this.listSincUp[index].showSpinner = false;
      this.listSincUp.sort((a, b) => a.progress - b.progress);
    }
  }

  private getIndex(local, type) {
    let index = 0;
    if (type == 'D') {
      for (let i = 0; i < this.listSincDown.length; i++) {
        if (this.listSincDown[i].id == local) {
          index = i;
          i = this.listSincDown.length;
          return index;
        }
      }
    } else if (type == 'U') {
      for (let i = 0; i < this.listSincUp.length; i++) {
        if (this.listSincUp[i].id == local) {
          index = i;
          i = this.listSincUp.length;
          return index;
        }
      }
    }
  }

  /*
  * #############
  * Métodos para Enviar Dados
  * #############
  * */

  private upChecklists(uuid) {
    let promise = new Promise((resolve) => {
      this.enviaAvaliacoes(uuid).then(() => {
        this.enviaRespostas(uuid).then(() => {
          resolve();
        }).catch(err => {
          console.error('Falha ao enviar respostas', err);
        });
      }).catch(err => {
        console.error('Falha ao enviar avaliacoes', err);
      });
    });

    return promise;

  }

  private enviaAvaliacoes(uuid) {
    let promise = new Promise(resolve => {
      this.avaliacoesChecklistProvider.getAvaliacoesParaSincronizar().then((avaliacoes: Array<any>) => {
        let total_av = avaliacoes.length;
        let indexAV = this.getIndex('avaliacoes', 'U');

        if (total_av > 0) {

          this.listSincUp[indexAV].total = total_av;
          avaliacoes.push({'device_uuid': uuid});
          this.avaliacoesChecklistProvider.apiStoreAvaliacoesChk(avaliacoes).subscribe((result: Array<any>) => {

            let incAV = 100 / result.length;
            result.forEach((value) => {

              this.avaliacoesChecklistProvider.updateSinc(value.id, value.id_mobile).then(() => {
                this.listSincUp[indexAV].progress += incAV;
                if (Math.round(this.listSincUp[indexAV].progress) >= 100) {

                  this.listSincUp[indexAV].progress = 100;
                  this.marcaSincronizado(indexAV, 'U');
                  resolve();
                }
              }).catch((err) => {
                console.error('falha ao atualizar id_web da avaliação do chk', err, value)
              });
            });

          }, err => {
            console.error('Falha ao enviar avaliacao check', err);
          })

        } else {
          this.listSincUp[indexAV].progress = 100;
          this.marcaSincronizado(indexAV, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar avaliacoes', err);
      });
    });

    return promise;
  }

  private enviaRespostas(uuid) {
    let promise = new Promise(resolve => {
      this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          this.perguntasProvider.getRespostasParaSincronizar(db).then((respostas_salvas: Array<any>) => {

            let total_resp = respostas_salvas.length;

            let indexResp = this.getIndex('respostas', 'U');

            if (total_resp > 0) {
              let incResp = 100 / total_resp;
              this.listSincUp[indexResp].total = total_resp;

              respostas_salvas.forEach((resposta) => {

                resposta.device_uuid = uuid;
                if (resposta.resposta == 'true') {
                  resposta.resposta = 1;
                } else if (resposta.resposta == 'false') {
                  resposta.resposta = 0;
                }
                this.perguntasId.push(resposta.id);
                this.perguntasProvider.apiStoreResposta(resposta).subscribe((res: any) => {

                  this.perguntasProvider.updateSinc(db, res.id, res.id_mobile, res.avaliacao_id_web).then(() => {

                    this.listSincUp[indexResp].progress += incResp;
                    if (Math.round(this.listSincUp[indexResp].progress) >= 100) {

                      this.listSincUp[indexResp].progress = 100;
                      this.marcaSincronizado(indexResp, 'U');
                      resolve();
                    }
                  }).catch((err) => console.error('Falha ao atualizar resposta local', err));
                }, (err) => console.error('Falha ao salvar resposta sem foto na api', err, resposta))
              });
            } else {
              this.listSincUp[indexResp].progress = 100;
              this.marcaSincronizado(indexResp, 'U');
              resolve();

            }

          }).catch(err => {
            console.error('Falha ao consultar respostas para sincronizar', err)
          })
        });
    });

    return promise;
  }

  private upPlanoDeAcao(uuid) {
    let promise = new Promise((resolve) => {
      this.enviaAvaliacoesIniciais(uuid).then(() => {
        this.enviaAcoes(uuid).then(() => {
          this.enviaBaixas(uuid).then(() => {
            this.enviaPerguntasAcoes(uuid).then(() => {
              this.enviaRenovacoes(uuid).then(() => {
                resolve();
              });
            })
          });

        });
      });
    });

    return promise;
  }

  private enviaAvaliacoesIniciais(uuid) {
    let promise = new Promise((resolve) => {
      this.avaliacoesProvider.getAllToSinc().then((avaliacoes_inicias: Array<any>) => {

        let total_avi = avaliacoes_inicias.length;
        let incAVI = 100 / total_avi;
        let indexAVI = this.getIndex('avaliacoes_iniciais', 'U');
        this.listSincUp[indexAVI].total = total_avi;
        if (total_avi > 0) {

          for (let avaliacao of avaliacoes_inicias) {

            avaliacao.device_uuid = uuid;
            avaliacao.com_acao = avaliacao.gerou_acao == 1 || avaliacao.gerou_acao == "true";
            this.avaliacoesProvider.apiStore(avaliacao).subscribe((response: any) => {
              this.avaliacoesProvider.updateSinc(response.id, response.id_mobile).then(() => {
                this.listSincUp[indexAVI].progress += incAVI;
                if (Math.round(this.listSincUp[indexAVI].progress) >= 100) {

                  this.listSincUp[indexAVI].progress = 100;
                  this.marcaSincronizado(indexAVI, 'U');
                  resolve();
                }
              });
              this.avaliacoesProvider.enviaFotos(response.id_mobile, response.id);
            });
          }
        } else {
          this.listSincUp[indexAVI].progress = 100;
          this.marcaSincronizado(indexAVI, 'U');
          resolve();
        }
      });
    });

    return promise;
  }

  private enviaAcoes(uuid) {
    let promise = new Promise(resolve => {
      this.acoesProvider.getAllToSinc().then((acoes: Array<any>) => {

        let total_acoes = acoes.length;
        let incAC = 100 / total_acoes;
        let indexAC = this.getIndex('acoes', 'U');
        this.listSincUp[indexAC].total = total_acoes;
        if (total_acoes > 0) {

          for (let acao of acoes) {
            acao.device_uuid = uuid;
            this.acoesProvider.apiStore(acao).subscribe((response: any) => {
              this.acoesProvider.updateSinc(response.id, response.id_mobile).then(() => {

                this.listSincUp[indexAC].progress += incAC;
                if (Math.round(this.listSincUp[indexAC].progress) >= 100) {

                  this.listSincUp[indexAC].progress = 100;
                  this.marcaSincronizado(indexAC, 'U');
                  resolve();
                }
              });

              this.acoesProvider.enviaFotos(response.id_mobile, response.id);
            }, err => {
              console.error('Falha ao enviar acao api sem foto', err, acao)
            })
          }
        } else {
          this.listSincUp[indexAC].progress = 100;
          this.marcaSincronizado(indexAC, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar ações para sincronização', err);
      })

    });

    return promise;
  }

  private enviaBaixas(uuid) {
    let promise = new Promise(resolve => {
      this.baixasProvider.getAllToSinc().then((baixas: Array<any>) => {

        let total_baixas = baixas.length;
        let incBX = 100 / total_baixas;
        let indexBX = this.getIndex('baixas', 'U');
        this.listSincUp[indexBX].total = total_baixas;
        if (total_baixas > 0) {

          for (let baixa of baixas) {

            baixa.device_uuid = uuid;
            this.baixasProvider.apiStore(baixa).subscribe((response: any) => {

              this.baixasProvider.updateSinc(response.id, response.id_mobile, response.acao_id).then(() => {

                this.listSincUp[indexBX].progress += incBX;
                if (Math.round(this.listSincUp[indexBX].progress) >= 100) {

                  this.listSincUp[indexBX].progress = 100;
                  this.marcaSincronizado(indexBX, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualiza na sync baixa', err, response);
              });

              this.baixasProvider.enviaFotos(response.id_mobile, response.id)
            }, err => {
              console.error('Falha ao enviar baixa api sem foto', err, baixa)
            });
          }
        } else {
          this.listSincUp[indexBX].progress = 100;
          this.marcaSincronizado(indexBX, 'U');
          resolve();
        }

      }).catch(err => {
        console.error('Falha ao consultar baixas das ações', err);
      })
    });

    return promise;

  }

  private enviaRenovacoes(uuid) {
    let promise = new Promise(resolve => {
      this.baixasProvider.getAllRenovadasToSinc().then((acoes_renovadas: Array<any>) => {

        let total_renovadas = acoes_renovadas.length;
        let incRN = 100 / total_renovadas;
        let indexRN = this.getIndex('renovadas', 'U');
        this.listSincUp[indexRN].total = total_renovadas;
        if (total_renovadas > 0) {

          for (let renovada of acoes_renovadas) {

            renovada.device_uuid = uuid;
            this.baixasProvider.apiStore(renovada).subscribe((response: any) => {

              this.baixasProvider.updateSincRenovacaoAcao(response.id, response.id_mobile).then(() => {

                this.listSincUp[indexRN].progress += incRN;
                if (Math.round(this.listSincUp[indexRN].progress) >= 100) {

                  this.listSincUp[indexRN].progress = 100;
                  this.marcaSincronizado(indexRN, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualiza na sync acoes renovadas', err, response);
              });

              this.baixasProvider.enviaFotosRenovacao(response.id_mobile, response.id)
            }, err => {
              console.error('Falha ao enviar baixa api sem foto', err, renovada)
            });
          }
        } else {
          this.listSincUp[indexRN].progress = 100;
          this.marcaSincronizado(indexRN, 'U');
          resolve();
        }

      }).catch(err => {
        console.error('Falha ao consultar ações renovadas', err);
      })
    });

    return promise;

  }

  private enviaPerguntasAcoes(uuid) {
    let promise = new Promise(resolve => {
      this.perguntasProvider.getPerguntaAcaoSincronizar().then((perguntas_acoes: Array<any>) => {

        let total_perguntas = perguntas_acoes.length;
        let incPGA = 100 / total_perguntas;
        let indexPGA = this.getIndex('perguntas_acoes', 'U');
        this.listSincUp[indexPGA].total = total_perguntas;
        if (total_perguntas > 0) {

          for (let pergunta_acao of perguntas_acoes) {

            pergunta_acao.device_uuid = uuid;
            this.perguntasProvider.apiStorePerguntaAcao(pergunta_acao).subscribe((response: any) => {
              this.perguntasProvider.updateSincPerguntaAcao(response.pergunta_id, response.mobile_key).then(() => {

                this.listSincUp[indexPGA].progress += incPGA;
                if (Math.round(this.listSincUp[indexPGA].progress) >= 100) {

                  this.listSincUp[indexPGA].progress = 100;
                  this.marcaSincronizado(indexPGA, 'U');
                  resolve();
                }
              }).catch((err) => {
                console.error('Falha ao atualizar pergunta_acao', err, response, pergunta_acao)
              })

            }, err => {
              console.error('Falha ao salvar na api pergunta que gerou ação', err, pergunta_acao);
            })
          }
        } else {
          this.listSincUp[indexPGA].progress = 100;
          this.marcaSincronizado(indexPGA, 'U');
          resolve();
        }

      }).catch(err => {
        console.error('Falha ao consultar ações geradas por perguntas para sincronizar', err);
      })
    });

    return promise;
  }

  private upProdutores() {
    let promise = new Promise(resolve => {
      this.enviaProdutores().then(() => {
        this.enviaPropriedades().then(() => {
          this.enviaGalpoes().then(() => {
            this.enviaLotes().then(() => {
              resolve();
            });
          });
        });
      })
    });

    return promise;
  }

  private enviaProdutores() {
    let promise = new Promise(resolve => {
      this.produtoresProvider.getAllToSyncProdutores().then((produtores: Array<any>) => {

        let total_produtores = produtores.length;
        let incPROD = 100 / total_produtores;
        let indexPROD = this.getIndex('produtores', 'U');
        this.listSincUp[indexPROD].total = total_produtores;
        if (total_produtores > 0) {

          for (let produtor of produtores) {

            this.produtoresProvider.apiUpdate(produtor).subscribe((response: any) => {

              response.ativo = response.ativo == 1;
              response.sincronizado = true;
              response.enviar_foto = false;
              response.id_web = response.id;
              response.foto = response.foto_base_64;
              this.produtoresProvider.update(response).then(() => {

                this.listSincUp[indexPROD].progress += incPROD;
                if (Math.round(this.listSincUp[indexPROD].progress) >= 100) {

                  this.listSincUp[indexPROD].progress = 100;
                  this.marcaSincronizado(indexPROD, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualizar produtor DB', err)
              })
            }, err => {
              console.error('Falha ao atualizar produtor api', err, produtor);
            });
          }
        } else {
          this.listSincUp[indexPROD].progress = 100;
          this.marcaSincronizado(indexPROD, 'U');
          resolve();
        }
      })
    });

    return promise;
  }

  private enviaPropriedades() {
    let promise = new Promise(resolve => {
      this.propriedadesProvider.getAllToSyncPropriedades().then((propriedades: Array<any>) => {
        let total_propriedades = propriedades.length;
        let incPROP = 100 / total_propriedades;
        let indexPROP = this.getIndex('propriedades', 'U');
        this.listSincUp[indexPROP].total = total_propriedades;
        if (total_propriedades > 0) {

          for (let propriedade of propriedades) {
            this.propriedadesProvider.apiUpdate(propriedade).subscribe((response: any) => {

              response.ativo = response.ativo == 1;
              response.sincronizado = true;
              this.propriedadesProvider.update(response).then(() => {
                this.listSincUp[indexPROP].progress += incPROP;
                if (Math.round(this.listSincUp[indexPROP].progress) >= 100) {

                  this.listSincUp[indexPROP].progress = 100;
                  this.marcaSincronizado(indexPROP, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualizar db propriedade', err, response);
              });
              this.propriedadesProvider.enviaFotos(propriedade.id);

            }, err => {
              console.error('Falha ao enviar propriedade api', err, propriedade);
            });
          }
        } else {
          this.listSincUp[indexPROP].progress = 100;
          this.marcaSincronizado(indexPROP, 'U');
          resolve();
        }

      }).catch(err => {
        console.error('Falha ao consultar propriedades para sincronizar', err)
      });
    });

    return promise;
  }

  private enviaGalpoes() {
    let promise = new Promise(resolve => {
      this.galpoesProvider.getAllToSyncGalpoes().then((galpoes: Array<any>) => {

        let total_galpoes = galpoes.length;
        let incGP = 100 / total_galpoes;
        let indexGP = this.getIndex('galpoes', 'U');
        this.listSincUp[indexGP].total = total_galpoes;
        if (total_galpoes > 0) {

          for (let galpao of galpoes) {

            this.galpoesProvider.apiUpdate(galpao).subscribe((response: any) => {

              response.ativo = response.ativo ? 1 : 0;
              response.sincronizado = true;

              this.galpoesProvider.update(response).then(() => {
                this.listSincUp[indexGP].progress += incGP;
                if (Math.round(this.listSincUp[indexGP].progress) >= 100) {

                  this.listSincUp[indexGP].progress = 100;
                  this.marcaSincronizado(indexGP, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualizar galpão', err, response);
              });
              this.galpoesProvider.enviaFotos(galpao.id);
            }, err => {
              console.error('Falha ao enviar galpao api', err, galpao);
            });
          }
        } else {
          this.listSincUp[indexGP].progress = 100;
          this.marcaSincronizado(indexGP, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar galpões para sincronizar', err)
      })

    });

    return promise;
  }

  private enviaLotes() {
    let promise = new Promise(resolve => {
      this.lotesProvider.getAllToSync().then((lotes: Array<any>) => {

        let total_lotes = lotes.length;
        let incLT = 100 / total_lotes;
        let indexLT = this.getIndex('lotes', 'U');
        this.listSincUp[indexLT].total = total_lotes;
        if (total_lotes > 0) {

          for (let lote of lotes) {

            this.lotesProvider.apiUpdate(lote).subscribe((response: any) => {

              this.lotesProvider.updateSinc(response.id).then(() => {
                this.listSincUp[indexLT].progress += incLT;
                if (Math.round(this.listSincUp[indexLT].progress) >= 100) {

                  this.listSincUp[indexLT].progress = 100;
                  this.marcaSincronizado(indexLT, 'U');
                  resolve();
                }
              }).catch(err => {
                console.error('Falha ao atualizar lote', err, response);
              });
            }, err => {
              console.error('Falha ao enviar lote api', err, lote);
            });
          }
        } else {
          this.listSincUp[indexLT].progress = 100;
          this.marcaSincronizado(indexLT, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar lotes para sincronizar', err)
      })

    });

    return promise;
  }

  private upRecontagensPlantel(uuid) {
    let promise = new Promise((resolve) => {
      this.enviaRecontagens(uuid).then(() => {
        resolve();
      });
    });

    return promise;
  }

  private enviaRecontagens(uuid) {
    let promise = new Promise(resolve => {
      this.recontagemPlantelProvider.getAllToSinc().then((recontagens: Array<any>) => {

        let total_recontagens = recontagens.length;
        let incRECPLAN = 100 / total_recontagens;
        let indexRECPLAN = this.getIndex('recontagem_plantel', 'U');
        this.listSincUp[indexRECPLAN].total = total_recontagens;
        if (total_recontagens > 0) {

          for (let recontagem of recontagens) {
            this.recontagemPlantelProvider.getAllToSyncCelasUteis(recontagem.id).then((celasUteis: Array<any>) => {
              recontagem._method = "patch";
              recontagem.celas_uteis = [];
              for (let cela of celasUteis) {
                cela.device_uuid = uuid;
                cela.id_mobile = cela.id;
                recontagem.celas_uteis.push(cela);
              }

              this.recontagemPlantelProvider.apiStore(recontagem).subscribe((response: any) => {
                this.recontagemPlantelProvider.updateSinc(response.id).then(() => {
                  for (let cela of response.celas_uteis) {
                    this.recontagemPlantelProvider.updateSyncTotalCelasUteis(cela.id_mobile, cela.id)
                  }

                  this.listSincUp[indexRECPLAN].progress += incRECPLAN;
                  if (Math.round(this.listSincUp[indexRECPLAN].progress) >= 100) {

                    this.listSincUp[indexRECPLAN].progress = 100;
                    this.marcaSincronizado(indexRECPLAN, 'U');
                    resolve();
                  }
                });

              }, err => {
                console.error('Falha ao enviar recontagens', err, recontagem)
              })
            });
          }
        } else {
          this.listSincUp[indexRECPLAN].progress = 100;
          this.marcaSincronizado(indexRECPLAN, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar ações para sincronização', err);
      })

    });

    return promise;
  }

  private upVisitasLotes(uuid) {
    let promise = new Promise((resolve) => {
      this.enviaLancamentosVisitas(uuid).then(() => {
        resolve();
      });
    });

    return promise;
  }

  private enviaLancamentosVisitas(uuid) {
    let promise = new Promise(resolve => {
      this.visitasProvider.getAllLancamentosToSinc().then((lancamentos: Array<any>) => {

        let total_lancamentos = lancamentos.length;
        let incLANCAMENTO = 100 / total_lancamentos;
        let indexLANCAMENTO = this.getIndex('visitas_lotes', 'U');
        this.listSincUp[indexLANCAMENTO].total = total_lancamentos;
        if (total_lancamentos > 0) {

          for (let lancamento of lancamentos) {
            this.visitasProvider.getAllRespostasLancamentoToSync(lancamento.id).then((respostas: Array<any>) => {
              lancamento._method = "patch";
              lancamento.respostas = [];
              for (let resposta of respostas) {
                resposta.device_uuid = uuid;
                resposta.id_mobile = resposta.id;
                lancamento.respostas.push(resposta);
              }

              this.visitasProvider.apiStore(lancamento).subscribe((response: any) => {
                this.visitasProvider.updateSinc(response.lancamento.id).then(() => {
                  for (let resp of response.respostas) {
                    this.visitasProvider.updateSyncRespostas(resp.id_mobile, resp.id)
                  }

                  this.listSincUp[indexLANCAMENTO].progress += incLANCAMENTO;
                  if (Math.round(this.listSincUp[indexLANCAMENTO].progress) >= 100) {

                    this.listSincUp[indexLANCAMENTO].progress = 100;
                    this.marcaSincronizado(indexLANCAMENTO, 'U');
                    resolve();
                  }
                });

              }, err => {
                console.error('Falha ao enviar lancamentos', err, lancamento)
              })
            });
          }
        } else {
          this.listSincUp[indexLANCAMENTO].progress = 100;
          this.marcaSincronizado(indexLANCAMENTO, 'U');
          resolve();
        }
      }).catch(err => {
        console.error('Falha ao consultar ações para sincronização', err);
      })

    });

    return promise;
  }

  /*
  * Métodos para Baixar Dados do Produtores
  * */

  private downProdutores(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixaBaseProdutores(lastUpdate).then(() => {
        this.baixaProdutores(lastUpdate).then(() => {
          this.baixaPropriedades(lastUpdate).then(() => {
            this.baixaGalpoes(lastUpdate).then(() => {
              this.baixaLotes(lastUpdate).then(() => {
                resolve();
              }).catch(err => {
                console.error('falha lt', err)
              });
            });
          });
        });
      });
    });

    return promise;
  }

  private baixaBaseProdutores(lastUpdate) {
    let promise = new Promise(resolve => {
      let total = 0;
      let inc = 0;
      let indexBASEP = this.getIndex('base_produtores', 'D');
      this.tpIntegracaoProvider.apiGetAll(lastUpdate).subscribe((tpIntegracoes: Array<any>) => {

        if (tpIntegracoes) {
          total += tpIntegracoes.length
        }

        this.stpIntegracaoProvider.apiGetAll(lastUpdate).subscribe((stpIntegracoes: Array<any>) => {

          if (stpIntegracoes) {
            total += stpIntegracoes.length
          }
          this.comedouroProvider.apiGetAll(lastUpdate).subscribe((comedouros: Array<any>) => {
            if (comedouros) {
              total += comedouros.length
            }

            this.galpoesProvider.apiGetTiposGalpoesAll(lastUpdate).subscribe((tpGalpoes: Array<any>) => {
              if (tpGalpoes) {
                total += tpGalpoes.length
              }

              this.lotesProvider.apiGetLinhagensAll(lastUpdate).subscribe((linhagens: Array<any>) => {
                if (linhagens) {
                  total += linhagens.length
                }

                this.lotesProvider.apiGetStatusAll(lastUpdate).subscribe((status: Array<any>) => {
                  if (status) {
                    total += status.length
                  }

                  this.lotesProvider.apiGetSexoAll(lastUpdate).subscribe((sexos: Array<any>) => {
                    if (sexos) {
                      total += sexos.length
                    }

                    this.lotesProvider.apiGetFabricasAll(lastUpdate).subscribe((fabricas: Array<any>) => {

                      if (fabricas) {
                        total += fabricas.length
                      }

                      inc = parseFloat(Number(100 / total).toFixed(2));
                      this.listSincDown[indexBASEP].total = total;
                      let total_progess = inc * total;

                      if (tpIntegracoes.length) {
                        for (let tpIntegracao of tpIntegracoes) {
                          this.tpIntegracaoProvider.update(tpIntegracao).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.tpIntegracaoProvider.insert(tpIntegracao).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              }).catch(err => {
                                console.error('falha', err);
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          }).catch(err => {
                            console.error('falha', err);
                          });
                        }
                      }

                      if (stpIntegracoes.length) {
                        for (let stpIntegracao of stpIntegracoes) {
                          this.stpIntegracaoProvider.update(stpIntegracao).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.stpIntegracaoProvider.insert(stpIntegracao).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (comedouros.length) {
                        for (let comedouro of comedouros) {
                          this.comedouroProvider.update(comedouro).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.comedouroProvider.insert(comedouro).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (tpGalpoes.length) {
                        for (let tpGalpao of tpGalpoes) {
                          this.galpoesProvider.updateTpGalpao(tpGalpao).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.galpoesProvider.insertTpGalpao(tpGalpao).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (linhagens.length) {
                        for (let linhagem of linhagens) {
                          this.lotesProvider.updateLinhagem(linhagem).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.lotesProvider.insertLinhagem(linhagem).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (status.length) {
                        for (let statu of status) {
                          this.lotesProvider.updateStatusLote(statu).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.lotesProvider.insertStatusLote(statu).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (sexos.length) {
                        for (let sexo of sexos) {
                          this.lotesProvider.updateSexosLote(sexo).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.lotesProvider.insertSexosLote(sexo).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      }

                      if (fabricas.length) {
                        for (let fabrica of fabricas) {
                          this.lotesProvider.updateFabrica(fabrica).then((data_up: any) => {

                            this.listSincDown[indexBASEP].progress += inc;
                            if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                              this.listSincDown[indexBASEP].progress = 100;
                              this.marcaSincronizado(indexBASEP, 'D');
                              resolve();
                            }

                            if (data_up.rowsAffected == 0) {
                              this.lotesProvider.insertFabrica(fabrica).then(() => {
                                if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                  this.listSincDown[indexBASEP].progress = 100;
                                  this.marcaSincronizado(indexBASEP, 'D');
                                  resolve();
                                }
                              });
                            } else {
                              if (Number(parseFloat(this.listSincDown[indexBASEP].progress).toFixed(2)) >= Number(total_progess.toFixed(2))) {
                                this.listSincDown[indexBASEP].progress = 100;
                                this.marcaSincronizado(indexBASEP, 'D');
                                resolve();
                              }
                            }
                          });
                        }
                      } else {
                        this.listSincDown[indexBASEP].progress = 100;
                        this.marcaSincronizado(indexBASEP, 'D');
                        resolve();
                      }

                      if (total == 0) {
                        this.listSincDown[indexBASEP].total = 0;
                        this.listSincDown[indexBASEP].progress = 100;
                        this.marcaSincronizado(indexBASEP, 'D');
                        resolve();
                      }

                    }, err => {
                      console.error('Falha apiGetFabricasAll', err, lastUpdate);
                    });
                  }, err => {
                    console.error('Falha apiGetSexoAll', err, lastUpdate);
                  });
                }, err => {
                  console.error('Falha apiGetStatusAll', err, lastUpdate);
                });
              }, err => {
                console.error('Falha apiGetLinhagensAll', err, lastUpdate);
              });
            }, err => {
              console.error('Falha apiGetTiposGalpoesAll', err, lastUpdate);
            });
          }, err => {
            console.error('Falha comedouroProvider apiGetAll', err, lastUpdate);
          });
        }, err => {
          console.error('Falha stpIntegracaoProvider apiGetAll', err, lastUpdate);
        });
      }, err => {
        console.error('Falha ao buscar produtores para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaProdutores(lastUpdate) {

    let promise = new Promise(resolve => {
      this.produtoresProvider.apiGetAll(lastUpdate).subscribe((produtores: Array<any>) => {

        let indexPROD = this.getIndex('produtores', 'D');
        if (produtores.length > 0) {
          let total_produtores = produtores.length;
          let incPROD = 100 / total_produtores;
          this.listSincDown[indexPROD].total = total_produtores;

          for (let produtor of produtores) {

            this.produtoresProvider.update(produtor).then((data_up: any) => {
              this.listSincDown[indexPROD].progress += incPROD;

              if (data_up.rowsAffected == 0) {
                if (Math.round(this.listSincDown[indexPROD].progress) >= 100) {

                  this.listSincDown[indexPROD].progress = 100;
                  this.marcaSincronizado(indexPROD, 'D');
                  resolve();
                }

                this.produtoresProvider.insert(produtor).then((data_ins: any) => {

                  if (Math.round(this.listSincDown[indexPROD].progress) >= 100) {

                    this.listSincDown[indexPROD].progress = 100;
                    this.marcaSincronizado(indexPROD, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir produtor', err, produtor)
                })
              } else {
                if (Math.round(this.listSincDown[indexPROD].progress) >= 100) {

                  this.listSincDown[indexPROD].progress = 100;
                  this.marcaSincronizado(indexPROD, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar produtor', err, produtor)
            });
          }
        } else {
          this.listSincDown[indexPROD].total = 0;
          this.listSincDown[indexPROD].progress = 100;
          this.marcaSincronizado(indexPROD, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao buscar produtores para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaPropriedades(lastUpdate) {

    let promise = new Promise(resolve => {
      this.propriedadesProvider.apiGetAll(lastUpdate).subscribe((propriedades: Array<any>) => {

        let indexPROP = this.getIndex('propriedades', 'D');
        if (propriedades.length > 0) {
          let total_propriedades = propriedades.length;
          let incPROP = 100 / total_propriedades;
          this.listSincDown[indexPROP].total = total_propriedades;

          for (let propriedade of propriedades) {

            this.propriedadesProvider.update(propriedade).then((data_up: any) => {
              this.listSincDown[indexPROP].progress += incPROP;

              if (data_up.rowsAffected == 0) {
                this.propriedadesProvider.insert(propriedade).then(() => {

                  if (Math.round(this.listSincDown[indexPROP].progress) >= 100) {

                    this.listSincDown[indexPROP].progress = 100;
                    this.marcaSincronizado(indexPROP, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir propriedade', err, propriedade)
                })
              } else {
                if (Math.round(this.listSincDown[indexPROP].progress) >= 100) {

                  this.listSincDown[indexPROP].progress = 100;
                  this.marcaSincronizado(indexPROP, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar propriedade', err, propriedade)
            });
          }
        } else {
          this.listSincDown[indexPROP].total = 0;
          this.listSincDown[indexPROP].progress = 100;
          this.marcaSincronizado(indexPROP, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao buscar propriedades para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaGalpoes(lastUpdate) {

    let promise = new Promise(resolve => {
      this.galpoesProvider.apiGetAll(lastUpdate).subscribe((galpoes: Array<any>) => {

        let indexGP = this.getIndex('galpoes', 'D');
        let total_galpoes = galpoes.length;

        if (total_galpoes > 0) {
          let incGP = 100 / total_galpoes;
          this.listSincDown[indexGP].total = total_galpoes;
          for (let galpao of galpoes) {

            this.galpoesProvider.update(galpao).then((data_up: any) => {
              this.listSincDown[indexGP].progress += incGP;

              if (data_up.rowsAffected == 0) {
                this.galpoesProvider.insert(galpao).then(() => {

                  if (Math.round(this.listSincDown[indexGP].progress) >= 100) {

                    this.listSincDown[indexGP].progress = 100;
                    this.marcaSincronizado(indexGP, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir galpao', err, galpao)
                })
              } else {

                if (Math.round(this.listSincDown[indexGP].progress) >= 100) {

                  this.listSincDown[indexGP].progress = 100;
                  this.marcaSincronizado(indexGP, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar galpao', err, galpao)
            });
          }
        } else {
          this.listSincDown[indexGP].total = 0;
          this.listSincDown[indexGP].progress = 100;
          this.marcaSincronizado(indexGP, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao buscar galpoes para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaLotes(lastUpdate) {

    let promise = new Promise(resolve => {
      this.lotesProvider.apiGetAll(lastUpdate).subscribe((lotes: Array<any>) => {

        let indexLT = this.getIndex('lotes', 'D');
        let total_lotes = lotes.length;
        if (total_lotes > 0) {
          let incLT = 100 / total_lotes;
          this.listSincDown[indexLT].total = total_lotes;

          for (let lote of lotes) {

            this.lotesProvider.update(lote).then((data_up: any) => {
              this.listSincDown[indexLT].progress += incLT;

              if (data_up.rowsAffected == 0) {
                this.lotesProvider.insert(lote).then(() => {

                  if (Math.round(this.listSincDown[indexLT].progress) >= 100) {

                    this.listSincDown[indexLT].progress = 100;
                    this.marcaSincronizado(indexLT, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir lote', err, lote)
                })
              } else {
                if (Math.round(this.listSincDown[indexLT].progress) >= 100) {

                  this.listSincDown[indexLT].progress = 100;
                  this.marcaSincronizado(indexLT, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar lote', err, lote)
            });
          }
        } else {
          this.listSincDown[indexLT].total = 0;
          this.listSincDown[indexLT].progress = 100;
          this.marcaSincronizado(indexLT, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao buscar lotes para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  /*
  * Métodos para Baixar Dados do Plano de Ação
  * */
  private downPlanoAcao(lastUpdate) {
    let promise = new Promise((resolve) => {
      this.baixaBasePlanoAcao(lastUpdate).then(() => {
        this.baixaAcoes(lastUpdate).then(() => {
          this.baixaAcaoesBaixadas(lastUpdate).then(() => {
            this.baixaAvaliacoesInicias(lastUpdate).then(() => {
              this.baixaRenovacoesAcaoes(lastUpdate).then(() => {
                resolve();
              });
            });
          });
        });
      })
    });

    return promise;
  }

  private baixaBasePlanoAcao(lastUpdate) {
    let promise = new Promise(resolve => {
      let total = 0;
      let inc = 0;
      let indexBASEPA = this.getIndex('base_plano_acoes', 'D');
      this.integradorasProvider.apiGetAll(lastUpdate).subscribe((integradoras: Array<any>) => {
        if (integradoras) {
          total += integradoras.length
        }

        this.tpInfoAcoesProvider.apiGetAll(lastUpdate).subscribe((informacoes_acoes: Array<any>) => {
          if (informacoes_acoes) {
            total += informacoes_acoes.length
          }

          this.situacoesAcoesProvider.apiGetAll(lastUpdate).subscribe((situacoes: Array<any>) => {
            if (situacoes) {
              total += situacoes.length
            }

            this.grupoAcoesProvider.apiGetAll(lastUpdate).subscribe((grupos: Array<any>) => {
              if (grupos) {
                total += grupos.length
              }

              this.pontoAcoesProvider.apiGetAll(lastUpdate).subscribe((pontos: Array<any>) => {
                if (pontos) {
                  total += pontos.length
                }

                this.paramAcoesProvider.apiGetAll(lastUpdate).subscribe((param_acoes: Array<any>) => {
                  if (param_acoes) {
                    total += param_acoes.length
                  }

                  this.paramPgtoProvider.apiGetAll(lastUpdate).subscribe((param_pgtos: Array<any>) => {
                    if (param_pgtos) {
                      total += param_pgtos.length
                    }

                    inc = parseFloat(Number(100 / total).toFixed(2));
                    this.listSincDown[indexBASEPA].total = total;
                    let total_progress = parseFloat(Number(inc * total).toFixed(2));

                    if (integradoras.length > 0) {
                      for (let integradora of integradoras) {
                        this.integradorasProvider.update(integradora).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.integradorasProvider.insert(integradora).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (informacoes_acoes.length > 0) {
                      for (let informacao_acao of informacoes_acoes) {
                        this.tpInfoAcoesProvider.update(informacao_acao).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.tpInfoAcoesProvider.insert(informacao_acao).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (situacoes.length > 0) {
                      for (let situacao of situacoes) {
                        this.situacoesAcoesProvider.update(situacao).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.situacoesAcoesProvider.insert(situacao).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (grupos.length > 0) {
                      for (let grupo of grupos) {
                        this.grupoAcoesProvider.update(grupo).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.grupoAcoesProvider.insert(grupo).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (pontos.length > 0) {
                      for (let ponto of pontos) {
                        this.pontoAcoesProvider.update(ponto).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.pontoAcoesProvider.insert(ponto).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (param_acoes.length > 0) {
                      for (let param_acao of param_acoes) {
                        this.paramAcoesProvider.update(param_acao).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.paramAcoesProvider.insert(param_acao).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (param_pgtos.length > 0) {
                      for (let param_pgto of param_pgtos) {
                        this.paramPgtoProvider.update(param_pgto).then((data_up: any) => {
                          this.listSincDown[indexBASEPA].progress += inc;
                          if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASEPA].progress = 100;
                            this.marcaSincronizado(indexBASEPA, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.paramPgtoProvider.insert(param_pgto).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASEPA].progress = 100;
                                this.marcaSincronizado(indexBASEPA, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASEPA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASEPA].progress = 100;
                              this.marcaSincronizado(indexBASEPA, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    } else {
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                    if (total == 0) {
                      this.listSincDown[indexBASEPA].total = 0;
                      this.listSincDown[indexBASEPA].progress = 100;
                      this.marcaSincronizado(indexBASEPA, 'D');
                      resolve();
                    }

                  });
                });
              });
            });
          });
        })
      })

    });

    return promise
  }

  private baixaAcoes(lastUpdate) {
    let promise = new Promise(resolve => {
      this.acoesProvider.apiGetAll(lastUpdate).subscribe((acoes: Array<any>) => {

        let indexAC = this.getIndex('acoes', 'D');
        if (acoes.length > 0) {
          let total_acoes = acoes.length;

          let incAC = 100 / total_acoes;
          this.listSincDown[indexAC].total = total_acoes;
          for (let acao of acoes) {

            acao.id_web = acao.id;
            acao.avaliacao_inicial = acao.avaliacao_inicial_id;

            this.acoesProvider.update(acao).then((data_up: any) => {
              this.listSincDown[indexAC].progress += incAC;

              if (data_up == null || data_up.rowsAffected == 0) {

                this.acoesProvider.insert(acao, null).then(() => {
                  if (Math.round(this.listSincDown[indexAC].progress) >= 100) {

                    this.listSincDown[indexAC].progress = 100;
                    this.marcaSincronizado(indexAC, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir acao', err, acao)
                })
              } else {
                if (Math.round(this.listSincDown[indexAC].progress) >= 100) {

                  this.listSincDown[indexAC].progress = 100;
                  this.marcaSincronizado(indexAC, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar acao', err, acao)
            });
          }
        } else {
          this.listSincDown[indexAC].total = 0;
          this.listSincDown[indexAC].progress = 100;
          this.marcaSincronizado(indexAC, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao consultar ações para download', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaAcaoesBaixadas(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixasProvider.apiGetAll(lastUpdate).subscribe((baixas: Array<any>) => {

        let indexBXA = this.getIndex('baixas_acoes', 'D');
        if (baixas.length) {

          let total_baixas = baixas.length;
          let incBXA = 100 / total_baixas;
          this.listSincDown[indexBXA].total = total_baixas;
          for (let baixa of baixas) {
            baixa.id_web = baixa.id;
            baixa.acao_id_web = baixa.acao_id;
            baixa.acao_id = baixa.acao.id_mobile;

            this.baixasProvider.update(baixa).then((data_up: any) => {

              this.listSincDown[indexBXA].progress += incBXA;
              if (data_up.rowsAffected == 0) {

                this.baixasProvider.insert(baixa).then(() => {

                  if (Math.round(this.listSincDown[indexBXA].progress) >= 100) {

                    this.listSincDown[indexBXA].progress = 100;
                    this.marcaSincronizado(indexBXA, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir baixa', err, baixa)
                })
              } else {

                if (Math.round(this.listSincDown[indexBXA].progress) >= 100) {

                  this.listSincDown[indexBXA].progress = 100;
                  this.marcaSincronizado(indexBXA, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('Falha ao atualiza baixa', err, baixa);
            })
          }
        } else {
          this.listSincDown[indexBXA].total = 0;
          this.listSincDown[indexBXA].progress = 100;
          this.marcaSincronizado(indexBXA, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao baixar acoes', err, lastUpdate);
      });
    });

    return promise;

  }

  private baixaRenovacoesAcaoes(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixasProvider.apiGetAllRenovacoes(lastUpdate).subscribe((renovacoes: Array<any>) => {

        let indexRNA = this.getIndex('renovadas', 'D');
        if (renovacoes.length) {

          let total_renovacoes = renovacoes.length;
          let incRNA = 100 / total_renovacoes;
          this.listSincDown[indexRNA].total = total_renovacoes;
          for (let renovacao of renovacoes) {
            renovacao.id_web = renovacao.id;
            renovacao.acao_id_web = renovacao.acao_id;

            this.baixasProvider.updateRenovacaoAcao(renovacao).then((data_up: any) => {

              this.listSincDown[indexRNA].progress += incRNA;
              if (data_up.rowsAffected == 0) {

                renovacao.resposta = renovacao.justificativa;
                this.baixasProvider.insertRenovacaoAcao(renovacao).then(() => {

                  if (Math.round(this.listSincDown[indexRNA].progress) >= 100) {

                    this.listSincDown[indexRNA].progress = 100;
                    this.marcaSincronizado(indexRNA, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir renovacção da acao', err, renovacao)
                })
              } else {

                if (Math.round(this.listSincDown[indexRNA].progress) >= 100) {

                  this.listSincDown[indexRNA].progress = 100;
                  this.marcaSincronizado(indexRNA, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('Falha ao atualiza renovacção da acao', err, renovacao);
            })
          }
        } else {
          this.listSincDown[indexRNA].total = 0;
          this.listSincDown[indexRNA].progress = 100;
          this.marcaSincronizado(indexRNA, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao baixar renovações', err, lastUpdate);
      });
    });

    return promise;

  }

  private baixaAvaliacoesInicias(lastUpdate) {
    let promise = new Promise(resolve => {
      this.avaliacoesProvider.apiGetAll(lastUpdate).subscribe((avaliacoes: Array<any>) => {

        let indexAVI = this.getIndex('avaliacoes_iniciais', 'D');
        if (avaliacoes.length) {

          let total_avaliacoes = avaliacoes.length;
          let incAVI = 100 / total_avaliacoes;
          this.listSincDown[indexAVI].total = total_avaliacoes;
          for (let avalicao of avaliacoes) {

            avalicao.id_web = avalicao.id;
            this.avaliacoesProvider.update(avalicao).then((data_up: any) => {

              this.listSincDown[indexAVI].progress += incAVI;
              if (data_up.rowsAffected == 0) {

                this.avaliacoesProvider.insert(avalicao).then(() => {

                  if (Math.round(this.listSincDown[indexAVI].progress) >= 100) {

                    this.listSincDown[indexAVI].progress = 100;
                    this.marcaSincronizado(indexAVI, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir avalicao', err, avalicao)
                })
              } else {

                if (Math.round(this.listSincDown[indexAVI].progress) >= 100) {

                  this.listSincDown[indexAVI].progress = 100;
                  this.marcaSincronizado(indexAVI, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('Falha ao atualiza avalicao', err, avalicao);
            })
          }
        } else {
          this.listSincDown[indexAVI].total = 0;
          this.listSincDown[indexAVI].progress = 100;
          this.marcaSincronizado(indexAVI, 'D');
          resolve();
        }
      }, err => {
        console.error('Falha ao baixar avaliacoes', err, lastUpdate);
      })
    });

    return promise;
  }

  /*
  * Métodos para Baixar dados do Checklist
  * */

  private downChecklist(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixaBaseChecklist(lastUpdate).then(() => {
        this.baixaAvaliacoesComRespostas(lastUpdate).then(() => {
          resolve();
        });
      });
    });

    return promise;
  }

  private baixaBaseChecklist(lastUpdate) {
    let promise = new Promise(resolve => {
      let total = 0;
      let inc = 0;

      let indexBASECHK = this.getIndex('base_checklist', 'D');

      this.checklistProvider.apiGetSubtiposIntegracoesAll().subscribe((subtipos: Array<any>) => {
        if (subtipos) {
          total += subtipos.length
        }
        this.checklistProvider.apiGetFaseAll(lastUpdate).subscribe((fases: Array<any>) => {
          if (fases) {
            total += fases.length
          }
          this.checklistProvider.apiGetAll(lastUpdate).subscribe((checklists: Array<any>) => {

            if (checklists) {
              total += checklists.length
            }
            this.perguntasProvider.apiGetAll(lastUpdate).subscribe((perguntas: Array<any>) => {

              if (perguntas) {
                total += perguntas.length
              }
              this.cotacaoProvider.apiGetAll(lastUpdate).subscribe((cotacoes: Array<any>) => {

                if (cotacoes) {
                  total += cotacoes.length
                }
                this.intervalosProvider.apiGetAll(lastUpdate).subscribe((intervalos: Array<any>) => {

                  if (intervalos) {
                    total += intervalos.length
                  }
                  this.periodosProvider.apiGetAll(lastUpdate).subscribe((periodos: Array<any>) => {

                    if (periodos) {
                      total += periodos.length
                    }
                    inc = parseFloat(Number(100 / total).toFixed(2));
                    let total_progress = parseFloat(Number(inc * total).toFixed(2));

                    this.listSincDown[indexBASECHK].total = total;

                    if (subtipos) {
                      for (let subtipo of subtipos) {
                        this.checklistProvider.deleteChecklistSubtipoIntegracao().then(() => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          this.checklistProvider.insertChecklistSubtipoIntegracao(subtipo).then(() => {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          });
                        });
                      }
                    }

                    if (fases) {
                      for (let fase of fases) {
                        this.checklistProvider.updateFase(fase).then((data_up: any) => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          if (data_up.rowsAffected == 0) {
                            this.checklistProvider.insertFase(fase).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (checklists) {
                      for (let checklist of checklists) {
                        this.checklistProvider.update(checklist).then((data_up: any) => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          if (data_up.rowsAffected == 0) {
                            this.checklistProvider.insert(checklist).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (perguntas) {
                      for (let pergunta of perguntas) {
                        this.perguntasProvider.update(pergunta).then((data_up: any) => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          if (data_up.rowsAffected == 0) {
                            this.perguntasProvider.insert(pergunta).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (cotacoes) {
                      for (let cotacao of cotacoes) {
                        this.cotacaoProvider.update(cotacao).then((data_up: any) => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          if (data_up.rowsAffected == 0) {
                            this.cotacaoProvider.insert(cotacao).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (intervalos) {
                      for (let intervalo of intervalos) {

                        this.intervalosProvider.update(intervalo).then((data_up: any) => {

                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));
                          if (data_up.rowsAffected == 0) {
                            this.intervalosProvider.insert(intervalo).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (periodos) {
                      for (let periodo of periodos) {
                        this.periodosProvider.update(periodo).then((data_up: any) => {
                          this.listSincDown[indexBASECHK].progress += Number(inc.toFixed(2));

                          if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                            this.listSincDown[indexBASECHK].progress = 100;
                            this.marcaSincronizado(indexBASECHK, 'D');
                            resolve();
                          }

                          if (data_up.rowsAffected == 0) {
                            this.periodosProvider.insert(periodo).then(() => {
                              if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                                this.listSincDown[indexBASECHK].progress = 100;
                                this.marcaSincronizado(indexBASECHK, 'D');
                                resolve();
                              }
                            });
                          } else {
                            if (Number(parseFloat(this.listSincDown[indexBASECHK].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                              this.listSincDown[indexBASECHK].progress = 100;
                              this.marcaSincronizado(indexBASECHK, 'D');
                              resolve();
                            }
                          }
                        });
                      }
                    }

                    if (total == 0) {
                      this.listSincDown[indexBASECHK].total = 0;
                      this.listSincDown[indexBASECHK].progress = 100;
                      this.marcaSincronizado(indexBASECHK, 'D');
                      resolve();
                    }

                    // })
                  })
                })
              })
            })

          })
        })
      });
    });

    return promise;
  }

  private baixaAvaliacoesComRespostas(lastUpdate) {
    let promise = new Promise(resolve => {
      this.avaliacoesChecklistProvider.apiGetAll(lastUpdate).subscribe((avaliacoes: Array<any>) => {

        let indexAVCHK = this.getIndex('avaliacoes_com_respostas', 'D');
        let total_av = avaliacoes.length;
        if (avaliacoes.length > 0) {

          let total_avaliacoes = avaliacoes.length;
          for (let avaliacao of avaliacoes) {
            total_avaliacoes += avaliacao.respostas.length;
          }
          let incAVCHK = 100 / total_avaliacoes;
          this.listSincDown[indexAVCHK].total = total_avaliacoes;

          for (let avaliacao of avaliacoes) {
            this.avaliacoesChecklistProvider.update(avaliacao).then((data_up: any) => {
              this.listSincDown[indexAVCHK].progress += incAVCHK;

              if (data_up.rowsAffected == 0) {
                this.avaliacoesChecklistProvider.insert(avaliacao).then(() => {

                  if (Math.round(this.listSincDown[indexAVCHK].progress) >= 100) {

                    this.listSincDown[indexAVCHK].progress = 100;
                    this.marcaSincronizado(indexAVCHK, 'D');
                    resolve();
                  }
                  this.salvaResposta(avaliacao, indexAVCHK, incAVCHK, resolve, total_av);
                  total_av--;

                }).catch(err => {
                  console.error('falha ao inserir avaliacao', err, avaliacao)
                })
              } else {

                this.salvaResposta(avaliacao, indexAVCHK, incAVCHK, resolve, total_av);
                total_av--;
                if (Math.round(this.listSincDown[indexAVCHK].progress) >= 100) {

                  this.listSincDown[indexAVCHK].progress = 100;
                  this.marcaSincronizado(indexAVCHK, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar avaliacao', err, avaliacao)
            });
          }
        } else {
          this.listSincDown[indexAVCHK].total = 0;
          this.listSincDown[indexAVCHK].progress = 100;
          this.marcaSincronizado(indexAVCHK, 'D');
          resolve();
        }
      }, err => {
        console.error('Falha ao consultar avaliacoes com respostas', err, lastUpdate)
      });
    });

    return promise;
  }

  private baixaEstados(lastUpdate) {
    let promise = new Promise(resolve => {
      this.empresaProvider.apiGetEstadosAll(lastUpdate).subscribe((estados: Array<any>) => {
        this.empresaProvider.insertEstado(estados).then(() => {

          resolve();
        }).catch(err => {
          console.error('falha ao inserir estado', err, estados)
        });
      }, err => {
        console.error('Falha ao buscar estados para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaMunicipios(lastUpdate) {
    let promise = new Promise(resolve => {
      this.empresaProvider.apiGetMunicipiosAll(lastUpdate).subscribe((municipios: Array<any>) => {
        this.empresaProvider.insertMunicipio(municipios).then(() => {
          resolve();
        }).catch(err => {
          console.error('falha ao inserir municipio', err, municipios)
        });
      }, err => {
        console.error('Falha ao buscar municipios para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaVersoes(lastUpdate) {
    let promise = new Promise(resolve => {
      this.empresaProvider.apiGetVersoesAll(lastUpdate).subscribe((versoes: Array<any>) => {
        let inc = 0;
        let total_versoes = 0;

        if (versoes) {

          total_versoes = versoes.length;
          for (let versao of versoes) {

            inc++;
            this.empresaProvider.updateVersao(versao).then((data_up: any) => {
              if (data_up.rowsAffected == 0) {
                this.empresaProvider.insertVersao(versao).then(() => {

                  if (inc == total_versoes) {
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir versao', err, versao)
                })
              } else {
                if (inc == total_versoes) {
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar versao', err, versao)
            });
          }
        } else {
          resolve();
        }


      }, err => {
        console.error('Falha ao buscar produtores para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private downRecontagem(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixaIntervaloCelasUteis(lastUpdate).then(() => {
        this.baixaRecontagemComCelas(lastUpdate).then(() => {
          resolve();
        });
      });
    });

    return promise;
  }

  private baixaIntervaloCelasUteis(lastUpdate) {
    let promise = new Promise(resolve => {
      this.recontagemPlantelProvider.apiGetAllIntervalosCelasUteis(lastUpdate).subscribe((intervalos: Array<any>) => {

        let indexINTER = this.getIndex('celas_uteis', 'D');
        if (intervalos.length > 0) {
          let total_intervalos = intervalos.length;
          let incINTER = 100 / total_intervalos;
          this.listSincDown[indexINTER].total = total_intervalos;

          for (let intervalo of intervalos) {

            this.recontagemPlantelProvider.updateIntervalosCelasuteis(intervalo).then((data_up: any) => {
              this.listSincDown[indexINTER].progress += incINTER;

              if (data_up.rowsAffected == 0) {
                if (Math.round(this.listSincDown[indexINTER].progress) >= 100) {

                  this.listSincDown[indexINTER].progress = 100;
                  this.marcaSincronizado(indexINTER, 'D');
                  resolve();
                }

                this.recontagemPlantelProvider.insertIntervalosCelasuteis(intervalo).then((data_ins: any) => {

                  if (Math.round(this.listSincDown[indexINTER].progress) >= 100) {

                    this.listSincDown[indexINTER].progress = 100;
                    this.marcaSincronizado(indexINTER, 'D');
                    resolve();
                  }
                }).catch(err => {
                  console.error('falha ao inserir intervalo de celas uteis', err, intervalo)
                })
              } else {
                if (Math.round(this.listSincDown[indexINTER].progress) >= 100) {

                  this.listSincDown[indexINTER].progress = 100;
                  this.marcaSincronizado(indexINTER, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar intervalo de celas uteis', err, intervalo)
            });
          }
        } else {
          this.listSincDown[indexINTER].total = 0;
          this.listSincDown[indexINTER].progress = 100;
          this.marcaSincronizado(indexINTER, 'D');
          resolve();
        }

      }, err => {
        console.error('Falha ao buscar intervalo de celas uteis para baixar', err, lastUpdate);
      })
    });

    return promise;
  }

  private baixaRecontagemComCelas(lastUpdate) {
    let promise = new Promise(resolve => {
      this.recontagemPlantelProvider.apiGetAllRecontagemPlantel(lastUpdate).subscribe((recontagens: Array<any>) => {

        let indexRECPLAN = this.getIndex('recontagem_plantel', 'D');
        let total_recplan = recontagens.length;
        if (recontagens.length > 0) {

          let total_recontagem = recontagens.length;
          let incRECPLAN = 100 / total_recontagem;
          this.listSincDown[indexRECPLAN].total = total_recontagem;

          for (let recontagem of recontagens) {
            this.recontagemPlantelProvider.update(recontagem).then((data_up: any) => {

              this.listSincDown[indexRECPLAN].progress += incRECPLAN;

              if (data_up.rowsAffected == 0) {
                this.recontagemPlantelProvider.insert(recontagem).then((data_ins) => {

                  if (Math.round(this.listSincDown[indexRECPLAN].progress) >= 100) {

                    this.listSincDown[indexRECPLAN].progress = 100;
                    this.marcaSincronizado(indexRECPLAN, 'D');
                    resolve();
                  }
                  this.salvaRecontagemCelasUteis(recontagem, indexRECPLAN, incRECPLAN, resolve, total_recplan);
                  total_recplan--;

                }).catch(err => {
                  console.error('falha ao inserir recontagem de plantel', err, recontagem)
                })
              } else {

                this.salvaRecontagemCelasUteis(recontagem, indexRECPLAN, incRECPLAN, resolve, total_recplan);
                total_recplan--;
                if (Math.round(this.listSincDown[indexRECPLAN].progress) >= 100) {

                  this.listSincDown[indexRECPLAN].progress = 100;
                  this.marcaSincronizado(indexRECPLAN, 'D');
                  resolve();
                }
              }
            }).catch(err => {
              console.error('falha ao atualizar recontagem de plantel', err, recontagem)
            });
          }
        } else {
          this.listSincDown[indexRECPLAN].total = 0;
          this.listSincDown[indexRECPLAN].progress = 100;
          this.marcaSincronizado(indexRECPLAN, 'D');
          resolve();
        }
      }, err => {
        console.error('Falha ao consultar recontagem com celas úteis', err, lastUpdate)
      });
    });

    return promise;
  }

  private downVisitas(lastUpdate) {
    let promise = new Promise(resolve => {
      this.baixaBaseVisitas(lastUpdate).then(() => {
        resolve();
      });
    });

    return promise;
  }

  private baixaBaseVisitas(lastUpdate) {
    let promise = new Promise(resolve => {
      let total = 0;
      let inc = 0;

      let indexBASEVISITA = this.getIndex('base_visitas', 'D');

      this.visitasProvider.apiGetAllTiposCampos(lastUpdate).subscribe((campos: Array<any>) => {
        if (campos) {
          total += campos.length
        }

        this.visitasProvider.apiGetVisitasComPerguntas(lastUpdate).subscribe((visitas: Array<any>) => {
          if (visitas) {
            total += visitas.length;

            for (let visita of visitas) {
              total += visita.perguntas.length;
            }
          }


          this.visitasProvider.apiGetLacamentosComRespostas(lastUpdate).subscribe((lancamentos: Array<any>) => {
            if (lancamentos) {
              total += lancamentos.length;

              for (let lancamento of lancamentos) {
                total += lancamento.respostas.length;
              }
            }

            inc = parseFloat(Number(100 / total).toFixed(2));
            let total_progress = parseFloat(Number(inc * total).toFixed(2));

            this.listSincDown[indexBASEVISITA].total = total;

            if (campos) {
              for (let campo of campos) {
                this.listSincDown[indexBASEVISITA].progress += Number(inc.toFixed(2));
                this.visitasProvider.insertTipoCampo(campo).then(() => {
                  if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                    this.listSincDown[indexBASEVISITA].total = 0;
                    this.listSincDown[indexBASEVISITA].progress = 100;
                    this.marcaSincronizado(indexBASEVISITA, 'D');
                    resolve();
                  }
                });
              }
            }

            if (visitas) {
              for (let visita of visitas) {
                this.visitasProvider.updateVisitaLote(visita).then((data_up: any) => {

                  this.listSincDown[indexBASEVISITA].progress += Number(inc.toFixed(2));
                  if (data_up.rowsAffected == 0) {
                    this.visitasProvider.insertVisitaLote(visita).then(() => {
                      if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                        this.listSincDown[indexBASEVISITA].total = 0;
                        this.listSincDown[indexBASEVISITA].progress = 100;
                        this.marcaSincronizado(indexBASEVISITA, 'D');
                        resolve();
                      }
                      this.salvaPerguntasVisita(visita, inc, resolve, indexBASEVISITA, total_progress);
                    });
                  } else {
                    if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                      this.listSincDown[indexBASEVISITA].total = 0;
                      this.listSincDown[indexBASEVISITA].progress = 100;
                      this.marcaSincronizado(indexBASEVISITA, 'D');
                      resolve();
                    }
                    this.salvaPerguntasVisita(visita, inc, resolve, indexBASEVISITA, total_progress);
                  }
                });
              }
            }

            if (lancamentos) {
              for (let lancamento of lancamentos) {
                this.visitasProvider.updateVisitaLancamento(lancamento).then((data_up: any) => {

                  this.listSincDown[indexBASEVISITA].progress += Number(inc.toFixed(2));
                  if (data_up.rowsAffected == 0) {
                    this.visitasProvider.insertVisitaLancamento(lancamento).then(() => {
                      if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                        this.listSincDown[indexBASEVISITA].total = 0;
                        this.listSincDown[indexBASEVISITA].progress = 100;
                        this.marcaSincronizado(indexBASEVISITA, 'D');
                        resolve();
                      }
                      this.salvaRespostasVisitaLancamento(lancamento, inc, resolve, indexBASEVISITA, total_progress);

                    });
                  } else {
                    if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                      this.listSincDown[indexBASEVISITA].total = 0;
                      this.listSincDown[indexBASEVISITA].progress = 100;
                      this.marcaSincronizado(indexBASEVISITA, 'D');
                      resolve();
                    }
                    this.salvaRespostasVisitaLancamento(lancamento, inc, resolve, indexBASEVISITA, total_progress);
                  }
                });
              }
            }

            if(total == 0){
              this.listSincDown[indexBASEVISITA].total = 0;
              this.listSincDown[indexBASEVISITA].progress = 100;
              this.marcaSincronizado(indexBASEVISITA, 'D');
              resolve();
            }

          }, err => {
            console.error('Falha ao consultar apiGetLacamentosComRespostas', err, lastUpdate)
          });
        }, err => {
          console.error('Falha ao consultar apiGetVisitasComPerguntas', err, lastUpdate)
        });

      }, err => {
        console.error('Falha ao consultar apiGetAllTiposCampos', err, lastUpdate)
      });
    });

    return promise;
  }


  /*
  * Métodos para Enviar dados de Erro e Base de dados
  * */

  private enviaBase() {
    this.storage.get('SERVER_URL').then((SERVER_URL: any) => {

      this.storage.get('access_token').then((access_token) => {

        let options: FileUploadOptions = {
          fileKey: 'base',
          fileName: `planoa.db`,
          chunkedMode: false,
          httpMethod: 'POST',
          params: {'device': this.device.model},
          headers: {
            'Authorization': `Bearer ${access_token}`,
          }
        };
        let fileTransfer = this.fileTransfer.create();
        fileTransfer.upload('/sdcard/planoa.db', `${SERVER_URL}/importar/base`, options).then((res) => {
          this.storage.set('bases_to_export', false);

        }).catch(err => {
          console.error('falha ao enviar base de dados')
        })

      });
    });
  }

  private enviaErros() {
    this.exceptionProvider.getAllSync().then((erros: Array<any>) => {

      for (let erro of erros) {
        this.exceptionProvider.storeApi(erro).subscribe(result => {
          this.exceptionProvider.updateSync(erro.id);

        }, err => {
          console.error('erro ao enviar erro', err);
          this.exceptionProvider.insert(err, 'sincronizacao.ts', 'enviaerros');
        });
      }
      this.exceptionProvider.deleteAfterSync();
    });
  }

  private enviaLocalizacoes() {
    this.geolocationProvider.getAllSync().then((data: Array<any>) => {
      this.geolocationProvider.enviaLocalizacoesOffline(data);
    });
  }

  /*
  * Métodos Auxiliares
  * */

  private salvaResposta(avaliacao, indexAVCHK, incAVCHK, resolve, total_av) {
    if (total_av > 0 && avaliacao.respostas.length > 0) {
      for (let resposta of avaliacao.respostas) {
        resposta.avaliacao_id_web = resposta.avaliacao_id;
        resposta.avaliacao_id = avaliacao.id_mobile;

        this.respostasProvider.update(resposta).then((data_up_resp) => {
          if (data_up_resp.rowsAffected == 0) {

            resposta.id_web = resposta.id;

            this.respostasProvider.insert(resposta).then(() => {
              this.listSincDown[indexAVCHK].progress += incAVCHK;
              if (Math.round(this.listSincDown[indexAVCHK].progress) >= 100) {

                this.listSincDown[indexAVCHK].progress = 100;
                this.marcaSincronizado(indexAVCHK, 'D');
                resolve();
              }
            })
          } else {
            this.listSincDown[indexAVCHK].progress += incAVCHK;
            if (Math.round(this.listSincDown[indexAVCHK].progress) >= 100) {

              this.listSincDown[indexAVCHK].progress = 100;
              this.marcaSincronizado(indexAVCHK, 'D');
              resolve();
            }
          }
        })
      }
    } else {
      this.listSincDown[indexAVCHK].progress = 100;
      this.marcaSincronizado(indexAVCHK, 'D');
      resolve();
    }


  }

  private salvaRecontagemCelasUteis(recontagem, indexRECPLANT, incRECPLAN, resolve, total_recplan) {
    if (total_recplan > 0 && recontagem.celas_uteis.length > 0) {
      for (let cela of recontagem.celas_uteis) {
        this.recontagemPlantelProvider.updateTotalCela(cela).then((data_up_resp) => {
          if (data_up_resp.rowsAffected == 0) {

            cela.id_web = cela.id;

            this.recontagemPlantelProvider.insertTotalCela(cela).then((data_ins_resp) => {
              this.listSincDown[indexRECPLANT].progress += incRECPLAN;
              if (Math.round(this.listSincDown[indexRECPLANT].progress) >= 100) {

                this.listSincDown[indexRECPLANT].progress = 100;
                this.marcaSincronizado(indexRECPLANT, 'D');
                resolve();
              }
            })
          } else {
            this.listSincDown[indexRECPLANT].progress += incRECPLAN;
            if (Math.round(this.listSincDown[indexRECPLANT].progress) >= 100) {

              this.listSincDown[indexRECPLANT].progress = 100;
              this.marcaSincronizado(indexRECPLANT, 'D');
              resolve();
            }
          }
        })
      }
    } else {
      this.listSincDown[indexRECPLANT].progress = 100;
      this.marcaSincronizado(indexRECPLANT, 'D');
      resolve();
    }


  }

  private salvaPerguntasVisita(visita, inc, resolve, indexBASEVISITA, total_progress) {
    if (visita.perguntas.length > 0) {
      for (let pergunta of visita.perguntas) {
        this.listSincDown[indexBASEVISITA].progress += Number(inc.toFixed(2));

        this.visitasProvider.updatePergunta(pergunta).then((data_up_resp) => {
          if (data_up_resp.rowsAffected == 0) {

            this.visitasProvider.insertPergunta(pergunta).then(() => {
              if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                this.listSincDown[indexBASEVISITA].progress = 100;
                this.marcaSincronizado(indexBASEVISITA, 'D');
                resolve();
              }
            })
          } else {
            if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
              this.listSincDown[indexBASEVISITA].progress = 100;
              this.marcaSincronizado(indexBASEVISITA, 'D');
              resolve();
            }
          }
        })
      }
    } else {
      this.listSincDown[indexBASEVISITA].progress = 100;
      this.marcaSincronizado(indexBASEVISITA, 'D');
      resolve();
    }


  }

  private salvaRespostasVisitaLancamento(lancamento, inc, resolve, indexBASEVISITA, total_progress) {
    if (lancamento.respostas.length > 0) {
      for (let resposta of lancamento.respostas) {

        this.listSincDown[indexBASEVISITA].progress += Number(inc.toFixed(2));

        this.visitasProvider.updateResposta(resposta).then((data_up_resp) => {
          if (data_up_resp.rowsAffected == 0) {

            this.visitasProvider.insertResposta(resposta).then(() => {
              if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
                this.listSincDown[indexBASEVISITA].progress = 100;
                this.marcaSincronizado(indexBASEVISITA, 'D');
                resolve();
              }
            })
          } else {
            if (Number(parseFloat(this.listSincDown[indexBASEVISITA].progress).toFixed(2)) >= Number(total_progress.toFixed(2))) {
              this.listSincDown[indexBASEVISITA].progress = 100;
              this.marcaSincronizado(indexBASEVISITA, 'D');
              resolve();
            }
          }
        })
      }
    } else {
      this.listSincDown[indexBASEVISITA].progress = 100;
      this.marcaSincronizado(indexBASEVISITA, 'D');
      resolve();
    }


  }

	private baixaThumbsPerguntas(lastSync) {

		let promise = new Promise(resolve => {
			this.perguntasProvider.apiGetThumbsPerguntas(lastSync).subscribe((fotos: Array<any>) => {
				if (fotos.length > 0) {
					for (let foto of fotos) {
						this.perguntasProvider.salvaOldThumbPergunta(foto.pergunta_id, foto.galpao_id, foto.foto64)
					}
				}
			}, err => {
				console.error('erro ao baixar fotos',err)
			});
		});

		return promise;
	}
}
