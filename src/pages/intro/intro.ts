import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SlideProvider} from "../../providers/slide/slide";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
  providers: [
    SlideProvider
  ]
})
export class IntroPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private slideProvider: SlideProvider) {
  }

  goToTabsPage(){
    this.slideProvider.setConfigSlide('false');
    this.navCtrl.setRoot(TabsPage);
  }

}
