import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Tecnico, TecnicoProvider} from "../../providers/tecnico/tecnico";
import {Storage} from "@ionic/storage";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {CameraProvider} from "../../providers/camera/camera";
import {UtilsService} from "../../services/utils.service";
import {DbExceptionProvider} from "../../providers/db-exception/db-exception";
import {FormControl, FormGroup} from "@angular/forms";
import {Keyboard} from "@ionic-native/keyboard";
import {AuthProvider} from "../../providers/auth/auth";
import {FirestoreDatabaseProvider} from "../../providers/firestore-database/firestore-database";

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
  providers: [Keyboard],

})
export class PerfilPage implements OnInit{
  tecnico: Tecnico;
  img: String;
  fileTransfer: any;
  tecnicoForm: FormGroup;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private tecnicoProvider: TecnicoProvider,
              private camera: CameraProvider,
              private keyboard: Keyboard,
              private networkService: CheckNetworkProvider,
              private messageService: AlertMessagesProvider,
              private authProvider: AuthProvider,
              private storage: Storage,
              private utils:UtilsService,
              private exceptionProvider:DbExceptionProvider,
              private fbDbProvider:FirestoreDatabaseProvider,
  ) {}

  ngOnInit() {
    this.tecnicoForm = new FormGroup({
      'id': new FormControl(null),
      'name': new FormControl(''),
      'celular': new FormControl(''),
      'password': new FormControl(null),
      'forcar_atualizacao': new FormControl(''),
      'respostas_automaticas_chk': new FormControl(false),
      'foto': new FormControl('assets/imgs/user_default.png'),
    });

    this.tecnicoProvider.get().then((result: any) => {
      if(result != null){

        this.tecnicoForm.controls['id'].setValue(result.id);
        this.tecnicoForm.controls['name'].setValue(result.name);
        this.tecnicoForm.controls['celular'].setValue(result.celular);
        this.tecnicoForm.controls['forcar_atualizacao'].setValue(result.forcar_atualizacao);
        this.tecnicoForm.controls['respostas_automaticas_chk'].setValue(result.respostas_automaticas_chk);

        if(result.foto){
          this.tecnicoForm.controls['foto'].setValue(result.foto);
        }
      }
    })
  }

  openCamera() {
    this.messageService.showConfirmationAlert('Alterar foto!', 'Escolha o local de onde deseja buscar nova foto', 'Meu Album', 'Tirar Foto')
      .then((result: boolean) => {

        this.utils.checkDisck().then((data) => {

          if (data) {

            this.camera.capturarFoto(result, 300, 300, true, true).then((imageData: string) => {

              this.tecnico.foto = 'data:image/jpeg;base64,' + imageData;
              this.tecnicoForm.controls['foto'].setValue(this.tecnico.foto);
              this.tecnicoProvider.update(this.tecnico);
            }, (err) => {
              this.exceptionProvider.insert(err, 'perfil.ts', 'openCamera');
              this.fbDbProvider.addExceptionApp(err, 'perfil.ts', 'openCamera');
              console.error('erro ao capturar img', err)
            });
          }
        })
      });
  }

  update() {

    let showLoading = this.messageService.showLoading('Atualizando, aguarde...');
    this.tecnico = this.tecnicoForm.value;
    this.tecnico.forcar_atualizacao = this.tecnico.forcar_atualizacao ? 1:0;
    this.tecnicoProvider.update(this.tecnico);

    if(this.networkService.isConnected()){
      let dados = {
        celular: this.tecnico.celular,
        password : this.tecnico.password,
        foto : this.tecnico.foto,
        id : this.tecnico.id
      };

      this.authProvider.checkLogin().then((data: any) => {

        if (data == "success") {
          this.tecnicoProvider.apiUpdate(dados).then((res) => {
            this.messageService.showToastSuccess('Dados atualizados com sucesso!');
            showLoading.dismiss();
          }).catch((err) => {
            if(err.name == "TimeoutError"){
              this.messageService.showAlert('Opss!', 'Sua conexão parece estar lenta, seus dados foram salvos, mas não foram sincronizados!').then(()=>{
                this.messageService.showToastSuccess('Dados atualizados com sucesso!');
              });
            }else{
              this.messageService.showToastError('Ops!, Houve uma falha ao atualizar seus dados.');
            }
            // this.exceptionProvider.insert(err, 'perfil.ts', 'update - apiUpdate');
            showLoading.dismiss();
            console.error('api erro', err)
          });
        }
      })
    }else{
      this.messageService.showToastSuccess('Dados atualizados com sucesso!');
      showLoading.dismiss();
    }
  }

}
