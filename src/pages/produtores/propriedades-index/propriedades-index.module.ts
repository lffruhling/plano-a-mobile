import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropriedadesIndexPage } from './propriedades-index';

@NgModule({
  declarations: [
    PropriedadesIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(PropriedadesIndexPage),
  ],
})
export class PropriedadesIndexPageModule {}
