import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PropriedadesProvider} from "../../../providers/produtores/propriedades";
import {ProdutorEditPage} from "../produtor-edit/produtor-edit";
import {PropriedadesEditPage} from "../propriedades-edit/propriedades-edit";

/**
 * Generated class for the PropriedadesIndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-propriedades-index',
  templateUrl: 'propriedades-index.html',
})
export class PropriedadesIndexPage {

  protected termo_busca;
  protected propriedades = [];

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private propriedadesService: PropriedadesProvider
  ) {}

  ionViewWillEnter() {
    if (!(this.termo_busca && this.termo_busca.trim() != '')) {
      this.propriedades = [];
      this.carregaPropriedades();
    }
  }

  carregaPropriedades(){
    let promisse = new Promise((resolve,reject) =>{
      this.propriedadesService.getAll().then((data:any) =>{
        this.propriedades = data;
        resolve();
      }).catch((err) => {
        console.error('falha ao buscar propriedades', err);
        reject();
      })
    });

    return promisse;
  }

  buscarPropriedades(ev: any) {
    // Voltar para todos os itens
    this.carregaPropriedades().then(()=>{

      // pega valor a ser pesquisado
      const val = ev.target.value;

      // se o valor pesquisado for vazio, não filtra
      if (val && val.trim() != '') {
        this.propriedades = this.propriedades.filter((propriedade) => {
          if(propriedade.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1){
            return (propriedade.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }else if(propriedade.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1){
            return (propriedade.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }
        })
      }
    });
  }

  editar(id){
    this.navCtrl.push(PropriedadesEditPage, {id:id});
  }


}
