import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropriedadesEditPage } from './propriedades-edit';

@NgModule({
  /*declarations: [
    PropriedadesEditPage,
  ],*/
  imports: [
    IonicPageModule.forChild(PropriedadesEditPage),
  ],
})
export class PropriedadesEditPageModule {}
