import {Component, OnInit, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup} from "@angular/forms";
import {PropriedadesProvider} from "../../../providers/produtores/propriedades";
import {Propriedade} from "../../../class/Propriedade";
import {Keyboard} from "@ionic-native/keyboard";
import {TipoIntegracaoProvider} from "../../../providers/base/tipoIntegracao";
import {SubtipoIntegracoesProvider} from "../../../providers/base/SubtipoIntegracoes";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {Geolocation} from "@ionic-native/geolocation";
import {DbExceptionProvider} from "../../../providers/db-exception/db-exception";
import {UtilsService} from "../../../services/utils.service";
import {CameraProvider} from "../../../providers/camera/camera";
import {UniqueIdProvider} from "../../../providers/unique-id/unique-id";
import {IntegradoraProvider} from "../../../providers/base/integradora";
import {FirestoreDatabaseProvider} from "../../../providers/firestore-database/firestore-database";
import {GeolocationProvider} from "../../../providers/geolocation/geolocation";

declare var google;

@IonicPage()
@Component({
  selector: 'page-propriedades-edit',
  templateUrl: 'propriedades-edit.html',
  providers: [Keyboard],
})
export class PropriedadesEditPage implements OnInit {
  @ViewChild(Content) content: Content;

  protected map: any;
  protected hideMap = true;
  protected propriedadeForm: FormGroup;
  protected tipo_integracoes: any[] = [];
  protected subtipo_integracoes: any[] = [];
  protected integradoras: any[] = [];
  protected id;
  protected fotos: any[] = [];
  protected existGeo = true;
  protected obsGeolocation;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private propriedadeService: PropriedadesProvider,
    private integradoraService: IntegradoraProvider,
    private tipoIntegracaoService: TipoIntegracaoProvider,
    private subTipoIntegracaoService: SubtipoIntegracoesProvider,
    private keyboard: Keyboard,
    private network: CheckNetworkProvider,
    private messageService: AlertMessagesProvider,
    private geolocation: Geolocation,
    private exceptionProvider: DbExceptionProvider,
    private utils: UtilsService,
    private cameraService: CameraProvider,
    private uniqueKeyService: UniqueIdProvider,
    private fbDbProvider: FirestoreDatabaseProvider,
    private geolocationProvider: GeolocationProvider
  ) {
    this.tipoIntegracaoService.getAllToSelect().then((data: any) => {
      this.tipo_integracoes = data;
    });

    this.integradoraService.getAllToSelect().then((data: any) => {
      this.integradoras = data;
    });

    this.id = this.navParams.get('id');
  }

  ngOnInit() {

    this.propriedadeForm = new FormGroup({
      'id': new FormControl(this.id),
      'id_web': new FormControl(null),
      'produtor_id': new FormControl(null),
      'integradora_id': new FormControl(null),
      'tipo_integracao_id': new FormControl(null),
      'subtipo_integracao_id': new FormControl(null),
      'estado_id': new FormControl(null),
      'municipio_id': new FormControl(null),
      'descricao': new FormControl(''),
      'logradouro': new FormControl(''),
      'complemento': new FormControl(''),
      'ie': new FormControl(''),
      'latitude': new FormControl(''),
      'longitude': new FormControl(''),
      'ativo': new FormControl(null),
      'sincronizado': new FormControl(false),
      'integradora': new FormControl(''),
      'produtor': new FormControl(''),
      'codErp': new FormControl(''),
      'deleted_at': new FormControl(''),
    });

    this.propriedadeService.get(this.id).then((propriedade: Propriedade) => {
      if (propriedade.latitude && propriedade.longitude) {
        this.existGeo = true;
      } else {
        this.existGeo = false;
      }
      this.getSubTiposInteg(propriedade.tipo_integracao_id, true).then(() => {
        this.integradoraService.get(propriedade.integradora_id).then((integradora) => {
          let integracao = this.tipo_integracoes.filter(integracao => {
            return integracao.id === propriedade.tipo_integracao_id;
          });

          let subtipo_integracao = this.subtipo_integracoes.filter(subtipo => {
            return subtipo.id === propriedade.subtipo_integracao_id;
          });

          this.propriedadeForm.controls['id_web'].setValue(propriedade.id_web);
          this.propriedadeForm.controls['produtor_id'].setValue(propriedade.produtor_id);
          this.propriedadeForm.controls['integradora_id'].setValue(integradora);
          this.propriedadeForm.controls['tipo_integracao_id'].setValue(integracao[0]);
          this.propriedadeForm.controls['subtipo_integracao_id'].setValue(subtipo_integracao[0]);
          this.propriedadeForm.controls['estado_id'].setValue(propriedade.estado_id);
          this.propriedadeForm.controls['municipio_id'].setValue(propriedade.municipio_id);
          this.propriedadeForm.controls['descricao'].setValue(propriedade.descricao);
          this.propriedadeForm.controls['logradouro'].setValue(propriedade.logradouro);
          this.propriedadeForm.controls['complemento'].setValue(propriedade.complemento);
          this.propriedadeForm.controls['latitude'].setValue(propriedade.latitude);
          this.propriedadeForm.controls['longitude'].setValue(propriedade.longitude);
          this.propriedadeForm.controls['ativo'].setValue(propriedade.ativo);
          this.propriedadeForm.controls['integradora'].setValue(propriedade.integradora);
          this.propriedadeForm.controls['produtor'].setValue(propriedade.produtor);
          this.propriedadeForm.controls['ativo'].setValue(propriedade.ativo);
			this.propriedadeForm.controls['codErp'].setValue(propriedade.codErp);
          this.propriedadeForm.controls['deleted_at'].setValue(propriedade.deleted_at);

          if (propriedade.ie == 'null') {
            this.propriedadeForm.controls['ie'].setValue(null);
          } else {
            this.propriedadeForm.controls['ie'].setValue(propriedade.ie);
          }
        });


      });

    });

    this.propriedadeService.getFotos(this.id).then((fotos: any[]) => {
      this.fotos = fotos;
    }).catch((err) => {
      console.error('falha ao carregar fotos', err);
    });
  }

  store() {
    let loading = this.messageService.showLoading('Salvando alterações...');

    this.propriedadeForm.controls['subtipo_integracao_id'].setValue(this.propriedadeForm.value.subtipo_integracao_id.id);
    this.propriedadeForm.controls['tipo_integracao_id'].setValue(this.propriedadeForm.value.tipo_integracao_id.id);
    this.propriedadeForm.controls['integradora_id'].setValue(this.propriedadeForm.value.integradora_id.id);
	this.propriedadeForm.controls['codErp'].setValue(parseInt(this.propriedadeForm.value.codErp));

    this.propriedadeService.atualizaPropriedadeApi(this.propriedadeForm.value).then((res) => {
      loading.dismiss();
      this.messageService.showToastSuccess('Propriedade atualizada com sucesso!');
      this.navCtrl.pop();
    }).catch((err) => {
      this.messageService.showToastError('Falha ao atualizar propriedade');
      console.error('erro ao atualizar propriedade', err);
      loading.dismiss();
    });
  }

  getSubTiposInteg(event, isInternal = false) {
    let tipo_integracao_id;
    if (isInternal) {
      tipo_integracao_id = event;
    } else {
      tipo_integracao_id = event.value.id;
    }

    this.subtipo_integracoes = [];
    this.propriedadeForm.controls['subtipo_integracao_id'].setValue(null);
    let promisse = new Promise((resolve, reject) => {
      this.subTipoIntegracaoService.getAllToSelect(tipo_integracao_id).then((data: any) => {
        this.subtipo_integracoes = data;
        resolve();
      }).catch((err) => {
        console.error('falha ao carregar subtipos', err);
        reject();
      });
    });

    return promisse;
  }

  getGeolocation() {
    let loading = this.messageService.showLoading('Buscando localização...');

    let options = {
      enableHighAccuracy: true,
      timeout: 30000,
      maximumAge: 0
    };
    this.geolocation.getCurrentPosition(options).then((resp: any) => {
      if(resp != null){
        this.propriedadeForm.controls['latitude'].setValue(resp.coords.latitude);
        this.propriedadeForm.controls['longitude'].setValue(resp.coords.longitude);
        this.existGeo = true;

        if (this.network.isConnected()) {

          const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);

          const mapOptions = {
            zoom: 18,
            center: position
          };

          this.map = new google.maps.Map(document.getElementById('map'), mapOptions);

          new google.maps.Marker({
            position: position,
            map: this.map
          });

          this.hideMap = false;

          let dimensions = this.content.getContentDimensions();
          this.content.scrollTo(0, dimensions.contentHeight + 100, 800);
        } else {
          this.messageService.showToast(`Geolocalização coletada com sucesso! (${resp.coords.latitude}) (${resp.coords.longitude})`, 6000, 'middle');
        }
        loading.dismiss();
      }

    }).catch(error => {
      this.messageService.showToastError('Falha ao recuperar a localização!');
      loading.dismiss();
      console.error('Erro ao recuperar sua posição', error);
    });
  }

  newFoto() {
    let mobile_key = this.uniqueKeyService.generateKey();
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.propriedadeService.insertFoto({
            propriedade_id: this.id,
            mobile_key: mobile_key,
            img: foto
          }).then((data) => {
            this.fotos.push({
              id: data.insertId,
              propriedade_id: this.id,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err => {
            console.error('falha salvar foto propriedade - tela', err);
          });


        }).catch(err => {
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  marcaPrincipal(foto) {
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Marcar Foto Principal?', 'Deseja marcar esta foto como principal?', 'Sim, Marcar', 'Cancelar').then(result => {
      if (result) {
        this.propriedadeService.updateFotoPrincipal(foto_id, foto.propriedade_id).then(() => {
          this.fotos.forEach((_foto, index) => {
            this.fotos[index].principal = false;
            if (this.fotos[index].id == foto_id) {
              this.fotos[index].principal = true;
            }
          });
        });
      }
    });
  }

  removeFoto(foto) {
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result => {
      if (result) {
        this.propriedadeService.deleteFoto(foto_id).then(() => {
          this.fotos = this.fotos.filter((_foto) => {
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

  public removerPropriedade() {
    this.messageService.showConfirmationAlert('Apagar', `Deseja realmente apagar a propriedade?`, 'Sim, Apagar!', 'Não').then((res) => {
      if (res) {
        let loading = this.messageService.showLoading(`Apagando Propriedade`);
        this.propriedadeService.delete(this.id).then(() => {
          loading.dismiss();
          this.navCtrl.pop();
        }).catch(err => {
          this.messageService.showToastError('Falha ao remover galpão!');
          loading.dismiss();
        })
      }
    });
  }

}
