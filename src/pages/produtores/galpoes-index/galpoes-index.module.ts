import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GalpoesIndexPage } from './galpoes-index';

@NgModule({
  declarations: [
    GalpoesIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(GalpoesIndexPage),
  ],
})
export class GalpoesIndexPageModule {}
