import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GalpoesProvider} from "../../../providers/produtores/galpoes";
import {GalpoesEditPage} from "../galpoes-edit/galpoes-edit";

@IonicPage()
@Component({
  selector: 'page-galpoes-index',
  templateUrl: 'galpoes-index.html',
})
export class GalpoesIndexPage {
  protected galpoes = [];
  protected termo_busca;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private galpaoService: GalpoesProvider
  ) {}

  ionViewWillEnter() {
    if (!(this.termo_busca && this.termo_busca.trim() != '')) {
      this.galpoes = [];
      this.carregaGalpoes();
    }
  }

  carregaGalpoes(){
    let promisse = new Promise((resolve, reject) =>{
      this.galpaoService.getAll().then((data:any) =>{
        this.galpoes = data;
        resolve();
      }).catch((err)=>{
        console.error('falha ao buscar galpoes', err);
        reject();
      })
    });

    return promisse;
  }

  buscarGalpoes(ev: any){
    this.carregaGalpoes().then(()=>{
      const val = ev.target.value;

      if(val && val.trim() != ''){
        this.galpoes = this.galpoes.filter((galpao)=>{
          /*Busca pelo nome do galpão/produtor/propriedade*/
          if(galpao.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1){
            return (galpao.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }else if(galpao.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1){
            return (galpao.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }else if(galpao.propriedade.toLowerCase().indexOf(val.toLowerCase()) > -1){
            return (galpao.propriedade.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }
        });
      }
    })
  }

  editar(id){
    this.navCtrl.push(GalpoesEditPage, {id:id});
  }

}
