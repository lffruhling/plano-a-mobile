import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GalpoesEditPage } from './galpoes-edit';

@NgModule({
  /*declarations: [
    GalpoesEditPage,
  ],*/
  imports: [
    IonicPageModule.forChild(GalpoesEditPage),
  ],
})
export class GalpoesEditPageModule {}
