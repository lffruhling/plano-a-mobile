import {Component, OnInit, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Keyboard} from "@ionic-native/keyboard";
import {FormControl, FormGroup} from "@angular/forms";
import {GalpoesProvider} from "../../../providers/produtores/galpoes";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {Geolocation} from "@ionic-native/geolocation";
import {Galpao} from "../../../class/Galpao";
import {ComedourosProvider} from "../../../providers/comedouros/comedouros";
import {IonicSelectableComponent} from "ionic-selectable";
import {EmpresaProvider} from "../../../providers/empresa/empresa";
import {DbExceptionProvider} from "../../../providers/db-exception/db-exception";
import {UtilsService} from "../../../services/utils.service";
import {CameraProvider} from "../../../providers/camera/camera";
import {UniqueIdProvider} from "../../../providers/unique-id/unique-id";

@IonicPage()
@Component({
  selector: 'page-galpoes-edit',
  templateUrl: 'galpoes-edit.html',
  providers: [Keyboard],
})
export class GalpoesEditPage implements OnInit {
  @ViewChild(Content) content: Content;
  @ViewChild('tipoComedouroSelect') tipoComedouroSelect: IonicSelectableComponent;

  protected map: any;
  protected hideMap = true;
  protected galpaoForm: FormGroup;
  protected tipo_comedouros: any[] = [];
  protected tipo_galpoes: any[] = [];
  protected integracao_aves:any = false;
  protected id;
  protected fotos: any[] = [];

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private keyboard: Keyboard,
    private network: CheckNetworkProvider,
    private diagnostic: Diagnostic,
    private locationAccurace: LocationAccuracy,
    private messageService: AlertMessagesProvider,
    private geolocation: Geolocation,
    private galpaoService: GalpoesProvider,
    private comedouroService: ComedourosProvider,
    private empresaProvider: EmpresaProvider,
	  private exceptionProvider:DbExceptionProvider,
    private utils: UtilsService,
    private cameraService: CameraProvider,
    private uniqueKeyService: UniqueIdProvider,
    ) {
    this.comedouroService.getAllToSelect().then((data: any) => {
      this.tipo_comedouros = data;
    });

    this.galpaoService.getAllTiposGalpoesToSelect().then((data: any) => {
      this.tipo_galpoes = data;
    });

    this.empresaProvider.getIntegracaoAvesChk().then((data)=>{
      this.integracao_aves = data;
    });

    this.id = this.navParams.get('id');

  }

  ngOnInit() {
    this.galpaoForm = new FormGroup({
      'id': new FormControl(this.id),
      'produtor_id': new FormControl(null),
      'propriedade_id': new FormControl(null),
      'tipo_comedouro_id': new FormControl(null),
      'tipo_galpao_id': new FormControl(null),
      'descricao': new FormControl(null),
      'lotacao': new FormControl(null),
      'baias': new FormControl(null),
      'altura': new FormControl(null),
      'largura': new FormControl(null),
      'comprimento': new FormControl(null),
      'metros_quadrados': new FormControl(null),
      'observacao': new FormControl(null),
      'latitude': new FormControl(null),
      'longitude': new FormControl(null),
      'ativo': new FormControl(null),
      'sincronizado': new FormControl(false),
      'produtor': new FormControl(null),
      'propriedade': new FormControl(null),
      'codErp': new FormControl(null),
    });

    this.galpaoService.get(this.id).then((galpao: Galpao) => {
      let tipo_comedouro = this.tipo_comedouros.filter(tipo => {
        return tipo.id === galpao.tipo_comedouro_id;
      });

      let tipo_galpao = this.tipo_galpoes.filter(tipo => {
        return tipo.id === galpao.tipo_galpao_id;
      });

      this.galpaoForm.controls['produtor_id'].setValue(galpao.produtor_id);
      this.galpaoForm.controls['propriedade_id'].setValue(galpao.propriedade_id);
      this.galpaoForm.controls['tipo_comedouro_id'].setValue(tipo_comedouro[0]);
      this.galpaoForm.controls['tipo_galpao_id'].setValue(tipo_galpao[0]);
      this.galpaoForm.controls['descricao'].setValue(galpao.descricao);
      this.galpaoForm.controls['lotacao'].setValue(galpao.lotacao);
      this.galpaoForm.controls['baias'].setValue(galpao.baias);
      this.galpaoForm.controls['altura'].setValue(galpao.altura);
      this.galpaoForm.controls['largura'].setValue(galpao.largura);
      this.galpaoForm.controls['comprimento'].setValue(galpao.comprimento);
      this.galpaoForm.controls['metros_quadrados'].setValue(galpao.metros_quadrados);
      this.galpaoForm.controls['observacao'].setValue(galpao.observacao);
      this.galpaoForm.controls['ativo'].setValue(galpao.ativo);
      this.galpaoForm.controls['produtor'].setValue(galpao.produtor);
      this.galpaoForm.controls['propriedade'].setValue(galpao.propriedade);
      this.galpaoForm.controls['codErp'].setValue(galpao.codErp);
    });

    this.galpaoService.getFotosGalpao(this.id).then((fotos: any[]) => {
      this.fotos = fotos;
    }).catch((err) => {
      console.error('falha ao carregar fotos', err);
    })

  }

  store() {
    let loading = this.messageService.showLoading('Salvando alterações...');

    if(this.galpaoForm.value.tipo_comedouro_id){
      this.galpaoForm.controls['tipo_comedouro_id'].setValue(this.galpaoForm.value.tipo_comedouro_id.id);
    }
    if(this.galpaoForm.value.tipo_galpao_id){
      this.galpaoForm.controls['tipo_galpao_id'].setValue(this.galpaoForm.value.tipo_galpao_id.id);
    }

  this.galpaoForm.controls['codErp'].setValue(parseInt(this.galpaoForm.value.codErp));

    this.galpaoService.atualizaGalpaoApi(this.galpaoForm.value).then(() => {
      loading.dismiss();
      this.messageService.showToastSuccess('Galpão atualizado com sucesso!');
      this.navCtrl.pop();
    }).catch((err) => {
      if(err.name == "TimeoutError"){
        this.messageService.showAlert('Opss!', 'Sua conexão parece estar lenta, as alterações foram salvas, mas não sincronizadas!').then(()=>{
          this.messageService.showToastSuccess('Galpão atualizado com sucesso!');
          this.navCtrl.pop();
        });
      }else{
        this.messageService.showToastError('Falha ao atualizar galpão');
      }
      console.error('falha ao atualiza galpão', err, JSON.stringify(this.galpaoForm.value));
      loading.dismiss();
    })
  }

  calculaMt2() {
    let largura = parseFloat(this.galpaoForm.value.largura);
    let comprimento = parseFloat(this.galpaoForm.value.comprimento);
    let m2 = parseFloat(this.galpaoForm.value.metros_quadrados);

    if (largura > 0 && comprimento > 0) {
      m2 = largura * comprimento;
    }

    this.galpaoForm.controls['metros_quadrados'].setValue(m2.toFixed(2));

  }

  newFoto() {
    let mobile_key = this.uniqueKeyService.generateKey();
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.galpaoService.insertFoto({
            galpao_id: this.id,
            mobile_key: mobile_key,
            img: foto
          }).then((data)=>{
            this.fotos.push({
              id: data.insertId,
              galpao_id: this.id,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err=>{
            console.error('falha salvar foto acao - tela', err);
          });


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  marcaPrincipal(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Marcar Foto Principal?', 'Deseja marcar esta foto como principal?', 'Sim, Marcar', 'Cancelar').then(result=>{
      if(result){
        this.galpaoService.updateFotoPrincipal(foto_id, foto.galpao_id).then(()=>{
          this.fotos.forEach((_foto, index)=>{
            this.fotos[index].principal = false;
            if(this.fotos[index].id == foto_id){
              this.fotos[index].principal = true;
            }
          });
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        this.galpaoService.deleteFoto(foto_id).then(()=>{
          this.fotos = this.fotos.filter((_foto)=>{
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

  public removerGalpao(){
    this.messageService.showConfirmationAlert('Apagar', `Deseja realmente apagar o galpão?`, 'Sim, Apagar!', 'Não').then((res)=> {
      if (res) {
        let loading = this.messageService.showLoading(`Apagando Galpão`);
        this.galpaoService.delete(this.id).then(() => {
          loading.dismiss();
          this.navCtrl.pop();
        }).catch(err => {
          this.messageService.showToastError('Falha ao remover galpão!');
          loading.dismiss();
        })
      }
    });
  }

}
