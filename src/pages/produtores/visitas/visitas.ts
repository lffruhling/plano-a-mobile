import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {VisitasProvider} from "../../../providers/visitas/visitas";
import {VisitaLotePage} from "../../visita-lote/visita-lote";

/**
 * Generated class for the VisitasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visitas',
  templateUrl: 'visitas.html',
})
export class VisitasPage {
  searchQuery: string = '';
  protected visitas = [];
  protected termo_busca;

  constructor(private navCtrl: NavController, private navParams: NavParams, private visitasProvider:VisitasProvider) {
  }

  ionViewWillEnter() {
    if (!(this.termo_busca && this.termo_busca.trim() != '')) {
      this.visitas = [];
      this.carregaTodasVisitas();
    }
  }

  carregaTodasVisitas(){
    let promisse = new Promise((resolve, reject) =>{
      this.visitasProvider.getVisitas().then((data:any) =>{
        console.log('visitas', data)
        this.visitas = data;
        resolve();
      }).catch((err) =>{
        console.error('falha ao buscar visitas', err);
        reject();
      })
    });

    return promisse;
  }

  buscarProdutoresVisita(ev: any) {
    // Voltar para todos os itens
    this.carregaTodasVisitas().then(()=>{

      // pega valor a ser pesquisado
      const val = ev.target.value;

      // se o valor pesquisado for vazio, não filtra
      if (val && val.trim() != '') {
        this.visitas = this.visitas.filter((visita) => {
          return (visita.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    });
  }

  editar(visita){
    this.navCtrl.push(VisitaLotePage, {
      visita_lancamento_id: visita.id,
      lote_id: visita.lote_id,
      propriedade_id: visita.propriedade_id,
      galpao_id: visita.galpao_id,
      checklist_id: visita.checklist_id,
      produtor: visita.produtor,
      galpao: visita.galpao,
      aplica_chk_gp: visita.aplica_chk_gp,
      lote: visita.lote,
    });


  }

}
