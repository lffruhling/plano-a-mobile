import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecontagemRelatoriosPage } from './recontagem-relatorios';

@NgModule({
  declarations: [
    RecontagemRelatoriosPage,
  ],
  imports: [
    IonicPageModule.forChild(RecontagemRelatoriosPage),
  ],
})
export class RecontagemRelatoriosPageModule {}
