import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {RecontagemPlantelProvider} from "../../../../providers/recontagem-plantel/recontagem-plantel";
import {ProdutorEditPage} from "../../produtor-edit/produtor-edit";
import {RecontagemShowPage} from "../recontagem-show/recontagem-show";

/**
 * Generated class for the RecontagemRelatoriosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recontagem-relatorios',
  templateUrl: 'recontagem-relatorios.html',
})
export class RecontagemRelatoriosPage {

  searchQuery: string = '';
  protected recontagens = [];
  protected termo_busca;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private recontagemPlatelProvider: RecontagemPlantelProvider
  ) {
  }

  ionViewWillEnter() {
    if (!(this.termo_busca && this.termo_busca.trim() != '')) {
      this.recontagens = [];
      this.carregaTodasRecontagens();
    }
  }

  carregaTodasRecontagens() {
    let promisse = new Promise((resolve, reject) => {
      this.recontagemPlatelProvider.getAll().then((data: any) => {
        this.recontagens = data;
        resolve();
      }).catch((err) => {
        console.error('falha ao buscar recontagens', err);
        reject();
      })
    });

    return promisse;
  }

  buscarRecontagens(ev: any) {
    // Voltar para todos os itens
    this.carregaTodasRecontagens().then(()=>{

      // pega valor a ser pesquisado
      const val = ev.target.value;

      // se o valor pesquisado for vazio, não filtra
      if (val && val.trim() != '') {
        this.recontagens = this.recontagens.filter((recontagem) => {
          return (recontagem.produtor.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    });
  }

  show(id){
    this.navCtrl.push('RecontagemShowPage', {id:id});
  }

}
