import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantelContagemPage } from './plantel-contagem';

@NgModule({
  // declarations: [
  //   PlantelContagemPage,
  // ],
  imports: [
    IonicPageModule.forChild(PlantelContagemPage),
  ],
})
export class PlantelContagemPageModule {}
