import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {Keyboard} from "@ionic-native/keyboard";
import {FormControl, FormGroup} from "@angular/forms";
import {RecontagemPlantelProvider} from "../../../../providers/recontagem-plantel/recontagem-plantel";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import {ModalSignPage} from "../../../modal-sign/modal-sign";
import {UniqueIdProvider} from "../../../../providers/unique-id/unique-id";
import * as moment from 'moment'
import {UtilsService} from "../../../../services/utils.service";
import {AuthProvider} from "../../../../providers/auth/auth";
import {CheckNetworkProvider} from "../../../../providers/check-network/check-network";
import {GeolocationProvider} from "../../../../providers/geolocation/geolocation";
import {EmpresaProvider} from "../../../../providers/empresa/empresa";
import {FirestoreDatabaseProvider} from "../../../../providers/firestore-database/firestore-database";


@IonicPage()
@Component({
  selector: 'page-plantel-contagem',
  templateUrl: 'plantel-contagem.html',
  providers: [Keyboard],
})
export class PlantelContagemPage implements OnInit {

  protected plantelForm: FormGroup;
  protected recontagem_id;
  protected data_prevista;
  protected celas_uteis;
  protected propriedade_id;
  protected galpao_id;
  protected intervalo_id;
  protected produtor;
  protected galpao;
  protected qtd_contrato_femeas;
  protected qtd_contrato_leitoes;
  protected qtd_lao_vigente;
  protected celas_maternidades;
  protected box_gestacao;
  protected gestacao_coletiva;
  protected retornoModal = false;
  protected now = moment().format('YYYY-MM-DD');
  protected obsGeolocation;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private keyboard: Keyboard,
              private recontagemPlantelProvider: RecontagemPlantelProvider,
              private messageService: AlertMessagesProvider,
              private plt: Platform,
              private modalCtrl: ModalController,
              private uniqueKeyService: UniqueIdProvider,
              private viewCtrl: ViewController,
              private utils: UtilsService,
              private authProvider: AuthProvider,
              private networkService: CheckNetworkProvider,
              private geolocationProvider: GeolocationProvider,
			  private empresaProvider: EmpresaProvider,
			  private fbDbProvider: FirestoreDatabaseProvider,
  ) {
    this.recontagem_id = this.navParams.get('recontagem_id');
    this.celas_uteis = this.navParams.get('celas_uteis');
    this.data_prevista = this.navParams.get('data_prevista');
    this.propriedade_id = this.navParams.get('propriedade_id');
    this.galpao_id = this.navParams.get('galpao_id');
    this.intervalo_id = this.navParams.get('intervalo_id');
    this.produtor = this.navParams.get('produtor');
    this.galpao = this.navParams.get('galpao');
    this.qtd_contrato_femeas = this.navParams.get('qtd_contrato_femeas');
    this.qtd_contrato_leitoes = this.navParams.get('qtd_contrato_leitoes');
    this.qtd_lao_vigente = this.navParams.get('qtd_lao_vigente');
    this.celas_maternidades = this.navParams.get('celas_maternidades');
    this.box_gestacao = this.navParams.get('box_gestacao');
    this.gestacao_coletiva = this.navParams.get('gestacao_coletiva');

  }

  ngOnInit() {
    this.plantelForm = new FormGroup({
      'id': new FormControl(this.recontagem_id),
      'intervalo_id': new FormControl(this.intervalo_id),
      'galpao_id': new FormControl(this.galpao_id),
      'data_prevista': new FormControl(this.data_prevista),
      'produtor': new FormControl(this.produtor),
      'galpao': new FormControl(this.galpao),
      'data_recontagem': new FormControl(this.now),
      'leitoas_nc': new FormControl(null),
      'leitoas_c': new FormControl(null),
      'matrizes_desc': new FormControl(null),
      'machos_desc': new FormControl(null),
      'machos_inteiros': new FormControl(null),
      'machos_vasec': new FormControl(null),
      'saldo_cidasc_femeas': new FormControl(null),
      'saldo_cidasc_machos': new FormControl(null),
      'qtd_ags_lnc': new FormControl(null),
      'qtd_ags_lc': new FormControl(null),
      'qtd_ags_mat_desc': new FormControl(null),
      'qtd_ags_mac_desc': new FormControl(null),
      'qtd_ags_mac_int': new FormControl(null),
      'qtd_ags_mac_vas': new FormControl(null),
      'qtd_contrato_femeas': new FormControl(this.qtd_contrato_femeas),
      'qtd_contrato_leitoes': new FormControl(this.qtd_contrato_leitoes),
      'qtd_lao_vigente': new FormControl(this.qtd_lao_vigente),
      'plantel_produtivo': new FormControl(null),
      'celas_maternidades': new FormControl(this.celas_maternidades),
      'box_gestacao': new FormControl(this.box_gestacao),
      'gestacao_coletiva': new FormControl(this.gestacao_coletiva),
      'assinatura_produtor': new FormControl(null),
      'latitude': new FormControl(null),
      'longitude': new FormControl(null),
      'observacoes': new FormControl(null),
    });

    for (let cela of this.celas_uteis){
      this.plantelForm.addControl(cela.cela_util,  new FormControl(null));
    }

    this.obsGeolocation = this.geolocationProvider.getGeolocation().then((geolocation:any) => {
      if(geolocation){
        this.plantelForm.controls['latitude'].setValue(geolocation.coords.latitude);
        this.plantelForm.controls['longitude'].setValue(geolocation.coords.longitude);
      }
    });

  }

  store(){
    let loading = this.messageService.showLoading('Salvando Recontagem...');
    this.retornoModal = true;

    if (!this.plt.is('ios')) {

      let modal = this.modalCtrl.create(ModalSignPage);
      modal.onDidDismiss((data) => {
      	if (data.retornoModal) {
      		this.plantelForm.controls['assinatura_produtor'].setValue(data.assinatura);
      		this.salvaRecontagem(loading);
      	}else{
      		this.empresaProvider.getParamChkObrigaCapturaAssinatura().then(result => {
      			if (result) {
      				this.messageService.showToastError('Capture a Assinatura do Produtor para finalizar a recontagem!');
      				modal.present();
      				return false;
      			} else {
      				this.salvaRecontagem(loading);
      			}
      		});
		  }
      });
      modal.present();
    }else{
		this.salvaRecontagem(loading);
	}
  }

  private salvaRecontagem(loading){
	  this.recontagemPlantelProvider.update(this.plantelForm.value, false).then((_) => {
		  loading.dismiss();
		  this.messageService.showToastSuccess('Recontagem salva com sucesso!');
		  this.dismiss();
		  for (let cela of this.celas_uteis) {
			  let data = {
				  'recontagem_plantel_id': this.plantelForm.value.id,
				  'celas_uteis_id': cela.id,
				  'quantidade': this.plantelForm.controls[cela.cela_util].value,
				  'mobile_key': this.uniqueKeyService.generateKey(),
			  };
			  this.recontagemPlantelProvider.insertTotalCela(data)
		  }
		  if (this.plantelForm.value.latitude) {
			  this.recontagemPlantelProvider.updateGeolocation(this.plantelForm.value.latitude, this.plantelForm.value.longitude, this.recontagem_id).then((_) => {
				  this.sincronizaDados();
			  });
		  } else {
			  this.sincronizaDados();
		  }

	  });
	  this.fbDbProvider.addRecontagem(this.plantelForm.value);
  }

  dismiss() {
    this.viewCtrl.dismiss(this.retornoModal);
  }

  private sincronizaDados(){
    if (this.networkService.isConnected()) {
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.utils.retornaUUID().then((uuid: any) => {
            let dados = this.plantelForm.value;

            this.recontagemPlantelProvider.getAllToSyncCelasUteis(this.plantelForm.value.id).then((celasUteis : any[])=>{
              dados._method = "patch";
              dados.celas_uteis = [];

              for(let cela of celasUteis){
                  cela.device_uuid = uuid;
                  cela.id_mobile = cela.id;
                  dados.celas_uteis.push(cela);
              }

              this.recontagemPlantelProvider.apiStore(dados).subscribe((result:any) =>{

                /*Funcoes de update*/
                this.recontagemPlantelProvider.updateSinc(result.recontagem.id).then(() => {
                  for (let cela of result.celas_uteis){
                    this.recontagemPlantelProvider.updateSyncTotalCelasUteis(cela.id_mobile, cela.id)
                  }
                });

              },error => {
                console.error('error ao enviar dados da recontagem', error)
              })
            });
          })
        }
      })
    }
  }

}
