import {Component, OnInit, style} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {SocialSharing} from "@ionic-native/social-sharing";
import {RecontagemPlantelProvider} from "../../../../providers/recontagem-plantel/recontagem-plantel";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import {EmpresaProvider} from "../../../../providers/empresa/empresa";
import {TecnicoProvider} from "../../../../providers/tecnico/tecnico";
import {File} from "@ionic-native/file";
import {FileOpener} from "@ionic-native/file-opener";

import domtoimage from 'dom-to-image';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from "moment";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-recontagem-show',
  templateUrl: 'recontagem-show.html',
})
export class RecontagemShowPage implements OnInit {
  private dados;
  private celas_uteis: Array<any> = [];
  private tecnico;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private socialSharing: SocialSharing,
    private recontagemPlantelProvider: RecontagemPlantelProvider,
    private messageService: AlertMessagesProvider,
    private empresaProvider: EmpresaProvider,
    private tecnicoProvider: TecnicoProvider,
    private file: File,
    private platform: Platform,
    private fileOpener: FileOpener,
  ) {
    this.tecnicoProvider.get().then(tecnico => {
      this.tecnico = tecnico;
    })
  }

  ngOnInit() {
    let id = this.navParams.get('id');

    this.recontagemPlantelProvider.find(id).then((recontagem) => {
      this.dados = recontagem;
    });
    this.recontagemPlantelProvider.getCelasUteisRecontagem(id).then((celas_uteis: Array<any>) => {
      this.celas_uteis = celas_uteis;
    });

  }

  compartilhar() {
    let loading = this.messageService.showLoading('Gerando relatório...');
    let message = `Relatório da Recontagem de Plantel ${this.dados.id}`;

    domtoimage.toPng(document.getElementById('print-to-share'), {quality: 0.95})
      .then((dataUrl) => {
        this.socialSharing.shareViaWhatsApp(message, dataUrl, null).catch(err => {
          console.error('erro ao compartilhar', err);
        });
        loading.dismiss();
      });
  }

  compartilharPdf() {
    let loading = this.messageService.showLoading('Gerando relatório em PDF ...');
    /*Declaro aqui porque o componente simplesmente não aceita passa o valor sem definilo como any*/
    let macho:any = {text:'Machos', style:'text'};
	let assinatura_chk = null;

	  if(this.dados.assinatura_produtor != null){
		  assinatura_chk = this.dados.assinatura_produtor;
	  }

	  let col_sign:any = {
		  text: '----------------------------------------------------------------',
		  bold: true
	  };

	  if(assinatura_chk != null){
		  col_sign = {
			  image: assinatura_chk,
			  fit: [200, 300]
		  };
	  }

	  /*Monta array da tabela*/
    let body_report = [
      [{text: 'Saldo Real na Granja', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Saldo Agriness', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}],
      [{text:'Leitoas não cobertas - L/N/C', style:'text'}, {text:this.dados.leitoas_nc, style:'value'}, {text:'Leitoas não cobertas - L/N/C', style:'text'}, {text:this.dados.qtd_ags_lnc, style:'value'}],
      [{text:'Leitoas não cobertas - L/N/C + Matrizes Produtivas - M/P', style:'text'}, {text:this.dados.leitoas_c, style:'value'}, {text:'Leitoas não cobertas - L/N/C + Matrizes Produtivas - M/P', style:'text'}, {text:this.dados.qtd_ags_lc, style:'value'}],
      [{text: 'Matrizes e Machos Descartes', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Matrizes e Machos Descartes', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}],
      [{text:'Fêmeas', style:'text'}, {text:this.dados.matrizes_desc, style:'value'}, {text:'Fêmeas', style:'text'}, {text:this.dados.qtd_ags_mat_desc, style:'value'}],

      [{text:'Machos', style:'text'}, {text:this.dados.machos_desc, style:'value'}, macho, {text:this.dados.qtd_ags_mac_desc, style:'value'}],

      [{text: 'Celas Úteis', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Saldo CIDASC', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}],
    ];

    try {
      this.celas_uteis.forEach((cela, index) => {
        /*
        * Faz uma copia do this.respostasPdf toda a vez que passa pelo loop da avaliação para não filtrar um array ja
        * filtrado e ocorrer o erro de não encontrar as respostas dentro do array
        * */
        switch (index) {
          case 0: body_report.push([{text: cela.descricao, style: 'text'}, {text:cela.quantidade, style:'value'},{text:'Fêmeas', style:'text'},{text:this.dados.saldo_cidasc_femeas, style:'value'}]); break;
          case 0: body_report.push([{text: cela.descricao, style: 'text'}, {text:cela.quantidade, style:'value'},{text:'Machos', style:'text'},{text:this.dados.saldo_cidasc_machos, style:'value'}]); break;
          case 1: body_report.push([{text: cela.descricao, style: 'text'}, {text:cela.quantidade, style:'value'},{text: 'Quantidade Contrato', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}]); break;
          case 2: body_report.push([{text: cela.descricao, style: 'text'}, {text:cela.quantidade, style:'value'},{text:'Plantel Fêmeas', style:'text'}, {text:this.dados.qtd_contrato_femeas, style:'value'}]); break;
          case 3: body_report.push([{text: cela.descricao, style: 'text'}, {text:cela.quantidade, style:'value'},{text:'Cota de Leitões', style:'text'}, {text:this.dados.qtd_contrato_leitoes, style:'value'}]); break;

        }


      });

      body_report.push([null, null,{text: 'Quantidade LAO Vigente', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}]);
      body_report.push([null, null,{text:'Quantidade Licença de Operação (LAO) Vigente', style:'text'}, {text:this.dados.qtd_lao_vigente, style:'value'}]);

      body_report.push([null, null,{text: 'Informações Adicionais', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}]);
      body_report.push([null, null,{text:'Plantel Produtivo', style:'text'}, {text:this.dados.plantel_produtivo, style:'value'}]);
      body_report.push([null, null,{text:'Celas de Maternidades', style:'text'}, {text:this.dados.celas_maternidades, style:'value'}]);
      body_report.push([null, null,{text:'Box de Gestação', style:'text'}, {text:this.dados.box_gestacao, style:'value'}]);
      body_report.push([null, null,{text:'Gestação Coletiva', style:'text'}, {text:this.dados.gestacao_coletiva, style:'value'}]);
      body_report.push([null, null,{text:'Plantel Total', style:'text'}, {text:(this.dados.celas_maternidades + this.dados.box_gestacao + this.dados.gestacao_coletiva), style:'value'}]);

      this.empresaProvider.get().then((empresa: any) => {
          var docDefinition = {
            content: [
              {
                style: 'table_header',
                table: {
                  widths: ['*', 'auto'],
                  headerRows: 2,
                  body: [
                    [
                      {
                        alignment: 'center',
                        text: `${(empresa.razao_social || ' *N/C ').toUpperCase()}\n CNPJ: ${empresa.cnpj}\n ${(this.tecnico.email || ' *N/C ').toUpperCase()}`
                      },
                      [
                        {
                          style: 'table_header_right',
                          widths: ['*', '*'],
                          table: {
                            body: [
                              [
                                {
                                  border: [false, false, false, false],
                                  bold: true,
                                  text: 'DATA RECONTAGEM'
                                },
                                {
                                  border: [false, false, false, false],
                                  alignment: 'right',
                                  text: moment(this.dados.data_recontagem, "YYYY-MM-DD").format('DD/MM/YYYY'),
                                }
                              ],
                              [
                                {
                                  border: [false, true, false, true],
                                  bold: true,
                                  text: 'TÉCNICO'
                                },
                                {
                                  border: [false, true, false, true],
                                  alignment: 'right',
                                  text: (this.tecnico.name || ' *N/C ').toUpperCase()
                                }
                              ],
                            ]
                          },
                          layout: {
                            fillColor: function (rowIndex, node, columnIndex) {
                              return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                            }
                          }
                        }
                      ],
                    ],
                    [
                      {
                        alignment: 'center',
                        bold: true,
                        text: `PRODUTOR: ${(this.dados.produtor || ' *N/C ').toUpperCase()} - ${(this.dados.propriedade || ' *N/C ').toUpperCase()} - ${(this.dados.galpao || ' *N/C ').toUpperCase()}`,
                        colSpan: 2
                      },
                      {}
                    ],
                  ]
                }
              },
              [
                {
                  alignment: 'center',
                  margin: [0, 5, 0, 5],
                  text: `RESULTADO DA CONTAGEM DE PLANTEL CHECKLIST`,
                },
                {
                  style: 'tableExample',
                  color: '#444',
                  table: {
                    widths: ['auto', 50, 'auto', 50],
                    headerRows: 2,
                    // keepWithHeaderRows: 1,
                    body: body_report
                  }
                },
              ],
              {
                alignment: 'center',
                text: '----------------------------------------------------------------------------------------------------------------------------------------------------------'
              },
				{
                text: 'Observações'
              },
				{
                text: this.dados.observacoes
              },
				{
                alignment: 'center',
                text: '----------------------------------------------------------------------------------------------------------------------------------------------------------'
              },
              {
                style: 'assinatura',
                alignment: 'center',
                columns: [
					col_sign,
					{
						text: '----------------------------------------------------------------',
						bold: true
					}
                ]
              }, {
                alignment: 'center',
                columns: [
                  {
                    text: (this.dados.produtor || ' *N/C ').toUpperCase(),
                    bold: true
                  },
                  {
                    text: (this.tecnico.name || ' *N/C ').toUpperCase(),
                    bold: true
                  }
                ]
              },
            ],
            styles: {
              tableHeader: {
                bold: true,
              },
              text:{
                bold: true,
                alignment:'left'
              },
              value:{
                alignment:'right'
              },
              assinatura: {
                margin: [0, 10, 0, 5]
              }
            },
            defaultStyle: {
              // alignment: 'justify'
            }
          };

          // const pdfDocGenerator = pdfMake.createPdf(docDefinition).open({}, window);
          let fileName = `relatorio_recontagem_plantel_${Date.now()}.pdf`;
          pdfMake.createPdf(docDefinition).getBuffer((buffer) => {
            var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
            let binaryArray = utf8.buffer; // Convert to Binary...

            let dirPath = "";
            if (this.platform.is('android')) {
              // dirPath = this.file.externalRootDirectory;
              dirPath = this.file.dataDirectory;
            } else if (this.platform.is('ios')) {
              dirPath = this.file.documentsDirectory;
            }

            let dirName = '';

            this.file.createDir(dirPath, dirName, true).then((dirEntry) => {
              let saveDir = dirPath + '/' + dirName + '/';
              this.file.createFile(saveDir, fileName, true).then((fileEntry) => {
                fileEntry.createWriter((fileWriter) => {
                  fileWriter.onwriteend = () => {
                    // this.hideLoading();
                    // this.showReportAlert('Report downloaded', saveDir + fileName);
                    this.fileOpener.open(saveDir + fileName, 'application/pdf')
                      .then(() => {
                        loading.dismiss();
                      })
                      .catch(e => console.error('Error openening file', e));
                  };
                  fileWriter.onerror = (e) => {
                    loading.dismiss();
                    console.error('Cannot write report ', e);
                  };
                  fileWriter.write(binaryArray);
                });
              }).catch((error) => {
                loading.dismiss();
                this.messageService.showToastError('Falha ao abrir arquivo');
                console.error('Cannot create file ', error);
              });
            }).catch((error) => {
              loading.dismiss();
              this.messageService.showToastError('Falha ao abrir local de armazenamento do arquivo');
              console.error('Cannot create folder ', error);
            });
          })
        }
      )
    }

    catch
      (e) {
      this.messageService.showToastError('Ops, houve uma falha ao gerar o PDF.');
      loading.dismiss();
    }
  }

}
