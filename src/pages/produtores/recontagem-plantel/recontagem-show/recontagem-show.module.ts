import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecontagemShowPage } from './recontagem-show';

@NgModule({
  declarations: [
    RecontagemShowPage,
  ],
  imports: [
    IonicPageModule.forChild(RecontagemShowPage),
  ],
})
export class RecontagemShowPageModule {}
