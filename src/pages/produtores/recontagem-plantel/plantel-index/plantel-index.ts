import {Component, ViewChild} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {IonicSelectableComponent} from "ionic-selectable";
import {PropriedadesProvider} from "../../../../providers/produtores/propriedades";
import {GalpoesProvider} from "../../../../providers/produtores/galpoes";
import {PeriodosProvider} from "../../../../providers/periodos/periodos";
import {PlantelContagemPage} from "../plantel-contagem/plantel-contagem";
import {RecontagemPlantelProvider} from "../../../../providers/recontagem-plantel/recontagem-plantel";

@IonicPage()
@Component({
  selector: 'page-plantel-index',
  templateUrl: 'plantel-index.html',
})
export class PlantelIndexPage {
  @ViewChild('galpaoSelect') galpaoSelect: IonicSelectableComponent;
  @ViewChild('intervaloSelect') intervaloSelect: IonicSelectableComponent;

  protected propriedades: any[] = [];
  protected galpoes: any[] = [];
  protected intervalos: any[] = [];
  protected recontagens: any[] = [];

  protected sem_chk = false;
  protected repetir_recontagem = false;
  protected param_propriedade_id;
  protected galpao_id;
  protected intervalo_id_slc;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private propriedadesProvider: PropriedadesProvider,
              private galpoesProvider: GalpoesProvider,
              private periodoProvider: PeriodosProvider,
              private modalCtrl: ModalController,
              private recontagemPlantelProvider: RecontagemPlantelProvider

  ){
    this.propriedadesProvider.getPropriedadesLocalRecontagemGroup().then((data: any[]) => {
      this.propriedades = data;
      this.sem_chk = false;
    });
  }

  getGalpao(event) {
    let propriedade_id = event.value.id;
    this.param_propriedade_id = propriedade_id;
    this.galpaoSelect.clear();


    this.intervaloSelect.clear();

    this.galpoes = [];
    this.intervalos = [];
    this.recontagens = [];
    this.sem_chk = false;

    this.galpoesProvider.getGalpoesLocal(propriedade_id, false).then((data: any[]) => {
      this.galpoes = data;
    });
  }

  getIntervalos(event) {
    let galpao_id = event.value.id;
    this.intervaloSelect.clear();

    this.intervalos = [];
    this.recontagens = [];
    this.sem_chk = false;
    this.galpao_id = galpao_id;

    this.recontagemPlantelProvider.getIntervalos().then((data: any[]) => {
      this.intervalos = data;
    })
  }

  getRecontagens(event){
    if(event != null){
      this.intervalo_id_slc = event.value.id;
    }
    this.carregaRecontagens();
  }

  abreRecontagem(recontagem){

  	this.recontagemPlantelProvider.getCelasUteis().then((data:any[])=>{
      let modal = this.modalCtrl.create(PlantelContagemPage, {
        recontagem_id: recontagem.id,
        data_prevista: recontagem.data_prevista,
        propriedade_id: this.param_propriedade_id,
        galpao_id: this.galpao_id,
        intervalo_id: this.intervalo_id_slc,
        produtor:recontagem.produtor,
        galpao:recontagem.galpao,
        celas_uteis: data,
		qtd_contrato_femeas: recontagem.qtd_contrato_femeas,
		qtd_contrato_leitoes: recontagem.qtd_contrato_leitoes,
		qtd_lao_vigente: recontagem.qtd_lao_vigente,
		celas_maternidades: recontagem.celas_maternidades,
		box_gestacao: recontagem.box_gestacaogetRecontagens,
		gestacao_coletiva: recontagem.gestacao_coletiva,
      });

      modal.onDidDismiss((data) => {
        if (data) {
          let index = this.recontagens.indexOf(recontagem);
          if (index > -1) {
            this.recontagens.splice(index, 1);
          }
        }
      });

      modal.present();
    });
  }

  carregaRecontagens(){

    if(this.intervalo_id_slc > 0){
      this.recontagemPlantelProvider.getRecontagens(this.galpao_id, this.intervalo_id_slc, this.repetir_recontagem).then((data:any[])=>{
        this.recontagens = data;
      });
    }
  }

  abs(value){
    return Math.abs(value);
  }

}
