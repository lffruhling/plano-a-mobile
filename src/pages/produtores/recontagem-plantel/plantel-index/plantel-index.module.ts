import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantelIndexPage } from './plantel-index';
import {IonicSelectableModule} from "ionic-selectable";

@NgModule({
  declarations: [
    PlantelIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(PlantelIndexPage),
    IonicSelectableModule,
  ],
})
export class PlantelIndexPageModule {}
