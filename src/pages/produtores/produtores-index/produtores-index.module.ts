import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutoresIndexPage } from './produtores-index';

@NgModule({
  declarations: [
    ProdutoresIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(ProdutoresIndexPage),
  ],
})
export class ProdutoresIndexPageModule {}
