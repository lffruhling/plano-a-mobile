import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProdutoresProvider} from "../../../providers/produtores/produtores";
import {ProdutorEditPage} from "../produtor-edit/produtor-edit";
import {EN_TAB_PAGES} from "../../../config";

/**
 * Generated class for the ProdutoresIndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-produtores-index',
  templateUrl: 'produtores-index.html',
})
export class ProdutoresIndexPage{
  searchQuery: string = '';
  protected produtores = [];
  protected termo_busca;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private produtoresService: ProdutoresProvider
  ) {}

  ionViewWillEnter() {
    if (!(this.termo_busca && this.termo_busca.trim() != '')) {
      this.produtores = [];
      this.carregaTodosProdutores();
    }
  }

  carregaTodosProdutores(){
    let promisse = new Promise((resolve, reject) =>{
      this.produtoresService.getAll().then((data:any) =>{
        this.produtores = data;
        resolve();
      }).catch((err) =>{
        console.error('falha ao buscar produtores', err);
        reject();
      })
    });

    return promisse;
  }

  buscarProdutores(ev: any) {
    // Voltar para todos os itens
    this.carregaTodosProdutores().then(()=>{

      // pega valor a ser pesquisado
      const val = ev.target.value;

      // se o valor pesquisado for vazio, não filtra
      if (val && val.trim() != '') {
        this.produtores = this.produtores.filter((produtor) => {
          return (produtor.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    });
  }

  editaProdutor(id){
    this.navCtrl.push(ProdutorEditPage, {id:id});
  }

}
