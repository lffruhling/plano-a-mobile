import {Component} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {BackbuttonService} from "../../services/backbutton.service";
import { EN_TAB_PAGES } from "../../config";
import {EmpresaProvider} from "../../providers/empresa/empresa";
import {SincronizacaoPage} from "../sincronizacao/sincronizacao";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-produtores',
  templateUrl: 'produtores.html',
})
export class ProdutoresPage{

  protected integracao_aves:any = true;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private nav: Nav,
    private authProvider: AuthProvider,
    private networkService: CheckNetworkProvider,
    private messagesService: AlertMessagesProvider,
    private backbuttonService: BackbuttonService,
    private empresaProvider: EmpresaProvider
  ) {
    this.empresaProvider.getIntegracaoAvesChk().then((data)=>{
      this.integracao_aves = data;
      console.log('this.integracao_aves', this.integracao_aves);
    });
  }

  ionViewWillEnter() {
    this.backbuttonService.pushPage(EN_TAB_PAGES.EN_TP_PRODUTOR, this.navCtrl);
  }

  openPages(page) {
    this.navCtrl.push(page)
  }

  swipe(event) {
    if(event.direction === 4) {
      this.navCtrl.parent.select(2);
    }else if(event.direction === 2){
      this.navCtrl.parent.select(0);
    }
  }

  doRefresh(refresher) {
    if(this.networkService.isConnected()){
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.nav.push(SincronizacaoPage, {automatico:true});
        }
        refresher.complete();
      });
    }else{
      refresher.complete();
      this.messagesService.showToast('Ops, não há uma conexão com a internet no momento!');
    }
  }

}
