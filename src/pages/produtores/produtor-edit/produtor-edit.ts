import {Component, OnInit, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup} from "@angular/forms";
import {ProdutoresProvider} from "../../../providers/produtores/produtores";
import {CameraProvider} from "../../../providers/camera/camera";
import {Produtor} from "../../../class/Produtor";
import {BrMaskerIonicServices3} from "brmasker-ionic-3";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import { Keyboard } from '@ionic-native/keyboard';
import { Geolocation } from '@ionic-native/geolocation';
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {UtilsService} from "../../../services/utils.service";
import {EmpresaProvider} from "../../../providers/empresa/empresa";

@IonicPage()
@Component({
  selector: 'page-produtor-edit',
  templateUrl: 'produtor-edit.html',
  providers: [Keyboard],
})
export class ProdutorEditPage implements OnInit {
  @ViewChild(Content) content: Content;

  map: any;
  hideMap = true;
  produtorForm: FormGroup;
  id:number;
  nome:String;
  protected integracao_aves:any = false;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private produtorService: ProdutoresProvider,
    private cameraService: CameraProvider,
    private brMasker: BrMaskerIonicServices3,
    private messageService: AlertMessagesProvider,
    private keyboard: Keyboard,
    private geolocation: Geolocation,
    private network: CheckNetworkProvider,
    private diagnostic: Diagnostic,
    private locationAccurace: LocationAccuracy,
    private utils: UtilsService,
    private empresaProvider: EmpresaProvider,
  ) {
    this.empresaProvider.getIntegracaoAvesChk().then(data=>{
      this.integracao_aves = data;
    })
  }

  ngOnInit(){
    this.id = this.navParams.get('id');

    this.produtorForm = new FormGroup({
      'id': new FormControl(this.id),
      'estado_id': new FormControl(null),
      'municipio_id': new FormControl(null),
      'matricula': new FormControl(''),
      'cpf': new FormControl(''),
      'nome': new FormControl(''),
      'logradouro': new FormControl(''),
      'complemento': new FormControl(''),
      'celular': new FormControl(''),
      'telefone': new FormControl(''),
      'email': new FormControl(''),
      'foto': new FormControl('assets/imgs/user_default.png'),
      'enviar_foto': new FormControl(false),
      'sincronizado': new FormControl(false),
      'ativo': new FormControl(null),
      'latitude': new FormControl(''),
      'longitude': new FormControl(''),
      'integradora': new FormControl(''),
    });

    this.produtorService.get(this.id).then((produtor:Produtor) => {
      this.nome = produtor.nome;

      this.produtorForm.controls['estado_id'].setValue(produtor.estado_id);
      this.produtorForm.controls['municipio_id'].setValue(produtor.municipio_id);
      this.produtorForm.controls['matricula'].setValue(produtor.matricula);
      this.produtorForm.controls['cpf'].setValue(this.brMasker.writeValuePerson(produtor.cpf));
      this.produtorForm.controls['nome'].setValue(produtor.nome);
      this.produtorForm.controls['logradouro'].setValue(produtor.logradouro);
      this.produtorForm.controls['complemento'].setValue(produtor.complemento);
      this.produtorForm.controls['celular'].setValue(produtor.celular);
      this.produtorForm.controls['telefone'].setValue(produtor.telefone);
      this.produtorForm.controls['email'].setValue(produtor.email);
      this.produtorForm.controls['latitude'].setValue(produtor.latitude);
      this.produtorForm.controls['longitude'].setValue(produtor.longitude);
      this.produtorForm.controls['enviar_foto'].setValue(produtor.enviar_foto);
      this.produtorForm.controls['ativo'].setValue(produtor.ativo);
      this.produtorForm.controls['integradora'].setValue(produtor.integradora);
      this.produtorForm.controls['sincronizado'].setValue(0);

      if(produtor.foto && produtor.foto != "null"){
          this.produtorForm.controls['foto'].setValue(produtor.foto);
      }
    });
  }

  capturaFoto(){

    this.utils.checkDisck().then((data)=> {

      if (data) {

        this.cameraService.capturarFoto(false, 600, 600, false, true).then((imageData: string) => {

          this.produtorForm.controls['foto'].setValue('data:image/jpeg;base64,' + imageData);
          this.produtorForm.controls['enviar_foto'].setValue(true);
        }).catch((err) => {
          console.error('erro ao caputar foto', err);
        })
      }
    })
  }

  store(){
    let loading = this.messageService.showLoading('Salvando alterações...');
    this.produtorService.atualizaProdutorApi(this.produtorForm.value, this.integracao_aves).then((data:any) => {
      loading.dismiss();
      this.messageService.showToastSuccess('Produtor atualizado com sucesso!');
      this.navCtrl.pop();
    }).catch((err)=>{
      console.error('erro ao atualizar produtor', err);
      if(err.name == "TimeoutError"){
        this.messageService.showAlert('Opss!', 'Sua conexão parece estar lenta, as alterações foram salvas, mas não sincronizadas!').then(()=>{
          this.navCtrl.pop();
          this.messageService.showToastSuccess('Produtor atualizado com sucesso!');
        });
      }else{
        this.messageService.showToastError('Falha ao atualizar produtor.');
      }
      loading.dismiss();
    });

  }

  public removerProdutor(){
    this.messageService.showConfirmationAlert('Apagar', `Deseja realmente apagar o produtor ${this.nome}?`, 'Sim, Apagar!', 'Não').then((res)=> {
      if (res) {
        let loading = this.messageService.showLoading(`Apagando Produtor ${this.nome}`);
        this.produtorService.delete(this.id).then(() => {
          loading.dismiss();
          this.navCtrl.pop();
        }).catch(err => {
          this.messageService.showToastError('Falha ao remover produtor!');
          loading.dismiss();
        })
      }
    });
  }
}
