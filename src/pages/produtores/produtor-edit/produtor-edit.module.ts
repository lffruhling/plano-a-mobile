import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutorEditPage } from './produtor-edit';

@NgModule({
  /*declarations: [
    ProdutorEditPage,
  ],*/
  imports: [
    IonicPageModule.forChild(ProdutorEditPage),
  ],
})
export class ProdutorEditPageModule {}
