import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAvaliacaoPage } from './modal-avaliacao';

@NgModule({
  imports: [
    IonicPageModule.forChild(ModalAvaliacaoPage),
  ],
})
export class ModalAvaliacaoPageModule {}
