import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {AcoesProvider} from '../../../providers/acoes/acoes';
import {AvaliacoesProvider} from "../../../providers/acoes/avaliacoes";
import {TecnicoProvider} from "../../../providers/tecnico/tecnico";
import * as moment from "moment";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {PropriedadesProvider} from "../../../providers/produtores/propriedades";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {FormControl, FormGroup} from "@angular/forms";
import {CameraProvider} from "../../../providers/camera/camera";
import {Propriedade} from "../../../class/Propriedade";
import {Storage} from "@ionic/storage";
import {UniqueIdProvider} from "../../../providers/unique-id/unique-id";
import {CalendarComponentOptions} from "ion2-calendar";
import {UtilsService} from "../../../services/utils.service";
import {AuthProvider} from "../../../providers/auth/auth";
import {DbExceptionProvider} from "../../../providers/db-exception/db-exception";
import {FirestoreDatabaseProvider} from "../../../providers/firestore-database/firestore-database";
import {ModalSignPage} from "../../modal-sign/modal-sign";
import {IntervalosProvider} from "../../../providers/intervalos/intervalos";

@IonicPage()
@Component({
  selector: 'page-modal-avaliacao',
  templateUrl: 'modal-avaliacao.html',
})
export class ModalAvaliacaoPage implements OnInit {
  protected parametro;
  protected propriedade;
  protected propriedade_id;
  protected grupo_acao_id;
  protected ponto_acao_id;
  protected parametro_id;
  protected gerou_acao;
  protected now = moment().format('YYYY-MM-DD');
  protected maxDate;
  acaoForm: FormGroup;
  protected retornoModal = false;
  protected mobile_key;
  protected acao_mobile_key;
  protected valor_padrao;
  protected fotos: any[] = [];
	protected isIos = false;
	protected assinautraProdutor = null;

  protected options;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private acoesProvider: AcoesProvider,
    private avaliacaoProvider: AvaliacoesProvider,
    private providerTecnico: TecnicoProvider,
    private networkService: CheckNetworkProvider,
    private propriedadeService: PropriedadesProvider,
    private messageService: AlertMessagesProvider,
    private cameraService: CameraProvider,
    private storage: Storage,
    private uniqueKeyService: UniqueIdProvider,
    private authProvider: AuthProvider,
    private utils: UtilsService,
    private dbException: DbExceptionProvider,
    private fbDbProvider: FirestoreDatabaseProvider,
    private acaoService: AcoesProvider,
	private modalCtrl: ModalController,
	private plt: Platform,
	private intervalosProvider:IntervalosProvider,
  ) {
    this.parametro = navParams.get('parametro');
    if (this.parametro.informacao_padrao == "true") {
      this.valor_padrao = "Sim";

    } else if (this.parametro.informacao_padrao == "false") {
      this.valor_padrao = "Não";
    } else {
      this.valor_padrao = this.parametro.informacao_padrao
    }

    this.propriedadeService.get(this.navParams.get('propriedade_id')).then((propriedade: Propriedade) => {
      this.propriedade = propriedade;
    }).catch((err) => {
      console.error('falha ao consultar propriedade no modal de avaliação', err)
    });

    this.acao_mobile_key = this.uniqueKeyService.generateKey();
    this.mobile_key = this.uniqueKeyService.generateKey();
  }

  ngOnInit() {
	  this.intervalosProvider.getDiasIntervaloAplicaChk().then(data => {
		  if(data != null){
			  this.maxDate = moment().add(data, 'day');
		  }else{
			  this.maxDate = moment().add(1, 'year');
		  }

		  const opt: CalendarComponentOptions = {
			  from: moment().toDate(),
			  to: this.maxDate.toDate(),
			  weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
			  monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
		  }
		  this.options = opt;
	  });

	  this.isIos = this.plt.is('ios');

	  this.acaoForm = new FormGroup({
      'propriedade_id': new FormControl(this.navParams.get('propriedade_id')),
      'tipo_integracao_id': new FormControl(this.navParams.get('tipo_integracao_id')),
      'situacao_id': new FormControl(1),
      'grupo_acao_id': new FormControl(this.navParams.get('grupo_acao_id')),
      'ponto_acao_id': new FormControl(this.navParams.get('ponto_acao_id')),
      'parametro_acao_id': new FormControl(this.navParams.get('parametro_id')),
      'gerou_acao': new FormControl(1),
      'avaliacao_inicial': new FormControl(''),
      'o_que': new FormControl(''),
      'como': new FormControl(''),
      'quem': new FormControl(''),
      'quando': new FormControl(''),
      'foto': new FormControl('assets/imgs/camera_800X300.png'),
      'com_foto': new FormControl(false),
      'mobile_key': new FormControl(this.acao_mobile_key),
      'data_criacao': new FormControl(this.now),
    })
  }

  ionViewDidEnter() {
    this.acaoForm.controls['propriedade_id'].setValue(this.navParams.get('propriedade_id'));
    this.acaoForm.controls['tipo_integracao_id'].setValue(this.navParams.get('tipo_integracao_id'));
    this.acaoForm.controls['grupo_acao_id'].setValue(this.navParams.get('grupo_acao_id'));
    this.acaoForm.controls['ponto_acao_id'].setValue(this.navParams.get('ponto_acao_id'));
    this.acaoForm.controls['parametro_acao_id'].setValue(this.navParams.get('parametro_id'));
  }

  gerarAcao() {
    let loading = this.messageService.showLoading('Salvando dados...');
    let acaoService = this.acoesProvider;
    let dateNow = new Date();
    let data_formatada = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + "-" + dateNow.getDate();
    let avaliacao: any = {
      propriedade_id: this.acaoForm.value.propriedade_id,
      parametro_id: this.acaoForm.value.parametro_acao_id,
      data_visita: data_formatada,
      gerou_acao: true,
      mobile_key: this.mobile_key,
    };

    if (this.utils.validaAcao(this.acaoForm.value)) {
      /*Salva AVI na base local*/

      this.avaliacaoProvider.insert(avaliacao).then((data_av) => {
        let avaliacao_id = data_av.insertId;
        let acao_id;
        let dados = this.acaoForm.value;
        dados.avaliacao_inicial = avaliacao_id;

        let dadosFB = this.acaoForm.value;
        dadosFB.avaliacao_inicial = avaliacao_id;
        /*Gera nova ação*/
        this.acoesProvider.insert(dados, this.assinautraProdutor).then(data_ac =>{
          acao_id = data_ac.insertId;
          dadosFB.id_mobile = acao_id;

          this.acaoService.ligaFotos(this.acao_mobile_key, acao_id);

          this.fbDbProvider.addAcao(dadosFB);
        }).catch((err) => {
          console.error('Falha ao gerar ação através da AVI', dados, err);
        });

        this.retornoModal = true;

        this.messageService.showToastSuccess('Ação gerada com sucesso!');

        this.dismiss();
        loading.dismiss();

        if (this.networkService.isConnected()) {
          this.authProvider.checkLogin().then((data: any) => {
            if (data == "success") {
              this.utils.retornaUUID().then((uuid: any) => {

                dados.id_mobile = avaliacao_id;
                dados.acao_id_mobile = acao_id;
                dados.device_uuid = uuid;
                dados.mobile_key = this.mobile_key;
                dados.acao_mobile_key = this.acao_mobile_key;
                dados.com_acao = true;

                this.avaliacaoProvider.apiStore(dados).subscribe((response: any) => {

                  /* Retorna uma avaliacao inicial com as ações criadas, neste caso uma, porém dentro de um array */
                  response.acoes.forEach(function (acao) {
                    acaoService.updateSinc(acao.id, acao.id_mobile).catch((err) => {
                      console.error('Falha ao atualiar ação', err)
                    });

                    acaoService.enviaFotos(acao_id, acao.id);
                  });

                  this.avaliacaoProvider.updateSinc(response.id, response.id_mobile).catch((err) => {
                    console.error('falha atualizar ação id', err, data)
                  });
                });
              });
            }
          });
        }

        this.fbDbProvider.addAvaliacaoInicial(avaliacao);
      }).catch((err) => {
        console.error('erro salva baixa', err);
        this.messageService.showToastError('Falha ao gerar ação!');
        loading.dismiss();
      });

    } else {
      this.messageService.showAlert('Ops', 'Houve uma falha ao preencher os dados da ação, tente novamente!');
      this.dbException.insert('falha ao salvar ação', 'modal-avaliacao.ts', 'gerarAcao', 'sql de insert ação', this.acaoForm.value);
      this.fbDbProvider.addExceptionApp('falha ao salvar ação', 'modal-avaliacao.ts', 'gerarAcao', 'sql de insert ação', this.acaoForm.value);
      loading.dismiss();
    }



  }

  dismiss() {
    this.viewCtrl.dismiss(this.retornoModal);
  }

  newFoto() {
    let mobile_key = this.acao_mobile_key;
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.acaoService.insertFoto({
            mobile_key: mobile_key,
            img: foto
          }).then((data) => {
            this.fotos.push({
              id: data.insertId,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err => {
            console.error('falha salvar foto acao - tela', err);
          });


        }).catch(err => {
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto) {
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result => {
      if (result) {
        this.acaoService.deleteFoto(foto_id).then(() => {
          this.fotos = this.fotos.filter((_foto) => {
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

	assinarAcao() {
		let modal = this.modalCtrl.create(ModalSignPage);
		modal.onDidDismiss((data) => {
			if (data.retornoModal) {
				this.assinautraProdutor = data.assinatura;
			}
		});
		modal.present();
	}
}
