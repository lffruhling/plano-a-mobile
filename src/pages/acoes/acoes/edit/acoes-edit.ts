import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AcoesProvider } from '../../../../providers/acoes/acoes';
import { PropriedadesProvider } from '../../../../providers/produtores/propriedades';
import { GrupoAcoesProvider } from '../../../../providers/acoes/grupoAcoes';
import { PontoAcoesProvider } from '../../../../providers/acoes/pontoAcoes';
import { ParametroAcoesProvider } from '../../../../providers/acoes/parametroAcoes';

import * as moment from "moment";
import {FormControl, FormGroup} from "@angular/forms";
import {CalendarComponentOptions} from "ion2-calendar";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import {CameraProvider} from "../../../../providers/camera/camera";
import {UtilsService} from "../../../../services/utils.service";
import {CheckNetworkProvider} from "../../../../providers/check-network/check-network";
import {AuthProvider} from "../../../../providers/auth/auth";
import {UniqueIdProvider} from "../../../../providers/unique-id/unique-id";
import {FirestoreDatabaseProvider} from "../../../../providers/firestore-database/firestore-database";
import {IntervalosProvider} from "../../../../providers/intervalos/intervalos";

@IonicPage()
@Component({
  selector: 'page-acoes-edit',
  templateUrl: 'acoes-edit.html',
})
export class AcoesEditPage implements OnInit {
  protected acao;
  protected propriedades;
  protected grupos;
  protected pontos;
  protected parametros;
  protected valor_padrao;
  protected now = moment().format('YYYY-MM-DD');
  protected maxDate;
  acaoForm: FormGroup;
  protected informacao_padrao;
  protected fotos: any[] = [];
  protected mobile_key;
  protected duplicada = false;

  protected options;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public propriedadesProvider: PropriedadesProvider,
    public grupoAcoesProvider: GrupoAcoesProvider,
    public pontoAcoesProvider: PontoAcoesProvider,
    public parametroAcoesProvider: ParametroAcoesProvider,
    private messageService: AlertMessagesProvider,
    private cameraService: CameraProvider,
    private acaoService: AcoesProvider,
    private utils: UtilsService,
    private networkService: CheckNetworkProvider,
    private authProvider: AuthProvider,
    private uniqueKeyService: UniqueIdProvider,
    private fbDbProvider: FirestoreDatabaseProvider,
	private intervalosProvider: IntervalosProvider,

  ) {
  	this.acao = navParams.get('acao');

  	this.carregarGrupos();
    this.carregarPontos(null);
    this.carregarParametros(null);
    this.mobile_key = this.uniqueKeyService.generateKey();
  }

  ngOnInit() {
	  this.intervalosProvider.getDiasIntervaloAplicaChk().then(data => {
		  if(data != null){
			  this.maxDate = moment().add(data, 'day');
		  }else{
			  this.maxDate = moment().add(1, 'year');
		  }

		  const opt: CalendarComponentOptions = {
			  from: moment().toDate(),
			  to: this.maxDate.toDate(),
			  weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
			  monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
		  }
		  this.options = opt;
	  });

    this.acaoForm = new FormGroup({
      'id': new FormControl(this.acao.id),
      'propriedade_id': new FormControl(this.acao.propriedade_id),
      'tipo_integracao_id': new FormControl(this.acao.tipo_integracao_id),
      'grupo_acao_id': new FormControl(null),
      'ponto_acao_id': new FormControl(this.acao.ponto_acao_id),
      'parametro_acao_id': new FormControl(this.acao.parametro_acao_id),
      'valor_padrao': new FormControl(this.acao.valor_padrao),
      'o_que': new FormControl(this.acao.o_que),
      'como': new FormControl(this.acao.como),
      'quem': new FormControl(this.acao.quem),
      'quando': new FormControl(this.acao.quando),
      'data_criacao': new FormControl(this.now),
      'mobile_key': new FormControl(this.acao.mobile_key),
    });

    this.grupoAcoesProvider.find(this.acao.grupo_acao_id).then(grupo=>{
      this.acaoForm.controls['grupo_acao_id'].setValue(grupo);

      this.pontoAcoesProvider.find(this.acao.ponto_acao_id).then(ponto=>{
        this.acaoForm.controls['ponto_acao_id'].setValue(ponto);

        this.parametroAcoesProvider.find(this.acao.parametro_acao_id).then(parametro=>{
          this.acaoForm.controls['parametro_acao_id'].setValue(parametro);

          this.valorPadrao(null, parametro);

        })

      })
    }).catch(err=>{
      console.error('falha ao consultar/preencher grupo acao', err);
    });

    this.acaoService.getFotos(this.acao.id).then((fotos: any[]) => {
      this.fotos = fotos;
    }).catch((err) => {
      console.error('falha ao carregar fotos', err);
    });
  }

  carregarGrupos() {
    let tipo_integracao_id = this.acao.tipo_integracao_id;
    this.grupoAcoesProvider.getGruposPorIntegracaoLocal(tipo_integracao_id).then((data: any) => {
      this.grupos = data;
    })
  }

  carregarPontos(event) {
    let grupo_acao_id;
    if(event == null){
      grupo_acao_id = this.acao.grupo_acao_id;
    }else{
      grupo_acao_id = event.value.id
    }

    this.pontoAcoesProvider.getPontosPorGrupoLocal(grupo_acao_id).then((data: any) => {
      this.pontos = data;
    })
  }

  carregarParametros(event) {
    let ponto_id;
    if(event == null){
      ponto_id = this.acao.ponto_id;
    }else{
      ponto_id = event.value.id
    }

    this.parametroAcoesProvider.getParametrosPorPontoLocal(ponto_id).then((data: any) => {
      this.parametros = data;
    })
  }

  valorPadrao(event, parametro = null) {
    let valor_padrao;
    if(event == null && parametro != null){
      valor_padrao = parametro.informacao_padrao
    }else{
      valor_padrao = event.value.informacao_padrao
    }

    if (valor_padrao == "true") {
      this.informacao_padrao = "Sim";

    } else if (valor_padrao == "false") {
      this.informacao_padrao = "Não";
    } else {
      this.informacao_padrao = valor_padrao
    }

    this.acaoForm.controls['valor_padrao'].setValue(valor_padrao);
  }

  newFoto() {
    let mobile_key = this.acao.mobile_key;
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          this.acaoService.insertFoto({
            mobile_key: mobile_key,
            img: foto,
            acao_id: this.acao.id
          }).then((data)=>{
            this.fotos.push({
              id: data.insertId,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err=>{
            console.error('falha salvar foto acao - tela', err);
          });


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        this.acaoService.deleteFoto(foto_id).then(()=>{
          this.fotos = this.fotos.filter((_foto)=>{
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

  update() {
    this.acaoForm.controls['grupo_acao_id'].setValue(this.acaoForm.value.grupo_acao_id.id);
    this.acaoForm.controls['parametro_acao_id'].setValue(this.acaoForm.value.parametro_acao_id.id);
    this.acaoForm.controls['ponto_acao_id'].setValue(this.acaoForm.value.ponto_acao_id.id);
    this.acaoForm.controls['quando'].setValue(this.acaoForm.value.quando);

    let loading = this.messageService.showLoading('Salvando dados...');

    this.acaoService.editaAcao(this.acaoForm.value).then(() => {

      this.navCtrl.pop();
      loading.dismiss();

      if (this.networkService.isConnected()) {
        this.authProvider.checkLogin().then((data: any) => {
          if (data == "success") {
            if(this.acao.id_web){
              this.acaoService.apiUpdate(this.acao.id_web, this.acaoForm.value).subscribe(data=>{
                /*Envia Fotos das ações*/
                this.acaoService.enviaFotos(this.acao.id, this.acao.id_web);
                this.acaoService.updateSinc(this.acao.id_web, this.acao.id);
              }, err=>{
                console.error('falha a atualizar acao', err);
              })
            }else{
              this.acaoService.apiStore(this.acaoForm.value).subscribe((data:any)=>{
                /*Envia Fotos das ações*/
                this.acaoService.enviaFotos(this.acao.id, data.id);
                this.acaoService.updateSinc(this.acao.id, data.id);
              }, err=>{
                console.error('falha a enviar acao', err);
              })
            }
          }
        });
      }

    }).catch(err => {
      loading.dismiss();
      console.error('erro update', err);
    });
  }

  duplicarAcao(){
    this.messageService.showConfirmationAlert('Duplicar Ação', 'Deseja realmente duplicar essa ação?', 'Sim, Duplicar', 'Cancelar').then((result)=>{
      if(result){
        this.duplicada = true;

        let newAcao = this.acaoForm.value;

        newAcao.grupo_acao_id = this.acaoForm.value.grupo_acao_id.id;
        newAcao.parametro_acao_id = this.acaoForm.value.parametro_acao_id.id;
        newAcao.ponto_acao_id = this.acaoForm.value.ponto_acao_id.id;
        newAcao.id = null;
        newAcao.mobile_key = this.mobile_key;

        this.acaoService.insert(newAcao, null).then((result)=>{
          var last_id = result.insertId;
          this.acaoForm.controls['id'].setValue(last_id);

          this.messageService.showToastSuccess('Ação Duplicada com sucesso!');
          if (this.networkService.isConnected()) {
            this.authProvider.checkLogin().then((data: any) => {
              if (data == "success") {
                this.utils.retornaUUID().then((uuid: any) => {
                  let dados = newAcao;
                  dados.id = last_id;
                  dados.device_uuid = uuid;

                  this.acaoService.apiStore(dados).subscribe((data: any) => {

                    this.acaoService.updateSinc(data.id, data.id_mobile);

                    /*Envia Fotos das ações*/
                    this.acaoService.enviaFotos(last_id, data.id);
                  })
                })
              }
            })
          }
          this.fbDbProvider.addAcao(newAcao);

        }).catch(err=>{
          this.messageService.showToastError('Falha ao duplicar Ação!');
        })
      }
    });
  }
}
