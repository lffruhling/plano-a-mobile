import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcoesEditPage } from './acoes-edit';

@NgModule({
  imports: [
    IonicPageModule.forChild(AcoesEditPage),
  ],
})
export class AcoesEditPageModule {}
