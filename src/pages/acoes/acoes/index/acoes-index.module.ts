import {LOCALE_ID, NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcoesIndexPage } from './acoes-index';

@NgModule({
  declarations: [
    AcoesIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(AcoesIndexPage),
  ],
  providers: [{provide: LOCALE_ID, useValue: 'pt-BR'}],
})
export class AcoesIndexPageModule {}
