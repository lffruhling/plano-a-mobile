import {Component} from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	ModalController,
	FabContainer,
	AlertController,
	Platform
} from 'ionic-angular';
import {AcoesCreatePage} from '../create/acoes-create';
import {AcoesProvider} from '../../../../providers/acoes/acoes';
import {ModalBaixaPage} from "../../modal-baixa/modal-baixa";
import {AcoesEditPage} from "../edit/acoes-edit";
import {CheckNetworkProvider} from "../../../../providers/check-network/check-network";
import * as moment from "moment";
import {CalendarModal, CalendarModalOptions} from "ion2-calendar";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import domtoimage from 'dom-to-image';
import {SocialSharing} from "@ionic-native/social-sharing";
import {PropriedadesProvider} from "../../../../providers/produtores/propriedades";
import {EmpresaProvider} from "../../../../providers/empresa/empresa";
import {TecnicoProvider} from "../../../../providers/tecnico/tecnico";
import {ProdutoresProvider} from "../../../../providers/produtores/produtores";
import {FileOpener} from "@ionic-native/file-opener";
import {File} from "@ionic-native/file";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {ModalSignPage} from "../../../modal-sign/modal-sign";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
	selector: 'page-acoes-index',
	templateUrl: 'acoes-index.html',
})
export class AcoesIndexPage {
	protected acoes = new Array<any>();
	protected acoesOriginais = new Array<any>();
	protected now = moment().format('YYYY-MM-DD');
	protected isSearchbarOpened = false;
	protected busca: string = null;
	AcoesCreatePage: any;
	protected termo_busca;
	protected propriedades;
	private tecnico;

	protected datas;

	dateRange: {
		from: Date;
		to: Date
	} = {
		to: new Date(Date.now() + (7 * (24 * 3600 * 1000))),
		from: new Date()
	};

	constructor(
		public navCtrl: NavController,
		public modalCtrl: ModalController,
		public navParams: NavParams,
		public acaoesProvider: AcoesProvider,
		private networkService: CheckNetworkProvider,
		private messageService: AlertMessagesProvider,
		private socialSharing: SocialSharing,
		private propriedadesProvider: PropriedadesProvider,
		private alertCtrl: AlertController,
		private empresaProvider: EmpresaProvider,
		private tecnicoProvider: TecnicoProvider,
		private produtorProvider: ProdutoresProvider,
		private platform: Platform,
		private fileOpener: FileOpener,
		private file: File,
	) {
		this.AcoesCreatePage = AcoesCreatePage;

		this.propriedadesProvider.getAllToSelect().then((propriedades: any) => {
			this.propriedades = propriedades;
		});

		this.tecnicoProvider.get().then(tecnico => {
			this.tecnico = tecnico;
		});
	}

	ionViewWillEnter() {
		let loading = this.messageService.showLoading('Atualizando lista de ações...');
		this.acaoesProvider.getAll().then(
			(data: any) => {
				this.acoes = data;
				this.acoesOriginais = data;
				loading.dismiss();

				if (this.isSearchbarOpened) {
					this.filtraAcoes(this.termo_busca);
				}
			}, err => {
				console.error('erro', err);
				loading.dismiss();
			});
	}

	abrirModal(acao) {
		let modal = this.modalCtrl.create(ModalBaixaPage, {acao: acao});

		modal.onDidDismiss((data) => {
			if (data.remover_item) {

				let index = this.acoes.indexOf(acao);
				let indexOri = this.acoesOriginais.indexOf(acao);

				if (index > -1) {
					this.acoes.splice(index, 1);
				}

				if (indexOri > -1) {
					this.acoesOriginais.splice(indexOri, 1);

				}
			} else {
				let index = this.acoes.indexOf(acao);

				this.acoes[index].quando = data.result.data_prevista;
				this.acoes[index].renovada = 1;
				this.acoesOriginais[index].quando = data.result.data_prevista;
				this.acoesOriginais[index].renovada = 1;

			}
		});
		modal.present();
	}

	editarAcao(acao) {
		this.navCtrl.push(AcoesEditPage, {
			acao: acao,
		})
	}

	mostraCampo(fab: FabContainer) {
		this.isSearchbarOpened = true;
		fab.close();
	}

	buscarAcoes(ev: any) {
		/*pega valor a ser pesquisado*/
		this.busca = ev.target.value;

		this.filtraAcoes(this.busca);
	}

	protected filtraAcoes(termo) {
		/*se o valor pesquisado for vazio, não filtra*/
		if (termo && termo.trim() != '') {
			this.acoes = this.acoes.filter((acao) => {
				return (acao.produtor_propriedade.toLowerCase().indexOf(termo.toLowerCase()) > -1);
			})
		} else {
			/*Voltar para todos os itens*/
			this.acoes = JSON.parse(JSON.stringify(this.acoesOriginais));
		}
	}

	abrirCalendario(fab: FabContainer) {
		const options: CalendarModalOptions = {
			canBackwardsSelected: true,
			pickMode: 'range',
			title: 'Calendário',
			defaultDateRange: this.dateRange,
			doneLabel: 'Aplicar',
			closeLabel: 'Cancelar',
			weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
			monthFormat: 'MM/YYYY'
		};

		let myCalendar = this.modalCtrl.create(CalendarModal, {
			options: options
		});

		myCalendar.present();

		myCalendar.onDidDismiss((date, type) => {
			if (type === 'done') {
				this.dateRange = Object.assign({}, {
					from: date.from.dateObj,
					to: date.to.dateObj,
				});
				this.datas = date;
			} else {
				this.datas = null;
			}

			let loading = this.messageService.showLoading('Buscando Ações...');
			this.acaoesProvider.findAcoes(this.datas, this.busca).then((result: any) => {
				this.acoes = result;
				loading.dismiss();
			}).catch(err => {
				console.error('falha ao consultar lotes por datas', err)
				loading.dismiss();
			});
		});
		fab.close();
	}

	limpaFiltros(fab: FabContainer) {
		let loading = this.messageService.showLoading('Buscando Ações...');
		/*Voltar para todos os itens*/
		this.acoes = JSON.parse(JSON.stringify(this.acoesOriginais));
		this.isSearchbarOpened = false;
		loading.dismiss();
		fab.close();
	}

	compartilharAcao(acao) {
		let loading = this.messageService.showLoading('Preparando para compartilhar Ação...');

		domtoimage.toPng(document.getElementById(`print-to-share-${acao.id}`), {quality: 0.95})
			.then((dataUrl) => {
				this.socialSharing.share("Ação gerada pelo Sistema Plano A", null, dataUrl, null).catch((err) => console.error('erro ao compartilhar', err));
				loading.dismiss();
			});
	}

	protected geraPdfAcoes(propriedade_id) {
		let loading = this.messageService.showLoading('Gerando relatório em PDF ...');
		let acoesTable: Array<any> = [];

		try {
			this.produtorProvider.findByPropriedade(propriedade_id).then((produtor: any) => {
				let produtor_concat = `${produtor.produtor} - ${produtor.propriedade}` || ' *N/C ';

				this.acaoesProvider.getAllByPropriedade(propriedade_id).then((acoes: any) => {
					let col_sign: any = {
						text: '----------------------------------------------------------------',
						bold: true
					};

					acoesTable.push(['Grupo', 'Ponto', 'O Que', 'Como', 'Quem', 'Previsão', 'Situação']);
					for (let acao of acoes) {
						acoesTable.push([acao.grupo, acao.ponto, acao.o_que, acao.como, acao.quem, moment(acao.quando, "YYYY-MM-DD").format('DD/MM/YYYY'), acao.situacao]);
						if (acao.assinatura_produtor != null) {
							col_sign = {
								image: acao.assinatura_produtor,
								fit: [200, 300]
							};
						}
					}

					let body_report = [
						{
							style: 'table_respostas',
							table: {
								widths: [55, 65, '*', '*', 70, 68, 60],
								body: acoesTable
							}
						}
					];

					this.empresaProvider.get().then((empresa: any) => {
						let docDefinition = {
							pageOrientation: 'landscape',
							content: [
								{
									style: 'table_header',
									table: {
										widths: ['*', 'auto'],
										headerRows: 2,
										body: [
											[
												{
													alignment: 'center',
													text: `${(empresa.razao_social || ' *N/C ').toUpperCase()}\n CNPJ: ${empresa.cnpj}\n ${(this.tecnico.email || ' *N/C ').toUpperCase()}`
												},
												[
													{
														style: 'table_header_right',
														widths: ['*', '*'],
														table: {
															body: [
																[
																	{
																		border: [false, false, false, false],
																		bold: true,
																		text: 'RELATÓRIO DE AÇÕES'
																	},
																	{
																		border: [false, false, false, false],
																		alignment: 'right',
																		text: 'PENDENTES',
																	}
																],
																[
																	{
																		border: [false, true, false, true],
																		bold: true,
																		text: 'TÉCNICO'
																	},
																	{
																		border: [false, true, false, true],
																		alignment: 'right',
																		text: (this.tecnico.name || ' *N/C ').toUpperCase()
																	}
																],
																[
																	{
																		border: [false, false, false, false],
																		bold: true,
																		text: 'DATA IMPRESSÃO'
																	},
																	{
																		border: [false, false, false, false],
																		alignment: 'right',
																		text: moment().format('DD/MM/YYYY')
																	}
																]
															]
														},
														layout: {
															fillColor: function (rowIndex, node, columnIndex) {
																return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
															}
														}
													}
												],
											],
											[
												{
													alignment: 'center',
													bold: true,
													text: `PRODUTOR: ${(produtor.produtor || ' *N/C ').toUpperCase()} - ${(produtor.propriedade || ' *N/C ').toUpperCase()}`,
													colSpan: 2
												},
												{}
											],
										]
									}
								},
								body_report,
								{
									style: 'assinatura',
									alignment: 'center',
									columns: [
										col_sign,
										{
											text: '----------------------------------------------------------------',
											bold: true
										}
									]
								},
								{
									alignment: 'center',
									columns: [
										{
											text: (produtor_concat).toUpperCase(),
											bold: true
										},
										{
											text: (this.tecnico.name || ' *N/C ').toUpperCase(),
											bold: true
										}
									]
								}
							]
						};

						let fileName = `relatorio_acoes_${Date.now()}.pdf`;
						pdfMake.createPdf(docDefinition).getBuffer((buffer) => {
							var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
							let binaryArray = utf8.buffer; // Convert to Binary...

							let dirPath = "";
							if (this.platform.is('android')) {
								// dirPath = this.file.externalRootDirectory;
								dirPath = this.file.dataDirectory;
							} else if (this.platform.is('ios')) {
								dirPath = this.file.documentsDirectory;
							}

							let dirName = '';
							this.file.createDir(dirPath, dirName, true).then((dirEntry) => {
								let saveDir = dirPath + '/' + dirName + '/';
								this.file.createFile(saveDir, fileName, true).then((fileEntry) => {
									fileEntry.createWriter((fileWriter) => {
										fileWriter.onwriteend = () => {
											// this.hideLoading();
											// this.showReportAlert('Report downloaded', saveDir + fileName);
											this.fileOpener.open(saveDir + fileName, 'application/pdf')
												.then(() => {
													loading.dismiss();
												})
												.catch(e => console.error('Error openening file', e));
										};
										fileWriter.onerror = (e) => {
											loading.dismiss();
											console.error('Cannot write report ', e);
										};
										fileWriter.write(binaryArray);
									});
								}).catch((error) => {
									loading.dismiss();
									this.messageService.showToastError('Falha ao abrir arquivo');
									console.error('Cannot create file ', error);
								});
							}).catch((error) => {
								loading.dismiss();
								this.messageService.showToastError('Falha ao abrir local de armazenamento do arquivo');
								console.error('Cannot create folder ', error);
							});
						});

					});

				});
			});

		} catch (e) {
			console.error('falha ao gerar relatorio', e);
			this.messageService.showToastError('Ops, houve uma falha ao gerar o PDF.');
			loading.dismiss();
		}
	}

	public selecionaPropriedade(local) {
		let alert = this.alertCtrl.create();

		alert.setTitle('Selecione uma Propriedade');


		for (let propriedade of this.propriedades) {
			alert.addInput({
				type: 'radio',
				label: propriedade.nome,
				value: propriedade.id,
				checked: false
			});
		}
		alert.addButton('Cancelar');
		alert.addButton({
			text: 'OK',
			handler: data => {
				switch (local) {
					case 1 :
						this.geraPdfAcoes(data);
						break;
					case 2 :
						this.assinaAcoes(data);
						break;
				}
			}
		});
		alert.present();
	}

	public assinaAcoes(propriedade_id) {
		let modal = this.modalCtrl.create(ModalSignPage);
		modal.onDidDismiss((data) => {
			if (data.retornoModal) {
				this.acaoesProvider.updateAssinatura(data.assinatura, propriedade_id);
			}
		});
		modal.present();
	}
}
