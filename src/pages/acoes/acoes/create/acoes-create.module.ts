import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcoesCreatePage } from './acoes-create';

@NgModule({
  imports: [
    IonicPageModule.forChild(AcoesCreatePage),
  ],
})
export class AcoesCreatePageModule {}
