import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform} from 'ionic-angular';

import {AcoesProvider} from '../../../../providers/acoes/acoes';
import {PropriedadesProvider} from '../../../../providers/produtores/propriedades';
import {GrupoAcoesProvider} from '../../../../providers/acoes/grupoAcoes';
import {PontoAcoesProvider} from '../../../../providers/acoes/pontoAcoes';
import {ParametroAcoesProvider} from '../../../../providers/acoes/parametroAcoes';

import * as moment from 'moment'
import {CheckNetworkProvider} from "../../../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import {FormControl, FormGroup} from "@angular/forms";
import {CameraProvider} from "../../../../providers/camera/camera";
import {UniqueIdProvider} from "../../../../providers/unique-id/unique-id";
import {CalendarComponentOptions} from "ion2-calendar";
import {UtilsService} from "../../../../services/utils.service";
import {AuthProvider} from "../../../../providers/auth/auth";
import {FirestoreDatabaseProvider} from "../../../../providers/firestore-database/firestore-database";
import {AcoesShowPage} from "../show/acoes-show";
import {GeolocationProvider} from "../../../../providers/geolocation/geolocation";
import {ModalSignPage} from "../../../modal-sign/modal-sign";
import {IntervalosProvider} from "../../../../providers/intervalos/intervalos";

@IonicPage()
@Component({
	selector: 'page-acoes-create',
	templateUrl: 'acoes-create.html',
})
export class AcoesCreatePage implements OnInit {
	protected propriedades;
	protected grupos;
	protected pontos;
	protected parametros;
	protected valor_padrao;
	protected now = moment().format('YYYY-MM-DD');
	protected maxDate;
	acaoForm: FormGroup;
	protected mobile_key;
	protected informacao_padrao;
	protected fotos: any[] = [];
	protected isIos = false;
	protected assinautraProdutor = null;

	protected obsGeolocation;

	protected options;
	// CalendarComponentOptions = {
	// 	from: this.minDate.toDate(),
	// 	to: this.maxDate.toDate(),
	// 	weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
	// 	monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
	// };

	constructor(
		private navCtrl: NavController,
		private navParams: NavParams,
		private acaoService: AcoesProvider,
		private propriedadesProvider: PropriedadesProvider,
		private grupoAcoesProvider: GrupoAcoesProvider,
		private pontoAcoesProvider: PontoAcoesProvider,
		private parametroAcoesProvider: ParametroAcoesProvider,
		private networkService: CheckNetworkProvider,
		private messageService: AlertMessagesProvider,
		private cameraService: CameraProvider,
		private uniqueKeyService: UniqueIdProvider,
		private utils: UtilsService,
		private authProvider: AuthProvider,
		private fbDbProvider: FirestoreDatabaseProvider,
		private geolocationProvider: GeolocationProvider,
		private modalCtrl: ModalController,
		private plt: Platform,
		private intervalosProvider: IntervalosProvider,
	) {
		this.propriedadesProvider.getPropriedadesLocal().then(
			data => {
				this.propriedades = data;
			}
		);

		this.mobile_key = this.uniqueKeyService.generateKey();
	}

	ngOnInit() {
		this.intervalosProvider.getDiasIntervaloAplicaChk().then(data => {
			if(data != null){
				this.maxDate = moment().add(data, 'day');
			}else{
				this.maxDate = moment().add(1, 'year');
			}

			const opt: CalendarComponentOptions = {
				from: moment().toDate(),
				to: this.maxDate.toDate(),
				weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
			}
			this.options = opt;
		});

		this.isIos = this.plt.is('ios');

		this.acaoForm = new FormGroup({
			'propriedade_id': new FormControl(''),
			'tipo_integracao_id': new FormControl(''),
			'situacao_id': new FormControl(1),
			'grupo_acao_id': new FormControl(''),
			'ponto_acao_id': new FormControl(''),
			'parametro_acao_id': new FormControl(''),
			'valor_padrao': new FormControl(''),
			'o_que': new FormControl(''),
			'como': new FormControl(''),
			'quem': new FormControl(''),
			'quando': new FormControl(''),
			'foto': new FormControl('assets/imgs/camera_800X300.png'),
			'com_foto': new FormControl(false),
			'considerar_calculos': new FormControl(false),
			'mobile_key': new FormControl(this.mobile_key),
			'data_criacao': new FormControl(this.now),
			'latitude': new FormControl(null),
			'longitude': new FormControl(null),
		});

		this.obsGeolocation = this.geolocationProvider.getGeolocation().then((geolocation: any) => {
			if (geolocation) {
				this.acaoForm.controls['latitude'].setValue(geolocation.coords.latitude);
				this.acaoForm.controls['longitude'].setValue(geolocation.coords.longitude);
			}
		});
	}

	carregarGrupos(event) {
		let tipo_integracao_id = event.value.tipo_integracao_id;
		this.acaoForm.controls['tipo_integracao_id'].setValue(tipo_integracao_id);
		this.grupoAcoesProvider.getGruposPorIntegracaoLocal(tipo_integracao_id).then((data: any) => {
			this.grupos = data;
		})
	}

	carregarPontos(event) {
		let grupo_acao_id = event.value.id;
		this.pontoAcoesProvider.getPontosPorGrupoLocal(grupo_acao_id).then((data: any) => {
			this.pontos = data;
		})
	}

	carregarParametros(event) {
		let ponto_id = event.value.id;
		this.parametroAcoesProvider.getParametrosPorPontoLocal(ponto_id).then((data: any) => {
			this.parametros = data;
		})
	}

	valorPadrao(event) {
		if (event.value.informacao_padrao == "true") {
			this.informacao_padrao = "Sim";

		} else if (event.value.informacao_padrao == "false") {
			this.informacao_padrao = "Não";
		} else {
			this.informacao_padrao = event.value.informacao_padrao
		}

		this.acaoForm.controls['valor_padrao'].setValue(event.value.informacao_padrao);
	}

	newFoto() {
		let mobile_key = this.mobile_key;
		let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
		this.utils.checkDisck().then((data) => {

			if (data) {

				this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
					loading.dismiss();
					let foto  ='data:image/jpeg;base64,'+base64File;
					this.acaoService.insertFoto({
						mobile_key: mobile_key,
						img: foto,
						acao_id: null
					}).then((data) => {
						this.fotos.push({
							id: data.insertId,
							mobile_key: mobile_key,
							img: foto
						});
					}).catch(err => {
						console.error('falha salvar foto acao - tela', err);
					});


				}).catch(err => {
					loading.dismiss();
					console.error('imagem não selecionada', err);
				});
			}
		});
	}

	removeFoto(foto) {
		let foto_id = foto.id;
		this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result => {
			if (result) {
				this.acaoService.deleteFoto(foto_id).then(() => {
					this.fotos = this.fotos.filter((_foto) => {
						return foto_id != _foto.id;
					});
				});
			}
		});
	}

	store() {
		this.acaoForm.controls['grupo_acao_id'].setValue(this.acaoForm.value.grupo_acao_id.id);
		this.acaoForm.controls['parametro_acao_id'].setValue(this.acaoForm.value.parametro_acao_id.id);
		this.acaoForm.controls['ponto_acao_id'].setValue(this.acaoForm.value.ponto_acao_id.id);
		this.acaoForm.controls['propriedade_id'].setValue(this.acaoForm.value.propriedade_id.id);

		let loading = this.messageService.showLoading('Salvando dados...');

		this.acaoService.insert(this.acaoForm.value, this.assinautraProdutor).then(data => {

			let last_id = data.insertId;

			this.acaoService.ligaFotos(this.mobile_key, last_id);
			this.navCtrl.pop();
			this.messageService.showToastSuccess('Ação gerada com sucesso!');
			loading.dismiss();

			this.messageService.showConfirmationAlert('Ver Ação?', 'Gostaria ver a ação aberta?', 'Sim, Ver Agora', 'Cancelar').then(result => {
				if (result) {
					this.acaoService.get(last_id).then(acao => {
						this.navCtrl.push(AcoesShowPage, {'acao': acao})
					});
				}
			});

			if (this.acaoForm.value.latitude) {
				this.acaoService.updateGeolocation(this.acaoForm.value.latitude, this.acaoForm.value.longitude, last_id).then((_) => {
					this.sincroniza(last_id);
				});
			} else {
				this.sincroniza(last_id);
			}


		});

		this.fbDbProvider.addAcao(this.acaoForm.value);
	}

	private sincroniza(last_id) {
		if (this.networkService.isConnected()) {
			this.authProvider.checkLogin().then((data: any) => {
				if (data == "success") {
					this.utils.retornaUUID().then((uuid: any) => {
						let dados = this.acaoForm.value;
						dados.id_mobile = last_id;
						dados.device_uuid = uuid;
						dados.assinatura_produtor = this.assinautraProdutor;

						this.acaoService.apiStore(dados).subscribe((data: any) => {
							this.acaoService.updateSinc(data.id, data.id_mobile);

							/*Envia Fotos das ações*/
							this.acaoService.enviaFotos(last_id, data.id);
						})
					})
				}
			})
		}
	}

	assinarAcao() {
		let modal = this.modalCtrl.create(ModalSignPage);
		modal.onDidDismiss((data) => {
			if (data.retornoModal) {
				this.assinautraProdutor = data.assinatura;
			}
		});
		modal.present();
	}
}
