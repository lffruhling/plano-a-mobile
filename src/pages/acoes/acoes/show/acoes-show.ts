import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AcoesProvider} from "../../../../providers/acoes/acoes";
import domtoimage from 'dom-to-image';
import {SocialSharing} from "@ionic-native/social-sharing";
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import * as moment from "moment";

@IonicPage()
@Component({
  selector: 'page-acoes-show',
  templateUrl: 'acoes-show.html',
})
export class AcoesShowPage {
  protected acao;
  protected now = moment().format('YYYY-MM-DD');

  constructor(
    protected navCtrl: NavController,
    protected navParams: NavParams,
    public acaoesProvider: AcoesProvider,
    private socialSharing: SocialSharing,
    private messageService: AlertMessagesProvider,
  ) {
    this.acao = navParams.get('acao');
  }

  compartilharAcao(){
    let loading = this.messageService.showLoading('Preparando para compartilhar Ação...');

    domtoimage.toPng(document.getElementById(`print-to-share`), {quality: 0.95})
      .then((dataUrl) => {
        this.socialSharing.share("Ação gerada pelo Sistema Plano A", null, dataUrl, null).then((data)=>{
            console.log("COmpartilhado com sucesso", data); },
          (err)=>{ console.error('erro ao compartilhar',err);
          });
        loading.dismiss();
      });
  }

}
