import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcoesShowPage } from './acoes-show';

@NgModule({
  imports: [
    IonicPageModule.forChild(AcoesShowPage),
  ],
})
export class AcoesShowPageModule {}
