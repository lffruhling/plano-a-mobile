import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { BaixaAcoesProvider } from '../../../providers/acoes/baixaAcoes';
import {AcoesProvider} from "../../../providers/acoes/acoes";
import * as moment from "moment";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {FormControl, FormGroup} from "@angular/forms";
import {CameraProvider} from "../../../providers/camera/camera";
import {UniqueIdProvider} from "../../../providers/unique-id/unique-id";
import {UtilsService} from "../../../services/utils.service";
import {AuthProvider} from "../../../providers/auth/auth";
import {FirestoreDatabaseProvider} from "../../../providers/firestore-database/firestore-database";
import {Acao} from "../../../class/Acao";
import {CalendarComponentOptions} from "ion2-calendar";
import {GeolocationProvider} from "../../../providers/geolocation/geolocation";

@IonicPage()
@Component({
  selector: 'page-modal-baixa',
  templateUrl: 'modal-baixa.html',
})
export class ModalBaixaPage implements OnInit{
  protected acao;
  protected resolvido = 0;
  protected retornoModal = { remover_item : false, result: null};
  protected now = moment().format('YYYY-MM-DD');
  baixaAcaoForm: FormGroup;
  private labelResposta = 'Como foi resolvido';
  protected mobile_key;
  protected fotos: any[] = [];
  protected maxDate = moment().add(3, 'year');
  protected minDate = moment();

  protected options: CalendarComponentOptions = {
    from: this.minDate.toDate(),
    to: this.maxDate.toDate(),
    weekdays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    monthPickerFormat: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
  };

  protected obsGeolocation;

  constructor(
  	private navCtrl: NavController,
  	private navParams: NavParams,
  	private viewCtrl: ViewController,
    private baixaAcaoService: BaixaAcoesProvider,
    private providerAcao: AcoesProvider,
    private networkService: CheckNetworkProvider,
    private messageService: AlertMessagesProvider,
    private cameraService: CameraProvider,
    private uniqueKeyService: UniqueIdProvider,
    private authProvider: AuthProvider,
    private utils: UtilsService,
    private fbDbProvider: FirestoreDatabaseProvider,
    private geolocationProvider: GeolocationProvider,
) {
    this.acao = navParams.get('acao');

    this.mobile_key = this.uniqueKeyService.generateKey();
  }

  ngOnInit(){
    this.baixaAcaoForm = new FormGroup({
      'acao_id': new FormControl(this.acao.id),
      'acao_id_web': new FormControl(''),
      'resolvido': new FormControl(null),
      'resposta': new FormControl(''),
      'data': new FormControl(this.now),
      'foto': new FormControl('assets/imgs/camera.png'),
      'com_foto': new FormControl(false),
      'data_prevista': new FormControl(null),
      'data_renovacao': new FormControl(this.now),
      'mobile_key': new FormControl(this.mobile_key),
      'latitude': new FormControl(null),
      'longitude': new FormControl(null),
    });

    this.providerAcao.get(this.acao.id).then((acao:Acao)=>{
      this.baixaAcaoForm.controls['acao_id_web'].setValue(acao.id_web);
    });

    this.obsGeolocation = this.geolocationProvider.getGeolocation().then((geolocation:any) => {
      if(geolocation){
        this.baixaAcaoForm.controls['latitude'].setValue(geolocation.coords.latitude);
        this.baixaAcaoForm.controls['longitude'].setValue(geolocation.coords.longitude);
      }
    });

  }

  baixarAcao() {
    let loading = this.messageService.showLoading('Salvando dados...');
    if(this.baixaAcaoForm.value.resolvido){
      this.salvaBaixa(loading);
      this.fbDbProvider.addBaixaAcao(this.baixaAcaoForm.value);
    }else{
      if(this.baixaAcaoForm.value.data_prevista != null){
        this.salvaRenovacaoAcao(loading);
        this.fbDbProvider.addRenovacaoAcao(this.baixaAcaoForm.value);
      }else{
        this.salvaBaixa(loading);
        this.fbDbProvider.addBaixaAcao(this.baixaAcaoForm.value);
      }
    }
  }

  setJustificativaPadrao(){
    if(!this.baixaAcaoForm.value.resolvido){
      this.baixaAcaoForm.controls['resposta'].setValue(`Não Realizado até ${moment(this.acao.quando, "YYYY-MM-DD").format('DD/MM/YYYY')}. Foi fixado um novo prazo para a data de ${moment(this.baixaAcaoForm.value.data_prevista, "YYYY-MM-DD").format('DD/MM/YYYY')}.`);
    }
  }

  setLabelResposta(valor) {
    if(valor){
      this.labelResposta = 'Como foi resolvido'
    }else {
      this.labelResposta = 'Justificativa'
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(this.retornoModal);
  }

  newFoto() {
    let mobile_key = this.mobile_key;
    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          let foto_dados = {
            mobile_key: mobile_key,
            img: foto
          };

          this.baixaAcaoService.insertFoto(foto_dados).then((data)=>{
            this.fotos.push({
              id: data.insertId,
              mobile_key: mobile_key,
              img: foto
            });
          }).catch(err=>{
            console.error('falha salvar foto acao - tela', err);
          });

          this.baixaAcaoService.insertFotoRenovacao(foto_dados)


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        this.baixaAcaoService.deleteFoto(foto_id).then(()=>{
          this.fotos = this.fotos.filter((_foto)=>{
            return foto_id != _foto.id;
          });
        });
      }
    });
  }

  salvaBaixa(loading){
    this.baixaAcaoService.insert(this.baixaAcaoForm.value).then((baixa)=>{

      let last_id = baixa.insertId;
      this.baixaAcaoService.ligaFotos(this.mobile_key, last_id);
      this.providerAcao.baixaAcao(this.acao.id).catch((err)=>{
        console.error('erro ao atualizar baixa da ação na ação', err);
      });

      this.retornoModal = { remover_item : true, result: null};
      this.messageService.showToastSuccess('Ação baixada com sucesso!');
      this.dismiss();
      loading.dismiss();

      if(this.baixaAcaoForm.value.latitude){
        this.baixaAcaoService.updateGeolocationBaixa(this.baixaAcaoForm.value.latitude, this.baixaAcaoForm.value.longitude, last_id).then((_)=>{
          this.sincronizaBaixa(last_id);
        });
      }else{
        this.sincronizaBaixa(last_id);
      }

    }).catch((err)=>{
      console.error('erro salva baixa', err);
      loading.dismiss();
      this.messageService.showToastError('Falha ao baixar ação!');
    });
  }

  salvaRenovacaoAcao(loading){
    let data = {
      'id_web': null,
      'acao_id': this.baixaAcaoForm.value.acao_id_web == null ? this.baixaAcaoForm.value.acao_id : this.baixaAcaoForm.value.acao_id_web,
      'data_prevista': this.baixaAcaoForm.value.data_prevista,
      'data_renovacao': this.baixaAcaoForm.value.data_renovacao,
      'resposta': this.baixaAcaoForm.value.resposta, //nesse caso vira uma justificativa
      'assinatura': null,
      'mobile_key': this.baixaAcaoForm.value.mobile_key,
      'deleted_at': null,
    };

    this.baixaAcaoService.insertRenovacaoAcao(data).then((renovacao)=>{
      let last_id = renovacao.insertId;

      // ligar fotos
      this.baixaAcaoService.ligaFotosRenovacao(this.mobile_key, last_id);

      this.messageService.showToastSuccess('Ação renovada com sucesso!');
      this.retornoModal = {
        remover_item : false,
        result: this.baixaAcaoForm.value
      };

      this.dismiss();
      loading.dismiss();

      if(this.baixaAcaoForm.value.latitude){
        this.baixaAcaoService.updateGeolocationRenovacao(this.baixaAcaoForm.value.latitude, this.baixaAcaoForm.value.longitude, last_id).then((_)=>{
          this.sincronizaRenovacao(last_id);
        });
      }else{
        this.sincronizaRenovacao(last_id);
      }
    })

  }

  private sincronizaBaixa(last_id){
    if(this.networkService.isConnected() && this.acao.id_web){
      this.authProvider.checkLogin().then((data: any) => {

        if (data == "success") {
          this.utils.retornaUUID().then((uuid: any) => {
            let dados = this.baixaAcaoForm.value;
            dados.id_mobile = last_id;
            dados.device_uuid = uuid;

            this.baixaAcaoService.apiBaixaAcao(dados).subscribe((response:any) => {
              this.baixaAcaoService.updateSinc(response.id, last_id, response.acao_id).catch((err)=>{
                console.error('erro ao atualizar id_web da baixa', err);
              });

              this.providerAcao.baixaAcao(this.acao.id).catch((err)=>{
                console.error('erro ao atualizar baixa da ação na ação', err);
              });

              /*Enviar Fotos*/
              this.baixaAcaoService.enviaFotos(last_id, response.id);
            },err=>{
              console.error('falha ao enviar ação', err)
            });
          });
        }
      });
    }
  }

  private sincronizaRenovacao(last_id){
    if(this.networkService.isConnected() && this.acao.id_web){
      this.authProvider.checkLogin().then((data: any) => {

        if (data == "success") {
          this.utils.retornaUUID().then((uuid: any) => {
            let dados = this.baixaAcaoForm.value;
            dados.id_mobile = last_id;
            dados.device_uuid = uuid;

            this.baixaAcaoService.apiBaixaAcao(dados).subscribe((response:any) => {
              this.baixaAcaoService.updateSincRenovacaoAcao(response.id, last_id).catch((err)=>{
                console.error('erro ao atualizar id_web da baixa', err);
              });

              /*Enviar Fotos*/
              this.baixaAcaoService.enviaFotosRenovacao(last_id, response.id);
            },err=>{
              console.error('falha ao enviar ação', err)
            });
          });
        }
      });
    }
  }
}
