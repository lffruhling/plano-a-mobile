import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalBaixaPage } from './modal-baixa';

@NgModule({
  imports: [
    IonicPageModule.forChild(ModalBaixaPage),
  ],
})
export class ModalBaixaPageModule {}
