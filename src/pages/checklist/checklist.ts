import { Component } from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {BackbuttonService} from "../../services/backbutton.service";
import { EN_TAB_PAGES } from "../../config";
import {SincronizacaoPage} from "../sincronizacao/sincronizacao";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-checklist',
  templateUrl: 'checklist.html',
})
export class ChecklistPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav: Nav,
    private authProvider: AuthProvider,
    private networkService: CheckNetworkProvider,
    private messagesService: AlertMessagesProvider,
    private backbuttonService: BackbuttonService,
  ) {
  }

  ionViewWillEnter() {
    this.backbuttonService.pushPage(EN_TAB_PAGES.EN_TP_CHECKLIST, this.navCtrl);
  }

  openPages(page) {
      this.navCtrl.push(page)
  }

  swipe(event) {
    if(event.direction === 4) {
      this.navCtrl.parent.select(1);
    }else if(event.direction === 2){
      this.navCtrl.parent.select(3);
    }
  }

  doRefresh(refresher) {
    if(this.networkService.isConnected()) {
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.nav.push(SincronizacaoPage, {automatico:true});
        }
        refresher.complete();
      });
    }else{
      refresher.complete();
      this.messagesService.showToast('Ops, não há uma conexão com a internet no momento!');
    }
  }

}
