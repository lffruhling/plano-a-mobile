import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelatoriosPage } from './relatorios';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    RelatoriosPage,
  ],
  imports: [
    IonicPageModule.forChild(RelatoriosPage),
    CalendarModule
  ],
})
export class RelatoriosPageModule {}
