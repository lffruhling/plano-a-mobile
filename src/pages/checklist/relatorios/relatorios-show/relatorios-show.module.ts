import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelatoriosShowPage } from './relatorios-show';
import {SocialSharing} from "@ionic-native/social-sharing";

@NgModule({
  declarations: [
    RelatoriosShowPage,
  ],
  imports: [
    IonicPageModule.forChild(RelatoriosShowPage),
  ],
  providers:[
    SocialSharing,
  ]
})
export class RelatoriosShowPageModule {}
