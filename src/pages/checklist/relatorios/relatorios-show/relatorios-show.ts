import {AfterViewInit, Component, ElementRef, NgModule, OnInit, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {LotesProvider} from "../../../../providers/produtores/lotes";
import {LOCALE_ID} from '@angular/core';
import {Chart} from 'chart.js';
import {File} from "@ionic-native/file";
import {SocialSharing} from "@ionic-native/social-sharing";

import domtoimage from 'dom-to-image';
import {AlertMessagesProvider} from "../../../../providers/alert-messages/alert-messages";
import {PeriodosProvider} from "../../../../providers/periodos/periodos";
import {EmpresaProvider} from "../../../../providers/empresa/empresa";

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {FileOpener} from "@ionic-native/file-opener";
import {TecnicoProvider} from "../../../../providers/tecnico/tecnico";
import * as moment from "moment";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
	selector: 'page-relatorios-show',
	templateUrl: 'relatorios-show.html',
})
@NgModule({
	providers: [{provide: LOCALE_ID, useValue: 'pt-BR'}]
})
export class RelatoriosShowPage implements OnInit, AfterViewInit {
	@ViewChild('pesoCanvas') testeCanvas: ElementRef;

	private dados;
	private questionarios: Array<any>;
	private totais;
	private local;
	private considerar_cotacao;
	private respostas_nulas = false;
	private pgto_chk_kg;
	private tecnico;
	protected now = moment().format('DD/MM/YYYY h:mm');

	conformes: number = 0;
	naoConformes: number = 0;
	naoSeAplica: number = 0;
	chartVar: any;

	private respostasPdf: Array<any> = [];
	private respostasPdfOriginais: Array<any> = [];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private loteService: LotesProvider,
		private periodoService: PeriodosProvider,
		private file: File,
		private socialSharing: SocialSharing,
		private messageService: AlertMessagesProvider,
		private empresaProvider: EmpresaProvider,
		private platform: Platform,
		private fileOpener: FileOpener,
		private tecnicoProvider: TecnicoProvider,
	) {
		this.empresaProvider.getRespostasNulasChk().then(data => {
			this.respostas_nulas = data;
		});

		this.empresaProvider.getParamPgtoChkKg().then((data) => {
			this.pgto_chk_kg = data;
		}).catch((err) => {
			console.error('Falha ao buscar parametro da empresa', err);
		});

		this.tecnicoProvider.get().then(tecnico => {
			this.tecnico = tecnico;
		})
	}

	ngOnInit() {
		let id = this.navParams.get('id');
		let local = this.navParams.get('local');
		this.local = local;

		if (local == 'lote') {
			this.empresaProvider.getParamConsiderarCotacao().then((cotacao: any) => {
				this.considerar_cotacao = cotacao;

				this.loteService.getDadosLote(id, cotacao).then((data: any) => {
					this.dados = data;
				}).catch((err) => {
					console.error('erro ao consultar cabeçalho do lote', err);
				});

				this.loteService.getChecklistsComTotais(id, cotacao).then((data: Array<any>) => {
					this.questionarios = data;
					/*Carrega Dados para imprimir o relatório*/
					this.loteService.getChecklistsRespostasLote(id).then((respostas: Array<any>) => {
						this.respostasPdf = respostas;
					})
				}).catch((err) => {
					console.error('erro ao consultar cabeçalho do lote', err);
				});

				this.loteService.getTotaisDoLote(id).then((data: Array<any>) => {
					this.totais = data;
				}).catch((err) => {
					console.error('erro ao consultar cabeçalho do lote', err);
				});
			});
		} else {
			this.empresaProvider.getParamConsiderarCotacao().then((data) => {

				this.considerar_cotacao = data;

				this.empresaProvider.getParamAgruparChks().then((agrupar_chk) => {
					this.periodoService.getDadosPeriodo(id, this.considerar_cotacao, agrupar_chk).then((data: any) => {
						this.dados = data;
					}).catch((err) => {
						console.error('erro ao consultar cabeçalho do periodo', err);
					});

					this.periodoService.getChecklistsComTotais(id, this.considerar_cotacao, agrupar_chk).then((data: Array<any>) => {
						this.questionarios = data;

						/*Carrega Dados para imprimir o relatório*/
						this.periodoService.getChecklistsRespostasPeriodo(id, agrupar_chk).then((respostas: Array<any>) => {
							this.respostasPdf = respostas;
						})

					}).catch((err) => {
						console.error('erro ao consultar cabeçalho do periodo', err);
					});

					this.periodoService.getTotaisDoPeriodo(id, this.considerar_cotacao, agrupar_chk).then((data: Array<any>) => {
						this.totais = data;
					}).catch((err) => {
						console.error('erro ao consultar cabeçalho do periodo', err);
					});
				});
			}).catch((err) => {
				console.error('Falha ao buscar parametro da empresa', err);
			});
		}
	}

	ngAfterViewInit() {
		let id = this.navParams.get('id');
		let local = this.navParams.get('local');

		if (local == 'lote') {
			this.loteService.getTotaisDoLote(id).then((data: any) => {
				this.conformes = data.total_conformes;
				this.naoConformes = data.total_nao_conformes;
				this.naoSeAplica = data.total_nao_se_aplica;
				this.showChart();

			}).catch((err) => {
				console.error('erro ao consultar cabeçalho do lote', err);
			});
		} else {
			/*TO-DO detalhes do período*/
			this.empresaProvider.getParamAgruparChks().then((agrupar_chk) => {
				this.periodoService.getTotaisDoPeriodo(id, this.considerar_cotacao, agrupar_chk).then((data: any) => {
					this.conformes = data.total_conformes;
					this.naoConformes = data.total_nao_conformes;
					this.naoSeAplica = data.total_nao_se_aplica > 0 ? data.total_nao_se_aplica : 0;
					this.showChart();

				}).catch((err) => {
					console.error('erro ao consultar cabeçalho do lote', err);
				});
			});
		}
	}

	showChart() {
		var total_perguntas = 0;
		this.empresaProvider.getRespostasNulasChk().then(result => {
			total_perguntas = this.naoConformes + this.naoSeAplica + this.conformes;
			let cores = [];
			let legendas = [];
			let dados = [];
			if (result) {
				cores = [
					'rgba(245, 61, 61, 1)',
					'rgba(212, 212, 212, 1)',
					'rgba(28, 176, 67, 1)'
				];

				legendas = [
					'Não Conformes %',
					'Não Se Aplica %',
					'Conformes %'
				];

				dados = [
					parseFloat(((this.naoConformes * 100) / total_perguntas).toFixed(2)),
					parseFloat(((this.naoSeAplica * 100) / total_perguntas).toFixed(2)),
					parseFloat(((this.conformes * 100) / total_perguntas).toFixed(2)),
				];
			} else {
				total_perguntas = this.naoConformes + this.conformes;
				cores = [
					'rgba(245, 61, 61, 1)',
					'rgba(28, 176, 67, 1)'
				];

				legendas = [
					'Não Conformes %',
					'Conformes %'
				];

				dados = [
					parseFloat(((this.naoConformes * 100) / total_perguntas).toFixed(2)),
					parseFloat(((this.conformes * 100) / total_perguntas).toFixed(2)),
				];
			}

			this.chartVar = new Chart(this.testeCanvas.nativeElement, {
				type: 'pie',
				data: {
					datasets: [{
						data: dados,
						backgroundColor: cores
					}],
					labels: legendas
				},
				options: {
					legend: {
						display: false
					},
					tooltips: {
						enabled: true
					}
				}

			})

		});

	}

	compartilhar() {
		let loading = this.messageService.showLoading('Gerando relatório...');
		let local = this.navParams.get('local');
		let message = null;
		if (local == 'lote') {
			message = `Relatório do lote ${this.dados.nro_lote}`;
		} else {
			message = `Relatório do intervalo ${this.dados.intervalo}`;
		}

		domtoimage.toPng(document.getElementById('print-to-share'), {quality: 0.95})
			.then((dataUrl) => {
				this.socialSharing.shareViaWhatsApp(message, dataUrl, null).catch(err => {
					console.error('erro ao compartilhar', err);
				});
				loading.dismiss();
			});
	}

	compartilharPdf() {


		// let id = this.navParams.get('id');
		// if (this.respostasPdf.length == 0) {
		//   this.empresaProvider.getParamAgruparChks().then((agrupar_chk) => {
		//     /*Carrega Dados para imprimir o relatório*/
		//     this.periodoService.getChecklistsRespostasPeriodo(id, agrupar_chk).then((respostas: Array<any>) => {
		//       this.respostasPdf = respostas;
		//       this.geraPdf();
		//     })
		//   })
		// }else{
		this.geraPdf();
		// }
	}

	geraPdf() {
		let totalizador_geral;
		let body_report = [];
		let local_text, tipo_aplicacao_text;
		let loading = this.messageService.showLoading('Gerando relatório em PDF ...');
		let assinatura_chk = null;
		/*Monta array da tabela*/
		try {
			this.questionarios.forEach((avaliacao) => {
				if (avaliacao.assinatura != null) {
					assinatura_chk = avaliacao.assinatura;
				}
				/*
				* Faz uma copia do this.respostasPdf toda a vez que passa pelo loop da avaliação para não filtrar um array ja
				* filtrado e ocorrer o erro de não encontrar as respostas dentro do array
				* */
				let respostasPdfFiltro: Array<any> = JSON.parse(JSON.stringify(this.respostasPdf));
				let respostas: Array<any> = [];
				let totalizadores;
				let count = 1;

				respostas.push(['#', 'Pergunta', 'Resposta', 'Pts.']);
				respostasPdfFiltro.filter((resposta: any) => {

					if (avaliacao.id_web != null && (avaliacao.id_web == resposta.avaliacao_id_web)) {
						respostas.push([count++, resposta.descricao, resposta.resposta, resposta.valor]);
					} else if (avaliacao.id == resposta.avaliacao_id) {
						respostas.push([count++, resposta.descricao, resposta.resposta, resposta.valor]);
					}
				});
				let total_pts_prod = 0;
				let total_pts_prod_geral = 0;
				total_pts_prod = (avaliacao.pts_total_conf - avaliacao.pts_total_n_conf);
				total_pts_prod_geral = (this.dados.pts_total_conf - this.dados.pts_total_n_conf);


				if (this.pgto_chk_kg) {
					if (this.considerar_cotacao) {
						let valor_total_chk = ((avaliacao.valor_total_chk * avaliacao.total_peso_av) * ((avaliacao.peso_chk * 100) / avaliacao.total_peso_av)) / 100;
						let valor_total_prod = 0;
						let valor_total_prod_geral = 0;

						let valor_chk = (this.dados.valor_total_chk * this.dados.peso_chk);
						let peso_atingido = 0;
						let pts_total_prod = 0;
						let peso_total_prod = 0;

						if (this.local == 'lote') {
							valor_total_prod_geral = ((this.dados.valor_total_chk * this.dados.peso_chk) * this.dados.total_peso_av);
							valor_total_prod = ((this.dados.valor_total_chk * this.dados.peso_chk) * this.dados.total_peso_av);
							peso_atingido = (this.dados.total_peso_av);
							pts_total_prod = ((this.dados.pts_total_conf - this.dados.pts_total_n_conf) / this.questionarios.length);
							peso_total_prod = this.dados.total_peso_av;
						} else {
							valor_total_prod_geral = (this.dados.peso_total_conf * this.dados.valor_total_chk);
							valor_total_prod = (((avaliacao.valor_total_chk * avaliacao.total_peso_av) * ((avaliacao.peso_chk * 100) / avaliacao.total_peso_av) / 100) * (avaliacao.pts_total_conf - avaliacao.pts_total_n_conf)) / 100;
							peso_atingido = this.dados.peso_total_conf;
							pts_total_prod = ((avaliacao.pts_total_conf - avaliacao.pts_total_n_conf) * 1);
							peso_total_prod = ((avaliacao.peso_chk * (avaliacao.pts_total_conf - avaliacao.pts_total_n_conf)) / 100);
						}

						totalizadores = [
							[{text: 'Conformes:', bold: true}, avaliacao.conformes],
							[{text: 'Não Conformes:', bold: true}, avaliacao.nao_conformes],
							[{
								text: 'Pontuação Total do Produtor:',
								bold: true
							}, `${(pts_total_prod).toFixed(2)}%`],
							[{
								text: 'Peso Total do Checklist:',
								bold: true
							}, `KG ${(avaliacao.peso_chk).toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Peso Atingido:',
								bold: true
							}, `KG ${(peso_total_prod).toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Valor Total do Checklist:',
								bold: true
							}, `R$ ${valor_total_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{
								text: 'Total Recebido pelo Produtor:',
								bold: true
							}, `R$ ${valor_total_prod.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{text: 'Técnico Avaliador:', bold: true}, (this.tecnico.name || ' *N/C ').toUpperCase()],
							[{
								text: 'Aplicado Em:',
								bold: true
							}, moment(avaliacao.data_aplicacao, "YYYY-MM-DD").format('DD/MM/YYYY')],
						];


						totalizador_geral = [
							[{
								text: 'Pontuação Total do Produtor:',
								bold: true
							}, `${(this.dados.pts_total_conf - this.dados.pts_total_n_conf).toFixed(2)}%`],
							[{
								text: 'Valor Total do Checklist:',
								bold: true
							}, `R$ ${valor_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{
								text: 'Peso Total Avaliações:',
								bold: true
							}, `KG ${this.dados.peso_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Peso Total Atingido:',
								bold: true
							}, `KG ${peso_atingido.toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Total Recebido pelo Produtor:',
								bold: true
							}, `R$ ${valor_total_prod_geral.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
						]
					} else {
						totalizadores = [
							[{text: 'Conformes:', bold: true}, avaliacao.conformes],
							[{text: 'Não Conformes:', bold: true}, avaliacao.nao_conformes],
							[{
								text: 'Pontuação Total do Produtor:',
								bold: true
							}, `${(total_pts_prod).toFixed(2)}%`],
							[{
								text: 'Peso Total do Checklist:',
								bold: true
							}, `KG ${(avaliacao.peso_chk).toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Peso Atingido:',
								bold: true
							}, `KG ${(((avaliacao.peso_chk * (avaliacao.pts_total_conf - avaliacao.pts_total_n_conf)) / 100)).toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Valor Total do Checklist:',
								bold: true
							}, `R$ ${avaliacao.valor_total_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{
								text: 'Total Recebido pelo Produtor:',
								bold: true
							}, `R$ ${(((avaliacao.pts_total_conf - avaliacao.pts_total_n_conf) * avaliacao.valor_total_chk) / 100).toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{text: 'Técnico Avaliador:', bold: true}, (this.tecnico.name || ' *N/C ').toUpperCase()],
							[{
								text: 'Aplicado Em:',
								bold: true
							}, moment(avaliacao.data_aplicacao, "YYYY-MM-DD").format('DD/MM/YYYY')],
						];

						totalizador_geral = [
							[{
								text: 'Pontuação Total do Produtor:',
								bold: true
							}, `${(total_pts_prod_geral).toFixed(2)}%`],
							[{
								text: 'Valor Total do Checklist:',
								bold: true
							}, `R$ ${this.dados.valor_total_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
							[{
								text: 'Peso Total Avaliações:',
								bold: true
							}, `KG ${this.dados.peso_chk.toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Peso Total Atingido:',
								bold: true
							}, `KG ${((this.dados.peso_chk * (this.dados.pts_total_conf - this.dados.pts_total_n_conf)) / 100).toLocaleString('pt-BR', {
								minimumFractionDigits: 3,
								maximumFractionDigits: 3
							})}`],
							[{
								text: 'Total Recebido pelo Produtor:',
								bold: true
							}, `R$ ${(((this.dados.pts_total_conf - this.dados.pts_total_n_conf) * this.dados.valor_total_chk) / 100).toLocaleString('pt-BR', {
								minimumFractionDigits: 2,
								maximumFractionDigits: 2
							})}`],
						]
					}

				} else {
					totalizadores = [
						[{text: 'Conformes:', bold: true}, avaliacao.conformes],
						[{text: 'Não Conformes:', bold: true}, avaliacao.nao_conformes],
						[{
							text: 'Pontuação Total do Produtor:',
							bold: true
						}, `${(total_pts_prod).toFixed(2)}%`],
						[{
							text: 'Valor Total do Checklist:',
							bold: true
						}, `R$ ${avaliacao.valor_total_chk.toLocaleString('pt-BR', {
							minimumFractionDigits: 2,
							maximumFractionDigits: 2
						})}`],
						[{
							text: 'Total Recebido pelo Produtor:',
							bold: true
						}, `R$ ${(((avaliacao.pts_total_conf - avaliacao.pts_total_n_conf) * avaliacao.valor_total_chk) / 100).toLocaleString('pt-BR', {
							minimumFractionDigits: 2,
							maximumFractionDigits: 2
						})}`],
						[{text: 'Técnico Avaliador:', bold: true}, (this.tecnico.name || ' *N/C ').toUpperCase()],
						[{
							text: 'Aplicado Em:',
							bold: true
						}, moment(avaliacao.data_aplicacao, "YYYY-MM-DD").format('DD/MM/YYYY')],
					];

					totalizador_geral = [
						[{
							text: 'Pontuação Total do Produtor:',
							bold: true
						}, `${(total_pts_prod_geral).toFixed(2)}%`],
						[{
							text: 'Valor Total do Checklist:',
							bold: true
						}, `R$ ${this.dados.valor_total_chk.toLocaleString('pt-BR', {
							minimumFractionDigits: 2,
							maximumFractionDigits: 2
						})}`],
						[{
							text: 'Total Recebido pelo Produtor:',
							bold: true
						}, `R$ ${(((this.dados.pts_total_conf - this.dados.pts_total_n_conf) * this.dados.valor_total_chk) / 100).toLocaleString('pt-BR', {
							minimumFractionDigits: 2,
							maximumFractionDigits: 2
						})}`],
					]
				}

				body_report.push({
						alignment: 'center',
						margin: [0, 5, 0, 5],
						text: `RESULTADO DO CHECKLIST ${(avaliacao.descricao || ' *N/C ').toUpperCase()}`,
					},
					{
						style: 'table_respostas',
						table: {
							widths: [15, '*', 80, 35],
							body: respostas
						}
					},
					{
						style: 'table_totalizador_chk',
						table: {
							widths: ['*', '*'],
							body: totalizadores
						}
					});

			});

			if (this.local == 'lote') {
				local_text = 'LOTE';
				tipo_aplicacao_text = `#${this.dados.nro_lote}`
			} else {
				local_text = 'PERÍODO';
				tipo_aplicacao_text = (this.dados.intervalo || ' *N/C ').toUpperCase()
			}

			let col_sign: any = {
				text: '----------------------------------------------------------------',
				bold: true
			};

			if (assinatura_chk != null) {
				col_sign = {
					image: assinatura_chk,
					fit: [200, 300]
				};
			}

			this.empresaProvider.get().then((empresa: any) => {
				var docDefinition = {
					content: [
						{
							style: 'table_header',
							table: {
								widths: ['*', 'auto'],
								headerRows: 2,
								body: [
									[
										{
											alignment: 'center',
											text: `${(empresa.razao_social || ' *N/C ').toUpperCase()}\n CNPJ: ${empresa.cnpj}\n ${(this.tecnico.email || ' *N/C ').toUpperCase()}`
										},
										[
											{
												style: 'table_header_right',
												widths: ['*', '*'],
												table: {
													body: [
														[
															{
																border: [false, false, false, false],
																bold: true,
																text: local_text
															},
															{
																border: [false, false, false, false],
																alignment: 'right',
																text: tipo_aplicacao_text,
															}
														],
														[
															{
																border: [false, true, false, true],
																bold: true,
																text: 'TÉCNICO'
															},
															{
																border: [false, true, false, true],
																alignment: 'right',
																text: (this.tecnico.name || ' *N/C ').toUpperCase()
															}
														],
														[
															{
																border: [false, false, false, false],
																bold: true,
																text: 'DATA IMPRESSÃO'
															},
															{
																border: [false, false, false, false],
																alignment: 'right',
																text: moment().format('DD/MM/YYYY')
															}
														]
													]
												},
												layout: {
													fillColor: function (rowIndex, node, columnIndex) {
														return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
													}
												}
											}
										],
									],
									[
										{
											alignment: 'center',
											bold: true,
											text: `PRODUTOR: ${(this.dados.produtor_nome || ' *N/C ').toUpperCase()} - ${(this.dados.propriedade_descricao || ' *N/C ').toUpperCase()} - ${(this.dados.galpao_descricao || ' *N/C ').toUpperCase()}`,
											colSpan: 2
										},
										{}
									],
								]
							}
						},
						body_report,
						{
							alignment: 'center',
							text: '----------------------------------------------------------------------------------------------------------------------------------------------------------'
						},
						{
							style: 'table_totalizadores',
							table: {
								widths: ['*', '*'],
								body: totalizador_geral
							}
						},
						{
							style: 'assinatura',
							alignment: 'center',
							columns: [
								col_sign,
								{
									text: '----------------------------------------------------------------',
									bold: true
								}
							]
						}, {
							alignment: 'center',
							columns: [
								{
									text: (this.dados.produtor_nome || ' *N/C ').toUpperCase(),
									bold: true
								},
								{
									text: (this.tecnico.name || ' *N/C ').toUpperCase(),
									bold: true
								}
							]
						},
					],
					styles: {
						table_header: {
							fontSize: 10,
							margin: [0, 0, 0, 0]
						},
						table_header_right: {
							fontSize: 8,
							margin: [0, 0, 0, 0]
						},
						table_respostas: {
							margin: [0, 0, 0, 0]
						},
						table_totalizador_chk: {
							margin: [0, -1, 0, 0]
						},
						table_totalizadores: {
							margin: [0, 5, 0, 0]
						},
						assinatura: {
							margin: [0, 10, 0, 5]
						}
					},
					defaultStyle: {
						// alignment: 'justify'
					}
				};

				// const pdfDocGenerator = pdfMake.createPdf(docDefinition).open({}, window);
				let fileName = `relatorio_checklist_${Date.now()}.pdf`;
				pdfMake.createPdf(docDefinition).getBuffer((buffer) => {
					var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
					let binaryArray = utf8.buffer; // Convert to Binary...

					let dirPath = "";
					if (this.platform.is('android')) {
						// dirPath = this.file.externalRootDirectory;
						dirPath = this.file.dataDirectory;
					} else if (this.platform.is('ios')) {
						dirPath = this.file.documentsDirectory;
					}

					let dirName = '';

					this.file.createDir(dirPath, dirName, true).then((dirEntry) => {
						let saveDir = dirPath + '/' + dirName + '/';
						this.file.createFile(saveDir, fileName, true).then((fileEntry) => {
							fileEntry.createWriter((fileWriter) => {
								fileWriter.onwriteend = () => {
									// this.hideLoading();
									// this.showReportAlert('Report downloaded', saveDir + fileName);
									this.fileOpener.open(saveDir + fileName, 'application/pdf')
										.then(() => {
											loading.dismiss();
										})
										.catch(e => {
											console.error('Error openening file', e)
											if (e.status) {
												this.messageService.showAlert('Ops...', 'Você precisa de um leitor de PDF para ver este arquivo.');
												loading.dismiss();
											}
										});
								};
								fileWriter.onerror = (e) => {
									loading.dismiss();
									console.error('Cannot write report ', e);
								};
								fileWriter.write(binaryArray);
							});
						}).catch((error) => {
							loading.dismiss();
							this.messageService.showToastError('Falha ao abrir arquivo');
							console.error('Cannot create file ', error);
						});
					}).catch((error) => {
						loading.dismiss();
						this.messageService.showToastError('Falha ao abrir local de armazenamento do arquivo');
						console.error('Cannot create folder ', error);
					});
				})
			})
		} catch (err) {
			console.error('falha ao gerar o PDF', err);
			this.messageService.showToastError('Ops, houve uma falha ao gerar o PDF.');
			loading.dismiss();
		}
	}
}
