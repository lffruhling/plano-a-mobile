import {Component, NgModule, OnInit} from '@angular/core';
import {FabContainer, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {LotesProvider} from "../../../providers/produtores/lotes";
import { LOCALE_ID } from '@angular/core';
import {
  CalendarModal,
  CalendarModalOptions
} from 'ion2-calendar'
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {PeriodosProvider} from "../../../providers/periodos/periodos";
import {EmpresaProvider} from "../../../providers/empresa/empresa";

@IonicPage()
@Component({
  selector: 'page-relatorios',
  templateUrl: 'relatorios.html',
})
@NgModule({
  providers: [{provide: LOCALE_ID, useValue: 'pt-BR'}],
})
export class RelatoriosPage implements OnInit{
  private lotes = new Array<any>();
  private lotesOriginal = new Array<any>();
  private periodos = new Array<any>();
  private periodosOriginal = new Array<any>();

  private isSearchbarOpened = false;
  private ordenacao = true;
  private direcao;
  private datas;
  private chk_periodo;
  private pgto_chk_kg;
  private considerar_cotacao;

  dateRange: {
    from: Date;
    to: Date
  } = {
    from: new Date( Date.now() + (-7*(24*3600*1000))),
    to: new Date()
  };
  tipo_relatorio: string = "lote";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loteProvider: LotesProvider,
    private periodoProvider: PeriodosProvider,
    public modalCtrl: ModalController,
    private messageService:AlertMessagesProvider,
    private empresaProvider: EmpresaProvider,
  ) {
  }

  ngOnInit(){
    this.empresaProvider.getParamChkPeriodo().then((data)=>{
      this.chk_periodo = data;
      if(data){
        this.tipo_relatorio = 'periodo';
      }
    }).catch((err)=>{
      console.error('Falha ao buscar parametro da empresa', err);
    });

    this.empresaProvider.getParamPgtoChkKg().then((data)=>{
      this.pgto_chk_kg = data;
    }).catch((err)=>{
      console.error('Falha ao buscar parametro da empresa', err);
    });

    this.empresaProvider.getParamConsiderarCotacao().then((considerar_cotacao)=>{
      this.considerar_cotacao = considerar_cotacao;

      this.loteProvider.getLotesRespondidos(null, null, this.considerar_cotacao).then((data:any) =>{
        this.lotes = data;
        this.lotesOriginal = data;
      }).catch((err)=>{
        console.error('falha ao consultar lotes com respostas', err);
      });

      this.empresaProvider.getParamAgruparChks().then((agrupa_chk)=>{
        this.periodoProvider.getPeriodosRespondidos(null,null, this.considerar_cotacao, agrupa_chk).then((periodos:any) =>{
          this.periodos = periodos;
          this.periodosOriginal = periodos;
        }).catch((err)=>{
          console.error('falha ao consultar periodos com respostas', err);
        });
      });

    }).catch((err)=>{
      console.error('Falha ao buscar parametro da empresa', err);
    });
  }

  show(id){
    this.navCtrl.push('RelatoriosShowPage',{id:id, local:this.tipo_relatorio});
  }

  mostraCampo(fab: FabContainer){
    this.isSearchbarOpened=true;
    fab.close();
  }

  buscarRelatorios(ev: any) {
    /*pega valor a ser pesquisado*/
    const val = ev.target.value;

    /*se o valor pesquisado for vazio, não filtra*/
    if(this.tipo_relatorio == 'lote'){
      if (val && val.trim() != '') {
        this.lotes = this.lotes.filter((lote) => {
          return (lote.produtor_nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        /*Voltar para todos os itens*/
        this.lotes = JSON.parse(JSON.stringify(this.lotesOriginal));
      }
    }else{
      if (val && val.trim() != '') {
        this.periodos = this.periodos.filter((periodo) => {
          return (periodo.produtor_nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        /*Voltar para todos os itens*/
        this.periodos = JSON.parse(JSON.stringify(this.periodosOriginal));
      }
    }
  }

  abrirCalendario(fab: FabContainer) {
    const options: CalendarModalOptions = {
      canBackwardsSelected: true,
      pickMode: 'range',
      title: 'Calendário',
      defaultDateRange: this.dateRange,
      doneLabel: 'Aplicar',
      closeLabel: 'Cancelar',
      // closeIcon: true,
      weekdays:['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthFormat:'MM/YYYY'
    };

    let myCalendar = this.modalCtrl.create(CalendarModal, {
      options: options
    });

    myCalendar.present();

    myCalendar.onDidDismiss((date, type) => {
      if (type === 'done') {
        this.dateRange = Object.assign({}, {
          from: date.from.dateObj,
          to: date.to.dateObj,
        });
        this.datas = date;
      }else{
        this.datas = null;
      }

      if(this.tipo_relatorio == 'lote') {
        let loading = this.messageService.showLoading('Buscando lotes...');
        this.loteProvider.getLotesRespondidos(this.datas, this.direcao).then((result: any) => {
          this.lotes = result;
          loading.dismiss();
        }).catch(err => {
          console.error('falha ao consultar lotes por datas', err)
          loading.dismiss();
        });
      }else{
        let loading = this.messageService.showLoading('Buscando períodos...');
        this.periodoProvider.getPeriodosRespondidos(this.datas, this.direcao, this.considerar_cotacao).then((result: any) => {
          this.periodos = result;
          loading.dismiss();
        }).catch(err => {
          console.error('falha ao consultar períodos por datas', err)
          loading.dismiss();
        });
      }
    });
    fab.close();
  }

  ordenaAZ(fab: FabContainer){

    this.ordenacao = !this.ordenacao;

    if(this.ordenacao){
      this.direcao = 'DESC' ;
    }else{
      this.direcao = 'ASC';
    }

    if(this.tipo_relatorio == 'lote') {
      let loading = this.messageService.showLoading('Buscando lotes...');
      this.loteProvider.getLotesRespondidos(this.datas, this.direcao).then((result: any) => {
        this.lotes = result;
        loading.dismiss();
      }).catch(err => {
        console.error('falha ao ordenar lotes por nome', err);
        loading.dismiss();
      });
    }else{
      let loading = this.messageService.showLoading('Buscando períodos...');
      this.periodoProvider.getPeriodosRespondidos(this.datas, this.direcao, this.considerar_cotacao).then((result: any) => {
        this.periodos = result;
        loading.dismiss();
      }).catch(err => {
        console.error('falha ao consultar períodos por datas', err)
        loading.dismiss();
      });
    }
    fab.close();
  }

  limpaFiltros(fab: FabContainer){
    if(this.tipo_relatorio == 'lote') {
      let loading = this.messageService.showLoading('Buscando lotes...');
      /*Voltar para todos os itens*/
      this.lotes = JSON.parse(JSON.stringify(this.lotesOriginal));
      this.isSearchbarOpened = false;
      loading.dismiss();
    }else {
      let loading = this.messageService.showLoading('Buscando períodos...');
      /*Voltar para todos os itens*/
      this.periodos = JSON.parse(JSON.stringify(this.periodosOriginal));
      this.isSearchbarOpened = false;
      loading.dismiss();
    }
    fab.close();
  }

}
