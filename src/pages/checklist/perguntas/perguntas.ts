import {Component, ViewChild} from '@angular/core';
import {
	Content,
	Events,
	IonicPage,
	ModalController,
	NavController,
	NavParams,
	ViewController,
	Platform, AlertController
} from 'ionic-angular';
import {PerguntasProvider} from "../../../providers/checklist/perguntas";
import {Camera} from "@ionic-native/camera";
import {FileTransfer} from "@ionic-native/file-transfer";
import {Storage} from "@ionic/storage";
import {AuthProvider} from "../../../providers/auth/auth";
import * as moment from "moment";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../../../providers/check-network/check-network";
import {FormArray, FormBuilder, FormControl} from "@angular/forms";
import {CameraProvider} from "../../../providers/camera/camera";
import {UniqueIdProvider} from "../../../providers/unique-id/unique-id";
import {LotesProvider} from "../../../providers/produtores/lotes";
import {ModalGeraAcaoPage} from "../../modal-gera-acao/modal-gera-acao";
import {AcoesProvider} from "../../../providers/acoes/acoes";
import {HttpClient} from "@angular/common/http";
import {EmpresaProvider} from "../../../providers/empresa/empresa";
import {PeriodosProvider} from "../../../providers/periodos/periodos";
import {ModalSignPage} from "../../modal-sign/modal-sign";
import {AvaliacoesChecklistsProvider} from "../../../providers/checklist/avaliacoes-checklists";
import {UtilsService} from "../../../services/utils.service";
import {FirestoreDatabaseProvider} from "../../../providers/firestore-database/firestore-database";
import {TecnicoProvider} from "../../../providers/tecnico/tecnico";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../../../providers/database/database";
import {ImageResizer} from "@ionic-native/image-resizer";
import {Base64} from "@ionic-native/base64";
import {DomSanitizer} from "@angular/platform-browser";
import {GeolocationProvider} from "../../../providers/geolocation/geolocation";
import {ParametroAcoesProvider} from "../../../providers/acoes/parametroAcoes";


@IonicPage()
@Component({
  selector: 'page-perguntas',
  templateUrl: 'perguntas.html',
})
export class PerguntasPage {
  @ViewChild(Content) content: Content;

  protected avaliacao_id_aux;
  protected checklist_id;
  protected propriedade_id;
  protected galpao_id;
  protected intervalo_id;
  protected lote_id = null;
  protected periodo_id = null;
  protected grupo_aplicacao_id = null;
  protected lote;
  protected periodo;
  protected descricao;
  protected perguntas;
  protected consultaAux;
  protected descartaAux;
  protected respostas_nulas = false;
  protected total_respostas = 0.0;
  protected showAlertClose = true;

  private urlApi;
  protected classePergunta = "danger";
  protected retornoModal = false;
  protected mobile_key;
  protected obsGeolocation;
  protected latitude;
  protected longitude;
  protected totalResposta = 0;

  respostasFrom = this.fb.group({
    respostas: this.fb.array([])
  });

  constructor(
    private http: HttpClient,
    private navCtrl: NavController,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private camera: Camera,
    private transfer: FileTransfer,
    private storage: Storage,
    private perguntasProvider: PerguntasProvider,
    private acaoProvider: AcoesProvider,
    private lotesProvider: LotesProvider,
    private periodoProvider: PeriodosProvider,
    private authProvider: AuthProvider,
    private messageService: AlertMessagesProvider,
    private networkService: CheckNetworkProvider,
    private fb: FormBuilder,
    private cameraService: CameraProvider,
    private uniqueKeyService: UniqueIdProvider,
    private modalCtrl: ModalController,
    private events: Events,
    private empresaProvider: EmpresaProvider,
    private avaliacaoChkProvider: AvaliacoesChecklistsProvider,
    private utils: UtilsService,
    private fbDbProvider: FirestoreDatabaseProvider,
    private tecnicoProvider: TecnicoProvider,
    private dbProvider: DatabaseProvider,
    private imageResizer: ImageResizer,
    private base64: Base64,
    private sanitizer: DomSanitizer,
    private plt: Platform,
    private geolocationProvider: GeolocationProvider,
    private parametroProvider: ParametroAcoesProvider,
	private alertCtrl: AlertController,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/checklist/resposta`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/checklist/resposta`;

          }
        });
      }
    });

    this.mobile_key = this.uniqueKeyService.generateKey();

    this.empresaProvider.getRespostasNulasChk().then((data) => {
      this.respostas_nulas = data;
    });

    this.geolocationProvider.getGeolocation();

	  this.registerBackButton();
  }


  ionViewDidLoad() {
    this.checklist_id = this.navParams.get('checklist_id');
    this.propriedade_id = this.navParams.get('propriedade_id');
    this.galpao_id = this.navParams.get('galpao_id');
    this.intervalo_id = this.navParams.get('intervalo_id');
    this.lote_id = this.navParams.get('lote_id');
    this.periodo_id = this.navParams.get('periodo_id');
    this.grupo_aplicacao_id = this.navParams.get('grupo_aplicacao_id');

    this.descricao = this.navParams.get('descricao');
    this.consultaAux = this.navParams.get('consultaAux');
    this.descartaAux = this.navParams.get('descartaAux');

    if (this.lote_id > 0) {
      this.lotesProvider.get(this.lote_id).then((lote) => {
        this.lote = lote;

      });
    }

    if (this.periodo_id) {
      this.periodoProvider.get(this.periodo_id).then((periodo) => {
        this.periodo = periodo;

      });
    }

    let avaliacao = [
      this.uniqueKeyService.generateKey(),
      this.checklist_id,
      this.lote_id,
      this.periodo_id,
      this.grupo_aplicacao_id,
      moment().format('YYYY-MM-DD'),
      0,
      0,
      false,
    ];

    console.log('this.descartaAux', this.descartaAux);
    if(this.descartaAux){
      this.avaliacaoChkProvider.getAvaliacoesAux(this.checklist_id, this.lote_id, this.periodo_id).then((data) => {
        if (data.rows.length > 0) {
          this.avaliacaoChkProvider.deleteRespostasAux(data.rows.item(0).id).then(() => {
            this.avaliacaoChkProvider.deleteAvaliacaoAux(data.rows.item(0).id).then(() => {
              this.criaAvalicaoAux(avaliacao)
            });
          });
        }
      });

    }else{
      this.avaliacaoChkProvider.getAvaliacoesAux(this.checklist_id, this.lote_id, this.periodo_id).then((data) => {
      	console.log('getAvaliacoesAux', data.rows.length);
        if (data.rows.length == 0) {
          this.criaAvalicaoAux(avaliacao)
        } else {
          this.avaliacao_id_aux = data.rows.item(0).id;

          this.carregaRespostasAuto(false);
        }
      });
    }

  }

  criaAvalicaoAux(avaliacao){
    this.avaliacaoChkProvider.salvaAvaliacaoChkAux(avaliacao).then((result) => {
      this.avaliacao_id_aux = result.insertId;

      this.carregaRespostasAuto(true)
    });
  }

  get respostas() {
    return this.respostasFrom.get('respostas') as FormArray;
  }

  carregaRespostasAuto(salva_avalicao_aux) {

    this.perguntasProvider.getChecklistLocal(this.checklist_id).then((perguntas: Array<any>) => {
      this.tecnicoProvider.getParamRespostaAutoChk().then((result) => {
        if (result) {

          let loading = this.messageService.showLoading('Consultando respostas anteriores...');
          this.avaliacaoChkProvider.getRespostasAvaliacao(this.checklist_id, this.intervalo_id, this.propriedade_id).then((respostas_avaliacao: Array<any>) => {

            if(respostas_avaliacao.length > 0){
              let total_perguntas = perguntas.length;
              let data_resposta = moment().format('YYYY-MM-DD');
              this.total_respostas = 0;

              perguntas.forEach((pergunta) => {

                let resposta_auto: Array<any> = respostas_avaliacao.filter(resposta => {
                  return pergunta.id === resposta.pergunta_id;
                });

                if (resposta_auto.length > 0) {
					this.total_respostas += resposta_auto[0].valor;

					resposta_auto[0].data_resposta = data_resposta;
                  resposta_auto[0].mobile_key = this.mobile_key;
                  resposta_auto[0].avaliacao_id_aux = this.avaliacao_id_aux;
                  resposta_auto[0].pergunta_id = pergunta.id;
                  resposta_auto[0].foto = null;

                  if (salva_avalicao_aux) {
                    this.perguntasProvider.salvaRespostaAux(resposta_auto[0]).then((result: any) => {
                      total_perguntas--;
                      this.addPerguntas(pergunta.id, pergunta.parametro_id, this.avaliacao_id_aux, pergunta.valor_max, pergunta.valor_min, pergunta.descricao, data_resposta, result.insertId, resposta_auto[0].valor, resposta_auto[0].resposta, resposta_auto[0].thumb_foto, resposta_auto[0].foto_base_64);

                      if (total_perguntas == 0) {
                        loading.dismiss();

                      }
                    }).catch((err) => {
                      console.error('erro salva resposta aux', err);
                    })
                  } else {
                    total_perguntas--;
                    this.addPerguntas(pergunta.id, pergunta.parametro_id, this.avaliacao_id_aux, pergunta.valor_max, pergunta.valor_min, pergunta.descricao, data_resposta, resposta_auto[0].id, resposta_auto[0].valor, resposta_auto[0].resposta, resposta_auto[0].thumb_foto, resposta_auto[0].foto_base_64);

                    if (total_perguntas == 0) {
                      loading.dismiss();

                    }
                  }

                } else {
                  total_perguntas--;
                  if (total_perguntas == 0) {
                    loading.dismiss();

                  }
                }

              });
            }else{
              let data_resposta = moment().format('YYYY-MM-DD');
              loading.dismiss();
              perguntas.forEach((pergunta) => {
                this.addPerguntas(pergunta.id, pergunta.parametro_id, this.avaliacao_id_aux, pergunta.valor_max, pergunta.valor_min, pergunta.descricao, data_resposta);
              });
            }
          });

        } else {
          let data_resposta = moment().format('YYYY-MM-DD');
          perguntas.forEach((pergunta) => {
            this.addPerguntas(pergunta.id, pergunta.parametro_id, this.avaliacao_id_aux, pergunta.valor_max, pergunta.valor_min, pergunta.descricao, data_resposta);
          });
        }
      }).catch(err => {
        console.error('falha ao consultar dados param resp auto', err)
      });
    });
  }

  addPerguntas(pergunta_id, parametro_id, avaliacao_id, valor_max, valor_min, descricao, data_resposta, resposta_aux_id = null, valor = null, resposta = null, thumb_foto = null, foto_base64 = null) {
    let foto: any;
    foto = 'assets/imgs/camera_800X300.png';
    if (thumb_foto) {
      if (thumb_foto.substring(0, 10) == 'data:image') {
        thumb_foto = this.sanitizer.bypassSecurityTrustUrl(thumb_foto.replace(/\n/g, ''));
        foto = null;
      } else {
        thumb_foto = thumb_foto
      }
    } else {
      thumb_foto = thumb_foto
    }

    /*Consulta tabela AUX no caso de ser uma continuação*/
    if (this.consultaAux) {
      this.perguntasProvider.getRespostaAux(pergunta_id, avaliacao_id).then((result_resp_aux) => {
        if (result_resp_aux) {
          if (result_resp_aux.thumb_foto) {
            if (result_resp_aux.thumb_foto.substring(0, 10) == 'data:image') {
              thumb_foto = this.sanitizer.bypassSecurityTrustUrl(result_resp_aux.thumb_foto.replace(/\n/g, ''));
              foto = null;
            } else {
              thumb_foto = result_resp_aux.thumb_foto
            }
          } else {
            thumb_foto = result_resp_aux.thumb_foto
          }

          this.respostas.push(this.fb.group({
            'pergunta_id': this.fb.control(pergunta_id),
            'parametro_id': this.fb.control(parametro_id),
            'descricao': this.fb.control(descricao),
            'avaliacao_id_aux': this.fb.control(avaliacao_id),
            'data_resposta': this.fb.control(data_resposta),
            'valor_max': this.fb.control(valor_max),
            'valor_min': this.fb.control(valor_min),
            'valor': this.fb.control(result_resp_aux.valor),
            'device_uuid': this.fb.control(''),
            'resposta': this.fb.control(result_resp_aux.resposta),
            'nao_se_aplica': this.fb.control(result_resp_aux.nao_se_aplica),
            'foto': this.fb.control(foto),
            'foto_base64': this.fb.control(result_resp_aux.foto_base64),
            'thumb_foto': this.fb.control(thumb_foto),
            'com_foto': this.fb.control(result_resp_aux.com_foto),
            'mobile_key': new FormControl(result_resp_aux.mobile_key),
            'acao_id': this.fb.control(result_resp_aux.acao_id),
            'isInvalid': this.fb.control(false),
            'com_acao': this.fb.control(result_resp_aux.acao_id > 0),
            'resposta_aux_id': this.fb.control(result_resp_aux.id),
          }));
        } else {
          this.respostas.push(this.fb.group({
            'pergunta_id': this.fb.control(pergunta_id),
            'parametro_id': this.fb.control(parametro_id),
            'descricao': this.fb.control(descricao),
            'avaliacao_id_aux': this.fb.control(avaliacao_id),
            'data_resposta': this.fb.control(data_resposta),
            'valor_max': this.fb.control(valor_max),
            'valor_min': this.fb.control(valor_min),
            'valor': this.fb.control(valor),
            'device_uuid': this.fb.control(''),
            'resposta': this.fb.control(resposta),
            'nao_se_aplica': this.fb.control(null),
            'foto': this.fb.control(foto),
            'foto_base64': this.fb.control(foto_base64),
            'thumb_foto': this.fb.control(null),
            'com_foto': this.fb.control(false),
            'mobile_key': new FormControl(this.mobile_key),
            'acao_id': this.fb.control(0),
            'isInvalid': this.fb.control(null),
            'com_acao': this.fb.control(false),
            'resposta_aux_id': this.fb.control(resposta_aux_id),
          }));
        }
      }).catch(err => {
        console.error('falha ao buscar resposta salva', err);
      });

    } else {
      this.respostas.push(this.fb.group({
        'pergunta_id': this.fb.control(pergunta_id),
        'parametro_id': this.fb.control(parametro_id),
        'descricao': this.fb.control(descricao),
        'avaliacao_id_aux': this.fb.control(avaliacao_id),
        'data_resposta': this.fb.control(data_resposta),
        'valor_max': this.fb.control(valor_max),
        'valor_min': this.fb.control(valor_min),
        'valor': this.fb.control(valor),
        'device_uuid': this.fb.control(''),
        'resposta': this.fb.control(resposta),
        'nao_se_aplica': this.fb.control(null),
        'foto': this.fb.control(foto),
        'foto_base64': this.fb.control(foto_base64),
        'thumb_foto': this.fb.control(thumb_foto),
        'com_foto': this.fb.control(false),
        'mobile_key': new FormControl(this.mobile_key),
        'acao_id': this.fb.control(0),
        'com_acao': this.fb.control(false),
        'isInvalid': this.fb.control(null),
        'resposta_aux_id': this.fb.control(resposta_aux_id),
      }));
    }

    this.obsGeolocation = this.geolocationProvider.getGeolocation().then((geolocation: any) => {
      if (geolocation) {
        this.latitude = geolocation.coords.latitude;
        this.longitude = geolocation.coords.longitude;
      }
    },err => {
      console.error('getGeolocation', err)
    });
  }

  capturaFoto(item) {
    let loading = this.messageService.showLoading('Abrindo a camera...');
    this.utils.checkDisck().then((data) => {

      if (data) {

        let respostasFormArray = this.respostas;

        this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) =>{
        	let foto  ='data:image/jpeg;base64,'+base64File;
          respostasFormArray.at(item).patchValue({'thumb_foto': foto});
          respostasFormArray.at(item).patchValue({'foto_base64': foto, 'com_foto': false});
          this.salvaRespostaAux(this.respostasFrom.value.respostas[item], item, respostasFormArray);
          loading.dismiss();

        }).catch(error => {
			loading.dismiss();
	  	});


      } else {
        loading.dismiss();
      }
    });

  }

  responderPergunta(item, value, nsa = null) {
    let respostasFormArray = this.respostas;

    respostasFormArray.at(item).patchValue({'resposta': value});

    if (value == 1) {
      respostasFormArray.at(item).patchValue({'valor': this.respostasFrom.value.respostas[item].valor_max});
    } else {
      respostasFormArray.at(item).patchValue({'valor': this.respostasFrom.value.respostas[item].valor_min});
    }

    if (nsa == 1) {
      respostasFormArray.at(item).patchValue({'nao_se_aplica': true});
      respostasFormArray.at(item).patchValue({'valor': this.respostasFrom.value.respostas[item].valor_max});
    } else {
      respostasFormArray.at(item).patchValue({'nao_se_aplica': null});
    }

    this.salvaRespostaAux(this.respostasFrom.value.respostas[item], item, respostasFormArray);

    this.total_respostas = 0;
    this.respostasFrom.value.respostas.forEach((pergunta) => {
      this.total_respostas += pergunta.valor;
    });
  }

  private salvaRespostaAux(resposta: any, item, respostasFormArray) {
    if (resposta.resposta_aux_id) {
      this.perguntasProvider.atualizaRespostaAux(resposta).catch((err) => {
        console.error('falha ao atualizar resposta aux', err);
      })
    } else {
      this.perguntasProvider.salvaRespostaAux(resposta).then((result: any) => {
        /*Salvar o id da resposta aux*/
        respostasFormArray.at(item).patchValue({'resposta_aux_id': result.insertId});
      }).catch((err) => {
        console.error('erro salva resposta aux', err);
      })
    }
  }

  abreModalAcao(pergunta_id, parametro_id, item) {
    let mobile_key = this.uniqueKeyService.generateKey();
    let grupo_id;
    let ponto_id;
    this.empresaProvider.getParamChkPeriodo().then((result) => {
      let modal;

      this.parametroProvider.find(parametro_id).then((parametro) =>{

        if(parametro){
          grupo_id = parametro.grupo_acao_id;
          ponto_id = parametro.ponto_acao_id;
        }

        if (result) {
          modal = this.modalCtrl.create(ModalGeraAcaoPage, {
            pergunta_id: pergunta_id,
            grupo_id: grupo_id,
            ponto_id: ponto_id,
            parametro_id: parametro_id,
            propriedade_id: this.propriedade_id,
            tipo_integracao_id: this.periodo_id ? this.periodo.tipo_integracao_id : this.lote.tipo_integracao_id
          });
        } else {
          modal = this.modalCtrl.create(ModalGeraAcaoPage, {
            pergunta_id: pergunta_id,
            grupo_id: grupo_id,
            ponto_id: ponto_id,
            parametro_id: parametro_id,
            propriedade_id: this.lote.propriedade_id,
            tipo_integracao_id: this.lote.tipo_integracao_id
		  });
        }

        modal.onDidDismiss((result) => {
          let respostasFormArray = this.respostas;

          /* Se gerou ação no modal*/
          if (result.retornoModal) {
            /* Desabilita botão de gerar ação na pergunta */
            respostasFormArray.at(item).patchValue({'acao_id': result.acao_id});

            this.perguntasProvider.updateRespostaAuxComAcao(this.respostasFrom.value.respostas[item].resposta_aux_id, result.acao_id);

            /* Salva ligação entre a ação e a pergunta que originou a ação */
            this.perguntasProvider.salvaPerguntaAcao({
              lote_id: this.lote_id,
              pergunta_id: pergunta_id,
              acao_id: result.acao_id,
              mobile_key: mobile_key,
              grupo_id: grupo_id,
              ponto_id: ponto_id,
              parametro_id: parametro_id,
            });
          }

        });

        modal.present();

      });

    });

  }

  store() {
    this.retornoModal = true;
	this.showAlertClose = false;
	let respostasFormArray = this.respostas;
    let respostas: Array<any> = this.respostasFrom.value.respostas;
    let loadingRespostas = this.messageService.showLoading('Salvando Respostas...');
    let isInvalid = false;

    respostas.forEach((pergunta, item) => {
      if (pergunta.resposta == null && pergunta.nao_se_aplica == null) {
        respostasFormArray.at(item).patchValue({'isInvalid': true});
        isInvalid = true;
      } else {
        respostasFormArray.at(item).patchValue({'isInvalid': false});
      }
    });

    if (isInvalid) {
      this.messageService.showToastError('Ops, algumas perguntas ficaram sem respostas!');
      this.content.scrollToTop(800);
      loadingRespostas.dismiss();
      return false;
    }

    /*Abre modal de Assinatura*/
    this.empresaProvider.getParamChkCapturaAssinatura().then((result) => {

      if (result && !this.plt.is('ios')) {

        let modal = this.modalCtrl.create(ModalSignPage);
        modal.onDidDismiss((data) => {

          if (data.retornoModal) {

            this.avaliacaoChkProvider.salvaAssinaturaChkAux(this.avaliacao_id_aux, data.assinatura).catch((err) => {
              console.error('flaha ao salvar assinatura', err);
            });
            this.salvaRespostas(respostas, loadingRespostas,data.assinatura);
          } else {

            this.empresaProvider.getParamChkObrigaCapturaAssinatura().then(result => {

              if (result) {

                this.messageService.showToastError('Capture a Assinatura do Produtor para finalizar o checklist!');
                modal.present();
                return false;
              } else {

                this.salvaRespostas(respostas, loadingRespostas, data.assinatura);
              }
            })
          }

        });
        modal.present();
      } else {
        this.salvaRespostas(respostas, loadingRespostas, null);
      }
    });

  }

  dismiss() {
	  if(this.showAlertClose){
		  this.alertCtrl.create({
			  title: 'Sair?',
			  message: 'Deseja realmente sair da aplicação do checklist?',
			  buttons: [
				  {
					  text: 'Não',
					  handler: () => {}
				  },
				  {
					  text: 'Sim, sair',
					  handler: () => {
						  this.viewCtrl.dismiss(this.retornoModal);
					  }
				  }
			  ]
		  }).present();
	  }else{
		  this.viewCtrl.dismiss(this.retornoModal);
	  }

  }

  private salvaRespostas(respostas, loadingRespostas, assinatura) {

    let avaliacao_id_aux = this.avaliacao_id_aux;
    this.perguntasProvider.salvaRespostas(respostas, avaliacao_id_aux, this.latitude, this.longitude, assinatura).then((avaliacao: any) => {
      let avaliacao_id: number = avaliacao.id;
      let checklist_id: number = avaliacao.checklist_id;
      if(this.periodo_id != null){
        this.empresaProvider.getParamRepetirChk().then(data => {
          if(data){
            this.perguntasProvider.deletaAvaliacaoComRespostas(this.periodo_id, avaliacao_id);

          }
        });
      }
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        db.transaction(function (tx) {

          tx.executeSql('SELECT considerar_cotacao, chk_pgto_kg FROM empresa LIMIT 1', [], function (_, result_empresa) {
            let considerar_cotacao = result_empresa.rows.item(0).considerar_cotacao;
            let chk_pgto_kg = result_empresa.rows.item(0).chk_pgto_kg;

            if (considerar_cotacao == 1) {
              tx.executeSql('SELECT valor FROM cotacoes WHERE data_final IS NULL ORDER BY id DESC LIMIT 1', [], function (_, result_cotacao) {
                tx.executeSql('UPDATE checklists_avaliacoes SET valor_chk=? WHERE id =?', [result_cotacao.rows.item(0).valor, avaliacao_id]);
              })
            } else {
              tx.executeSql('SELECT valor_total FROM checklists WHERE id = ?', [checklist_id], function (_, result_checklist) {
                tx.executeSql('UPDATE checklists_avaliacoes SET valor_chk=? WHERE id =?', [result_checklist.rows.item(0).valor_total, avaliacao_id]);
              })
            }

            if (chk_pgto_kg == 1) {
              tx.executeSql('SELECT valor_total FROM checklists WHERE id = ?', [checklist_id], function (_, result_checklist) {
                tx.executeSql('UPDATE checklists_avaliacoes SET peso_chk=? WHERE id =?', [result_checklist.rows.item(0).valor_total, avaliacao_id]);
              })
            }

          });
        }).then(() => {
          this.messageService.showConfirmationAlert('Visualizar Relatório?', 'Gostaria de ver o relatório dos resultados do checklist?', 'Sim, Ver Agora', 'Cancelar').then(result => {
            if (result) {
              let local = '';
              if (this.lote_id > 0) {
                local = 'lote';
                this.navCtrl.push('RelatoriosShowPage', {id: this.lote_id, local: local, envio_automatico: true});
              } else if (this.periodo_id > 0) {
                local = 'periodo';
                this.navCtrl.push('RelatoriosShowPage', {id: this.periodo_id, local: local, envio_automatico: true});

              }
              this.dismiss();
              loadingRespostas.dismiss();
              this.messageService.showToastSuccess('Respostas salvas com sucesso!');
            } else {
              this.dismiss();
              loadingRespostas.dismiss();
              this.messageService.showToastSuccess('Respostas salvas com sucesso!');
            }

            if (this.networkService.isConnected()) {
              this.authProvider.checkLogin().then((data: any) => {
                if (data == "success") {
                  this.utils.retornaUUID().then((uuid: any) => {
                    this.avaliacaoChkProvider.get(avaliacao_id).then(avaliacao => {
                      avaliacao.device_uuid = uuid;
                      this.avaliacaoChkProvider.apiStoreAvaliacaoChk(avaliacao).subscribe((result: any) => {

                        this.avaliacaoChkProvider.updateSinc(result.id, result.id_mobile);
                        let avaliacao_id_web = result.id;

                        this.perguntasProvider.getRespostasDaAvaliacaoParaSincronizar(avaliacao_id).then((respostas_salvas: Array<any>) => {
                          respostas_salvas.forEach((resposta, index) => {

                            resposta.device_uuid = uuid;
                            resposta.id_mobile = resposta.id;
                            resposta.resposta_aux_id = respostas[index].resposta_aux_id;
                            resposta.avaliacao_id_web = avaliacao_id_web;
                            this.perguntasProvider.sincRespostaAux(resposta).catch(err => {

                              console.error('erro ao remover resposta da aux', err, resposta);
                            });

                            this.perguntasProvider.apiStoreResposta(resposta).subscribe((res: any) => {

                              this.perguntasProvider.updateSinc(db,res.id, res.id_mobile, res.avaliacao_id_web).then(()=>{
                                this.totalResposta++;
                                /*Quando totalResposta == respostas_salvas.length ai carrega as fotos*/
                                if(this.totalResposta == respostas_salvas.length){
                                  respostas_salvas.forEach((resposta) => {
                                    this.perguntasProvider.uploadFoto(db, resposta.id);
                                  })
                                }
                              })
                                .catch((err) => console.error('Falha ao atualizar resposta local', err));
                            }, (err) => console.error('Falha ao salvar resposta sem foto na api', err, resposta))
                          });
                        });
                      })
                    })

                  });
                }
              });
            }

			  respostas.forEach(resposta => {
				  if (resposta.foto_base64 != null) {
					  this.perguntasProvider.salvaOldThumbPergunta(resposta.pergunta_id,this.galpao_id,resposta.thumb_foto);
				  }
			  });
          });

        }).catch(err => {
          console.error('falaha ao salavr respostas 1', err);
        });
      });
    }).catch(err => {
      console.error('falaha ao salavr respostas 2', err);
    });

    this.avaliacaoChkProvider.getAvaliacaoIdAux(this.avaliacao_id_aux).then((avaliacao) => {
      this.fbDbProvider.addAvalicaoChecklist(avaliacao, respostas);
    })

  }

	registerBackButton() {
		this.plt.registerBackButtonAction(() => {
			this.dismiss();
		});
	}

}
