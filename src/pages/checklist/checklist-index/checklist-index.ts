import {Component, ViewChild} from '@angular/core';
import {AlertController, IonicPage, ModalController, Nav, NavController, NavParams} from 'ionic-angular';
import {PropriedadesProvider} from "../../../providers/produtores/propriedades";
import {GalpoesProvider} from "../../../providers/produtores/galpoes";
import {LotesProvider} from "../../../providers/produtores/lotes";
import {PerguntasProvider} from "../../../providers/checklist/perguntas";
import {ChecklistProvider} from "../../../providers/checklist/checklist";
import {PerguntasPage} from "../perguntas/perguntas";
import {EmpresaProvider} from "../../../providers/empresa/empresa";
import {PeriodosProvider} from "../../../providers/periodos/periodos";
import {IonicSelectableComponent} from "ionic-selectable";
import {AlertMessagesProvider} from "../../../providers/alert-messages/alert-messages";

@IonicPage()
@Component({
  selector: 'page-checklist-index',
  templateUrl: 'checklist-index.html',
})
export class ChecklistIndexPage{
  @ViewChild('galpaoSelect') galpaoSelect: IonicSelectableComponent;
  @ViewChild('loteSelect') loteSelect: IonicSelectableComponent;
  @ViewChild('intervaloSelect') intervaloSelect: IonicSelectableComponent;

  protected propriedades: any[] = [];
  protected galpoes: any[] = [];
  protected lotes: any[] = [];
  protected intervalos: any[] = [];
  protected checklists: any[] = [];
  protected lote_id;
  protected lote_id_slc;
  protected subtipo_integracao_id;
  protected param_propriedade_id;
  protected galpao;
  protected galpao_id;
  protected intervalo_id;
  protected intervalo_id_slc;
  protected sem_chk = false;
  protected chk_periodo = null;
  protected aplica_chk_lote = false;
  protected repetir_chk = false;
  protected enable_repeat_chk = false;
  private OrdenarAplicacaoChk = false;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private propriedadesProvider: PropriedadesProvider,
    private galpoesProvider: GalpoesProvider,
    private lotesProvider: LotesProvider,
    private checklistProvider: ChecklistProvider,
    private perguntasProvider: PerguntasProvider,
    private nav: Nav,
    private empresaProvider: EmpresaProvider,
    private periodoProvider: PeriodosProvider,
    private messageService: AlertMessagesProvider,
	private alertCtrl: AlertController,
  ) {

    this.empresaProvider.getParamChkPeriodo().then((data)=>{
      this.chk_periodo = data;
      this.propriedadesProvider.getPropriedadesLocalWithChk(true, this.chk_periodo).then((data: any[]) => {
        this.propriedades = data;
        this.sem_chk = false;
      });
    }).catch((err)=>{
      console.error('Falha ao buscar parametro da empresa', err);
    })


    this.empresaProvider.getParamRepetirChk().then((data:boolean)=>{
      this.enable_repeat_chk = data;
    });

    this.empresaProvider.getParamOrdenaAplicacaoChk().then((data:boolean)=>{
      this.OrdenarAplicacaoChk = data;
    })
  }

  getGalpao(event) {
    let propriedade_id = event.value.id;
    this.param_propriedade_id = propriedade_id;
    this.galpaoSelect.clear();

    if(this.loteSelect){
      this.loteSelect.clear();
    }

    if(this.intervaloSelect){
      this.intervaloSelect.clear();
    }

    this.galpoes = [];
    this.lotes = [];
    this.intervalos = [];
    this.checklists = [];
    this.sem_chk = false;

    this.galpoesProvider.getGalpoesLocal(propriedade_id).then((data: any[]) => {
      this.galpoes = data;
    });
  }

  getLote(event) {
    let galpao_id = event.value.id;
    this.aplica_chk_lote = event.value.aplica_chk_lote;

    this.lotes = [];
    this.checklists = [];
    this.sem_chk = false;
    this.lotesProvider.getLotesLocal(galpao_id).then((data: any[]) => {
      this.loteSelect.clear();
      this.lotes = data;
    })
  }

  getIntervalos(event) {
    let galpao_id = event.value.id;
    this.intervaloSelect.clear();

    this.intervalos = [];
    this.checklists = [];
    this.sem_chk = false;
    this.galpao_id = galpao_id;

    this.periodoProvider.getIntervalosDeAplicacoesLocal(galpao_id).then((data: any[]) => {
      this.intervalos = data;
    })
  }

  getChecklist(event:any, isRepeat:boolean = false) {
    if(!isRepeat){
      if(event != null){
        this.subtipo_integracao_id = event.value.subtipo_integracao_id;
        this.lote_id_slc = event.value.id;
      }
    }

    this.checklists = [];

    this.checklistProvider.getChecklistLocal(this.subtipo_integracao_id, this.lote_id_slc, this.repetir_chk).then((data: Array<any>) => {
      this.checklists = data;
      this.sem_chk = data.length == 0;
    })
  }

  getChecklistNoIntervalo(event:any, isRepeat:boolean = false){
    if(!isRepeat){
      if(event != null){
        this.intervalo_id_slc = event.value.id;
      }
    }
    this.checklists = [];

    this.empresaProvider.getParamAgruparChks().then((agrupa:boolean) => {
      this.checklistProvider.getChecklistNoPeriodoLocal(this.galpao_id, this.intervalo_id_slc, this.repetir_chk, agrupa).then((data: Array<any>) => {
        this.checklists = data;
        this.sem_chk = data.length == 0;
      });
    });
  }

  reiniciarChk(checklist, consultaAux:boolean = false){
	  this.alertCtrl.create({
		  title: 'Reiniciar avaliação?',
		  message: 'Deseja descartar fotos e respostas da avaliação e recomeçar?',
		  buttons: [
			  {
				  text: 'Não',
				  handler: () => {}
			  },
			  {
				  text: 'Sim, Descartar Avaliação',
				  handler: () => {
					  this.abrirPerguntas(checklist, consultaAux, true);
				  }
			  }
		  ]
	  }).present();
  }

  abrirPerguntas(checklist, consultaAux:boolean = false, descartaAux:boolean = false) {

    if(checklist.ordem_aplicacao > checklist.chk_aplicavel && this.checklists.length > 1 && this.OrdenarAplicacaoChk){
      this.messageService.showAlert('Oops! Checklist fora da Ordem!', 'Existe outro checklist que deve ser aplicado antes deste!');
    }else{

      let modal = this.modalCtrl.create(PerguntasPage, {
        checklist_id: checklist.id,
        propriedade_id: this.param_propriedade_id,
        galpao_id: this.galpao_id,
        intervalo_id: this.intervalo_id_slc,
        descricao: checklist.descricao,
        lote_id: this.lote_id_slc,
        periodo_id: checklist.periodo_id,
        grupo_aplicacao_id: checklist.grupo_aplicacao_id,
        consultaAux: consultaAux,
        descartaAux: descartaAux,
      });

      modal.onDidDismiss((data) => {
        if (data) {
          let index = this.checklists.indexOf(checklist);
          if (index > -1) {
            this.checklists.splice(index, 1);
          }
          this.empresaProvider.getParamOrdenaAplicacaoChk().then(data =>{
            if(data){
              this.carregaChecklists();
            }
          })
        }
      });

      modal.present();
    }
  }

  carregaChecklists(){
    if(this.chk_periodo){
      if(this.intervalo_id_slc > 0){
        this.getChecklistNoIntervalo(null, this.repetir_chk);
      }
    }else{
      if(this.lote_id_slc > 0 ){
        this.getChecklist(null, this.repetir_chk);
      }
    }
  }

  abs(value){
    return Math.abs(value);
  }

}
