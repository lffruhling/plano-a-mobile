import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChecklistIndexPage } from './checklist-index';
import {IonicSelectableModule} from "ionic-selectable";

@NgModule({
  declarations: [
    ChecklistIndexPage,
  ],
  imports: [
    IonicPageModule.forChild(ChecklistIndexPage),
    IonicSelectableModule
  ],
})
export class ChecklistIndexPageModule {}
