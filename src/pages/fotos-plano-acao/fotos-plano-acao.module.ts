import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FotosPlanoAcaoPage } from './fotos-plano-acao';

@NgModule({
  // declarations: [
  //   FotosPlanoAcaoPage,
  // ],
  imports: [
    IonicPageModule.forChild(FotosPlanoAcaoPage),
  ],
})
export class FotosPlanoAcaoPageModule {}
