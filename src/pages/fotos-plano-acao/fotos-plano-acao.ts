import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UtilsService} from "../../services/utils.service";
import {CameraProvider} from "../../providers/camera/camera";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import {AvaliacoesProvider} from "../../providers/acoes/avaliacoes";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-fotos-plano-acao',
  templateUrl: 'fotos-plano-acao.html',
})
export class FotosPlanoAcaoPage {

  protected titulo: string;
  protected origin: string;
  protected fotos: any[] = [];
  protected id_avi: number;
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private utils: UtilsService,
    private messageService: AlertMessagesProvider,
    private cameraService: CameraProvider,
    private avaliacaoService: AvaliacoesProvider,
    private networkService: CheckNetworkProvider,
    private authProvider: AuthProvider,
  ) {
    this.origin = this.navParams.get('origin');
    this.titulo = this.navParams.get('title');
    this.id_avi = this.navParams.get('id_avi');
  }

  ionViewWillLeave(){
    this.messageService.showToastSuccess('Resolvido com sucesso!');

    if(this.networkService.isConnected()) {
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.utils.retornaUUID().then((uuid: any) => {
            this.avaliacaoService.get(this.id_avi).then((avaliacao:any)=>{
              let dados = avaliacao;
              dados.id_mobile = avaliacao.id;
              dados.device_uuid = uuid;
              dados.com_acao = false;
              dados.gerou_acao = false;

              this.avaliacaoService.apiStore(dados).subscribe((response:any)=>{
                let dados:any = {
                  propriedade_id: avaliacao.propriedade_id,
                  parametro_id: avaliacao.parametro_id,
                  tecnico_id: avaliacao.tecnico_id,
                  data_visita: avaliacao.data_visita,
                  gerou_acao:avaliacao.gerou_acao,
                  mobile_key:avaliacao.mobile_key,
                  id_web: response.id,
                  id: avaliacao.id,
                };

                this.avaliacaoService.update(dados);

                /*Envia Fotos Avaliacao Inicial*/
                this.avaliacaoService.enviaFotos(avaliacao.id, response.id);
              })
            })
          })

        }
      });
    }

  }

  newFoto() {
    let mobile_key = this.navParams.get('mobile_key');
    let id_avi = this.navParams.get('id_avi');

    let loading = this.messageService.showLoading('Abrindo a câmera... Aguarde!');
    this.utils.checkDisck().then((data) => {

      if (data) {

		  this.cameraService.capturarFoto(false,1100,900,false,true).then((base64File) => {
			  loading.dismiss();
			  let foto  ='data:image/jpeg;base64,'+base64File;

          switch (this.origin) {
            case 'avaliacao-inicial':{
              this.avaliacaoService.insertFoto({
                mobile_key: mobile_key,
                img: foto,
                avaliacao_id: id_avi,
              }).then((data)=>{
                this.fotos.push({
                  id: data.insertId,
                  avaliacao_id: id_avi,
                  mobile_key: mobile_key,
                  img: foto
                });
              }).catch(err=>{
                console.error('falha salvar foto avaliacao - tela', err);
              });
              break;
            }
          }


        }).catch(err=>{
          loading.dismiss();
          console.error('imagem não selecionada', err);
        });
      }
    });
  }

  removeFoto(foto){
    let foto_id = foto.id;
    this.messageService.showConfirmationAlert('Remover Foto?', 'Deseja realmente remover esta foto?', 'Sim, Remover', 'Cancelar').then(result=>{
      if(result){
        switch (this.origin) {
          case 'avaliacao-inicial':{
            this.avaliacaoService.deleteFoto(foto_id).then(()=>{
              this.fotos = this.fotos.filter((_foto)=>{
                return foto_id != _foto.id;
              });
            });
            break;
          }
        }
      }
    });
  }

}
