import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {GeolocationProvider} from "../../providers/geolocation/geolocation";
import {UniqueIdProvider} from "../../providers/unique-id/unique-id";
import {VisitasProvider} from "../../providers/visitas/visitas";
import {Keyboard} from "@ionic-native/keyboard";
import {AlertMessagesProvider} from "../../providers/alert-messages/alert-messages";
import * as moment from "moment";
import {PerguntasPage} from "../checklist/perguntas/perguntas";
import {ChecklistProvider} from "../../providers/checklist/checklist";
import {CheckNetworkProvider} from "../../providers/check-network/check-network";
import {AuthProvider} from "../../providers/auth/auth";
import {UtilsService} from "../../services/utils.service";
import {LotesProvider} from "../../providers/produtores/lotes";

@IonicPage()
@Component({
  selector: 'page-visita-lote',
  templateUrl: 'visita-lote.html',
  providers: [Keyboard],
})

export class VisitaLotePage {
  protected retornoModal = false;
  protected mobile_key;
  protected latitude;
  protected longitude;
  protected visita_lancamento_id;
  protected lote_id;
  protected propriedade_id;
  protected galpao_id;
  protected checklist_id;
  protected produtor;
  protected galpao;
  protected aplica_chk_gp;
  protected lote;
  protected sexos;
  protected check;
  protected checkRespondido = null;
  protected obsGeolocation;

  perguntas: Array<any> = [];
  perguntasFormGroup = this.fb.group({
    respostas: this.fb.array([])
  });

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private keyboard: Keyboard,
              private fb: FormBuilder,
              private geolocationProvider: GeolocationProvider,
              private uniqueKeyService: UniqueIdProvider,
              private visitaProvider: VisitasProvider,
              private messageService: AlertMessagesProvider,
              private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private checklistProvider: ChecklistProvider,
              private networkService: CheckNetworkProvider,
              private authProvider: AuthProvider,
              private utils: UtilsService,
              private lotesProvider: LotesProvider,
  ) {
    this.geolocationProvider.getGeolocation();
  }

  ionViewDidLoad() {
    this.visita_lancamento_id = this.navParams.get('visita_lancamento_id');
    this.lote_id = this.navParams.get('lote_id');
    this.checklist_id = this.navParams.get('checklist_id');
    if (this.checklist_id > 0) {
      this.checkRespondido = false;
      this.checklistProvider.findCheck(this.checklist_id).then((check) => {
        this.check = check;
      })
    }
    this.propriedade_id = this.navParams.get('propriedade_id');
    this.galpao_id = this.navParams.get('galpao_id');
    this.produtor = this.navParams.get('produtor');
    this.galpao = this.navParams.get('galpao');
    this.aplica_chk_gp = this.navParams.get('aplica_chk_gp');
    this.lote = this.navParams.get('lote');

    this.visitaProvider.getSexoLotes().then((sexos: Array<any>) => {
      this.sexos = sexos;
    });

    this.visitaProvider.getPerguntas(this.visita_lancamento_id).then((perguntas: Array<any>) => {
      const group: any = {};
      perguntas.forEach((pergunta, index) => {
        this.perguntas.push({
          'key': `cpo_${index}`,
          'label': pergunta.descricao,
          'required': true,
          'type': pergunta.tipo_campo,
          'lancamento_visita_id': this.visita_lancamento_id,
          'pergunta_id': pergunta.id,
          'tb_update': pergunta.tabela_de_atualizacao,
          'cpo_update': pergunta.campo_de_atualizacao,
        });
        group[`cpo_${index}`] = new FormControl(pergunta.resposta, Validators.required)
      });

      this.perguntasFormGroup = new FormGroup(group);
    });

    this.obsGeolocation = this.geolocationProvider.getGeolocation().then((geolocation: any) => {
      if (geolocation) {
        this.latitude = geolocation.coords.latitude;
        this.longitude = geolocation.coords.longitude;
      }
    }, err => {
      console.error('getGeolocation', err)
    });
  }

  storeVisita() {
    this.retornoModal = true;
    let loadingRespostas = this.messageService.showLoading('Salvando Avaliação...');

    if (!this.perguntasFormGroup.valid) {
      this.messageService.showToastError('Ops, algumas perguntas ficaram sem respostas!');
      loadingRespostas.dismiss();
      return false;
    }

    let count = 0;
    let lancamentoVisita = {
      'id': this.visita_lancamento_id,
      'data_visita': moment().format('YYYY-MM-DD'),
      'latitude': this.latitude,
      'longitude': this.longitude,
    };

    this.visitaProvider.storeLancamentoVisita(lancamentoVisita)
      .then(data => {

        if (this.perguntas.length == 0) {
          loadingRespostas.dismiss();
          this.sincronizaDados(lancamentoVisita);
          this.messageService.showToastSuccess('Visita finalizada com sucesso!');
          this.viewCtrl.dismiss(this.retornoModal);
        } else {
          for (let key in this.perguntasFormGroup.value) {
            let pergunta = this.perguntas.find((p) => p.key == key);
            let respostaVisita = {
              'mobile_key': this.uniqueKeyService.generateKey(),
              'lancamento_visita_id': pergunta.lancamento_visita_id,
              'pergunta_id': pergunta.pergunta_id,
              'resposta': this.perguntasFormGroup.value[key],
            };

            this.visitaProvider.storeResposta(respostaVisita)
              .then(data => {
                count++;

                if (pergunta.tb_update != null) {
                  if (pergunta.cpo_update != null) {
                    this.visitaProvider.updateDinamyc(pergunta.tb_update, pergunta.cpo_update, this.perguntasFormGroup.value[key], this.lote_id)
                      .catch(err => console.error('falha ao salvar dinamico', err));
                  }
                }

                if (count == this.perguntas.length) {
                  loadingRespostas.dismiss();
                  this.sincronizaDados(lancamentoVisita);
                  this.messageService.showToastSuccess('Visita finalizada com sucesso!');
                  this.viewCtrl.dismiss(this.retornoModal);
                }
              }).catch(err => console.error('falha ao salvar resposta', err));
          }

        }
      })
      .catch(err => console.error('falha ao salvar lancamento', err));
  }

  abrirChecklist(checklist) {
    let modal = this.modalCtrl.create(PerguntasPage, {
      checklist_id: checklist.id,
      propriedade_id: this.propriedade_id,
      galpao_id: this.galpao_id,
      descricao: checklist.descricao,
      lote_id: this.lote_id,
      periodo_id: checklist.periodo_id,
      grupo_aplicacao_id: checklist.grupo_aplicacao_id,
      consultaAux: false,
      descartaAux: false,
    });

    modal.onDidDismiss((data) => {
      this.checkRespondido = true;
    });

    modal.present();
  }

  private sincronizaDados(lancamento) {
    if (this.networkService.isConnected()) {
      this.authProvider.checkLogin().then((data: any) => {
        if (data == "success") {
          this.utils.retornaUUID().then((uuid: any) => {

            this.visitaProvider.getAllRespostasLancamentoToSync(lancamento.id).then((respostas: Array<any>) => {
              lancamento._method = "patch";
              lancamento.respostas = [];
              for (let resposta of respostas) {
                resposta.device_uuid = uuid;
                resposta.id_mobile = resposta.id;
                lancamento.respostas.push(resposta);
              }

              this.visitaProvider.apiStore(lancamento).subscribe((response: any) => {
                this.visitaProvider.updateSinc(response.lancamento.id).then(() => {
                  for (let resp of response.respostas) {
                    this.visitaProvider.updateSyncRespostas(resp.id_mobile, resp.id);
                  }
                }).then((_)=>{
                  this.lotesProvider.getAllToSync().then((lotes:Array<any>)=>{
                    for(let lote of lotes){
                      this.lotesProvider.apiUpdate(lote).subscribe((data:any)=>{
                        this.lotesProvider.updateSinc(data.id)
                          .catch(err=>console.error('falha ao atualizar lote', err));
                      },err=>{
                        console.error('falha ao sincronizar dados do lote', err)
                      })
                    }
                  });
                });

              }, err => {
                console.error('Falha ao enviar lancamentos', err, lancamento)
              })
            });
          })
        }
      })
    }
  }


}
