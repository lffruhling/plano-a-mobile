import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitaLotePage } from './visita-lote';

@NgModule({
  // declarations: [
  //   VisitaLotePage,
  // ],
  imports: [
    IonicPageModule.forChild(VisitaLotePage),
  ],
})
export class VisitaLotePageModule {}
