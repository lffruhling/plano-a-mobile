import {Component, OnInit, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {GeolocationProvider} from "../../providers/geolocation/geolocation";

@IonicPage()
@Component({
  selector: 'page-modal-sign',
  templateUrl: 'modal-sign.html',
})
export class ModalSignPage implements OnInit{

  @ViewChild('imageCanvas') canvas:any;

  canvasElement:any;

  saveX: number;
  saveY: number;
  @ViewChild(Content) content:any;
  @ViewChild('fixedContainer') fixedContainer:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private plt:Platform,
              private screenOrientation: ScreenOrientation,
              private viewCtrl: ViewController,
              private geolocationProvider: GeolocationProvider
  ) {
    this.geolocationProvider.getGeolocation();
  }

  ngOnInit(){
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  ionViewDidEnter(){
    this.canvasElement = this.canvas.nativeElement;
    let width = this.plt.width();

    /*
    * Bug Meia tela Assinatura
    * Tenta solucionar o problema de abrir a assinatura em meia tela pegando o resultado da largura
    * e multiplicando por 3 ou seja 360*3 = 1080 que é +/- a largura das telas dos telefones.
    * */
    if(width < 450){
      width = this.plt.width() * 3
    }

    this.canvasElement.width = width + '';
    this.canvasElement.height = (this.plt.height() - 50) + '';
  }

  startDrawing(ev){
    var canvasPosition = this.canvasElement.getBoundingClientRect();
    this.saveX = ev.touches[0].pageX - canvasPosition.x;
    this.saveY = ev.touches[0].pageY - canvasPosition.y;
  }

  moved(ev){
    var canvasPosition = this.canvasElement.getBoundingClientRect();

    let currentX = ev.touches[0].pageX - canvasPosition.x;
    let currentY = ev.touches[0].pageY - canvasPosition.y;

    let ctx = this.canvasElement.getContext('2d');

    ctx.lineJoin = 'round';
    ctx.strokeColor = '#000000';
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();

    ctx.stroke();

    this.saveX = currentX;
    this.saveY = currentY;
  }

  limparTela(){
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height)

  }

  dismiss(retornoModal) {
    var dataUrl = this.canvasElement.toDataURL();
    this.viewCtrl.dismiss({
      retornoModal:retornoModal,
      assinatura: dataUrl
    });

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  }

}
