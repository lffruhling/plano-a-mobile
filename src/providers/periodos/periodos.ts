import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class PeriodosProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/periodos`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/periodos`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/periodos`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }



  public apiStoreBkp(value){
    return this.http.post(`${this.urlApi}/upload/bkp`, value);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(periodo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into periodo_aplicacoes (id, grupo_aplicacao_id, propriedade_id, galpao_id, lote_id, intervalo_id, checklist_id, data_inicio, data_final, vigente, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?)';
      let data = [
        periodo.id,
        periodo.grupo_aplicacao_id,
        periodo.propriedade_id,
        periodo.galpao_id,
        periodo.lote_id,
        periodo.intervalo_id,
        periodo.checklist_id,
        periodo.data_inicio,
        periodo.data_final,
        periodo.vigente,
        periodo.deleted_at
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'periodos.ts', 'insert',sql, data);
        console.error('Falha ao inserir periodo_aplicacoes', err, sql, data)
      })
    })
  }

  public update(periodo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update periodo_aplicacoes SET grupo_aplicacao_id=?, propriedade_id=?, galpao_id=?, lote_id=?, intervalo_id=?, checklist_id=?, data_inicio=?, data_final=?, vigente=?, deleted_at=? where id = ?;';
      let data = [
        periodo.grupo_aplicacao_id,
        periodo.propriedade_id,
        periodo.galpao_id,
        periodo.lote_id,
        periodo.intervalo_id,
        periodo.checklist_id,
        periodo.data_inicio,
        periodo.data_final,
        periodo.vigente,
        periodo.deleted_at,
        periodo.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'periodos.ts', 'update',sql, data);
        console.error('Falha ao atualizar periodo_aplicacoes', err, sql, data)
      })
    })
  }

  public getIntervalosDeAplicacoesLocal(galpao_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT DISTINCT(it.id),\n' +
        '\t\t(it.descricao || " - " || it.dias || " Dias") as descricao_com_dias\n' +
        '\tFROM intervalos AS it\n' +
        '\t\tLEFT JOIN periodo_aplicacoes AS per\n' +
        '\t\t\tON it.id = per.intervalo_id\n' +
        '\tWHERE per.galpao_id = ?\n' +
        '\t\tAND (per.deleted_at IS NULL) \n' +
        '\t\tAND (it.deleted_at IS NULL) \n' +
        '\t\tAND (it.somente_checklist = 1)';

      let params = [galpao_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let intervalos: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            intervalos.push(data.rows.item(i));
          }
          return intervalos;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getIntervalosDeAplicacoesLocal',sql, params);
        console.error('Falha ao executar sql periodos', err)
      });
    });
  }

  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT pa.*,  ' +
          ' prop.descricao as propriedade_descricao,  ' +
          ' prop.tipo_integracao_id   ' +
          'FROM periodo_aplicacoes AS pa ' +
          ' LEFT JOIN propriedades as prop ' +
          '  ON pa.propriedade_id = prop.id ' +
          'WHERE pa.id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'periodos.ts', 'get',sql, data);
            console.error('Falha ao executar sql busca periodo por ID', err)
          });
      });
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT * FROM periodo_aplicacoes';

      return db.executeSql(sql, [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let lotes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              lotes.push(data.rows.item(i));
            }
            return lotes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'periodos.ts', 'getAllBkp', sql);
          console.error('Falha ao consultar periodo_aplicacoes para sincronizar', err)
        });
    });
  }

  public getPeriodoComChecklist(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = "SELECT chk.descricao AS checklist_descricao, " +
          " prop.descricao AS propriedade_descricao, " +
          " (pa.data_inicio || ' - ' || COALESCE(pa.data_final, 'Data Final Indefinida')) AS periodo_vigente " +
          "FROM periodo_aplicacoes AS pa  " +
          " LEFT JOIN propriedades AS prop " +
          "  ON pa.propriedade_id = prop.id " +
          " LEFT JOIN checklists AS chk " +
          "  ON pa.checklist_id = chk.id " +
          "WHERE pa.id = ?";
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }

            return null;
          })
          .catch((err) => {
            this.exceptionProvider.insert(err, 'periodos.ts', 'getPeriodoComChecklist',sql, data);
            console.error('Falha ao executar sql busca periodo por ID', err)
          });
      });
  }

  public getPeriodosRespondidos(datas:any=null, direcao:string = null, considerar_cotacao:boolean = false, agrupar_chk:any = false){
    let filtro_data = '';
    let ordena_nome = ' ORDER BY data_ultima_avaliacao DESC ';
    let valor_total_chk = '';
    let pts_total_chk = '';
    let pts_total_conf = '';
    let pts_total_n_conf = '';
    let peso_total_conf = '';
    let peso_chk = '';
    let avaliacoes_count = '';

    if(datas){
      let from = datas.from.string;
      let to = datas.to.string;
      filtro_data = ` AND (data_aplicacao BETWEEN '${from}' AND '${to}') `;
    }

    if(direcao){
      ordena_nome += ` ,prod.nome ${direcao} `
    }

    if(considerar_cotacao){
      if(agrupar_chk){
        valor_total_chk = '(SELECT valor FROM cotacoes AS ctc WHERE data_final IS NULL \n' +
          '\t\t\t\tAND vigente = "true" \n' +
          '\t\t\t\tAND ctc.tipo_integracao_id = prop.tipo_integracao_id \n' +
          '\t\t\tORDER BY id DESC\n' +
          '\t\t\tLIMIT 1\n' +
          '\t\t) AS valor_total_chk,\n';
        peso_chk = ' (SELECT SUM(chk_av3.peso_chk)' +
          '  FROM checklists_avaliacoes AS chk_av3' +
          '  WHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id' +
          '   AND chk_av3.deleted_at IS NULL' +
          ' ) AS peso_chk ';
      }else{
        peso_chk = ' (SELECT SUM(chk.valor_total) ' +
          '  FROM checklists AS chk  ' +
          '  WHERE chk.id IN (SELECT DISTINCT(chk_av3.checklist_id) ' +
          '                  FROM checklists_avaliacoes AS chk_av3 ' +
          '                  WHERE chk_av3.periodo_id = peap.id)   ' +
          ') AS peso_chk ';
      }

    }else{
      if(agrupar_chk){
        valor_total_chk = ' (SELECT SUM(chk_av3.valor_chk) ' +
          '  FROM checklists_avaliacoes AS chk_av3 ' +
          '  WHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id ' +
          '   AND chk_av3.deleted_at IS NULL ' +
          ' ) AS valor_total_chk, ';
        peso_chk = ' (SELECT SUM(chk_av3.peso_chk)' +
          '  FROM checklists_avaliacoes AS chk_av3' +
          '  WHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id' +
          '   AND chk_av3.deleted_at IS NULL' +
          ' ) AS peso_chk ';
      }else{
        valor_total_chk ='\t(SELECT SUM(chk_av3.valor_chk)\n' +
          '\t\tFROM checklists_avaliacoes AS chk_av3\n' +
          '\t\tWHERE chk_av3.periodo_id = peap.id\n' +
          '\t\t\tAND chk_av3.deleted_at IS NULL\n' +
          '\t) AS valor_total_chk,\n';

        peso_chk = ' (SELECT SUM(chk.valor_total) ' +
          '  FROM checklists AS chk  ' +
          '  WHERE chk.id IN (SELECT DISTINCT(chk_av3.checklist_id) ' +
          '                  FROM checklists_avaliacoes AS chk_av3 ' +
          '                  WHERE chk_av3.periodo_id = peap.id)   ' +
          ') AS peso_chk ';
      }

    }

    if(agrupar_chk){
      avaliacoes_count = ' (SELECT COUNT(id) ' +
        '  FROM checklists_avaliacoes AS chk_av ' +
        '  WHERE peap.grupo_aplicacao_id = chk_av.grupo_aplicacao_id ' + filtro_data +
        '    AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)' +
        '  ) AS avaliacoes_count, ';
      pts_total_chk = ' (SELECT (total_chks/qtd_chk) AS media_total_chk ' +
        '  FROM (SELECT SUM(chk_perg.valor_max) as total_chks, ' +
        '    (SELECT count(chk_av1.checklist_id) ' +
        '      FROM checklists_avaliacoes AS chk_av1 ' +
        '      WHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id  ' +
        '      AND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null") ' +
        '    ) as qtd_chk ' +
        '    FROM checklists AS chk ' +
        '      LEFT JOIN checklists_perguntas AS chk_perg ' +
        '        ON chk.id = chk_perg.checklist_id ' +
        '    WHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id) ' +
        '                    FROM checklists_avaliacoes AS chk_av1 ' +
        '                    WHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id) ' +
        '      AND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")) ' +
        ') AS pts_total_chk, ';
      pts_total_conf = '\t(SELECT (total_respostas/qtd_chk) \n' +
        '\t\tFROM (SELECT SUM(valor) as total_respostas,\n' +
        '\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\tFROM (SELECT valor\n' +
        '\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\tON chk_av2.id = chk_resp.avaliacao_id OR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\t\tWHERE chk_av2.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\tAND (chk_av2.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\tAND chk_resp.resposta = 1\n' +
        '\t\t\t\t\tGROUP BY chk_resp.pergunta_id)\n' +
        '\t\t\t)\n' +
        '\t) AS pts_total_conf,\n';

      pts_total_n_conf = '\t(SELECT (total_respostas/qtd_chk) \n' +
        '\t\tFROM (SELECT SUM(valor) as total_respostas,\n' +
        '\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\tFROM (SELECT valor\n' +
        '\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\tON chk_av2.id = chk_resp.avaliacao_id OR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\t\tWHERE chk_av2.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\tAND (chk_av2.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\tAND chk_resp.resposta = 0\n' +
        '\t\t\t\t\tGROUP BY chk_resp.pergunta_id)\n' +
        '\t\t\t)\n' +
        '\t) AS pts_total_n_conf,\n';

      peso_total_conf = '\t(SELECT sum((SELECT ((sum(valor)*chk_av.peso_chk)/100) \n' +
		  '\t\t\t\t\t\t\t\t\tFROM checklists_respostas\tAS chk_resp\n' +
		  '\t\t\t\t\t\t\t\t\t\tWHERE chk_av.id_web = chk_resp.avaliacao_id_web OR (chk_resp.avaliacao_id_web IS NULL AND chk_av.id = chk_resp.avaliacao_id)\n' +
		  '\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at IS NULL' +
		  '\t\t\t\t\t\t\t\t\t\t)\n' +
		  '\t\t\t\t\t\t\t\t)AS peso_chk_calc\n' +
		  '\t\t\tFROM checklists_avaliacoes as chk_av\n' +
		  '\t\t\tWHERE chk_av.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
		  '\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
		  '\t\t) AS peso_total_conf,\n'
    }else{
      avaliacoes_count = ' (SELECT COUNT(id) ' +
        '  FROM checklists_avaliacoes AS chk_av ' +
        '  WHERE peap.id = chk_av.periodo_id ' + filtro_data +
        '    AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)' +
        '  ) AS avaliacoes_count, ';
      pts_total_chk = ' (SELECT SUM(chk_perg.valor_max) ' +
        '  FROM checklists AS chk ' +
        '    LEFT JOIN checklists_perguntas AS chk_perg ' +
        '      ON chk.id = chk_perg.checklist_id ' +
        '  WHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id) ' +
        '                  FROM checklists_avaliacoes AS chk_av1 ' +
        '                  WHERE chk_av1.periodo_id = peap.id) ' +
        '    AND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")  ' +
        ') AS pts_total_chk, ';
      pts_total_conf = ' (SELECT SUM(chk_resp.valor) ' +
        '  FROM checklists_avaliacoes AS chk_av2 ' +
        '    LEFT JOIN checklists_respostas AS chk_resp ' +
        '\t\t\tON chk_av2.id = chk_resp.avaliacao_id\n' +
        '\t\t\tOR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '  WHERE chk_av2.periodo_id = peap.id ' +
        '    AND (chk_av2.id_web = chk_resp.avaliacao_id_web \n' +
        '\t\t\t\tOR chk_resp.avaliacao_id_web IS NULL) ' +
        '    AND chk_av2.deleted_at IS NULL ' +
        '    AND chk_resp.resposta = 1 ' +
        ') AS pts_total_conf, ';

      pts_total_n_conf = ' (SELECT SUM(chk_resp.valor) ' +
        '  FROM checklists_avaliacoes AS chk_av2 ' +
        '    LEFT JOIN checklists_respostas AS chk_resp ' +
        '\t\t\tON chk_av2.id = chk_resp.avaliacao_id\n' +
        '\t\t\tOR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '  WHERE chk_av2.periodo_id = peap.id ' +
        '    AND (chk_av2.id_web = chk_resp.avaliacao_id_web \n' +
        '\t\t\t\tOR chk_resp.avaliacao_id_web IS NULL) ' +
        '    AND chk_av2.deleted_at IS NULL ' +
        '    AND chk_resp.resposta = 0 ' +
        ') AS pts_total_n_conf, ';

		peso_total_conf = '\t(SELECT sum((SELECT ((sum(valor)*chk_av.peso_chk)/100) \n' +
			'\t\t\t\t\t\t\t\t\tFROM checklists_respostas\tAS chk_resp\n' +
			'\t\t\t\t\t\t\t\t\t\tWHERE chk_av.id_web = chk_resp.avaliacao_id_web OR (chk_resp.avaliacao_id_web IS NULL AND chk_av.id = chk_resp.avaliacao_id)\n' +
			'\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at IS NULL' +
			'\t\t\t\t\t\t\t\t\t\t)\n' +
			'\t\t\t\t\t\t\t\t)AS peso_chk_calc\n' +
			'\t\t\tFROM checklists_avaliacoes as chk_av\n' +
			'\t\t\tWHERE chk_av.periodo_id = peap.id\n' +
			'\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
			'\t\t) AS peso_total_conf,\n'
    }

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql  = 'SELECT peap.id, ' +
        ' peap.grupo_aplicacao_id, ' +
        ' peap.propriedade_id, ' +
        ' peap.galpao_id, ' +
        ' gp.descricao AS galpao_descricao, ' +
        ' prop.descricao AS propriedade_descricao, ' +
        ' prod.nome AS produtor_nome, ' +
        ' (inter.dias || " Dias - " || inter.descricao) AS intervalo, ' +
        ' (SELECT chk_av.data_aplicacao ' +
        '  FROM checklists_avaliacoes AS chk_av ' +
        '  WHERE peap.id = chk_av.periodo_id ' + filtro_data +
        '    AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)' +
        '     ORDER BY chk_av.id DESC ' +
        '     LIMIT 1' +
        '  ) AS data_ultima_avaliacao, ' +
        avaliacoes_count +
        pts_total_chk +
        ' (SELECT SUM(chk_perg.valor_max) ' +
        '  FROM checklists_avaliacoes AS chk_av ' +
        '   LEFT JOIN checklists_respostas AS chk_resp ' +
        '    ON chk_av.id = chk_resp.avaliacao_id ' +
        '   LEFT JOIN checklists_perguntas AS chk_perg ' +
        '    ON chk_resp.pergunta_id = chk_perg.id ' +
        '  WHERE peap.id = chk_av.periodo_id ' +
        '   AND chk_av.deleted_at IS NULL ' +
        '   AND (chk_resp.nao_se_aplica = 1 OR chk_resp.nao_se_aplica = "true" )' +
        '   AND (chk_perg.deleted_at IS NULL ' +
        '    OR chk_perg.deleted_at = "null") ' +
        '  ) AS pts_total_resp_nulas, ' +
        pts_total_conf+
        pts_total_n_conf+
		peso_total_conf+
        valor_total_chk +
        peso_chk +
        'FROM periodo_aplicacoes AS peap ' +
        ' LEFT JOIN propriedades AS prop ' +
        '  ON peap.propriedade_id = prop.id ' +
        ' LEFT JOIN galpoes AS gp ' +
        '  ON peap.galpao_id = gp.id ' +
        ' LEFT JOIN produtores AS prod ' +
        '  ON prop.produtor_id = prod.id ' +
        ' LEFT JOIN intervalos AS inter ' +
        '  ON peap.intervalo_id = inter.id ';

      if(agrupar_chk){
        sql +=  '\tWHERE peap.id in (SELECT periodo_id FROM checklists_avaliacoes AS chk_av \n' +
          '\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
          '\t\t\t\t\t\t\t\t\t\t\tAND deleted_at IS NULL)\n'+
          ' GROUP BY grupo_aplicacao_id '+ ordena_nome;
      }else{
        sql +=  'WHERE avaliacoes_count > 0 '+ ordena_nome;
      }
		return db.executeSql(sql,[]).then( (data:any) =>{
        if (data.rows.length > 0) {
          let periodos: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            periodos.push(data.rows.item(i));
          }
          return periodos;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getPeriodosRespondidos',sql);
      });
    });
  }

  public getDadosPeriodo(periodo_id, considerar_cotacao, agrupar_chk:any = false){

    let sql = '';

    let valor_chk;
    if(considerar_cotacao){
      valor_chk = '\t\t(SELECT valor FROM cotacoes AS ctc WHERE data_final IS NULL \n' +
        '\t\t\t\tAND vigente = "true" \n' +
        '\t\t\t\tAND ctc.tipo_integracao_id = prop.tipo_integracao_id \n' +
        '\t\t\tORDER BY id DESC\n' +
        '\t\t\tLIMIT 1\n' +
        '\t\t) AS valor_total_chk,';
    }else{
      if(agrupar_chk) {
        valor_chk = '\t\t(SELECT SUM(chk_av3.valor_chk)\n' +
          '\t\t\tFROM checklists_avaliacoes AS chk_av3\n' +
          '\t\t\tWHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
          '\t\t\t\tAND chk_av3.deleted_at IS NULL\n' +
          '\t\t) AS valor_total_chk,\n';
      }else{
        valor_chk = '\t(SELECT SUM(chk_av3.valor_chk)\n' +
          '\t\tFROM checklists_avaliacoes AS chk_av3\n' +
          '\t\tWHERE chk_av3.periodo_id = peap.id\n' +
          '\t\t\tAND chk_av3.deleted_at IS NULL\n' +
          '\t) AS valor_total_chk,\n';
      }

    }
    if(agrupar_chk){
      sql = '\tSELECT gp.descricao AS galpao_descricao,\n' +
        '\t\tprop.descricao AS propriedade_descricao,\n' +
        '\t\ttpi.descricao AS integracao,\n' +
        '\t\tprod.nome AS produtor_nome,\n' +
        '\t\t(inter.dias || " Dias - " || inter.descricao) AS intervalo,\n' +
        '\t\t(SELECT count(id) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tWHERE peap.grupo_aplicacao_id = chk_av.grupo_aplicacao_id\n' +
        '\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t) AS avaliacoes_count,\n' +
        '\t(SELECT count(respostas) \n' +
        '\tFROM (SELECT chk_resp1.id as respostas FROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tLEFT JOIN checklists_respostas AS chk_resp1\n' +
        '\t\t\t\tON (chk_av.id = chk_resp1.avaliacao_id AND chk_av.id_web IS NULL) OR (chk_av.id_web = chk_resp1.avaliacao_id_web)\n' +
        '\t\t\tWHERE peap.grupo_aplicacao_id = chk_av.grupo_aplicacao_id\n' +
        '\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp1.deleted_at IS NULL\n' +
        '\t\t\tGROUP BY chk_resp1.pergunta_id)\n' +
        '\t) AS respostas_count,\n' +
        '\t\t(SELECT (total_chks/qtd_chk) AS media_total_chk \n' +
        '\t\t\tFROM (SELECT SUM(chk_perg.valor_max) as total_chks,\n' +
        '\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\tFROM checklists AS chk\n' +
        '\t\t\t\t\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\t\t\t\t\tON chk.id = chk_perg.checklist_id\n' +
        '\t\t\t\t\t\tWHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id)\n' +
        '\t\t\t\t\t\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t\t\t\t\t\t)\n' +
        '\t\t) AS pts_total_chk,\n' +
        '\t\t(SELECT (total_respostas/qtd_chk)\n' +
        '\t\t\tFROM (SELECT SUM(chk_resp.valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\tON chk_av2.id = chk_resp.avaliacao_id\n' +
        '\t\t\t\t\t\tWHERE chk_av2.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\tAND (chk_resp.nao_se_aplica == 1 OR chk_resp.nao_se_aplica == "true")\n' +
        '\t\t\t\t\t\t)\n' +
        '\t\t) AS pts_total_resp_nulas,\n' +
        '(SELECT (total_respostas/qtd_chk) FROM (SELECT SUM(valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM (SELECT valor\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tON chk_av2.id = chk_resp.avaliacao_id OR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av2.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av2.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.resposta = 1\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tGROUP BY chk_resp.pergunta_id))\n' +
        '\t\t) AS pts_total_conf,' +
		  '(SELECT (total_respostas/qtd_chk) FROM (SELECT SUM(valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM (SELECT valor\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tON chk_av2.id = chk_resp.avaliacao_id OR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av2.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av2.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.resposta = 0\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tGROUP BY chk_resp.pergunta_id))\n' +
        '\t\t) AS pts_total_n_conf,' +
		'\t(SELECT sum((SELECT ((sum(valor)*chk_av.peso_chk)/100) \n' +
		'\t\t\t\tFROM checklists_respostas\tAS chk_resp\n' +
		  '\t\t\t\t\t\t\t\t\t\tWHERE chk_av.id_web = chk_resp.avaliacao_id_web OR (chk_resp.avaliacao_id_web IS NULL AND chk_av.id = chk_resp.avaliacao_id)\n' +
		  '\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at IS NULL' +
		'\t\t\t ))AS peso_chk_calc\n' +
		'\tFROM checklists_avaliacoes as chk_av\n' +
		'\t\tWHERE chk_av.grupo_aplicacao_id = peap.grupo_aplicacao_id' +
		'\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
		') as peso_total_conf, \n' +
        valor_chk +
        '\t\t(SELECT SUM(chk_av3.peso_chk)\n' +
        '\t\t\tFROM checklists_avaliacoes AS chk_av3\n' +
        '\t\t\tWHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
        '\t\t\t\tAND chk_av3.deleted_at IS NULL\n' +
        '\t\t) AS peso_chk\n' +
        '\tFROM periodo_aplicacoes AS peap\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON peap.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN tipo_integracoes AS tpi\n' +
        '\t\t\tON prop.tipo_integracao_id = tpi.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON peap.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN intervalos AS inter\n' +
        '\t\t\tON peap.intervalo_id = inter.id\n' +
        '\tWHERE peap.grupo_aplicacao_id = (SELECT grupo_aplicacao_id FROM periodo_aplicacoes WHERE id = ?)  \n' +
        '\t\tAND peap.deleted_at IS NULL \n' +
        '\tGROUP BY peap.grupo_aplicacao_id ';
    }else{
      sql = 'SELECT gp.descricao AS galpao_descricao,\n' +
        '\tprop.descricao AS propriedade_descricao,\n' +
        '\ttpi.descricao AS integracao,\n' +
        '\tprod.nome AS produtor_nome,\n' +
        '\t(inter.dias || " Dias - " || inter.descricao) AS intervalo,\n' +
        '\t(SELECT count(id) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE peap.id = chk_av.periodo_id AND chk_av.deleted_at IS NULL\n' +
        '\t) AS avaliacoes_count,\n' +
        '\t(SELECT count(chk_resp1.id) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp1\n' +
        '\t\t\tON (chk_av.id = chk_resp1.avaliacao_id AND chk_resp1.avaliacao_id_web IS NULL)\n' +
        '\t\t\tOR chk_av.id_web = chk_resp1.avaliacao_id_web\n' +
        '\t\tWHERE peap.id = chk_av.periodo_id ' +
        '\t\t\tAND (chk_av.id_web = chk_resp1.avaliacao_id_web ' +
        '\t\t\t\tOR chk_resp1.avaliacao_id_web IS NULL)\n' +
        '\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp1.deleted_at IS NULL\n' +
        '\t) AS respostas_count,\n' +
        '\t(SELECT SUM(chk_perg.valor_max) FROM checklists AS chk\n' +
        '\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\tON chk.id = chk_perg.checklist_id\n' +
        '\t\tWHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id) FROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.periodo_id = peap.id AND chk_av1.deleted_at IS NULL)\n' +
        '\t\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t) AS pts_total_chk,\n' +
        '\t(SELECT SUM(chk_perg.valor_max) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\tON chk_av.id = chk_resp.avaliacao_id\n' +
        '\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\tON chk_resp.pergunta_id = chk_perg.id\n' +
        '\t\tWHERE peap.id = chk_av.periodo_id \n' +
        '\t\t\tAND (chk_av.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\tAND (chk_resp.nao_se_aplica = 1 OR chk_resp.nao_se_aplica = "true")\n' +
        '\t\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t) AS pts_total_resp_nulas,\n' +
        '\t(SELECT SUM(chk_resp.valor) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\tON chk_av.id = chk_resp.avaliacao_id\n' +
        '\t\t\tOR chk_av.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\tWHERE chk_av.periodo_id = peap.id \n' +
        '\t\t\tAND (chk_av.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp.resposta = 1\n' +
        '\t) AS pts_total_conf,\n' +
		  '\t(SELECT SUM(chk_resp.valor) FROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\tON chk_av.id = chk_resp.avaliacao_id\n' +
        '\t\t\tOR chk_av.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\tWHERE chk_av.periodo_id = peap.id \n' +
        '\t\t\tAND (chk_av.id_web = chk_resp.avaliacao_id_web OR chk_resp.avaliacao_id_web IS NULL)\n' +
        '\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\tAND chk_resp.resposta = 0\n' +
        '\t) AS pts_total_n_conf,\n' +
        valor_chk +
        '\t(SELECT SUM(chk_av3.peso_chk)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av3\n' +
        '\t\tWHERE chk_av3.periodo_id = peap.id\n' +
        '\t\t\tAND chk_av3.deleted_at IS NULL\n' +
        '\t) AS peso_chk\n' +
        ' FROM periodo_aplicacoes AS peap\n' +
        ' \tLEFT JOIN propriedades AS prop\n' +
        ' \t\tON peap.propriedade_id = prop.id\n' +
        '\tLEFT JOIN tipo_integracoes AS tpi\n' +
        '\t\tON prop.tipo_integracao_id = tpi.id\n' +
        '\tLEFT JOIN produtores AS prod\n' +
        '\t\tON prop.produtor_id = prod.id\n' +
        '\tLEFT JOIN galpoes AS gp\n' +
        '\t\tON peap.galpao_id = gp.id\n' +
        '\tLEFT JOIN intervalos AS inter\n' +
        '\t\tON peap.intervalo_id = inter.id\n' +
        ' WHERE peap.id = ?';
    }

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let params: any[] = [periodo_id];
      return db.executeSql(sql, params).then( (data:any) =>{
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getDadosPeriodo',sql, params);
        console.error('Falha ao executar sql periodo', err, sql, params)
      });
    });
  }

  public getChecklistsComTotais(periodo_id, considerar_cotacao, agrupar_chk:any = false){

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let total_peso_av = '';

      let valor_chk;
      if(considerar_cotacao){
        valor_chk = '\t\t(SELECT valor FROM cotacoes AS ctc WHERE data_final IS NULL \n' +
          '\t\t\t\tAND vigente = "true" \n' +
          '\t\t\t\tAND ctc.tipo_integracao_id = prop.tipo_integracao_id \n' +
          '\t\t\tORDER BY id DESC\n' +
          '\t\t\tLIMIT 1\n' +
          '\t\t) AS valor_total_chk,';
      }else{
        if(agrupar_chk) {
          valor_chk = '\t\t(SELECT SUM(chk_av3.valor_chk)\n' +
            '\t\t\tFROM checklists_avaliacoes AS chk_av3\n' +
            '\t\t\tWHERE chk_av3.grupo_aplicacao_id = peap.grupo_aplicacao_id\n' +
            '\t\t\t\tAND chk_av3.deleted_at IS NULL\n' +
            '\t\t) AS valor_total_chk,\n';
        }else{
          valor_chk = '\t(SELECT SUM(chk_av3.valor_chk)\n' +
            '\t\tFROM checklists_avaliacoes AS chk_av3\n' +
            '\t\tWHERE chk_av3.periodo_id = peap.id\n' +
            '\t\t\tAND chk_av3.deleted_at IS NULL\n' +
            '\t) AS valor_total_chk,\n';
        }

      }

      if(agrupar_chk){
       total_peso_av = '\t(SELECT SUM(valor_total) AS total_peso_av FROM checklists AS chk\n' +
         '\tWHERE chk.id IN (SELECT checklist_id FROM periodo_aplicacoes ' +
         '\t\t\t WHERE grupo_aplicacao_id = chk_av.grupo_aplicacao_id AND deleted_at IS NULL)' +
         '\t ) AS total_peso_av,'
      }

      let sql = 'SELECT chk_av.id, chk_av.id_web, ' +
        ' chk_av.data_aplicacao,' +
        ' chk_av.assinatura,' +
        ' chk.descricao, ' +
        ' SUM(CASE WHEN chk_resp.resposta == 1 THEN 1 ELSE 0 END) AS conformes, ' +
        ' SUM(CASE WHEN (chk_resp.resposta == 0 AND (chk_resp.nao_se_aplica IS NULL OR chk_resp.nao_se_aplica == 0)) THEN 1 ELSE 0 END) AS nao_conformes, ' +
        ' SUM(CASE WHEN (chk_resp.nao_se_aplica == 1 OR chk_resp.nao_se_aplica == "true") THEN 1 ELSE 0 END) AS nao_se_aplica, ' +
        total_peso_av +
        ' (SELECT SUM(chk_perg.valor_max) ' +
        '  FROM checklists AS chk1 ' +
        '   LEFT JOIN checklists_perguntas AS chk_perg ' +
        '    ON chk1.id = chk_perg.checklist_id ' +
        '  WHERE chk1.id == chk_av.checklist_id ' +
        '   AND (chk_perg.deleted_at IS NULL ' +
        '    OR chk_perg.deleted_at = "null") ' +
        ' ) AS pts_total_chk, ' +
        ' (SELECT SUM(chk_perg.valor_max) ' +
        '  FROM checklists_respostas AS chk_resp ' +
        '   LEFT JOIN checklists_perguntas AS chk_perg ' +
        '    ON chk_resp.pergunta_id = chk_perg.id ' +
        '  WHERE chk_av.id = chk_resp.avaliacao_id  ' +
        '   AND (chk_resp.nao_se_aplica = 1 OR chk_resp.nao_se_aplica = "true" )' +
        '   AND (chk_perg.deleted_at IS NULL ' +
        '    OR chk_perg.deleted_at = "null") ' +
        ' ) AS pts_total_resp_nulas, ' +
        '\t(SELECT SUM(valor)\n' +
        '\t\tFROM (SELECT valor\n' +
        '\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\tON (chk_resp.avaliacao_id = chk_av2.id AND chk_resp.id_web IS NULL)\n' +
        '\t\t\t\t\t\t\tOR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\tWHERE chk_av2.periodo_id = chk_av.periodo_id\n' +
        '\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\t\t\tAND chk_resp.resposta = 1\n' +
        '\t\t\t\t\tGROUP BY chk_resp.pergunta_id)\n' +
        '\t) AS pts_total_conf,\n' +
		  '\t(SELECT SUM(valor)\n' +
        '\t\tFROM (SELECT valor\n' +
        '\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\tON (chk_resp.avaliacao_id = chk_av2.id AND chk_resp.id_web IS NULL)\n' +
        '\t\t\t\t\t\t\tOR chk_av2.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\t\t\tWHERE chk_av2.periodo_id = chk_av.periodo_id\n' +
        '\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\t\t\tAND chk_resp.resposta = 0\n' +
        '\t\t\t\t\tGROUP BY chk_resp.pergunta_id)\n' +
        '\t) AS pts_total_n_conf,\n' +
        valor_chk +
        '\tpeso_chk\n' +
        '\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\tON (chk_resp.avaliacao_id = chk_av.id AND chk_resp.id_web IS NULL)\n' +
        '\t\t\tOR chk_av.id_web = chk_resp.avaliacao_id_web\n' +
        '\t\tLEFT JOIN checklists AS chk ' +
        '\t\t\tON chk_av.checklist_id = chk.id ' +
        '\t\tLEFT JOIN periodo_aplicacoes AS peap\n' +
        '\t\t\tON peap.id = chk_av.periodo_id\n' +
        '\t\tLEFT JOIN propriedades as prop\n' +
        '\t\t\tON prop.id = peap.propriedade_id\n';

      if(agrupar_chk){
        sql += ' WHERE chk_av.grupo_aplicacao_id = (SELECT grupo_aplicacao_id FROM periodo_aplicacoes WHERE id = ?) ' +
          ' AND chk_av.deleted_at IS NULL ' +
          ' AND chk_resp.deleted_at IS NULL ' +
          ' GROUP BY chk_av.id ' +
          ' ORDER BY chk.id ';
      }else{
        sql += ' WHERE chk_av.periodo_id = ? ' +
          '  AND chk_av.deleted_at IS NULL ' +
          ' AND chk_resp.deleted_at IS NULL ' +
          ' AND (chk_av.id_web = chk_resp.avaliacao_id_web \n' +
          '\t\t\t\tOR chk_resp.avaliacao_id_web IS NULL) ' +
          ' GROUP BY chk_av.id ';
      }

      let params: any[] = [periodo_id];
      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }
          return respostas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getChecklistsComTotais',sql, params);
        console.error('Falha ao executar sql periodo', err)
      });
    });
  }

  public getChecklistsRespostasPeriodo(periodo_id, agrupar_chk:any = false){

    let where_sql, where_sql_web;
    if(agrupar_chk){
      where_sql = 'SELECT id FROM checklists_avaliacoes WHERE grupo_aplicacao_id in (SELECT grupo_aplicacao_id FROM checklists_avaliacoes WHERE periodo_id  = ?) AND deleted_at is null';
      where_sql_web = 'SELECT id_web FROM checklists_avaliacoes WHERE grupo_aplicacao_id in (SELECT grupo_aplicacao_id FROM checklists_avaliacoes WHERE periodo_id  = ?) AND deleted_at is null';
    }else{
      where_sql = 'SELECT id FROM checklists_avaliacoes WHERE periodo_id  = ? AND deleted_at is null';
      where_sql_web = 'SELECT id_web FROM checklists_avaliacoes WHERE periodo_id  = ? AND deleted_at is null';
    }

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT chk_resp.avaliacao_id,' +
        '\t\tchk_resp.avaliacao_id_web,\n' +
        '\t\tchk_pg.descricao,\n' +
        '\t\t(CASE WHEN chk_resp.resposta == 1 \n' +
        '\t\t\tTHEN "Conforme" \n' +
        '\t\t\tELSE "Não Conforme" END\n' +
        '\t\t) AS resposta,\n' +
        '\t\t(CASE WHEN chk_resp.valor > 0 \n' +
        '\t\t\t\tTHEN ("+ " || chk_resp.valor) \n' +
        '\t\t\tWHEN chk_resp.valor > 0 \n' +
        '\t\t\t\tTHEN ("- " || chk_resp.valor) \n' +
        '\t\t\tELSE valor END\n' +
        '\t\t) AS valor\n' +
        '\tFROM checklists_respostas AS chk_resp\n' +
        '\t\tLEFT JOIN checklists_perguntas AS chk_pg\n' +
        '\t\t\tON chk_resp.pergunta_id = chk_pg.id\n' +
        '\tWHERE avaliacao_id_web in ('+ where_sql_web +') \n' +
        '\t\tOR (avaliacao_id in ('+ where_sql +') AND chk_resp.avaliacao_id_web IS NULL) \n' +
        '\tGROUP BY pergunta_id \n' +
        '\tORDER BY chk_resp.avaliacao_id, pergunta_id';

      let params = [periodo_id,periodo_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }
          return respostas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getChecklistsComTotais',sql, params);
        console.error('Falha ao executar sql periodo', err)
      });
    });
  }

  public getTotaisDoPeriodo(periodo_id, considerar_cotacao, agrupar_chk:any = false){
    let sql = '';
    let valor_total_checklist = '';
    let valor = '';

    if(considerar_cotacao){
      valor_total_checklist = '\t(chk_av.valor_chk  ) AS valor_total_checklist,\n' ;
      valor = '\t(SELECT SUM(calc.total_valor_check) AS valor\n' +
        '\t\tFROM (SELECT COUNT(DISTINCT chk.id) total_chk,\n' +
        '\t\t\t\t\t\tchk_av8.periodo_id,\n' +
        '\t\t\t\t\t\t((SUM(chk_resp4.valor)*100)/SUM(chk_perg.valor_max)) AS total_peso_check,\n' +
        '\t\t\t\t\t\t(((SUM(chk_resp4.valor)*100)/SUM(chk_perg.valor_max)*(chk_av8.valor_chk))/100) AS total_valor_check\n' +
        '\t\t\t\t\tFROM checklists AS chk\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_avaliacoes AS chk_av8\n' +
        '\t\t\t\t\t\t\tON chk.id = chk_av8.checklist_id\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp4\n' +
        '\t\t\t\t\t\t\tON chk_av8.id = chk_resp4.avaliacao_id\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\t\t\t\tON chk_resp4.pergunta_id = chk_perg.id\n' +
        '\t\t\t\t\tWHERE chk.id in (SELECT checklist_id FROM checklists_avaliacoes WHERE periodo_id = chk_av8.periodo_id GROUP BY checklist_id)\n' +
        '\t\t\t\t\t\tAND chk_av8.periodo_id = chk_av.periodo_id\n' +
        '\t\t\t\t\t\tGROUP BY chk.id ) AS calc\n' +
        '\t) AS valor\n' ;
    }else{
      valor_total_checklist =  ' (SELECT sum(valor_total) AS valor_total ' +
        '  FROM checklists ' +
        '  WHERE id in (SELECT checklist_id ' +
        '      FROM checklists_avaliacoes AS chk_av3 ' +
        '      WHERE chk_av.periodo_id = chk_av3.periodo_id ' +
        '      GROUP BY checklist_id) ' +
        ' ) AS valor_total_checklist, ';

      valor = ' (SELECT sum(calc.total_valor_check) AS valor ' +
        '  FROM (SELECT count(DISTINCT chk.id) total_chk, ' +
        '     chk_av6.periodo_id,  ' +
        '     chk.valor_total, ' +
        '     sum(chk_resp4.valor) AS peso_resp, ' +
        '     sum(chk_perg.valor_max) AS peso_perg, ' +
        '     ((sum(chk_resp4.valor)*100)/sum(chk_perg.valor_max)) AS total_peso_check, ' +
        '     (((sum(chk_resp4.valor)*100)/sum(chk_perg.valor_max)*chk.valor_total)/100) AS total_valor_check ' +
        '    FROM checklists AS chk ' +
        '     LEFT JOIN checklists_avaliacoes AS chk_av6 ' +
        '      ON chk_resp4.avaliacao_id = chk_av6.id ' +
        '     LEFT JOIN checklists_respostas AS chk_resp4 ' +
        '      ON chk_av6.id = chk_resp4.avaliacao_id ' +
        '     LEFT JOIN checklists_perguntas AS chk_perg ' +
        '      ON chk_resp4.pergunta_id = chk_perg.id ' +
        '     WHERE chk.id in (SELECT checklist_id ' +
        '          FROM checklists_avaliacoes AS chk_av7 ' +
        '          WHERE chk_av6.periodo_id = chk_av7.periodo_id ' +
        '          GROUP BY checklist_id) ' +
        '      AND chk_av6.periodo_id = chk_av.periodo_id ' +
        '      GROUP BY chk.id) AS calc ' +
        ' ) AS valor ';
    }

    if(agrupar_chk){
      sql = 'SELECT (SELECT COUNT(chk_resp1.id) ' +
        '  FROM checklists_respostas AS chk_resp1 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av1 ' +
        '    ON (chk_resp1.avaliacao_id = chk_av1.id AND chk_resp1.id_web IS NULL) ' +
        '    OR chk_resp1.avaliacao_id_web = chk_av1.id_web ' +
        '  WHERE chk_av.grupo_aplicacao_id = chk_av1.grupo_aplicacao_id ' +
        '   AND chk_av1.deleted_at IS NULL ' +
        '   AND chk_resp1.deleted_at IS NULL ' +
        '   AND chk_resp1.resposta == 1 ' +
        ' ) AS total_conformes, ' +
        ' (SELECT COUNT(chk_resp2.id) ' +
        '  FROM checklists_respostas AS chk_resp2 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av2 ' +
        '    ON (chk_resp2.avaliacao_id = chk_av2.id AND chk_resp2.id_web IS NULL) ' +
        '    OR chk_resp2.avaliacao_id_web = chk_av2.id_web ' +
        '  WHERE chk_av.grupo_aplicacao_id = chk_av2.grupo_aplicacao_id ' +
        '   AND chk_av2.deleted_at IS NULL ' +
        '   AND chk_resp2.deleted_at IS NULL ' +
        '   AND chk_resp2.resposta == 0 ' +
        '   AND (chk_resp2.nao_se_aplica IS NULL OR chk_resp2.nao_se_aplica == 0) ' +
        ' ) AS total_nao_conformes, ' +
        ' (SELECT count(chk_resp3.id) ' +
        '  FROM checklists_respostas AS chk_resp3 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av3 ' +
        '    ON (chk_resp3.avaliacao_id = chk_av3.id AND chk_resp3.id_web IS NULL) ' +
        '    OR chk_resp3.avaliacao_id_web = chk_av3.id_web ' +
        '  WHERE chk_av.grupo_aplicacao_id = chk_av3.grupo_aplicacao_id ' +
        '   AND chk_av3.deleted_at IS NULL ' +
        '   AND chk_resp3.deleted_at IS NULL ' +
        '   AND (chk_resp3.nao_se_aplica = 1 OR chk_resp3.nao_se_aplica = "true") ' +
        ' ) AS total_nao_se_aplica, ' +
        ' (SUM(chk_perg.valor_max)/(SELECT count(id) FROM checklists_avaliacoes WHERE grupo_aplicacao_id = chk_av.grupo_aplicacao_id AND deleted_at is null)) AS valor_max,' +
        valor_total_checklist +
        '\t(SELECT (SUM(calc.total_peso_check)/SUM(total_chk)) AS peso\n' +
        '\t\tFROM (SELECT COUNT(DISTINCT chk.id) total_chk,\n' +
        '\t\t\t\t\t\tchk_av4.periodo_id,\n' +
        '\t\t\t\t\t\tchk_av4.valor_chk,\n' +
        '\t\t\t\t\t\tSUM(chk_resp3.valor) AS peso_resp,\n' +
        '\t\t\t\t\t\tSUM(chk_perg.valor_max) AS peso_perg,\n' +
        '\t\t\t\t\t\t((SUM(chk_resp3.valor)*100)/SUM(chk_perg.valor_max)) AS total_peso_check,\n' +
        '\t\t\t\t\t\t(((SUM(chk_resp3.valor)*100)/SUM(chk_perg.valor_max)*chk_av4.valor_chk)/100) AS total_valor_check\n' +
        '\t\t\t\t\tFROM checklists AS chk\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_avaliacoes AS chk_av4\n' +
        '\t\t\t\t\t\t\tON chk.id = chk_av4.checklist_id\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp3\n' +
        '\t\t\t\t\t\t\tON chk_av4.id = chk_resp3.avaliacao_id\n' +
        '\t\t\t\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\t\t\t\tON chk_resp3.pergunta_id = chk_perg.id\n' +
        '\t\t\t\t\tWHERE chk.id in (SELECT checklist_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av5\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av5.grupo_aplicacao_id = chk_av4.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\tGROUP BY checklist_id)\n' +
        '\t\t\t\t\t\tAND chk_av4.grupo_aplicacao_id = chk_av.grupo_aplicacao_id\n' +
        '\t\t\t\t\t\tGROUP BY chk.id) AS calc\n' +
        '\t) AS peso,\n' +
        valor +
        '\tFROM checklists_avaliacoes AS chk_av  \n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\tON (chk_resp.avaliacao_id = chk_av.id AND chk_av.id_web IS NULL)\n' +
        '\t\tLEFT JOIN checklists AS chk\n' +
        '\t\t\tON chk_av.checklist_id = chk.id\n' +
        '\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\tON chk_resp.pergunta_id = chk_perg.id\n' +
        '\tWHERE chk_av.grupo_aplicacao_id = (SELECT grupo_aplicacao_id FROM periodo_aplicacoes WHERE id = ?)\n' +
        '\t\tAND chk_av.deleted_at IS NULL'
    }else{
      sql = 'SELECT (SELECT COUNT(chk_resp1.id) ' +
        '  FROM checklists_respostas AS chk_resp1 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av1 ' +
        '    ON (chk_resp1.avaliacao_id = chk_av1.id AND chk_resp1.id_web IS NULL) ' +
        '    OR chk_resp1.avaliacao_id_web = chk_av1.id_web ' +
        '  WHERE chk_av.periodo_id = chk_av1.periodo_id ' +
        '   AND (chk_av.id_web = chk_resp1.avaliacao_id_web \n' +
        '\t\t\t\tOR chk_resp1.avaliacao_id_web IS NULL) ' +
        '   AND chk_av1.deleted_at IS NULL ' +
        '   AND chk_resp1.resposta == 1 ' +
        ' ) AS total_conformes, ' +
        ' (SELECT COUNT(chk_resp2.id) ' +
        '  FROM checklists_respostas AS chk_resp2 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av2 ' +
        '    ON (chk_resp2.avaliacao_id = chk_av2.id AND chk_resp2.id_web IS NULL) ' +
        '    OR chk_resp2.avaliacao_id_web = chk_av2.id_web ' +
        '  WHERE chk_av.periodo_id = chk_av2.periodo_id ' +
        '    AND (chk_av.id_web = chk_resp2.avaliacao_id_web \n' +
        '\t\t\t\tOR chk_resp2.avaliacao_id_web IS NULL)' +
        '    AND chk_av2.deleted_at IS NULL' +
        '    AND chk_resp2.resposta == 0 ' +
        '    AND (chk_resp2.nao_se_aplica IS NULL  ' +
        '           OR chk_resp2.nao_se_aplica == 0) ' +
        ' ) AS total_nao_conformes, ' +
        ' (SELECT count(chk_resp3.id) ' +
        '  FROM checklists_respostas AS chk_resp3 ' +
        '   LEFT JOIN checklists_avaliacoes AS chk_av3 ' +
        '    ON (chk_resp3.avaliacao_id = chk_av3.id AND chk_resp3.id_web IS NULL)  ' +
        '    OR chk_resp3.avaliacao_id_web = chk_av3.id_web ' +
        '  WHERE chk_av.periodo_id = chk_av3.periodo_id ' +
        '   AND (chk_av.id_web = chk_resp3.avaliacao_id_web \n' +
        '\t\t\t\tOR chk_resp3.avaliacao_id_web IS NULL)' +
        '   AND chk_av3.deleted_at IS NULL' +
        '   AND (chk_resp3.nao_se_aplica = 1 OR chk_resp3.nao_se_aplica = "true") ' +
        ' ) AS total_nao_se_aplica, ' +
        ' SUM(chk_perg.valor_max) AS valor_max, ' +
        valor_total_checklist +
        ' (SELECT (SUM(calc.total_peso_check)/SUM(total_chk)) AS peso ' +
        '  FROM (SELECT  COUNT(DISTINCT chk.id) total_chk, ' +
        '     chk_av4.periodo_id,  ' +
        '     chk.valor_total,  ' +
        '     SUM(chk_resp3.valor) AS peso_resp, ' +
        '     SUM(chk_perg.valor_max) AS peso_perg, ' +
        '     ((SUM(chk_resp3.valor)*100)/SUM(chk_perg.valor_max)) AS total_peso_check, ' +
        '     (((SUM(chk_resp3.valor)*100)/SUM(chk_perg.valor_max)*chk.valor_total)/100) AS total_valor_check ' +
        '    FROM checklists AS chk ' +
        '     LEFT JOIN checklists_avaliacoes AS chk_av4 ' +
        '      ON chk.id = chk_av4.checklist_id ' +
        '     LEFT JOIN checklists_respostas AS chk_resp3 ' +
        '      ON chk_av4.id = chk_resp3.avaliacao_id ' +
        '     LEFT JOIN checklists_perguntas AS chk_perg ' +
        '      ON chk_resp3.pergunta_id = chk_perg.id ' +
        '    WHERE chk.id in (SELECT checklist_id ' +
        '         FROM checklists_avaliacoes AS chk_av5 ' +
        '         WHERE chk_av5.periodo_id = chk_av4.periodo_id ' +
        '         GROUP BY checklist_id) ' +
        '     AND chk_av4.periodo_id = chk_av.periodo_id ' +
        '     GROUP BY chk.id) AS calc ' +
        ' ) AS peso, ' +
        valor +
        ' FROM checklists_avaliacoes AS chk_av ' +
        '  LEFT JOIN checklists_respostas AS chk_resp ' +
        '   ON chk_av.id = chk_resp.avaliacao_id ' +
        '  LEFT JOIN checklists AS chk ' +
        '   ON chk_av.checklist_id = chk.id ' +
        '  LEFT JOIN checklists_perguntas AS chk_perg ' +
        '   ON chk_resp.pergunta_id = chk_perg.id ' +
        ' WHERE chk_av.periodo_id = ? ' +
        '   AND chk_av.deleted_at IS NULL ' +
        ' GROUP BY chk_av.periodo_id';
    }


    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let params: any[] = [periodo_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getTotaisDoPeriodo',sql, params);
        console.error('Falha ao executar sql getTotaisDoPeriodo', err, sql)
      });
    });
  }

  public getPeriodoChkAuto(galpao_id, intervalo_id, checklist_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = ' SELECT id FROM periodo_aplicacoes AS peap ' +
        '  WHERE galpao_id = ?  ' +
        '    AND intervalo_id = ?  ' +
        '    AND checklist_id = ? ' +
        '    AND EXISTS (SELECT * FROM checklists_avaliacoes WHERE periodo_id = peap.id) ' +
        '  ORDER BY id DESC ' +
        '  LIMIT 1';
      let params = [galpao_id, intervalo_id, checklist_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getTotaisDoPeriodo',sql, params);
        console.error('Falha ao executar sql lotes', err, sql)
      });
    });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaPeriodos(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/periodos`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Períodos de Aplicação...');
          if (results){
            for (let result of results){
              let sql_up = 'update periodo_aplicacoes SET propriedade_id=?, galpao_id=?, lote_id=?, intervalo_id=?, checklist_id=?, data_inicio=?, data_final=?, vigente=? where id = ?;';
              let data_up = [result.propriedade_id, result.galpao_id, result.lote_id, result.intervalo_id, result.checklist_id, result.data_inicio, result.data_final, result.vigente, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO periodo_aplicacoes (id, propriedade_id, galpao_id, lote_id, intervalo_id, checklist_id, data_inicio, data_final, vigente) values (?, ?, ?, ?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.propriedade_id, result.galpao_id, result.lote_id, result.intervalo_id, result.checklist_id, result.data_inicio, result.data_final, result.vigente];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'periodos.ts', 'atualizaPeriodos', sql_ins, data_ins);
                    reject();
                  })
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'periodos.ts', 'atualizaPeriodos', sql_up,data_up);
                console.error('falha ao exectar sql ao inserir periodos', err);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          // this.exceptionProvider.insert(err, 'periodos.ts', 'atualizaPeriodos', `${url}/periodos`);
          console.error('falha ao consultar periodos', err);
          reject();
        })
      })
    });

    return promisse;
  }

}
