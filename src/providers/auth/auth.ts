import {Injectable, ViewChild} from "@angular/core";
import {catchError, tap} from 'rxjs/operators';
import { ReplaySubject , Observable} from "rxjs";
import { Storage } from "@ionic/storage";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { JwtHelperService } from "@auth0/angular-jwt";
import {CheckNetworkProvider} from "../check-network/check-network";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {Events, Nav} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class AuthProvider {
  @ViewChild('myNav') nav: Nav;
  private jwtTokenName = 'access_token';
  private urlApi;

  authUser = new ReplaySubject<any>(1);

  constructor(private readonly httpClient: HttpClient,
              private readonly storage: Storage,
              private readonly jwtHelper: JwtHelperService,
              private readonly network: CheckNetworkProvider,
              private readonly messageService: AlertMessagesProvider,
              private dbProvider: DatabaseProvider,
              private exceptionProvider:DbExceptionProvider,
              private events:Events,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;
          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  checkLogin() {
    var promisse = new Promise((resolve,reject) => {
        if (this.network.isConnected() && this.urlApi) {
          this.storage.get(this.jwtTokenName).then(jwt => {
            if (jwt && !this.jwtHelper.isTokenExpired(jwt)) {
              this.httpClient.get(`${this.urlApi}/me`).subscribe((data) => {
                  this.authUser.next(jwt);
                    resolve('success');
              },(err) => {
                this.storage.remove(this.jwtTokenName).then(() => this.authUser.next(null));
              });
            } else {
              this.storage.remove(this.jwtTokenName).then(() => {
                this.authUser.next(null)
              });

              this.storage.get('login').then((data) => {
                  // let loading = this.messageService.showLoading('Autenticando...');
                  /*Caso a conexão demore mais que 10s para autenticar*/
                  let timeout = setTimeout(() => {
                    this.messageService.showAlert('Opss!', 'Sua conexão parece estar lenta! A validação de login não foi efetuada, o aplicativo pode apresentar lentidão em alguns momentos.').then(()=>{
                      resolve('success');
                      // loading.dismiss();
                    });
                  }, 10000);
                  this.login(data,this.urlApi).subscribe(()=>{
                    // loading.dismiss();
                    this.dbProvider.getDB().then((db: SQLiteObject) => {
                      let sql = 'SELECT ultima_sincronizacao FROM tecnicos';
                      db.executeSql(sql,[]).then((data)=>{
                        if(data.rows.item(0) == null){
                          this.messageService.showConfirmationAlert('Sincronização Inicial', 'Deseja fazer a sincronização inicial agora?', 'Sim, Sincronizar!', 'Cancelar').then((res)=>{
                            if(res){
                              this.nav.push('SincronizacaoPage', {automatico:true});
                            }
                          })
                        }
                      }).catch(err=>{
                        this.exceptionProvider.insert(err, 'auth.ts', 'checkLogin', sql, null);
                        console.log('falha ao consultar ultima_sincronizacao');
                      })
                    });
                    resolve('success');
                    clearTimeout(timeout);

                  },()=>{
                    // loading.dismiss();
                    resolve('error');
                    clearTimeout(timeout);

                  });

              });
            }
          });
        }else {
          this.storage.get('login').then((data) => {
            if (!data) {
              this.authUser.next(null)
            }
          });
        }
    });
    return promisse;
  }

  login(values: any, SERVER_URL): Observable<any> {
    return this.httpClient.post(`${SERVER_URL}/login`, values, {responseType: 'text'})
      .pipe(tap(jwt => {
        this.handleJwtResponse(jwt)
      }),
        catchError((err: HttpErrorResponse) => {
          this.messageService.showToastError('Ops! Houve um problema ao efetuar o login. Tente novamente mais tarde!');
          // this.exceptionProvider.insert(err, 'auth.ts', 'login');

          return Observable.throw(err);
        }));
  }

  logout() {
    this.storage.remove('login');
    this.storage.remove('SERVER_URL');
    this.storage.remove(this.jwtTokenName).then(() => this.authUser.next(null));
  }

  private handleJwtResponse(jwt: string) {
    let token = JSON.parse(jwt).access_token;

    return this.storage.set(this.jwtTokenName, token)
      .then(() => this.authUser.next(token))
      .then(() => token);
  }
}