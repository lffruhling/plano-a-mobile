import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {Events, LoadingController} from "ionic-angular";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class ChecklistProvider {
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    public http: HttpClient,
    private readonly messageService: AlertMessagesProvider,
    private storage: Storage,
    private events: Events,
    private exceptionProvider: DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/checklist`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/checklist`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}/checklist`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/tecnicos?last_update=${lastUpdate}`);
  }

  public apiGetFaseAll(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/fases?last_update=${lastUpdate}`);
  }

  public apiGetSubtiposIntegracoesAll() {
    return this.http.get<any>(`${this.urlApi}/subtipos/integracoes`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  public getChecklistLocal(subtipo_integracao_id, lote_id, repetir) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = 'SELECT chk.*, ' +
        ' COALESCE(chk_fs.descricao, "Todas") AS fase_descricao, ' +
        ' sub_tp.descricao AS subtipo_integracao_descricao, ' +
        ' tpi.descricao AS integracao_descricao, ' +
        ' (SELECT count(id) FROM checklists_avaliacoes_aux  ' +
        '  WHERE lote_id = ? AND avaliacao_id IS NULL AND checklist_id = chk.id ' +
        ' ) AS respsotas_aux,  ' +
        ' chk.ordem_aplicacao, ' +
        ' chk.obriga_ordenacao, ' +
        '\t(SELECT ordem_aplicacao\n' +
        '\tFROM checklists AS chk\n' +
        '\tWHERE NOT EXISTS (SELECT * \n' +
        '\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av \n' +
        '\t\t\t\t\t\t\t\t\t\tWHERE chk.id = chk_av.checklist_id \n' +
        '\t\t\t\t\t\t\t\t\t\tAND chk_av.lote_id = ?)\n' +
        '\t\tAND deleted_at IS NULL\n' +
        '\tLIMIT 1) AS chk_aplicavel\n' +
        'FROM checklist_subtipo_integracao AS chk_sub_tp ' +
        ' LEFT JOIN checklists AS chk ' +
        '  ON chk_sub_tp.checklist_id = chk.id ' +
        ' LEFT JOIN subtipo_integracoes AS sub_tp ' +
        '  ON chk_sub_tp.subtipo_integracao_id = sub_tp.id ' +
        ' LEFT JOIN tipo_integracoes AS tpi ' +
        '  ON sub_tp.tipo_integracao_id = tpi.id ' +
        ' LEFT JOIN checklists_fases AS chk_fs ' +
        '  ON chk.fase_checklist_id = chk_fs.id ' +
        'WHERE chk_sub_tp.subtipo_integracao_id = ? ' +
        ' AND chk.deleted_at IS NULL ';
      if (!repetir) {
        sql += ' AND not exists (SELECT * FROM checklists_avaliacoes AS chk_av ' +
          '      WHERE chk.id = chk_av.checklist_id ' +
          '      AND lote_id = ? ' +
          '       AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)) ' +
          ' GROUP BY chk.id';
        params = [lote_id, lote_id, subtipo_integracao_id, lote_id];

      } else {
        params = [lote_id, lote_id, subtipo_integracao_id];

      }

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var checklist = data.rows.item(i);
            checklists.push(checklist);
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getChecklistLocal', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public getChecklistNoPeriodoLocal(galpao_id, intervalo_id, repetir, agrupa_chk) {


    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = "SELECT chk.id,  " +
        " data_inicio, " +
        " data_final, " +
        " Cast ((JulianDay(data_final) - JulianDay('now')) As Integer) as dias_vencimento, " +
        " pap.id AS periodo_id, " +
        " pap.grupo_aplicacao_id, " +
        " chk.descricao, " +
        " chk.ordem_aplicacao, " +
        " chk.obriga_ordenacao, " +
        "\t\tCOALESCE((SELECT ordem_aplicacao FROM checklists AS chk\n" +
        "\t\t\t\t\t\t\tWHERE NOT EXISTS (SELECT * FROM checklists_avaliacoes AS chk_av\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN periodo_aplicacoes as pap2\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tON pap2.id = chk_av.periodo_id\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE chk.id = chk_av.checklist_id\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND pap2.grupo_aplicacao_id = pap.grupo_aplicacao_id)\n" +
        "\t\t\t\t\t\t\t\tAND obriga_ordenacao == 1\n" +
        "\t\t\t\t\t\t\t\tAND deleted_at IS NULL\n" +
        "\t\t\t\t\t\t\tLIMIT 1),\n" +
        "\t\t\t1) AS chk_aplicavel,\n" +
        " COALESCE(chk_fs.descricao, 'Todas') AS fase_descricao, " +
        " (SELECT GROUP_CONCAT(subtp.descricao, ', ') AS descricao  " +
        "  FROM checklist_subtipo_integracao AS CHK_SUB_INT " +
        "  LEFT JOIN subtipo_integracoes AS subtp " +
        "   ON CHK_SUB_INT.subtipo_integracao_id = subtp.id " +
        "  WHERE checklist_id = chk.id " +
        " ) AS subtipo_integracao_descricao, " +
        " (SELECT GROUP_CONCAT(tpi.descricao, ', ') AS descricao  " +
        "  FROM checklist_subtipo_integracao AS CHK_SUB_INT " +
        "  LEFT JOIN subtipo_integracoes AS subtp " +
        "   ON CHK_SUB_INT.subtipo_integracao_id = subtp.id " +
        "  LEFT JOIN tipo_integracoes AS tpi " +
        "   ON tpi.id = subtp.tipo_integracao_id  " +
        "  WHERE checklist_id = chk.id " +
        " ) AS integracao_descricao, " +
        " (SELECT count(id) FROM checklists_avaliacoes_aux AS chk_av_aux " +
        "  WHERE periodo_id = pap.id " +
        "   AND avaliacao_id IS NULL " +
        "   AND checklist_id = chk.id " +
        "   AND EXISTS (SELECT id FROM checklists_respostas_aux AS chk_av_aux2 " +
        "      WHERE chk_av_aux.id = chk_av_aux2.avaliacao_id_aux) " +
        " ) AS respsotas_aux " +
        "FROM periodo_aplicacoes AS pap  " +
        " LEFT JOIN checklists AS chk " +
        "  ON pap.checklist_id = chk.id " +
        " LEFT JOIN checklists_fases AS chk_fs " +
        "  ON chk.fase_checklist_id = chk_fs.id " +
        " LEFT JOIN subtipo_integracoes AS subtp " +
        "  ON chk.subtipo_integracao_id = subtp.id " +
        " LEFT JOIN tipo_integracoes AS tpi " +
        "  ON tpi.id = subtp.tipo_integracao_id " +
        " LEFT JOIN intervalos AS intv\n" +
        "\t\t\tON intv.id = pap.intervalo_id \n" +
        "WHERE intervalo_id = ?  " +
        " AND galpao_id = ? " +
        " AND chk.deleted_at IS NULL " +
        " AND pap.deleted_at IS NULL";

      let params: any[];

      if (!repetir) {
        sql += " AND not exists (SELECT * FROM checklists_avaliacoes AS chk_av " +
          "     WHERE pap.id = chk_av.periodo_id AND (chk_av.deleted_at == 'null' OR chk_av.deleted_at IS NULL)) " +
          " AND vigente = ? ";
        params = [intervalo_id, galpao_id, true];
      }else{
        /*Para utilizar somente os checklist vigentes/Não atrasados é só mudas a lógica de dias_vencimento > -intv.dias PARA dias_vencimento > 0*/
        sql += "\t\tAND (not exists (SELECT * FROM checklists_avaliacoes AS chk_av \n" +
          "\t\t\t\t\t\t\t\t\t\tWHERE pap.id = chk_av.periodo_id AND (chk_av.deleted_at == 'null' OR chk_av.deleted_at IS NULL)) OR dias_vencimento > -intv.dias) " +
          " ORDER BY dias_vencimento DESC ";
        if(!agrupa_chk){
          sql += "LIMIT 2";

        }
        params = [intervalo_id, galpao_id];
      }

      // console.log('sql', sql);
      // console.log('params', params);

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            checklists.push(data.rows.item(i));
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getChecklistNoPeriodoLocal', sql, params);
        console.error('Falha ao executar sql checklists', err, sql)
      });
    });
  }

  public getByLoteAndDateAndFase(initialDate, finalDate, fase){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql_fase = '';
      if(fase == 3){
        sql_fase = ' date(lt.data_alojamento, "+50 day") AS date_limit ';
      }else if (fase == 4){
        sql_fase = ' date(lt.data_previsao_carregamento, "-10 day") AS date_limit ';
      }

      let params: any[];
      let sql = 'SELECT chk.id,' +
        ' lt.id AS lote_id,' +
        ' null AS periodo_id,' +
        ' "clipboard" AS icon, 2 AS type, ' +
        '  (prod.nome || " - " || prop.descricao || " - " || gp.descricao) AS location,  ' +
        '  chk.descricao AS message, ' +
        '  ("LOTE - " || lt.nro_lote) AS to_apply, ' +
        '  lt.data_alojamento AS alojamento, ' +
        '  lt.data_previsao_carregamento AS carregamento, ' +
        sql_fase +
        ' FROM checklist_subtipo_integracao AS chk_sub_tp ' +
        '  LEFT JOIN checklists AS chk ' +
        '   ON chk_sub_tp.checklist_id = chk.id ' +
        '  LEFT JOIN propriedades AS prop ' +
        '   ON chk_sub_tp.subtipo_integracao_id = prop.subtipo_integracao_id ' +
        '  LEFT JOIN produtores AS prod ' +
        '   ON prop.produtor_id = prod.id ' +
        '  LEFT JOIN lotes AS lt ' +
        '   ON prop.id = lt.propriedade_id ' +
        '  LEFT JOIN galpoes AS gp ' +
        '   ON lt.galpao_id = gp.id ' +
        ' WHERE date_limit BETWEEN ? AND ?' +
        '  AND chk.fase_checklist_id == ?' +
        '  AND NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av  ' +
        '           WHERE lote_id = lt.id  ' +
        '           AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)) ' +
        '  AND (chk.deleted_at IS NULL OR chk.deleted_at == "null")';

      params = [initialDate, finalDate, fase];

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var checklist = data.rows.item(i);
            checklists.push(checklist);
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByLoteAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public getByPeriodoAndDate(initialDate, finalDate){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = ' SELECT chk.id,' +
        ' prop.id AS propriedade_id,' +
        ' null AS lote_id,' +
        ' peap.id AS periodo_id,' +
        ' peap.propriedade_id,' +
        ' peap.intervalo_id,' +
        ' "clipboard" AS icon, 2 AS type, ' +
        ' "Checklist" as title,' +
        '  (prod.nome || " - " || prop.descricao) AS location,  ' +
        '  chk.descricao AS message, ' +
        '  peap.data_final AS date_limit  ' +
        ' FROM periodo_aplicacoes AS peap ' +
        '  LEFT JOIN checklists AS chk ' +
        '   ON peap.checklist_id = chk.id ' +
        '  LEFT JOIN propriedades AS prop ' +
        '   ON peap.propriedade_id = prop.id ' +
        '  LEFT JOIN produtores AS prod ' +
        '   ON prop.produtor_id = prod.id ' +
        ' WHERE peap.data_final BETWEEN ? AND ? ' +
        '  AND (peap.deleted_at == "null" OR peap.deleted_at IS NULL) ' +
        '  AND NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av  ' +
        '           WHERE periodo_id = peap.id  ' +
        '           AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)) ' +
        '  AND EXISTS (SELECT id FROM propriedades as prop WHERE id = peap.propriedade_id ' +
        '               AND (prop.deleted_at IS NULL OR prop.deleted_at == "null"))';

      params = [initialDate, finalDate];

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var checklist = data.rows.item(i);
            checklists.push(checklist);
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByPeriodoAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public find(id){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = ' select ordem_aplicacao from checklists where id = ?';

      params = [id];

      return db.executeSql(sql, params).then((data: any) => {
        return data.rows.item(0).ordem_aplicacao;
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByPeriodoAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public findCheck(id){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = ' select * from checklists where id = ?';

      params = [id];

      return db.executeSql(sql, params).then((data: any) => {
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByPeriodoAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public findChkAplicacaoAtual(lote_id){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = 'SELECT ordem_aplicacao\n' +
        '\t\tFROM checklists AS chk\n' +
        '\t\tWHERE NOT EXISTS (SELECT *\n' +
        '\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av \n' +
        '\t\t\t\t\t\t\t\t\t\t\tWHERE chk.id = chk_av.checklist_id\n' +
        '\t\t\t\t\t\t\t\t\t\t\tAND chk_av.lote_id = ?)\n' +
        '\t\tAND deleted_at IS NULL\n' +
        '\tLIMIT 1';

      params = [lote_id];

      return db.executeSql(sql, params).then((data: any) => {
        return data.rows.item(0).ordem_aplicacao;
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByPeriodoAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public insert(checklist) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'insert into checklists (id, subtipo_integracao_id, fase_checklist_id, ordem_aplicacao, obriga_ordenacao, descricao, valor_total, obrigatorio, deleted_at) values (?,?,?,?,?,?,?,?,?)';
      let data = [
        checklist.id,
        checklist.subtipo_integracao_id,
        checklist.fase_checklist_id,
        checklist.ordem_aplicacao,
        checklist.obriga_ordenacao,
        checklist.descricao,
        checklist.valor_total,
        checklist.obrigatorio,
        checklist.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'insert', sql, data);
        console.error('Falha ao inserir checklists', err, sql, data)
      })
    })
  }

  public update(checklist) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'update checklists SET subtipo_integracao_id=?, fase_checklist_id=?, ordem_aplicacao=?, obriga_ordenacao=?, descricao=?, valor_total=?, obrigatorio=?, deleted_at=? where id = ?;';
      let data = [
        checklist.subtipo_integracao_id,
        checklist.fase_checklist_id,
        checklist.ordem_aplicacao,
        checklist.obriga_ordenacao,
        checklist.descricao,
        checklist.valor_total,
        checklist.obrigatorio,
        checklist.deleted_at,
        checklist.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'update', sql, data);
        console.error('Falha ao atualizar checklists', err, sql, data)
      })
    })
  }

  public insertFase(fase) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'insert into checklists_fases (id, descricao, deleted_at) values (?,?,?)';
      let data = [
        fase.id,
        fase.descricao,
        fase.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'insertFase', sql, data);
        console.error('Falha ao inserir checklists_fases', err, sql, data)
      })
    })
  }

  public updateFase(fase) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'update checklists_fases set descricao=?, deleted_at=? where id =?';
      let data = [
        fase.descricao,
        fase.deleted_at,
        fase.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'updateFase', sql, data);
        console.error('Falha ao atualizar checklists_fases', err, sql, data)
      })
    })
  }

  public insertChecklistSubtipoIntegracao(chk_subtipo_integ){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = ' insert into checklist_subtipo_integracao (checklist_id, subtipo_integracao_id) values (?,?)';
      let data = [
        chk_subtipo_integ.checklist_id,
        chk_subtipo_integ.subtipo_integracao_id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'insertChecklistSubtipoIntegracao', sql, data);
        console.error('Falha ao inserir checklist_subtipo_integracao', err, sql, data)
      })
    })
  }

  public deleteChecklistSubtipoIntegracao(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = ' delete from checklist_subtipo_integracao';
      let data = [];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'deleteChecklistSubtipoIntegracao', sql, data);
        console.error('Falha ao deletar checklist_subtipo_integracao', err, sql, data)
      })
    })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaFasesChecklist(url) {
    /*Atualiza Ações*/
    var promisse = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        this.http.get(`${url}/checklist/fases`).subscribe((results: any) => {
          let loading = this.messageService.showLoading('Atualizando fazes dos checklists...');
          if (results) {
            for (let result of results) {
              let sql_up = 'update checklists_fases SET descricao=? where id = ?;';
              let data_up = [result.descricao, result.id];
              db.executeSql(sql_up, data_up).then((data: any) => {
                if (data.rowsAffected < 1) {
                  let sql_ins = 'INSERT OR IGNORE INTO checklists_fases (id, descricao) values (?, ?)';
                  let data_ins = [result.id, result.descricao];
                  db.executeSql(sql_ins, data_ins).catch(err => {
                    this.exceptionProvider.insert(err, 'checklists.ts', 'atualizaFasesChecklist', sql_ins, data_ins);
                    reject();
                  });
                }
              }).catch(err => {
                this.exceptionProvider.insert(err, 'checklists.ts', 'atualizaFasesChecklist', sql_up, data_up);
                reject();
              });
            }
          }
          loading.dismiss();
          resolve();
        });
      });
    });
    return promisse;
  }

  public atualizaChecklists(url) {
    /*Atualiza Ações*/
    var promisse = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        this.http.get(`${url}/checklist/tecnicos`).subscribe((results: any) => {
          let loading = this.messageService.showLoading('Atualizando Checklists...');
          if (results) {
            for (let result of results) {
              let sql_up = 'update checklists SET subtipo_integracao_id=?, fase_checklist_id=?, ordem_aplicacao=?,obriga_ordenacao=?, descricao=?, valor_total=?, obrigatorio=?, deleted_at=? where id = ?;';
              let data_up = [result.subtipo_integracao_id, result.fase_checklist_id, result.ordem_aplicacao, result.obriga_ordenacao, result.descricao, result.valor_total, result.obrigatorio, result.deleted_at, result.id];
              db.executeSql(sql_up, data_up).then((data: any) => {
                if (data.rowsAffected < 1) {
                  let sql_ins = 'INSERT OR IGNORE INTO checklists (id, subtipo_integracao_id, fase_checklist_id, ordem_aplicacao, obriga_ordenacao, descricao, valor_total, obrigatorio, deleted_at) values (?,?,?,?,?,?,?,?)';
                  let data_ins = [result.id, result.subtipo_integracao_id, result.fase_checklist_id, result.ordem_aplicacao, result.obriga_ordenacao, result.descricao, result.valor_total, result.obrigatorio, result.deleted_at]
                  db.executeSql(sql_ins, data_ins).catch(err => {
                    this.exceptionProvider.insert(err, 'checklists.ts', 'atualizaChecklists', sql_ins, data_ins);
                    reject();
                  })
                }
              }).catch(err => {
                this.exceptionProvider.insert(err, 'checklists.ts', 'atualizaChecklists', sql_up, data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        });
      });
    });
    return promisse;
  }

}
