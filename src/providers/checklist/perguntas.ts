import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {Resposta} from "../../class/Resposta";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";
import * as moment from "moment";
import {UniqueIdProvider} from "../unique-id/unique-id";

@Injectable()
export class PerguntasProvider {
  private urlApi;
  private totalFotos = 0;

  constructor(
    private dbProvider: DatabaseProvider,
    private http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private fileTransfer:FileTransfer,
    public events: Events,
    private exceptionProvider:DbExceptionProvider,
	private uniqueKeyService: UniqueIdProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;
          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/checklist/perguntas/tecnicos?last_update=${lastUpdate}`);
  }

  public apiStoreResposta(resposta) {
    return this.http.post(`${this.urlApi}/checklist/resposta`, resposta)
  }

  public apiStorePerguntaAcao(pergunta){
    return this.http.post(`${this.urlApi}/pergunta/gerou/acao`, pergunta);
  }

  public apiAtualizaFoto(resposta){
    return this.http.post(`${this.urlApi}/checklist/resposta/atualiza/fotos`, resposta);
  }

  public apiGetThumbsPerguntas(lastUpdate){
	  return this.http.get<any>(`${this.urlApi}/checklist/perguntas/thumbs?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(pergunta){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into checklists_perguntas (id, checklist_id, parametro_id, descricao, valor_min, valor_max, data_validade, ordem_aplicacao, deleted_at) values (?,?,?,?,?,?,?,?,?)';
      let data = [
        pergunta.id,
        pergunta.checklist_id,
        pergunta.parametro_id,
        pergunta.descricao,
        pergunta.valor_min,
        pergunta.valor_max,
        pergunta.data_validade,
        pergunta.ordem_aplicacao,
        pergunta.deleted_at
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'insert', sql, data);
        console.error('Falha ao inserir checklists_perguntas', err, sql, data)
      })
    })
  }

  public update(pergunta){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update checklists_perguntas SET checklist_id=?, parametro_id=?, descricao=?, valor_min=?, valor_max=?, data_validade=?, ordem_aplicacao=?, deleted_at=? where id = ?;';
      let data = [
        pergunta.checklist_id,
        pergunta.parametro_id,
        pergunta.descricao,
        pergunta.valor_min,
        pergunta.valor_max,
        pergunta.data_validade,
        pergunta.ordem_aplicacao,
        pergunta.deleted_at,
        pergunta.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'update', sql, data);
        console.error('Falha ao atualizar checklists_perguntas', err, sql, data)
      })
    })
  }

  public salvaOldThumbPergunta(pergunta_id, galpao_id, thumb){
  	let data_i = [
		pergunta_id,
		galpao_id,
		thumb,
	];

  	let data_u = [
		thumb,
		pergunta_id,
		galpao_id,
	];

  	let sql_i = 'INSERT INTO checklists_perguntas_old_thumbs (pergunta_id, galpao_id, foto64) VALUES (?,?,?)'
  	let sql_u = 'UPDATE checklists_perguntas_old_thumbs SET foto64=? WHERE pergunta_id =? and galpao_id = ?'
	this.dbProvider.getDB().then((db:SQLiteObject) => {
		db.executeSql(sql_u, data_u).then((t => {
			if (t.rowsAffected == 0) {
				db.executeSql(sql_i, data_i).catch(err=>console.error('falha ao inserir fotos', err));
			}
		})).catch(err=>console.error('falha ao salvar thumb', err));
	});
  }

  public salvaRespostas(respostas: Resposta[], avaliacao_id_aux, latitude, longitude, assinatura){
    var promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          db.transaction(function (getAVIAux) {
            getAVIAux.executeSql('SELECT * FROM checklists_avaliacoes_aux WHERE id=?', [avaliacao_id_aux], function (insAVI, result_avi_aux) {
              let avi_aux = result_avi_aux.rows.item(0);

              insAVI.executeSql('INSERT INTO checklists_avaliacoes (mobile_key, checklist_id, lote_id, periodo_id, grupo_aplicacao_id, data_aplicacao, assinatura, latitude, longitude) VALUES (?,?,?,?,?,?,?,?,?)',
                [
                  avi_aux.mobile_key,
                  avi_aux.checklist_id,
                  avi_aux.lote_id,
                  avi_aux.periodo_id,
                  avi_aux.grupo_aplicacao_id,
                  avi_aux.data_aplicacao,
				  assinatura,
                  latitude,
                  longitude,
                ], function (upAVIAux, avaliacao) {

                  let avaliacao_id = avaliacao.insertId;
                  upAVIAux.executeSql('UPDATE checklists_avaliacoes_aux SET avaliacao_id=?, salva=? WHERE id=?', [
                    avaliacao_id, true, avaliacao_id_aux
                  ], function (tx) {

                    let total_respostas = respostas.length;
                    respostas.forEach(resposta =>{
                      tx.executeSql('insert into checklists_respostas (avaliacao_id, pergunta_id, resposta, nao_se_aplica, valor, foto, foto_base64, data_resposta, com_foto, mobile_key, com_acao, acao_id) values (?,?,?,?,?,?,?,?,?,?,?,?)',[
                        avaliacao_id,
                        resposta.pergunta_id,
                        resposta.resposta,
                        resposta.nao_se_aplica,
                        resposta.valor,
                        resposta.foto,
                        resposta.foto_base64,
                        resposta.data_resposta,
                        resposta.com_foto,
                        resposta.mobile_key,
                        resposta.com_acao,
                        resposta.acao_id,
                      ], function (txUpRespAux, insResp) {
                        total_respostas -= 1;
                        if(total_respostas == 0){
                          resolve({
                            'id': avaliacao_id,
                            'checklist_id': avi_aux.checklist_id
                          });
                        }
                        txUpRespAux.executeSql('update checklists_respostas_aux set resposta_id = ? WHERE id = ?',[
                          insResp.insertId,
                          resposta.resposta_aux_id
                        ]);
                      }, (error) => {
                        console.error('falha ao inserir respostas', error);
                        reject(error);
                      });
                    });
                  }, (error) => {
                    console.error('falha ao atualiza avi aux', error);
                    reject(error);

                  });
                }, (error) => {
                  console.error('falha ao inserir avi', error);
                  reject(error);

                });
            }, (error) => {
              console.error('falha ao consultar avi aux', error);
              reject(error);

            });
          });
        });
    });

    return promise;
  }

  public salvaResposta(resposta: Resposta, avaliacao_id){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let data = [
        avaliacao_id,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.nao_se_aplica,
        resposta.valor,
        resposta.foto,
        resposta.foto_base64,
        resposta.data_resposta,
        resposta.com_foto,
        resposta.mobile_key
      ];

      let sql = `insert into checklists_respostas (avaliacao_id, pergunta_id, resposta, nao_se_aplica, valor, foto, foto_base64, data_resposta, com_foto, mobile_key) values (?,?,?,?,?,?,?,?,?,?)`;
      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'perguntas.ts', 'salvaResposta', sql, data);
        console.error('Erro ao incluir respostas', err)
      });
    });

  }

  public salvaRespostaAux(resposta: any){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let data = [
        resposta.mobile_key,
        resposta.avaliacao_id_aux,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.nao_se_aplica,
        resposta.valor,
        resposta.foto,
        resposta.foto_base64,
        resposta.thumb_foto,
        resposta.com_foto,
        resposta.data_resposta
      ];

      let sql = `insert into checklists_respostas_aux (mobile_key, avaliacao_id_aux, pergunta_id, resposta, nao_se_aplica, valor, foto, foto_base64, thumb_foto, com_foto, data_resposta) values (?,?,?,?,?,?,?,?,?,?,?)`;
      return  db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'salvaRespostaAux', sql, data);

      })
    });

  }

  public atualizaRespostaAux(resposta: any){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let data = [
        resposta.avaliacao_id_aux,
        resposta.resposta,
        resposta.nao_se_aplica,
        resposta.valor,
        resposta.foto,
        resposta.foto_base64,
        resposta.thumb_foto,
        resposta.com_foto,
        resposta.data_resposta,
        resposta.resposta_aux_id
      ];

      let sql = `update checklists_respostas_aux set avaliacao_id_aux=?, resposta=?, nao_se_aplica=?, valor=?, foto=?,  foto_base64=?, thumb_foto=?, com_foto=?, data_resposta =? WHERE id=?`;
      return  db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'salvaRespostaAux', sql, data);
      })
    });

  }

  public getRespostaAux(pergunta_id, avaliacao_id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT chk_resp_aux.*, ' +
          ' (SELECT count(chk_per_ac.lote_id) ' +
          ' FROM checklists_perguntas_acoes as chk_per_ac ' +
          ' WHERE chk_per_ac.pergunta_id = chk_resp_aux.pergunta_id ' +
          '  AND (chk_per_ac.lote_id = chk_av_aux.lote_id OR chk_per_ac.periodo_id = chk_av_aux.periodo_id) ' +
          ' ) AS com_acao ' +
          'FROM checklists_respostas_aux AS chk_resp_aux ' +
          ' LEFT JOIN checklists_avaliacoes_aux AS chk_av_aux ' +
          '  ON chk_resp_aux.avaliacao_id_aux = chk_av_aux.id ' +
          'WHERE chk_resp_aux.pergunta_id = ? AND chk_resp_aux.avaliacao_id_aux = ?';

        let data = [pergunta_id, avaliacao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            return data.rows.item(0);
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'perguntas.ts', 'getRespostaAux', sql, data);
          });
      });
  }

  public sincRespostaAux(resposta: any){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let data = [
        resposta.id,
        resposta.resposta_aux_id
      ];

      let sql = `update checklists_respostas_aux set resposta_id = ? WHERE id = ?`;
      return  db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'sincRespostaAux', sql, data);

      })
    });

  }

  public updateRespostaAuxComAcao(id_resposta_aux, acao_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update checklists_respostas_aux set com_acao = ?, acao_id =? WHERE id = ?';
      let data = [true, acao_id, id_resposta_aux];

      return db.executeSql(sql, data).then(data=>{
        console.info('resposta aux atualizada com sucesso', data);
      }).catch(err =>{
        console.error('falha ao atualizar resposta aux com acao', err, sql, data);
      })
    });

  }

  public salvaPerguntaAcao(dados:any){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let data = [
        dados.lote_id,
        dados.pergunta_id,
        dados.acao_id,
        dados.mobile_key,
      ];

      let sql = `insert into checklists_perguntas_acoes (lote_id, pergunta_id, acao_id, mobile_key) values (?, ?, ?, ?)`;
      return  db.sqlBatch([
        [sql, data],
      ]).catch(err => {
        this.exceptionProvider.insert(err, 'perguntas.ts', 'salvaPerguntaAcao', sql, data);
        console.error('Erro ao incluir checklists_perguntas_acoes', err)
      });
    });
  }

  public getChecklistLocal(checklist_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
          let sql = 'SELECT * FROM checklists_perguntas WHERE checklist_id = ? AND (deleted_at == "null" OR deleted_at IS NULL) ORDER BY ordem_aplicacao, id';
          let data: any[] = [checklist_id];

          return db.executeSql(sql, data).then( (data:any) =>{
            if (data.rows.length > 0) {
              let perguntas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var pergunta = data.rows.item(i);
                perguntas.push(pergunta);
              }
              return perguntas;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'perguntas.ts', 'getChecklistLocal', sql, data);
            console.error('Falha ao executar sql perguntas', err)
          });
        });

  }

  public getPerguntaAcaoSincronizar(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_perguntas_acoes WHERE sincronizada is null';

      return db.executeSql(sql,[]).then((data)=>{
        if (data.rows.length > 0) {
          let pergunta_acao: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            pergunta_acao.push(resposta);
          }
          return pergunta_acao;
        }else{
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'perguntas.ts', 'getPerguntaAcaoSincronizar', sql, null);
        console.error('Falha ao executar sql perguntas acoes', err)
      });
    });
  }

  public getRespostasParaSincronizar(db:SQLiteObject){
      let sql = 'SELECT chk_resp.id, \n' +
        '\tchk_resp.id_web,\n' +
        '\tchk_resp.mobile_key,\n' +
        '\tchk_resp.avaliacao_id,\n' +
        '\tchk_resp.pergunta_id,\n' +
        '\tchk_resp.acao_id,\n' +
        '\tchk_resp.resposta,\n' +
        '\tchk_resp.valor,\n' +
        '\tchk_resp.com_acao,\n' +
        '\tchk_resp.data_resposta,\n' +
        '\tchk_resp.deleted_at,\n' +
        '\tchk_resp.nao_se_aplica,\n' +
        '\tchk_av.periodo_id,\n' +
        '\tCOALESCE(chk_resp.avaliacao_id_web, chk_av.id_web) avaliacao_id_web\n' +
        'FROM checklists_respostas as chk_resp\n' +
        'LEFT JOIN checklists_avaliacoes chk_av\n' +
        '\tON chk_resp.avaliacao_id = chk_av.id\n' +
        'WHERE chk_resp.id_web is null';

      return db.executeSql(sql,[]).then((data:any)=>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }
          return respostas;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'getRespostasParaSincronizar', sql, null);

      });
  }

  public getRespostasDaAvaliacaoParaSincronizar(avaliacao_id:number){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id,\n' +
        'id_web,\n' +
        'mobile_key,\n' +
        'avaliacao_id,\n' +
        'pergunta_id,\n' +
        'acao_id,\n' +
        'resposta,\n' +
        'valor,\n' +
        'com_acao,\n' +
        'data_resposta,\n' +
        'deleted_at,\n' +
        'nao_se_aplica FROM checklists_respostas where id_web is null and avaliacao_id = ?';
      let data = [avaliacao_id];
      return db.executeSql(sql,data).then((data:any)=>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }
          return respostas;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'perguntas.ts', 'getRespostasParaSincronizar', sql, null);

      });
    });
  }

  public updateSinc(db: SQLiteObject,id_web: number, id_mobile:number, avaliacao_id_web: number){

        let data:any = [avaliacao_id_web,id_web,id_mobile];

        let sql = 'update checklists_respostas set avaliacao_id_web=?, id_web = ? where id = ?';

        return db.executeSql(sql, data).catch((err:any)=>{
          this.exceptionProvider.insert(err, 'perguntas.ts', 'getRespostasParaSincronizar', sql, data);
          console.error('erro ao atualiza checklists_respostas', err);
        });
  }

  public updateSincPerguntaAcao(pergunta_id:number, mobile_key:string){
    return this.dbProvider.getDB()
      .then((db:SQLiteObject) => {
        let sql = 'UPDATE checklists_perguntas_acoes SET sincronizada = ? WHERE pergunta_id = ? AND mobile_key = ?';

        let data = [true, pergunta_id, mobile_key];

        return db.executeSql(sql, data).catch((err:any)=>{
          this.exceptionProvider.insert(err, 'perguntas.ts', 'updateSincPerguntaAcao', sql, data);
          console.error('erro ao atualizar checklists_perguntas_acoes', err);
        });
      });
  }

  public getLatestId() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select id from checklists_respostas order by id desc limit 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);

              return item.id;
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'perguntas.ts', 'getLatestId', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public deletaAvaliacaoComRespostas(periodo_id, avaliacao_id){
    var promise = new Promise((resolve, reject) => {
      let now = moment().format('YYYY-MM-DD hh:mm:ss');
    this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        db.transaction(function (deleteAv) {
          deleteAv.executeSql('UPDATE checklists_avaliacoes SET deleted_at = ? WHERE id in (SELECT id FROM checklists_avaliacoes WHERE periodo_id=? AND deleted_at IS NULL AND id != ?)', [now,periodo_id,avaliacao_id], function (delAV, result_av) {
            delAV.executeSql('UPDATE checklists_respostas SET deleted_at = ? WHERE avaliacao_id in (SELECT id FROM checklists_avaliacoes WHERE periodo_id=? AND deleted_at IS NULL AND avaliacao_id != ?)', [now, periodo_id,avaliacao_id], function (deleteRPS, resultDeleteRPS) {
              resolve();
            }, (error) => {
              console.error('falha ao remover respostas', error);
              reject(error);
            });
          }, (error) => {
            console.error('falha ao remover avaliacoes', error);
            reject(error);
            });
        });
      });
  });
  return promise;

  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaPerguntas(url){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/checklist/perguntas/tecnicos`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando Perguntas...');
          if(results){
            for (let result of results){
              let sql_up = 'update checklists_perguntas SET checklist_id=?, descricao=?, valor_min=?, valor_max=?, data_validade=?, deleted_at=? where id = ?;';
              let data_up = [result.checklist_id, result.descricao, result.valor_min, result.valor_max, result.data_validade, result.deleted_at, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO checklists_perguntas (id, checklist_id, descricao, valor_min, valor_max, data_validade, deleted_at) values (?, ?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.checklist_id, result.descricao, result.valor_min, result.valor_max, result.data_validade, result.deleted_at];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'perguntas.ts', 'atualizaPerguntas', sql_ins, data_ins);
                    reject();
                  })
                }
              }).catch(err=>{
                this.exceptionProvider.insert(err, 'perguntas.ts', 'atualizaPerguntas', sql_up, data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        });
      });
    });
    return promisse;
  }

  public enviaFoto(dados_envio){

    var promisse = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        if (dados_envio.com_foto == true || dados_envio.com_foto == "true"){
          this.storage.get('access_token').then((access_token) => {
            let options: FileUploadOptions = {
              fileKey: 'foto',
              fileName: `resposta_${dados_envio.pergunta_id}_img.jpg`,
              // chunkedMode: false,
              httpMethod: 'POST',
              mimeType: "image/jpeg",
              params: dados_envio,
              headers: {
                'Authorization': `Bearer ${access_token}`,
              }
            };

            let fileTransfer = this.fileTransfer.create();

            this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
              let url;
              url = `${SERVER_URL}/checklist/resposta`;

              fileTransfer.upload(dados_envio.foto, url, options).then((retorno_api: any) => {

                let data_json = JSON.parse(retorno_api.response);

                this.updateSinc(db, data_json.id, data_json.id_mobile, data_json.avaliacao_id_web).then((res)=>{
                  resolve({'status': 'success', 'msg':'Foto enviada e atualizada com sucesso!', res});

                }).catch((err)=>{
                  console.error('Falha ao atualizar ação após enviar foto', err);
                  reject({'status': 'error', 'msg':'Falha ao atualizar ação após enviar foto', 'err': err});
                });

              }, (err) => {
                this.exceptionProvider.insert(err, 'perguntas.ts', 'atualizaPerguntas', url, dados_envio);
                console.error('erro enviar arquivo', err, dados_envio);
                reject({'status': 'error', 'msg':'Falha ao sincronizar foto', 'err': err});
              });

            }).catch((err) => {
              console.error('Falha ao buscar url da api para sincronizar foto');
              reject({'status': 'error', 'msg':'Falha ao buscar url da api para sincronizar foto', 'err': err});
            });

          }).catch((err)=>{
            console.error('Falha ao buscar token para envia foto da ação', err);
            reject({'status': 'error', 'msg':'Falha ao buscar token para envia foto da ação', 'err': err});
          });
        }else{
          resolve({'status': 'success', 'msg':'Ação sem foto não necessita de envio!'});
        }
      })
    });

    return promisse;
  }

  public uploadFoto(db: SQLiteObject,idResposta) {
    let promise = new Promise(resolve => {
      if(idResposta > 0){
        db.executeSql('SELECT * from checklists_respostas where id = ? and foto_base64 is not null', [idResposta])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let resposta = data.rows.item(0);
              if(resposta.foto_base64.trim().length > 0){
                this.apiAtualizaFoto(resposta).subscribe(() => {
                  db.executeSql('UPDATE checklists_respostas set foto_base64 = ? where id = ?', [null, idResposta]).then(()=>{
                    resolve(this.totalFotos++);
                  }).catch((err)=>{
                    resolve(this.totalFotos++);
                    console.error('falha ao remover base 64', err)
                  })
                }, (err)=>{
                  resolve(this.totalFotos++);
                  console.error('falha ao enviar foto', err)
                });
                this.enviaFoto(resposta)
              }else{
                resolve(this.totalFotos++)
              }

            }else{
              resolve(this.totalFotos++);
            }
          }).catch((err)=>{
          resolve(this.totalFotos++);
          console.error('falha ao consultar resposta', err)
        })
      }else{
        resolve(this.totalFotos++);
      }

    });

    return promise;
  }

}
