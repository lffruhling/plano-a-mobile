import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";

@Injectable()
export class RespostasProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private dbProvider: DatabaseProvider,
    private messageService: AlertMessagesProvider,
    private exceptionProvider:DbExceptionProvider,
    private storage: Storage,
    public events: Events,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/checklist`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/checklist`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/checklist`;
      }
    });
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(resposta){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into checklists_respostas (id_web, mobile_key, avaliacao_id, avaliacao_id_web, pergunta_id, resposta, nao_se_aplica, valor, data_resposta, deleted_at) values (?,?,?,?,?,?,?,?,?,?)';
      let data = [
        resposta.id_web,
        resposta.mobile_key,
        resposta.avaliacao_id,
        resposta.avaliacao_id_web,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.nao_se_aplica,
        resposta.valor,
        resposta.data_resposta,
        resposta.deleted_at
      ];

      return db.executeSql(sql,data).catch(err=>{
       this.exceptionProvider.insert(err, 'respostas.ts', 'insert', sql, data);
        console.error('Falha ao inserir checklists_avaliacoes', err, sql, data)
      })
    })
  }

  public update(resposta){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update checklists_respostas SET avaliacao_id=?, avaliacao_id_web=?, pergunta_id=?, resposta=?, nao_se_aplica=?, valor=?, data_resposta=?, deleted_at=? WHERE (mobile_key=? AND id_web =? AND avaliacao_id_web = ?) OR (id_web =? AND avaliacao_id_web = ?)';
      let data = [
        resposta.avaliacao_id,
        resposta.avaliacao_id_web,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.nao_se_aplica,
        resposta.valor,
        resposta.data_resposta,
        resposta.deleted_at,
        resposta.mobile_key,
        resposta.id,
        resposta.avaliacao_id_web,
        resposta.id,
        resposta.avaliacao_id_web
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'respostas.ts', 'update', sql, data);
        console.error('Falha ao atualizar checklists_respostas', err, sql, data)
      })
    })
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_respostas  WHERE id_web IS NULL';

      return db.executeSql(sql, []).then((data:any)=>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'respostas.ts', 'getAllBkp', sql);

      });
    });
  }

  public getAllAuxBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_respostas_aux  WHERE resposta_id IS NULL';

      return db.executeSql(sql, []).then((data:any)=>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'respostas.ts', 'getAllAuxBkp', sql);

      });
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/respostas/upload/bkp`, values)
  }

  public apiStoreAuxBkp(values) {
    return this.http.post(`${this.urlApi}/respostas/aux/upload/bkp`, values)
  }

  public apiStoreRespostaAcoesBkp(values) {
    return this.http.post(`${this.urlApi}/respostas/acoes/upload/bkp`, values)
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaRespostas(url){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/checklist/perguntas/respostas/tecnicos`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando Respostas...');
          if(results){
            for (let result of results){
              let sql_up = 'update checklists_respostas SET avaliacao_id=?, pergunta_id=?, resposta=?, valor=?, data_resposta=?, deleted_at=? where id_web = ?;';
              let data_up = [result.avaliacao_id, result.pergunta_id, result.resposta, result.valor, result.data_resposta, result.deleted_at, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO checklists_respostas (id, id_web, avaliacao_id, pergunta_id, resposta, valor, data_resposta, deleted_at) values (?,?,?,?,?,?,?,?)';
                  let data_ins = [result.id_mobile, result.id, result.avaliacao_id, result.pergunta_id, result.resposta, result.valor, result.data_resposta, result.deleted_at];
                  db.executeSql(sql_ins, data_ins).catch((err)=>{
                    this.exceptionProvider.insert(err, 'respostas.ts', 'update', sql_ins, data_ins);
                    console.error('Falha ao atualizar resposta', err);
                    reject();
                  });
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'respostas.ts', 'update', sql_up, data_up);
                console.error('Falha ao atualizar resposta', err)
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        });
      });
    });
    return promisse;
  }

}
