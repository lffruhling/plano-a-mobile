import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";
import {Device} from "@ionic-native/device";

@Injectable()
export class AvaliacoesChecklistsProvider {
  private urlApi;

  constructor(private dbProvider: DatabaseProvider,
              private http: HttpClient,
              private storage: Storage,
              public events: Events,
              private exceptionProvider:DbExceptionProvider,
              private uniqueDeviceID: UniqueDeviceID,
              private device: Device,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/checklist`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/checklist`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/checklist`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/avaliacoes/tecnico?last_update=${lastUpdate}`);
  }

  public apiStoreAvaliacaoChk(values) {
    return this.http.post(`${this.urlApi}/avaliacao`, values)
  }

  public apiStoreAvaliacoesChk(values) {
    return this.http.post(`${this.urlApi}/avaliacoes`, values)
  }

  public apiStoreAvaliacoesChkBkp(values) {
    return this.http.post(`${this.urlApi}/avaliacoes/upload/bkp`, values)
  }

  public apiStoreAvaliacoesChkAuxBkp(values) {
    return this.http.post(`${this.urlApi}/avaliacoes/aux/upload/bkp`, values)
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(avaliacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into checklists_avaliacoes (id_web, checklist_id, lote_id, periodo_id, grupo_aplicacao_id, mobile_key, data_aplicacao, valor_chk, peso_chk, deleted_at) values (?,?,?,?,?,?,?,?,?,?)';
      let data = [
        avaliacao.id,
        avaliacao.checklist_id,
        avaliacao.lote_id,
        avaliacao.periodo_id,
        avaliacao.grupo_aplicacao_id,
        avaliacao.mobile_key,
        avaliacao.data_aplicacao,
        avaliacao.valor_chk,
        avaliacao.peso_chk,
        avaliacao.deleted_at,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'insert', sql, data);
        console.error('Falha ao inserir checklists_avaliacoes', err, sql, data)
      })
    })
  }

  public update(avaliacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update checklists_avaliacoes SET checklist_id=?, lote_id=?, periodo_id=?, grupo_aplicacao_id=?, data_aplicacao=?, valor_chk=?, peso_chk=?, deleted_at=? WHERE id_web=?';
      let data = [
        avaliacao.checklist_id,
        avaliacao.lote_id,
        avaliacao.periodo_id,
        avaliacao.grupo_aplicacao_id,
        avaliacao.data_aplicacao,
        avaliacao.valor_chk,
        avaliacao.peso_chk,
        avaliacao.deleted_at,
        avaliacao.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'update', sql, data);
        console.error('Falha ao atualizar checklists_avaliacoes', err, sql, data)
      })
    })
  }

  public get(id){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM checklists_avaliacoes WHERE id=?';
      let data = [id];

      return db.executeSql(sql,data).then((data)=>{
        return data.rows.item(0);
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'get', sql, data);
        console.error('Falha ao consultar avaliacao', err, sql, data)
      })
    })
  }

  public getAvaliacao(checklist_id, lote_id, periodo_id){
    let sql = 'SELECT * ' +
      ' FROM checklists_avaliacoes AS chk_av ' +
      ' WHERE checklist_id = ? ' +
      '  AND (lote_id = ? OR periodo_id = ?)  ' +
      '  AND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL) ' +
      '  AND EXISTS (SELECT COUNT(id) ' +
      '                FROM checklists_respostas AS chk_resp ' +
      '                WHERE chk_av.id = chk_resp.avaliacao_id) ' +
      ' LIMIT 1';
    let data = [checklist_id, lote_id, periodo_id];

    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      return db.executeSql(sql, data).then((data:any) =>{
        return data.rows.item(0);
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacao', sql, data);

      });
    })
  }

  public getRespostasAvaliacao(checklist_id, intervalo_id,propriedade_id){

    let sql = 'SELECT chk_perg.id AS pergunta_id, \n' +
		'\tchk_perg.descricao, \n' +
		'\tCOALESCE(chk_resp.foto, "assets/imgs/camera_800X300.png") as foto,\n' +
		'\tCOALESCE(chk_resp.valor,0.0) AS valor,\n' +
		'\tCOALESCE(chk_resp.data_resposta,date()) AS data_resposta,\n' +
		'\tchk_resp.foto_base64,\n' +
		'\tchk_resp.resposta,\n' +
		'\tCOALESCE((SELECT foto64 FROM checklists_perguntas_old_thumbs \n' +
		'\t\tWHERE pergunta_id = chk_resp.pergunta_id \n' +
		'\t\tAND galpao_id = (SELECT galpao_id FROM periodo_aplicacoes \n' +
		'\t\tWHERE propriedade_id = ? LIMIT 1)), \n' +
		'\t\t"assets/imgs/camera_800X300.png"\n' +
		'\t) as thumb_foto\n' +
		'FROM checklists_perguntas AS chk_perg\n' +
		'\tLEFT JOIN checklists_respostas AS chk_resp\n' +
		'\tON chk_perg.id = chk_resp.pergunta_id AND chk_resp.avaliacao_id_web = ((SELECT chk_av.id_web\n' +
		'\tFROM checklists_avaliacoes as chk_av\n' +
		'\t\tINNER JOIN periodo_aplicacoes as peap\n' +
		'\t\t\tON peap.id = chk_av.periodo_id\n' +
		'\tWHERE peap.propriedade_id = ?\n' +
		'\t\tAND peap.intervalo_id = ?\n' +
		'\t\tAND chk_av.deleted_at IS NULL\n' +
		'\t\tAND EXISTS(SELECT id FROM checklists_respostas WHERE avaliacao_id_web = chk_av.id_web)\n' +
		'\tORDER BY chk_av.data_aplicacao DESC\n' +
		'\tLIMIT 1))\n' +
		'WHERE checklist_id = ?';

    let params = [propriedade_id, propriedade_id, intervalo_id, checklist_id];

    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      return db.executeSql(sql, params).then((data:any) =>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
      	console.error('errrooo', err)
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacao', sql, params);
      });
    })
  }

  public getAvaliacoesAux(checklist_id, lote_id, periodo_id){
    let sql = 'SELECT * FROM checklists_avaliacoes_aux WHERE avaliacao_id IS NULL AND checklist_id = ? AND (lote_id = ? OR periodo_id = ?) LIMIT 1';
    let data = [checklist_id, lote_id, periodo_id];

    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      return db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacoesAux', sql, data);

      });
    })
  }

  public getAvaliacaoIdAux(id){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM checklists_avaliacoes_aux WHERE id=?';
      let data = [id];

      return db.executeSql(sql,data).then((data)=>{
        return data.rows.item(0);
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacaoIdAux', sql, data);
        console.error('Falha ao consultar avaliacao aux', err, sql, data)
      })
    })

  }

  public getAvaliacoesParaSincronizar(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_avaliacoes where id_web is null';

      return db.executeSql(sql, []).then((data:any)=>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacoesParaSincronizar', sql, null);

      });
    });
  }

  public updateSinc(id_web: number, id_mobile:number){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update checklists_avaliacoes set id_web = ? where id = ?';

        let data = [id_web,id_mobile];

        return db.executeSql(sql, data).catch((err:any)=>{
          this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'updateSinc', sql, data);
          console.error('erro ao atualiza checklists_avaliacoes', err);
        })
      });
  }

  public salvaAvaliacaoChkAux(avaliacao){
    let sql = 'INSERT INTO checklists_avaliacoes_aux (mobile_key, checklist_id, lote_id, periodo_id, grupo_aplicacao_id, data_aplicacao, valor_chk, peso_chk, salva) VALUES (?,?,?,?,?,?,?,?,?)';
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      return db.executeSql(sql, avaliacao).catch((err:any)=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'salvaAvaliacaoChkAux', sql, avaliacao);
        console.error('erro ao salvar nova checklists_avaliacoes_aux', err, sql, avaliacao);
      });
    });

  }

  public salvaAssinaturaChkAux(id_avaliacao_aux, assinatura){
    let sql = 'UPDATE checklists_avaliacoes_aux SET assinatura=? WHERE id=?';
    let data = [assinatura, id_avaliacao_aux];

    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      return db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'salvaAssinaturaChkAux', sql, data);
        console.error('Falha ao salvar assinatura avaliação aux', err, sql, data);
      })
    });
  }

  public updateSincAux(avaliacao_id, id_avaliacao_aux){
    let sql = 'UPDATE checklists_avaliacoes_aux SET avaliacao_id=?, salva=? WHERE id=?';
    let data = [avaliacao_id, true, id_avaliacao_aux];

    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      return db.executeSql(sql, data).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'updateSincAux', sql, data);
        console.error('Falha ao atualizar id avaliação aux', err, sql, data);
      });
    });
  }

  public salvaAvaliacaoChk(avaliacao){
    let sql = 'INSERT INTO checklists_avaliacoes (mobile_key, checklist_id, lote_id, periodo_id, grupo_aplicacao_id, data_aplicacao, valor_chk, peso_chk, assinatura) VALUES (?,?,?,?,?,?,?,?,?)';
    let data = [
      avaliacao.mobile_key,
      avaliacao.checklist_id,
      avaliacao.lote_id,
      avaliacao.periodo_id,
      avaliacao.grupo_aplicacao_id,
      avaliacao.data_aplicacao,
      avaliacao.valor_chk,
      avaliacao.peso_chk,
      avaliacao.assinatura,
    ];
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      return db.executeSql(sql, data).catch((err:any)=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'salvaAvaliacaoChk', sql, data);
        console.error('erro ao salvar nova checklists_avaliacoes', err, sql, data);
      });
    });

  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_avaliacoes  WHERE id_web IS NULL';

      return db.executeSql(sql, []).then((data:any)=>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAllBkp', sql);

      });
    });
  }

  public getAllAuxBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_avaliacoes_aux WHERE avaliacao_id IS NULL';

      return db.executeSql(sql, []).then((data:any)=>{

        if (data.rows.length > 0) {
          let results: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var result = data.rows.item(i);
            results.push(result);
          }
          return results;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAllAuxBkp', sql);

      });
    });
  }

  public deleteAvaliacaoAux(avaliacao_id){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'DELETE FROM checklists_avaliacoes_aux WHERE id=?';
      let data = [avaliacao_id];

      return db.executeSql(sql,data).then((data)=>{
        return data;
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacaoIdAux', sql, data);
        console.error('Falha ao consultar avaliacao aux', err, sql, data)
      })
    })
  }

  public deleteRespostasAux(avaliacao_id){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'DELETE FROM checklists_respostas_aux WHERE avaliacao_id_aux = ?';
      let data = [avaliacao_id];

      return db.executeSql(sql,data).then((data)=>{
        return data;
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'getAvaliacaoIdAux', sql, data);
        console.error('Falha ao consultar avaliacao aux', err, sql, data)
      })
    })
  }


  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaAvaliacoesChkLocais(){

    /*Atualiza Avaliações*/
    var promisse = new Promise((resolve,reject) => {

      this.dbProvider.getDB().then((db:SQLiteObject) => {

        this.http.get(`${this.urlApi}/avaliacoes/tecnico`).subscribe((results:any) => {

          if (results){

            this.retornaUUID().then((device_uuid)=>{

              for (let avaliacao of results) {
                let sql_av_update = 'UPDATE checklists_avaliacoes SET checklist_id=?, lote_id=?, periodo_id=?, grupo_aplicacao_id=?, data_aplicacao=?, valor_chk=?, peso_chk=?  WHERE id_web=?';
                let params_up = [avaliacao.checklist_id, avaliacao.lote_id, avaliacao.periodo_id, avaliacao.grupo_aplicacao_id, avaliacao.data_aplicacao, avaliacao.valor_chk, avaliacao.peso_chk, avaliacao.id];
                let sql_av_ins = `REPLACE INTO checklists_avaliacoes (id, id_web, checklist_id, lote_id, periodo_id,  grupo_aplicacao_id, data_aplicacao, valor_chk=?, peso_chk=?) values (?,?,?,?,?,?,?,?,?)`;

                if(avaliacao.device_uuid != device_uuid) {
                  avaliacao.id_mobile = null;
                }
                let params_ins = [avaliacao.id_mobile, avaliacao.id, avaliacao.checklist_id, avaliacao.lote_id, avaliacao.periodo_id,  avaliacao.grupo_aplicacao_id, avaliacao.data_aplicacao, avaliacao.valor_chk, avaliacao.peso_chk];

                let sql_up_resp = 'UPDATE checklists_respostas SET avaliacao_id=?, pergunta_id=?, resposta=?, valor=?, data_resposta=?, valor_chk=?, peso_chk=?,  deleted_at=? WHERE id_web=?';
                let sql_ins_resp = 'REPLACE INTO checklists_respostas (id, id_web, avaliacao_id, pergunta_id, resposta, valor, data_resposta, valor_chk, peso_chk,  deleted_at) values (?,?,?,?,?,?,?,?,?,?)';


                db.executeSql(sql_av_update,params_up).then((res)=>{

                  if(res.rowsAffected == 0){

                    db.executeSql(sql_av_ins,params_ins).then(avalicao_ins=>{

                      for (let resposta of avaliacao.respostas){

                        resposta.avaliacao_id = avalicao_ins.insertId;
                        let params_up_resp = [resposta.avaliacao_id, resposta.pergunta_id, resposta.resposta, resposta.valor, resposta.data_resposta, resposta.deleted_at, resposta.id];
                        if(resposta.device_uuid != device_uuid) {
                          resposta.id_mobile = null;
                        }
                        let params_ins_resp = [resposta.id_mobile, resposta.id, resposta.avaliacao_id, resposta.pergunta_id, resposta.resposta, resposta.valor, resposta.data_resposta, resposta.deleted_at];
                        db.executeSql(sql_up_resp, params_up_resp).then((res_resp)=>{

                          if(res_resp.rowsAffected == 0){
                            db.executeSql(sql_ins_resp, params_ins_resp).catch(err=>{
                              this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_ins_resp, params_ins_resp);
                              console.error('falha ao inserir resposta', err);
                              reject();
                            })
                          }
                        }).catch(err=>{
                          this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_up_resp, params_up_resp);
                          console.error('falha ao atualizar resposta', err);
                          reject();
                        });

                      }

                    }).catch(err=>{
                      this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_av_update,params_up);
                      console.error('falha ao inserir avaliacao',err);
                      reject();

                    });
                  }else{

                    for (let resposta of avaliacao.respostas){

                      resposta.avaliacao_id = avaliacao.id_mobile;
                      let params_up_resp = [resposta.avaliacao_id, resposta.pergunta_id, resposta.resposta, resposta.valor, resposta.data_resposta, resposta.deleted_at, resposta.id];
                      if(resposta.device_uuid != device_uuid) {

                        resposta.id_mobile = null;
                      }
                      let params_ins_resp = [resposta.id_mobile, resposta.id, resposta.avaliacao_id, resposta.pergunta_id, resposta.resposta, resposta.valor, resposta.data_resposta, resposta.deleted_at];
                      db.executeSql(sql_up_resp, params_up_resp).then((res_resp)=>{

                        if(res_resp.rowsAffected == 0){
                          db.executeSql(sql_ins_resp, params_ins_resp).catch(err=>{
                            this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_ins_resp, params_ins_resp);
                            console.error('falha ao inserir resposta', err);
                            reject();
                          })
                        }
                      }).catch(err=>{
                        this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_up_resp, params_up_resp);
                        console.error('falha ao atualizar resposta', err);
                        reject();
                      });

                    }
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'avaliacaoes-checklists.ts', 'atualizaAvaliacoesChkLocais', sql_av_update,params_up);
                  console.error('update avaliacao',err);
                  reject();
                });
              }
            });
          }
          resolve();
        });
      });
    });

    return promisse;
  }

  retornaUUID(){
    let promise = new Promise((resolve)=>{
      this.uniqueDeviceID.get().then((uuid: any) => {

        resolve(uuid);
      }).catch(()=>{
        resolve(this.device.uuid);
      })
    });

    return promise;
  }

}
