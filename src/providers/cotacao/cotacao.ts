import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class CotacaoProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/cotacoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/cotacoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/cotacoes`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(cotacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into cotacoes (id, tipo_integracao_id, valor, data_inicio, data_final, considerar, vigente) values (?,?,?,?,?,?,?)';
      let data = [
        cotacao.id,
        cotacao.tipo_integracao_id,
        cotacao.valor,
        cotacao.data_inicio,
        cotacao.data_final,
        cotacao.considerar,
        cotacao.vigente,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'cotacao.ts', 'insert', sql, data);
        console.log('Falha ao inserir cotacoes', err, sql, data)
      })
    })
  }

  public update(cotacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update cotacoes SET  tipo_integracao_id=?, valor=?, data_inicio=?, data_final=?, considerar=?, vigente=? where id = ?;';
      let data = [
        cotacao.tipo_integracao_id,
        cotacao.valor,
        cotacao.data_inicio,
        cotacao.data_final,
        cotacao.considerar,
        cotacao.vigente,
        cotacao.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'cotacao.ts', 'update', sql, data);
        console.log('Falha ao atualizar cotacoes', err, sql, data)
      })
    })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaCotacoes(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/cotacoes`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Cotações...');
          if (results){
            for (let result of results){
              let sql_up = 'update cotacoes SET  tipo_integracao_id=?, valor=?, data_inicio=?, data_final=?, considerar=?, vigente=? where id = ?;';
              let data_up = [result.tipo_integracao_id, result.valor, result.data_inicio, result.data_final, result.considerar, result.vigente, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO cotacoes (id, tipo_integracao_id, valor, data_inicio, data_final, considerar, vigente) values (?, ?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.tipo_integracao_id, result.valor, result.data_inicio, result.data_final, result.considerar, result.vigente];
                  db.executeSql(sql_ins, data_ins).catch((err)=>{
                    this.exceptionProvider.insert(err, 'cotacao.ts', 'update', sql_up, data_up);
                    console.error('falha ao exectar sql ao inserir cotações', err);
                    reject();
                  });
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'cotacao.ts', 'update', sql_up, data_up);
                console.error('falha ao exectar sql ao inserir cotações', err);
                reject();
              });
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar cotações', err);
          // this.exceptionProvider.insert(err, 'cotacao.ts', 'update');

          reject();
        })
      })
    });

    return promisse;
  }

}
