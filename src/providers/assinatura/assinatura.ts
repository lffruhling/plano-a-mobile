import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {Storage} from "@ionic/storage";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Events} from "ionic-angular";

@Injectable()
export class AssinaturaProvider {
  private urlApi;

  constructor(private dbProvider: DatabaseProvider,
              private http: HttpClient,
              private storage: Storage,
              private events:Events,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
      this.urlApi = `${SERVER_URL}/checklist/respostas/assinatura`;
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/checklist/respostas/assinatura`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiStore(values) {
    return this.http.post(this.urlApi, values)
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(assinatura){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into checklists_respostas_assinaturas (mobile_key, lote_id, periodo_id, checklist_id, assinatura, data_assinatura) values (?,?,?,?,?,?)';
        let data = [
          assinatura.mobile_key,
          assinatura.lote_id,
          assinatura.periodo_id,
          assinatura.checklist_id,
          assinatura.assinatura,
          assinatura.data_assinatura,
        ];

        return db.executeSql(sql, data)
          .catch((e) => console.error('Falha ao executar sql assinatura', e));

      })
      .catch((e) => console.error('Falha ao salvar assinatura', e));
  }

  public updateSinc(id_web: number, id_mobile:number){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update checklists_respostas_assinaturas set id_web = ? where id = ?';

        let data = [id_web,id_mobile];

        return db.executeSql(sql, data).catch((err) => {
          console.error('falha ao atualiza checklists_respostas_assinaturas', err)
        })
      }).catch((err:any)=>{
        console.error('erro ao atualiza checklists_respostas_assinaturas', err);
      })
  }

  public getAllToSinc(){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM checklists_respostas_assinaturas WHERE id_web IS NULL';

        return db.executeSql(sql, [])
          .then((data: any) => {
              let assinaturas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                assinaturas.push(data.rows.item(i));
              }
              return assinaturas;
          }).catch((e) => console.error('Falha ao executar sql assinaturas', e));
      }).catch((e) => console.error('Falha ao salvar assinaturas', e));
  }

}
