import { Injectable } from '@angular/core';
import {AlertController, LoadingController, ToastController} from "ionic-angular";

@Injectable()
export class AlertMessagesProvider {

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
  }

  showToast(message: string, duration:number = 5000, position:string = 'bottom', styleCss:string = '', dismissOnPageChange:boolean = false){
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position,
      cssClass: styleCss,
      dismissOnPageChange: false,
    });

    toast.present();
  }

  showToastSuccess(message: string, duration:number = 3000){
    this.showToast(message,duration,'bottom', 'success');
  }

  showToastError(message: string, duration:number = 3000){
    this.showToast(message, duration,'middle', 'error');
  }

  showLoading(msg:string, spinner:string = null){
    let loading = this.loadingCtrl.create({
      spinner: spinner,
      content: msg
    });

    loading.present();

    return loading;
  }

  showConfirmationAlert(title:string, message:string, textoBtnSim:string, textoBtnNao:string){
    let promisse = new Promise((resolve, reject) => {
      this.alertCtrl.create({
        title:title,
        message:message,
        buttons: [
          {
            text:textoBtnNao,
            handler: ()=>{
              resolve(false);
            }
          },{
            text:textoBtnSim,
            handler: () => {
              resolve(true);
            }
          }
        ]
      }).present();
    });

    return promisse;
  }

  showAlert(title:string, message:string,) {
    let promisse = new Promise((resolve, reject) => {
      this.alertCtrl.create({
        title: title,
        subTitle: message,
        buttons: [
          {
            text:'OK',
            handler: ()=>{
              resolve(true);
            }
          }
        ]
      }).present();
    });

    return promisse;
  }

}
