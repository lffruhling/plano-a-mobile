import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class EmpresaProvider {

  protected urlApi;

  constructor(
    private http: HttpClient,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public getDadosEmpresaApi(){
    return this.http.get(`${this.urlApi}/empresa`);
  }

  public apiGetEstadosAll(lastUpdate){
    return this.http.get(`${this.urlApi}/estados?last_update=${lastUpdate}`);
  }

  public apiGetMunicipiosAll(lastUpdate){
    return this.http.get(`${this.urlApi}/municipios?last_update=${lastUpdate}`);
  }

  public apiGetVersoesAll(lastUpdate){
    return this.http.get(`http://versionador.sistemaplanoa.com.br/api/versao/1/APP?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public get(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT nome, razao_social, cnpj FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0);
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'get()', sql, null);
        console.error('falha ao consultar dados da empresa',err, sql)
      })
    })
  }

  public getParamChkPeriodo(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT chk_periodo FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).chk_periodo == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamChkPeriodo', sql, null);
        console.error('falha ao consultar dados da empresa chk_periodo',err, sql)
      })
    })
  }

  public getParamChkCapturaAssinatura(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT capturar_assinatura FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).capturar_assinatura == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamChkCapturaAssinatura', sql, null);
        console.error('falha ao consultar dados da empresa capturar_assinatura',err, sql)
      })
    })
  }

  public getParamChkObrigaCapturaAssinatura(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT obriga_captura_assinatura FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).obriga_captura_assinatura == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamChkObrigaCapturaAssinatura', sql, null);
        console.error('falha ao consultar dados da empresa obriga_captura_assinatura',err, sql)
      })
    })
  }

  public getParamChkObrigaCapturaGeolocalizacao(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT obriga_geolocalizacao FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).obriga_geolocalizacao == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamChkObrigaCapturaGeolocalizacao', sql, null);
        console.error('falha ao consultar dados da empresa obriga_geolocalizacao',err, sql)
      })
    })
  }

  public getParamConsiderarCotacao(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT considerar_cotacao FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).considerar_cotacao == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamConsiderarCotacao', sql, null);
        console.error('falha ao consultar dados da empresa considerar_cotacao',err, sql)
      })
    })
  }

  public getParamRepetirChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT repetir_chk FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).repetir_chk == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamRepetirChk', sql, null);
        console.error('falha ao consultar dados da empresa getParamRepetirChk',err, sql)
      })
    })
  }

  public getDiasToleranciaChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT dias_tolerancia_resposta_chk FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).dias_tolerancia_resposta_chk;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getDiasToleranciaChk', sql, null);
        console.error('falha ao consultar dados da empresa dias_tolerancia_resposta_chk',err, sql)
      })
    })
  }

  public getIntegracaoAvesChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT integracao_aves FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return (data.rows.item(0).integracao_aves == 1 || data.rows.item(0).integracao_aves == '1');
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getIntegracaoAvesChk', sql, null);
        console.error('falha ao consultar dados da empresa integracao_aves',err, sql)
      })
    })
  }

  public getRespostasNulasChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT respostas_nulas FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).respostas_nulas;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getRespostasNulasChk', sql, null);
        console.error('falha ao consultar dados da empresa respostas_nulas',err, sql)
      })
    })
  }

  public getParamPgtoChkKg(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT chk_pgto_kg FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).chk_pgto_kg == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamPgtoChkKg', sql, null);
        console.error('falha ao consultar dados da empresa getParamPgtoChkKg',err, sql)
      })
    })
  }

  public getParamAgruparChks(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT agrupar_checklists FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).agrupar_checklists == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamAgruparChks', sql, null);
        console.error('falha ao consultar dados da empresa getParamAgruparChks',err, sql)
      })
    })
  }

  public getParamOrdenaAplicacaoChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT considerar_ordem_aplicacao_chk FROM empresa LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).considerar_ordem_aplicacao_chk == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamOrdenaAplicacaoChk', sql, null);
        console.error('falha ao consultar dados da empresa getParamOrdenaAplicacaoChk',err, sql)
      })
    })
  }

  public getTotalEstados(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'select count(id) as total from estados';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).total < 27;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamOrdenaAplicacaoChk', sql, null);
        console.error('falha ao consultar dados da empresa getParamOrdenaAplicacaoChk',err, sql)
      })
    })
  }

  public getTotalMunicipios(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'select count(id) as total from municipios';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).total < 5570;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'empresa.ts', 'getParamOrdenaAplicacaoChk', sql, null);
        console.error('falha ao consultar dados da empresa getParamOrdenaAplicacaoChk',err, sql)
      })
    })
  }

  public atualizaDadosEmpresaDB(dados){
    let promisse = new Promise((resolve,reject)=>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        let sql_up = 'update empresa SET nome=?, razao_social=?, cnpj=?, logo_64=?, chk_periodo=?, dias_tolerancia_resposta_chk=?, capturar_assinatura=?, obriga_captura_assinatura=?, considerar_cotacao=?, integracao_aves=?, respostas_nulas=?, repetir_chk=?, chk_pgto_kg=?, agrupar_checklists=?, considerar_ordem_aplicacao_chk=?, obriga_geolocalizacao=? where id = ?;';
        let data_up = [dados.nome, dados.razao_social, dados.cnpj, dados.logo_64, dados.chk_periodo, dados.dias_tolerancia_resposta_chk, dados.capturar_assinatura, dados.obriga_captura_assinatura, dados.considerar_cotacao, dados.integracao_aves, dados.respostas_nulas, dados.repetir_chk,dados.chk_pgto_kg,dados.agrupar_checklists, dados.considerar_ordem_aplicacao_chk, dados.obriga_geolocalizacao, dados.id];
        db.executeSql(sql_up,data_up).then((data:any) =>{
          if(data.rowsAffected < 1){
            let sql_ins = 'INSERT OR IGNORE INTO empresa (id, nome, razao_social, cnpj, logo_64, chk_periodo, dias_tolerancia_resposta_chk, capturar_assinatura, obriga_captura_assinatura, considerar_cotacao, integracao_aves, respostas_nulas, repetir_chk, chk_pgto_kg, agrupar_checklists, considerar_ordem_aplicacao_chk, obriga_geolocalizacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            let data_ins = [dados.id, dados.nome, dados.razao_social, dados.cnpj, dados.logo_64, dados.chk_periodo, dados.dias_tolerancia_resposta_chk, dados.capturar_assinatura, dados.obriga_captura_assinatura, dados.considerar_cotacao, dados.integracao_aves, dados.respostas_nulas, dados.repetir_chk, dados.chk_pgto_kg, dados.agrupar_checklists, dados.considerar_ordem_aplicacao_chk, dados.obriga_geolocalizacao];
            db.executeSql(sql_ins, data_ins).then(()=>{
              resolve();
            }).catch(err=>{
              this.exceptionProvider.insert(err, 'empresa.ts', 'atualizaDadosEmpresaDB', sql_ins, data_ins);

            })
          }else{
            resolve();
          }
        }).catch((err)=>{
          this.exceptionProvider.insert(err, 'empresa.ts', 'atualizaDadosEmpresaDB', sql_up,data_up);
          console.error('falha ao exectar sql ao inserir dados da empresa', err)
        });
      })
    });
    return promisse;
  }

  public insertEstado(estados){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let dados = '';
      let count = estados.length;

      for (let result of estados) {
        let nome = result.nome.replace(/'/g, '"');
        let uf = result.uf.replace(/'/g, '"');

        dados += `(${result.id}, ${result.codigo}, '${nome}', '${uf}', ${result.regiao})`;
        if (count-- > 1) {
          dados += ','
        }
      }

      let sql = `REPLACE INTO estados (id, codigo, nome, uf, regiao) values ${dados}`;
      if (estados.length > 0) {
        return db.sqlBatch([
          [sql, []],
        ]).catch(err => {
          this.exceptionProvider.insert(err, 'empresa.ts', 'insertEstado', sql);
          console.error('Erro ao incluir estados', err)
        });
      }else{
        return null;
      }
    });
  }



  public insertMunicipio(municipios){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      if (municipios) {

        let dados = '';
        let count = municipios.length;

        for (let estado of municipios) {
          let nome = estado.nome.replace(/'/g, '"');
          let uf = estado.uf.replace(/'/g, '"');

          dados += `(${estado.id}, ${estado.codigo}, '${nome}', '${uf}')`;
          if (count-- > 1) {
            dados += ','
          }
        }

        let sql = `REPLACE INTO municipios (id, codigo, nome, uf) values ${dados}`;

        if (municipios.length > 0) {
          return db.sqlBatch([
            [sql, []],
          ]).catch(err => {
            this.exceptionProvider.insert(err, 'empresa.ts', 'insertMunicipio', sql);
            console.error('Erro ao incluir municipios', err)
          });
        }else{
          return null;
        }
      }
    });
  }

  public updateMunicipio(municipio){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'UPDATE municipios SET codigo=?, nome=?, uf=? WHERE id=?';
      let data = [
        municipio.codigo,
        municipio.nome,
        municipio.uf,
        municipio.id,
      ];

      return db.executeSql(sql,data).catch((err) => {
        this.exceptionProvider.insert(err, 'empresa.ts', 'updateMunicipio', sql, data);
        console.error('Falha ao executar sql update municipios', err)
      });
    });
  }

  public insertVersao(versao){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'INSERT INTO versoes (id, versao, melhorias, funcionalidades, bugs, data_liberacao) values (?,?,?,?,?,?)';
      let data = [
        versao.id,
        versao.versao,
        versao.melhorias,
        versao.funcionalidades,
        versao.bugs,
        versao.data_liberacao,
      ];

      return db.executeSql(sql,data).catch((err) => {
        this.exceptionProvider.insert(err, 'empresa.ts', 'insertVersao', sql, data);
        console.error('Falha ao executar sql insert municipios', err)
      });
    });
  }

  public updateVersao(versao){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'UPDATE versoes SET versao=?, melhorias=?, funcionalidades=?, bugs=?, data_liberacao=? WHERE id=?';
      let data = [
        versao.versao,
        versao.melhorias,
        versao.funcionalidades,
        versao.bugs,
        versao.data_liberacao,
        versao.id,
      ];

      return db.executeSql(sql,data).catch((err) => {
        this.exceptionProvider.insert(err, 'empresa.ts', 'updateVersao', sql, data);
        console.error('Falha ao executar sql update municipios', err)
      });
    });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaEmpresa(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/empresa`).subscribe((result:any) =>{
          let loading = this.messageService.showLoading('Atualizando Dados da Empresa...');
          let sql_up = 'update empresa SET nome=?, razao_social=?, cnpj=?, logo_64=?, chk_periodo=?, dias_tolerancia_resposta_chk=?, capturar_assinatura=?, obriga_captura_assinatura=?, considerar_cotacao=?, integracao_aves=?, respostas_nulas=?, repetir_chk=?, agrupar_checklists=?, considerar_ordem_aplicacao_chk=?, obriga_geolocalizacao=? where id = ?;';
          let data_up = [result.nome, result.razao_social, result.cnpj, result.logo_64, result.chk_periodo, result.dias_tolerancia_resposta_chk, result.capturar_assinatura, result.obriga_captura_assinatura, result.considerar_cotacao, result.integracao_aves, result.respostas_nulas,result.repetir_chk,result.agrupar_checklists, result.considerar_ordem_aplicacao_chk, result.obriga_geolocalizacao, result.id];
          db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO empresa (id, nome, razao_social, cnpj, logo_64, chk_periodo, dias_tolerancia_resposta_chk, capturar_assinatura, obriga_captura_assinatura, considerar_cotacao, integracao_aves, respostas_nulas, repetir_chk, agrupar_checklists, considerar_ordem_aplicacao_chk, obriga_geolocalizacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
                  let data_ins = [result.id, result.nome, result.razao_social, result.cnpj, result.logo_64, result.chk_periodo, result.dias_tolerancia_resposta_chk, result.capturar_assinatura, result.obriga_captura_assinatura, result.considerar_cotacao, result.integracao_aves, result.respostas_nulas, result.repetir_chk, result.agrupar_checklists, result.considerar_ordem_aplicacao_chk, result.obriga_geolocalizacao];
                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'empresa.ts', 'atualizaEmpresa', sql_ins, data_ins);
                  })
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'empresa.ts', 'atualizaEmpresa', sql_up, data_up);
                console.error('falha ao exectar sql ao inserir dados da empresa', err)
              });
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar dados da empresa', err);
          reject();
        })
      })
    });

    return promisse;
  }

}
