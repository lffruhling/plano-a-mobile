import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class LotesProvider {
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    public http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }


  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiGet(galpao_id) {
    return this.http.get<any>(`${this.urlApi}/lotes?galpao_id=${galpao_id}`);
  }

  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}/lotes?last_update=${lastUpdate}`);
  }

  public apiUpdate(lote){
    return this.http.patch(`${this.urlApi}/lotes/${lote.id}`, lote);
  }

  public apiGetLinhagensAll(lastUpdate){
    return this.http.get(`${this.urlApi}/linhagens?last_update=${lastUpdate}`);
  }

  public apiGetStatusAll(lastUpdate){
    return this.http.get(`${this.urlApi}/lotes/status?last_update=${lastUpdate}`);
  }

  public apiGetSexoAll(lastUpdate){
    return this.http.get(`${this.urlApi}/lotes/sexos?last_update=${lastUpdate}`);
  }

  public apiGetFabricasAll(lastUpdate){
    return this.http.get(`${this.urlApi}/lotes/fabricas?last_update=${lastUpdate}`);
  }

  public apiStoreBkp(value){
    return this.http.post(`${this.urlApi}/lotes/upload/bkp`, value);
  }
  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT lt.*,  ' +
          '  prop.descricao as propriedade_descricao,  ' +
          '  prop.tipo_integracao_id  ' +
          'FROM lotes AS lt  ' +
          '  LEFT JOIN propriedades as prop  ' +
          '    ON lt.propriedade_id = prop.id ' +
          'WHERE lt.id = ? AND (lt.deleted_at == "null" OR lt.deleted_at IS NULL)';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'lotes.ts', 'get',sql, data);
            console.error('Falha ao executar sql busca lote por ID', err)
          });
      });
  }

  public getAllToSync(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM lotes WHERE sincronizado = ?';
      let data = [false];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let galpoes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              galpoes.push(data.rows.item(i));
            }
            return galpoes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'lotes.ts', 'getAllToSync',sql,data);
          console.error('Falha ao consultar lotes para sincronizar', err)
        });
    });
  }

  public getAllBkp() {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT * FROM lotes';

      return db.executeSql(sql, [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let lotes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              lotes.push(data.rows.item(i));
            }
            return lotes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'lotes.ts', 'getAllBkp', sql);
          console.error('Falha ao consultar lotes para sincronizar', err)
        });
    });
  }

  public insert(lote){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into lotes (id, propriedade_id, galpao_id, lote_status_id, fabrica_id, sexo_lote_id, linhagem_id, nro_lote, nro_animais, data_alojamento, dias_alojamento, data_previsao_carregamento, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?)';
      let data = [
        lote.id,
        lote.propriedade_id,
        lote.galpao_id,
        lote.lote_status_id,
        lote.fabrica_id,
        lote.sexo_lote_id,
        lote.linhagem_id,
        lote.nro_lote,
        lote.nro_animais,
        lote.data_alojamento,
        lote.dias_alojamento,
        lote.data_previsao_carregamento,
        lote.deleted_at,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'insert',sql, data);
        console.error('Falha ao inserir lote', err, sql, data)
      })
    })
  }

  public update(lote){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update lotes set propriedade_id=?, galpao_id=?, lote_status_id=?, fabrica_id=?, sexo_lote_id=?, linhagem_id=?, nro_lote=?, nro_animais=?, data_alojamento=?, dias_alojamento=?, data_previsao_carregamento=?, deleted_at=? where id =?';
      let data = [
        lote.propriedade_id,
        lote.galpao_id,
        lote.lote_status_id,
        lote.fabrica_id,
        lote.sexo_lote_id,
        lote.linhagem_id,
        lote.nro_lote,
        lote.nro_animais,
        lote.data_alojamento,
        lote.dias_alojamento,
        lote.data_previsao_carregamento,
        lote.deleted_at,
        lote.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'update',sql, data);
        console.error('Falha ao atualizar lote', err, sql, data)
      })
    })
  }

  public updateSinc(id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update lotes set sincronizado=? where id=?';
        let data = [
          true,
          id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'lotes.ts', 'updateSinc',sql,data);
          console.error('Falha ao executar sql lotes updateSync', err)
        });
      });
  }

  public insertLinhagem(linhagem){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into linhagens (id, nome, descricao) values (?,?,?)';
      let data = [
        linhagem.id,
        linhagem.nome,
        linhagem.descricao,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'insertLinhagem',sql, data);
        console.error('Falha ao inserir linhagens', err, sql, data)
      })
    })
  }

  public updateLinhagem(linhagem){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update linhagens set nome=?, descricao=? where id =?';
      let data = [
        linhagem.nome,
        linhagem.descricao,
        linhagem.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'updateLinhagem',sql, data);
        console.error('Falha ao atualizar linhagens', err, sql, data)
      })
    })
  }

  public insertStatusLote(status){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into lotes_status (id, descricao) values (?,?)';
      let data = [
        status.id,
        status.descricao,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'insertStatusLote',sql, data);
        console.error('Falha ao inserir lotes_status', err, sql, data)
      })
    })
  }

  public updateStatusLote(status){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update lotes_status set descricao=? where id =?';
      let data = [
        status.descricao,
        status.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'updateStatusLote',sql, data);
        console.error('Falha ao atualizar lotes_status', err, sql, data)
      })
    })
  }

  public insertSexosLote(sexo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into lotes_sexos (id, descricao, ativo) values (?,?,?)';
      let data = [
        sexo.id,
        sexo.descricao,
        sexo.ativo,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'insertSexosLote',sql, data);
        console.error('Falha ao inserir lotes_sexos', err, sql, data)
      })
    })
  }

  public updateSexosLote(sexo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update lotes_sexos set descricao=?, ativo=? where id =?';
      let data = [
        sexo.descricao,
        sexo.ativo,
        sexo.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'updateSexosLote',sql, data);
        console.error('Falha ao atualizar lotes_sexos', err, sql, data)
      })
    })
  }

  public insertFabrica(fabrica){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into fabricas (id, nome, cnpj, ativo, deleted_at) values (?,?,?,?,?)';
      let data = [
        fabrica.id,
        fabrica.nome,
        fabrica.cnpj,
        fabrica.ativo,
        fabrica.deleted_at,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'insertFabrica',sql, data);
        console.error('Falha ao inserir fabricas', err, sql, data)
      })
    })
  }

  public updateFabrica(fabrica){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update fabricas set nome=?, cnpj=?, ativo=?, deleted_at=? where id =?';
      let data = [
        fabrica.nome,
        fabrica.cnpj,
        fabrica.ativo,
        fabrica.deleted_at,
        fabrica.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'lotes.ts', 'updateFabrica',sql, data);
        console.error('Falha ao atualizar fabricas', err, sql, data)
      })
    })
  }

  public getLotesLocal(galpao_id = null){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT l.id, l.nro_lote, p.subtipo_integracao_id FROM lotes AS l LEFT JOIN propriedades AS p ON p.id = l.propriedade_id WHERE (l.deleted_at == "null" OR l.deleted_at IS NULL)';
      let params: any[] = [];

      if(galpao_id){
        sql += ' AND galpao_id = ? AND lote_status_id != 3';
        params.push(galpao_id);
      }

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let lotes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var lote = data.rows.item(i);
            lotes.push(lote);
          }
          return lotes;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getLotesLocal',sql, params);
        console.error('Falha ao executar sql lotes', err)
      });
    }).catch((e) => console.error('Falha ao listar lotes', e));
  }

  public getLotesRespondidos(datas:any=null, direcao:string = null, considerar_cotacao:boolean = false){
    let filtro_data = '';
    let ordena_nome = ' \tORDER BY data_ultima_avaliacao DESC\n';
    let valor_chk = '';
    let peso = '';

    if(considerar_cotacao){
      peso = '\t(SELECT sum(peso_chk)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE lt.id = chk_av.lote_id\n' +
        '\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)' +
        '\t) AS peso_chk,\n'+
        '\t(SELECT SUM(peso_total) FROM (SELECT ((SUM(chk_resp.valor)*chk_av.peso_chk)/100) AS peso_total \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE lote_id = lt.id \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.resposta = 1 \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tGROUP BY chk_av.id)\n' +
        '\t) AS total_peso_av,';
      valor_chk = '\t(SELECT valor FROM cotacoes \n' +
        '\tWHERE data_final IS NULL \n' +
        '\tAND tipo_integracao_id = prop.tipo_integracao_id\n' +
        'ORDER BY id DESC \n' +
        'LIMIT 1) AS valor_total_chk';
    }else{
      valor_chk = '\t(SELECT SUM(chk_av1.valor_chk)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\tWHERE chk_av1.lote_id = lt.id\n' +
        '\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t) AS valor_total_chk\n';
    }

    if(datas){
      let from = datas.from.string;
      let to = datas.to.string;
      filtro_data = `\tAND (chk_av.data_aplicacao BETWEEN '${from}' AND '${to}')\n`;
    }

    if(direcao){
      ordena_nome += ` ,prod.nome ${direcao} `
    }

    return this.dbProvider.getDB().then((db:SQLiteObject) => {

      let sql = 'SELECT  lt.id,\n' +
        '\tlt.nro_lote,\n' +
        '\tgp.descricao AS galpao_descricao,\n' +
        '\tprop.descricao AS propriedade_descricao,\n' +
        '\tprod.nome AS produtor_nome,\n' +
        '\t(SELECT count(id)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE lt.id = chk_av.lote_id\n' + filtro_data +
        '\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)\n' +
        '\t) AS avaliacoes_count,\n' +
        '\t(SELECT chk_av.data_aplicacao\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE lt.id = chk_av.lote_id\n' + filtro_data +
        '\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)\n' +
        '\t\tORDER BY chk_av.id DESC LIMIT 1\n' +
        '\t) AS data_ultima_avaliacao,\n' +
        '\t(SELECT SUM(chk_perg.valor_max)\n' +
        '\t\tFROM checklists AS chk\n' +
        '\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\tON chk.id = chk_perg.checklist_id\n' +
        '\t\tWHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.lote_id = lt.id)\n' +
        '\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t) AS pts_total_chk,\n' +
        '\t(SELECT SUM(chk_resp.valor)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\tON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\tWHERE chk_av.lote_id = lt.id\n' +
        '\t\t\tAND chk_resp.resposta = 1 \n' +
        '\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t) AS pts_total_conf,' +
		  '\t(SELECT SUM(chk_resp.valor)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\tON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\tWHERE chk_av.lote_id = lt.id\n' +
        '\t\t\tAND chk_resp.resposta = 0 \n' +
        '\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t) AS pts_total_n_conf,' +
        peso +
        valor_chk +
        '\tFROM lotes AS lt\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON lt.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON lt.galpao_id = gp.id\n' +
        '\tWHERE avaliacoes_count > 0\n' + ordena_nome;

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let lotes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var lote = data.rows.item(i);
            lotes.push(lote);
          }
          return lotes;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getLotesRespondidos',sql);
        console.error('Falha ao executar sql lotes', err,sql)
      });
    });
  }

  public getDadosLote(lote_id, considerar_cotacao:boolean = false){
    let pts_total_conf = '';
    let pts_total_n_conf = '';
    let valor_total_chk = '';
    let peso = '';
    if(considerar_cotacao){
      peso = '\t(SELECT sum(peso_chk)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE lt.id = chk_av.lote_id\n' +
        '\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)' +
        '\t) AS peso_chk,\n'+
        '\t(SELECT SUM(peso_total) FROM (SELECT ((SUM(chk_resp.valor)*chk_av.peso_chk)/100) AS peso_total \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE lote_id = lt.id \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.resposta = 1 \n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_resp.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tGROUP BY chk_av.id)\n' +
        '\t) AS total_peso_av,';
      pts_total_conf = '\t(SELECT SUM(chk_resp.valor)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\tON chk_av.id = chk_resp.avaliacao_id\n' +
        '\t\tWHERE chk_av.lote_id = lt.id\n' +
        '\t\tAND chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\tAND chk_resp.resposta = 1 \n' +
        '\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t) AS pts_total_conf,';
      pts_total_n_conf = '\t(SELECT SUM(chk_resp.valor)\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\tON chk_av.id = chk_resp.avaliacao_id\n' +
        '\t\tWHERE chk_av.lote_id = lt.id\n' +
        '\t\tAND chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\tAND chk_resp.resposta = 0 \n' +
        '\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t) AS pts_total_n_conf,';
      valor_total_chk = '\t(SELECT valor FROM cotacoes \n' +
        '\tWHERE data_final IS NULL \n' +
        '\tAND tipo_integracao_id = prop.tipo_integracao_id\n' +
        'ORDER BY id DESC \n' +
        'LIMIT 1) AS valor_total_chk';
    }else{
      pts_total_conf = '\t\t(SELECT (total_respostas/qtd_chk)\n' +
        '\t\t\tFROM (SELECT SUM(chk_resp.valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\tWHERE chk_av1.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\tON (chk_av2.id = chk_resp.avaliacao_id AND chk_av2.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av2.id_web\n' +
        '\t\t\t\t\t\tWHERE chk_av2.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\tAND chk_resp.resposta = 1)\n' +
        '\t\t) AS pts_total_conf,\n';

      pts_total_n_conf = '\t\t(SELECT (total_respostas/qtd_chk)\n' +
        '\t\t\tFROM (SELECT SUM(chk_resp.valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\tWHERE chk_av1.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\tON (chk_av2.id = chk_resp.avaliacao_id AND chk_av2.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av2.id_web\n' +
        '\t\t\t\t\t\tWHERE chk_av2.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\tAND chk_resp.resposta = 0)\n' +
        '\t\t) AS pts_total_n_conf,\n';

      valor_total_chk = '\t\t(SELECT SUM(chk_av1.valor_chk)\n' +
      '\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
      '\t\t\tWHERE chk_av1.lote_id = lt.id\n' +
      '\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
      '\t\t) AS valor_total_chk\n' ;
    }

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT  lt.*,\n' +
        '\t\tCOALESCE(lin.nome,"Indefinida") AS linhagem,\n' +
        '\t\tgp.descricao AS galpao_descricao,\n' +
        '\t\tprop.descricao AS propriedade_descricao,\n' +
        '\t\tprod.nome AS produtor_nome,\n' +
        '\t\tlt_st.descricao AS status,\n' +
        '\t\tlt_sx.descricao AS sexo,\n' +
        '\t\t(SELECT count(id)\n' +
        '\t\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\tWHERE lt.id = chk_av.lote_id\n' +
        '\t\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)\n' +
        '\t\t) AS avaliacoes_count,\n' +
        '\t\t(SELECT count(chk_resp.id)\n' +
        '\t\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\t\t\tLEFT JOIN checklists_respostas as chk_resp\n' +
        '\t\t\t\t\tON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\tWHERE lt.id = chk_av.lote_id\n' +
        '\t\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t\t) AS respostas_count, \n' +
        '\t\t(SELECT SUM(chk_perg.valor_max)\n' +
        '\t\t\tFROM checklists AS chk\n' +
        '\t\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\t\tON chk.id = chk_perg.checklist_id\n' +
        '\t\t\tWHERE chk.id IN(SELECT DISTINCT(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\t\t\t\tWHERE chk_av1.lote_id = lt.id)\n' +
        '\t\t\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t\t) AS pts_total_chk,\n' +
        peso +
        pts_total_conf +
        pts_total_n_conf +
        '\t\t(SELECT COALESCE((total_respostas/qtd_chk),0)\n' +
        '\t\t\tFROM (SELECT SUM(chk_resp.valor) as total_respostas,\n' +
        '\t\t\t\t\t\t\t(SELECT count(chk_av1.checklist_id)\n' +
        '\t\t\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\t\t\t\t\tWHERE chk_av1.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\t\t\tAND (chk_av1.deleted_at IS NULL OR chk_av1.deleted_at = "null")\n' +
        '\t\t\t\t\t\t\t) as qtd_chk\n' +
        '\t\t\t\t\t\tFROM checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\t\t\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t\t\t\t\t\tON (chk_av2.id = chk_resp.avaliacao_id AND chk_av2.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av2.id_web\n' +
        '\t\t\t\t\t\tWHERE chk_av2.lote_id = lt.id\n' +
        '\t\t\t\t\t\t\tAND chk_av2.deleted_at IS NULL\n' +
        '\t\t\t\t\t\t\tAND chk_resp.nao_se_aplica = 1)\n' +
        '\t\t) AS pts_total_resp_nulas,' +
        valor_total_chk +
        '\tFROM lotes  AS lt\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON lt.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON lt.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN lotes_status AS lt_st\n' +
        '\t\t\tON lt.lote_status_id = lt_st.id\n' +
        '\t\tLEFT JOIN lotes_sexos AS lt_sx\n' +
        '\t\t\tON lt.sexo_lote_id = lt_sx.id\n' +
        '\t\tLEFT JOIN linhagens AS lin\n' +
        '\t\t\tON lt.linhagem_id = lin.id\n' +
        '\tWHERE lt.id = ?';
      let params: any[] = [lote_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getDadosLote',sql, params);
        console.error('Falha ao executar sql lotes', err)
      });
    }).catch((e) => console.error('Falha ao listar lotes', e));
  }

  public getChecklistsComTotais(lote_id, considerar_cotacao:boolean = false){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT chk_av.id, chk_av.id_web,\n' +
        '\t\tchk_av.data_aplicacao,\n' +
		  '\t\tchk_av.assinatura,\n' +
		  '\t\tchk_av.peso_chk,\n' +
        '\t\tchk.descricao,\n' +
        '\t\t((SUM(chk_resp.valor)*chk_av.peso_chk)/100) AS total_peso_av,\n ' +
        '\t\tSUM(CASE WHEN chk_resp.resposta == 1 THEN 1 ELSE 0 END) AS conformes,\n' +
        '\t\tSUM(CASE WHEN (chk_resp.resposta == 0 AND (chk_resp.nao_se_aplica IS NULL OR chk_resp.nao_se_aplica = 0)) THEN 1 ELSE 0 END) AS nao_conformes,\n' +
        '\t\tSUM(CASE WHEN chk_resp.nao_se_aplica == 1 THEN 1 ELSE 0 END) AS nao_se_aplica,\n' +
        '\t\t(SELECT SUM(chk_perg.valor_max)\n' +
        '\t\t\tFROM checklists AS chk\n' +
        '\t\t\t\tLEFT JOIN checklists_perguntas AS chk_perg\n' +
        '\t\t\t\t\tON chk.id = chk_perg.checklist_id\n' +
        '\t\t\tWHERE chk.id = chk_av.checklist_id\n' +
        '\t\t\t\tAND (chk_perg.deleted_at IS NULL OR chk_perg.deleted_at = "null")\n' +
        '\t\t) AS pts_total_chk,\n' +
        '\t\tSUM(CASE WHEN chk_resp.resposta == 1 THEN chk_resp.valor ELSE 0 END) AS pts_total_conf,\n' +
		'\t\tSUM(CASE WHEN chk_resp.resposta == 0 THEN chk_resp.valor ELSE 0 END) AS pts_total_n_conf,\n' +
        '\t\t(SELECT COALESCE(SUM(chk_resp.valor),0)\n' +
        '\t\t\t\tFROM checklists_respostas AS chk_resp\n' +
        '\t\t\t\tWHERE (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\t\t\t\tAND chk_resp.nao_se_aplica = 1 \n' +
        '\t\t\t\tAND (chk_resp.deleted_at IS NULL OR chk_resp.deleted_at = "null")\n' +
        '\t\t) AS pts_total_resp_nulas,' +
        '\t\t(chk_av.valor_chk) AS valor_total_chk\n' +
        '\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tLEFT JOIN checklists_respostas AS chk_resp\n' +
        '\t\t\t ON (chk_av.id = chk_resp.avaliacao_id AND chk_av.id_web IS NULL)\n' +
        '\t\t\t\tOR chk_resp.avaliacao_id_web = chk_av.id_web' +
        '\t\tLEFT JOIN checklists AS chk\n' +
        '\t\t\tON chk_av.checklist_id = chk.id\n' +
        '\tWHERE chk_av.lote_id = ?\n' +
        '\tAND chk_resp.avaliacao_id_web = chk_av.id_web\n' +
        '\tGROUP BY chk_av.id';

      let params: any[] = [lote_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }
          return respostas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getChecklistsComTotais',sql, params);
        console.error('Falha ao executar sql lotes', err, sql, params)
      });
    }).catch((e) => console.error('Falha ao listar lotes', e));
  }

  public getChecklistsRespostasLote(lote_id){

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT chk_resp.avaliacao_id, chk_resp.avaliacao_id_web,\n' +
        '\t\tchk_pg.descricao,\n' +
        '\t\t(CASE WHEN chk_resp.resposta == 1 \n' +
        '\t\t\tTHEN "Conforme" \n' +
        '\t\t\tELSE "Não Conforme" END\n' +
        '\t\t) AS resposta,\n' +
        '\t\t(CASE WHEN chk_resp.valor > 0 \n' +
        '\t\t\t\tTHEN ("+ " || chk_resp.valor) \n' +
        '\t\t\tWHEN chk_resp.valor > 0 \n' +
        '\t\t\t\tTHEN ("- " || chk_resp.valor) \n' +
        '\t\t\tELSE valor END\n' +
        '\t\t) AS valor\n' +
        '\tFROM checklists_respostAS AS chk_resp\n' +
        '\t\tLEFT JOIN checklists_perguntAS AS chk_pg\n' +
        '\t\t\tON chk_resp.pergunta_id = chk_pg.id\n' +
        '\tWHERE avaliacao_id in (SELECT id FROM checklists_avaliacoes WHERE lote_id  = ? AND deleted_at is null) \n' +
        '\t\tOR avaliacao_id_web in (SELECT id_web FROM checklists_avaliacoes WHERE lote_id  = ? AND deleted_at is null) \n' +
        '\tORDER BY chk_resp.avaliacao_id, pergunta_id';

      let params = [lote_id,lote_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let respostas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            respostas.push(resposta);
          }

          return respostas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getChecklistsRespostasLote',sql, params);
        console.error('Falha ao executar sql periodo', err)
      });
    });
  }


  public getTotaisDoLote(lote_id){

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT\n' +
        '\t\t(SELECT count(chk_resp1.id)\n' +
        '\t\t\tFROM checklists_respostas AS chk_resp1\n' +
        '\t\t\t\tLEFT JOIN checklists_avaliacoes AS chk_av1\n' +
        '\t\t\t\tON (chk_av1.id = chk_resp1.avaliacao_id AND chk_av1.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp1.avaliacao_id_web = chk_av1.id_web\n' +
        '\t\t\tWHERE chk_av.lote_id = chk_av1.lote_id\n' +
        '\t\t\t\tAND chk_resp1.resposta == 1\n' +
        '\t\t) AS total_conformes,\n' +
        '\t\t(SELECT count(chk_resp2.id)\n' +
        '\t\t\tFROM checklists_respostas AS chk_resp2\n' +
        '\t\t\t\tLEFT JOIN checklists_avaliacoes AS chk_av2\n' +
        '\t\t\t\tON (chk_av2.id = chk_resp2.avaliacao_id AND chk_av2.id_web IS NULL)\n' +
        '\t\t\t\t\tOR chk_resp2.avaliacao_id_web = chk_av2.id_web\n' +
        '\t\t\tWHERE chk_av.lote_id = chk_av2.lote_id\n' +
        '\t\t\t\tAND chk_resp2.resposta == 0\n' +
        '\t\t) AS total_nao_conformes\n' +
        '\t\tFROM checklists_avaliacoes AS chk_av\n' +
        '\t\tWHERE chk_av.lote_id = ?\n' +
        '\t\tGROUP BY chk_av.lote_id';

      let params: any[] = [lote_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        return data.rows.item(0);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'lotes.ts', 'getTotaisDoLote',sql, params);
        console.error('Falha ao executar sql lotes', err, sql)
      });
    });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  /*Atualiza tabela de Status do lote*/
  public atualizaStatusLotes(url){
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/lotes/status`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando status dos lotes...');
          if(results){
            for (let result of results){
              let sql_up = 'update lotes_status SET descricao=? where id = ?;';
              let data_up = [result.descricao, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO lotes_status (id, descricao) values (?, ?)';
                  let data_ins = [result.id, result.descricao];

                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaStatusLotes', sql_ins,data_ins);
                    reject();
                  })
                }
              }).catch(err=>{
                this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaStatusLotes',sql_up, data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        },err => {
          // this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaStatusLotes',`${url}/lotes/status`);
          console.error('Erro ao Abrir Base', err);
          reject();
        });
      });
    });
    return promisse;
  }

  /*Atualiza tabela de sexos do lote*/
  public atualizaSexoLotes(url){
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/lotes/sexos`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando sexo dos lotes...');
          if(results){
            for (let result of results){
              let sql_up = 'update lotes_sexos SET descricao=?, ativo=? where id = ?;';
              let data_up = [result.descricao, result.ativo, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO lotes_sexos (id, descricao, ativo) values (?, ?, ?)';
                  let data_ins = [result.id, result.descricao, result.ativo];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaSexoLotes',sql_ins, data_ins);
                    reject();
                  })
                }
              }).catch(err=>{
                this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaSexoLotes',sql_up,data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        }, err=>{
          // this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaSexoLotes',`${url}/lotes/sexos`);
          console.error('Erro ao Abrir Base', err);
          reject();
        });
      });
    });
    return promisse;
  }

  /*Atualiza tabela de fabricas*/
  public atualizaFabricasLotes(url){
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/lotes/fabricas`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando fabricas...');
          if(results){
            for (let result of results){
              let sql_up = 'update fabricas SET nome=?, cnpj=?, ativo=? where id = ?;';
              let data_up = [result.nome, result.cnpj, result.ativo, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO fabricas (id, nome, cnpj, ativo) values (?, ?, ?, ?)';
                  let data_ins = [result.id, result.nome, result.cnpj, result.ativo];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaFabricasLotes',sql_ins, data_ins);
                    reject();
                  });
                }
              }).catch(err=>{
                this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaFabricasLotes',sql_up,data_up);
                reject();
              });
            }
          }
          loading.dismiss();
          resolve();
        }, err=>{
          // this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaFabricasLotes',`${url}/lotes/fabricas`);

          console.error('Erro ao Abrir Base', err);
          reject();
        });
      });
    });
    return promisse;
  }

  public atualizaLotes(url){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/lotes/tecnicos`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando lotes...');
          if(results){
            for (let result of results){
              let sql_up = 'update lotes SET propriedade_id=?, galpao_id=?, lote_status_id=?, nro_lote=?, fabrica_id=?, sexo_lote_id=?, nro_animais=?, data_alojamento=?, dias_alojamento=?, data_previsao_carregamento=?, deleted_at=? where id = ?;';
              let data_up = [result.propriedade_id, result.galpao_id, result.lote_status_id, result.nro_lote, result.fabrica_id, result.sexo_lote_id, result.nro_animais, result.data_alojamento, result.dias_alojamento, result.data_previsao_carregamento,result.deleted_at, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO lotes (id, propriedade_id, galpao_id, lote_status_id, nro_lote, fabrica_id, sexo_lote_id, nro_animais, data_alojamento, dias_alojamento, data_previsao_carregamento, deleted_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)';
                  let data_ins = [result.id, result.propriedade_id, result.galpao_id, result.lote_status_id, result.nro_lote, result.fabrica_id, result.sexo_lote_id, result.nro_animais, result.data_alojamento, result.dias_alojamento, result.data_previsao_carregamento, result.deleted_at];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaFabricasLotes',sql_ins, data_ins);
                    reject();

                  })
                }
              }).catch(err=>{
                this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaFabricasLotes',sql_up,data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        }, err=>{
          // this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaLotes',`${url}/lotes/tecnicos`);
          console.error('Erro ao Abrir Base', err);
          reject();
        });
      });
    });
    return promisse;
  }

  public atualizaLinhagens(){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${this.urlApi}/linhagens`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Linhagens...');
          if (results){
            for (let result of results){
              let sql_up = 'update linhagens SET nome=? where id = ?;';
              let data_up = [result.nome, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO linhagens (id, nome) values (?,?)';
                  let data_ins = [result.id, result.nome];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaLinhagens',sql_ins, data_ins);
                    console.error('falha ao exectar sql ao inserir linhagens', err);
                    reject();
                  });
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaLinhagens',sql_up,data_up);
                console.error('falha ao exectar sql ao inserir linhagens', err);
                reject();
              });
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          // this.exceptionProvider.insert(err, 'lotes.ts', 'atualizaLinhagens',`${this.urlApi}/linhagens`);
          console.error('falha ao consultar linhagens', err);
          reject();
        })
      })
    });

    return promisse;
  }

}
