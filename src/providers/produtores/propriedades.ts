import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {Propriedade} from "../../class/Propriedade";
import {Storage} from "@ionic/storage";
import {CheckNetworkProvider} from "../check-network/check-network";
import {Events} from "ionic-angular";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {AuthProvider} from "../auth/auth";
import * as moment from "moment";

@Injectable()
export class PropriedadesProvider extends Propriedade{
  private urlApi;

  constructor(
      private dbProvider: DatabaseProvider,
      private http: HttpClient,
      private storage: Storage,
      private networkService: CheckNetworkProvider,
      private events: Events,
      private fileTransfer:FileTransfer,
      private authProvider: AuthProvider,
      private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/propriedades`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/propriedades`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/propriedades`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiFind(id) {
      return this.http.get<any>(this.urlApi);
  }

  public apiUpdate(propriedade){
    return this.http.patch(`${this.urlApi}/${propriedade.id}`, propriedade);
  }

  public apiStoreBkp(value){
    return this.http.post(`${this.urlApi}/upload/bkp`, value);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  /*Propriedade*/
  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT  ' +
          ' integ.nome AS integradora, ' +
          ' prod.nome AS produtor, ' +
          ' COALESCE((prod.nome || " - " || prop.descricao), prop.descricao) AS propriedade_concat_produtor, ' +
          ' prop.* ' +
          'FROM propriedades AS prop  ' +
          ' LEFT JOIN produtores AS prod  ' +
          '  ON prop.produtor_id  = prod.id ' +
          ' LEFT JOIN integradoras as integ ' +
          '  ON prop.integradora_id = integ.id ' +
          'WHERE prop.id = ? AND (prop.deleted_at == "null" OR prop.deleted_at IS NULL)';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);
              let propriedade = new Propriedade();
              propriedade.id = item.id;
              propriedade.id_web = item.id_web;
              propriedade.produtor_id = item.produtor_id;
              propriedade.integradora_id = item.integradora_id;
              propriedade.tipo_integracao_id = item.tipo_integracao_id;
              propriedade.subtipo_integracao_id = item.subtipo_integracao_id;
              propriedade.estado_id = item.estado_id;
              propriedade.municipio_id = item.municipio_id;
              propriedade.descricao = item.descricao;
              propriedade.logradouro = item.logradouro;
              propriedade.complemento = item.complemento;
              propriedade.ie = item.ie;
              propriedade.latitude = item.latitude;
              propriedade.longitude = item.longitude;
              propriedade.ativo = item.ativo;
              propriedade.propriedade_concat_produtor = item.propriedade_concat_produtor;
              propriedade.integradora = item.integradora;
              propriedade.produtor = item.produtor;
              propriedade.codErp = item.codErp;
              propriedade.deleted_at = item.deleted_at;

              return propriedade;
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'propriedades.ts', 'get',sql,data);
            console.error('Falha ao executar sql propriedade', err)
          });
      });
  }

  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT  ' +
          ' COALESCE((SELECT foto FROM propriedades_fotos where prop.id = propriedade_id AND principal == "true" AND deleted_at IS NULL), "assets/imgs/alert_image.png") AS foto, ' +
          ' muni.nome AS municipio, ' +
          ' muni.uf, ' +
          ' prop.*,  ' +
          ' COALESCE((p.nome || " - " || prop.descricao), prop.descricao) AS propriedade_concat_produtor, ' +
          ' p.nome AS produtor  ' +
          'FROM propriedades AS prop ' +
          ' LEFT JOIN produtores AS p  ' +
          '  ON prop.produtor_id = p.id ' +
          ' LEFT JOIN municipios AS muni ' +
          '  ON prop.municipio_id = muni.id '+
          'WHERE (prop.deleted_at == "null" OR prop.deleted_at IS NULL)';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let propriedades: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var propriedade = data.rows.item(i);
                propriedades.push(propriedade);
              }
              return propriedades;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'propriedades.ts', 'getAll', sql);
            console.error('Falha ao executar sql propriedades', err)
          });
      });
  }

  public getAllToSelect(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT prop.id,  ' +
        ' COALESCE((p.nome || " - " || prop.descricao), prop.descricao) AS nome ' +
        'FROM propriedades AS prop  ' +
        ' LEFT JOIN produtores AS p  ' +
        '  ON prop.produtor_id = p.id ' +
        'WHERE (prop.ativo = 1 ' +
        '         OR prop.ativo = "true") ' +
        ' AND (prop.deleted_at == "null" OR prop.deleted_at IS NULL) ' +
        ' ORDER BY p.nome, prop.descricao';

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let propriedades: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var propriedade = data.rows.item(i);
            propriedades.push(propriedade);
          }
          return propriedades;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'propriedades.ts', 'getPropriedadesLocal', sql);
        console.error('Falha ao executar sql propriedade', err)
      });
    });
  }

  public getPropriedadesLocal(isCheck:boolean = false){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT prop.*,  ' +
        ' COALESCE((p.nome || " - " || prop.descricao), prop.descricao) AS propriedade_concat_produtor ' +
        'FROM propriedades AS prop  ' +
        ' LEFT JOIN produtores AS p  ' +
        '  ON prop.produtor_id = p.id ' +
        'WHERE (prop.ativo = 1 ' +
        '         OR prop.ativo = "true") ';

      /*Se a consulta for utilizada para o CHK verifica se existem lotes ou se é no periodo*/
      if (isCheck){
        sql += ' AND EXISTS (SELECT id FROM lotes AS l  ' +
          '  WHERE l.propriedade_id = prop.id  ' +
          '   AND lote_status_id != 3) OR (SELECT chk_periodo FROM empresa LIMIT 1) = 1 ';
      }

      sql += ' AND (prop.deleted_at == "null" OR prop.deleted_at IS NULL) ' +
        ' ORDER BY p.nome, prop.descricao';

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let propriedades: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var propriedade = data.rows.item(i);
            propriedades.push(propriedade);
          }
          return propriedades;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'propriedades.ts', 'getPropriedadesLocal', sql);
        console.error('Falha ao executar sql propriedade', err)
      });
    });
  }

  public getPropriedadesLocalRecontagemGroup(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT prop.id,  \n' +
        '\t\tCOALESCE((p.nome || " - " || prop.descricao), prop.descricao) AS propriedade_concat_produtor,\n' +
        '\t\tCASE WHEN (SELECT rp.data_recontagem FROM recontagem_planteis as rp\n' +
        '\t\t\t\t\t\t\t\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\t\t\t\t\t\t\t\tON gp.id = rp.galpao_id\n' +
        '\t\t\t\t\t\t\t\tWHERE gp.propriedade_id = prop.id\n' +
		  '\t\t\t\t\t\t\t\tAND (strftime("%m","now") >= strftime("%m",rp.data_prevista) \n' +
		  '\t\t\t\t\t\t\t\t\t\tAND strftime("%Y","now") >= strftime("%Y",rp.data_prevista)) \n' +
		  '\t\t\t\t\t\t\t\tORDER BY rp.id DESC\n' +
        '\t\t\t\t\t\t\t\tLIMIT 1 ) IS NULL\n' +
		  '\t\t\t\t\t\t\t\tAND EXISTS(SELECT rp.id FROM recontagem_planteis as rp \n' +
		  '\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN galpoes AS gp\n' +
		  '\t\t\t\t\t\t\t\t\t\t\t\tON gp.id = rp.galpao_id\n' +
		  '\t\t\t\t\t\t\t\t\t\t\tWHERE gp.propriedade_id = prop.id ' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND rp.data_recontagem IS NULL' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND rp.deleted_at IS NULL' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND Cast ((JulianDay(rp.data_prevista) - JulianDay("now")) As Integer) <= 35)' +
        '\t\t\tTHEN "Com Recontagem Pendente"\n' +
        '\t\t\tELSE "Sem Recontagem Pendente"\n' +
        '\t\tEND AS grupo_desc,\n' +
        '\t\tCASE WHEN (SELECT rp.data_recontagem FROM recontagem_planteis as rp\n' +
        '\t\t\t\t\t\t\t\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\t\t\t\t\t\t\t\tON gp.id = rp.galpao_id\n' +
        '\t\t\t\t\t\t\t\tWHERE gp.propriedade_id = prop.id\n' +
        '\t\t\t\t\t\t\t\tAND (strftime("%m","now") >= strftime("%m",rp.data_prevista) \n' +
		  '\t\t\t\t\t\t\t\t\t\tAND strftime("%Y","now") >= strftime("%Y",rp.data_prevista)) \n' +
        '\t\t\t\t\t\t\t\tORDER BY rp.id DESC\n' +
        '\t\t\t\t\t\t\t\tLIMIT 1 ) IS NULL\n' +
		  '\t\t\t\t\t\t\t\tAND EXISTS(SELECT rp.id FROM recontagem_planteis as rp \n' +
		  '\t\t\t\t\t\t\t\t\t\t\tLEFT JOIN galpoes AS gp\n' +
		  '\t\t\t\t\t\t\t\t\t\t\t\tON gp.id = rp.galpao_id\n' +
		  '\t\t\t\t\t\t\t\t\t\t\tWHERE gp.propriedade_id = prop.id ' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND rp.data_recontagem IS NULL' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND rp.deleted_at IS NULL' +
		  '\t\t\t\t\t\t\t\t\t\t\t\t\tAND Cast ((JulianDay(rp.data_prevista) - JulianDay("now")) As Integer) <= 35)' +
        '\t\t\tTHEN 1\n' +
        '\t\t\tELSE 0\n' +
        '\t\tEND AS grupo_id \n' +
        '\tFROM propriedades AS prop\n' +
        '\t\tLEFT JOIN produtores AS p\n' +
        '\t\t\tON prop.produtor_id = p.id\n' +
        '\tWHERE (prop.ativo = 1 OR prop.ativo = "true")\n' +
        '\t\tAND (prop.deleted_at == "null" OR prop.deleted_at IS NULL)\n' +
        '\tORDER BY p.nome, prop.descricao';

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let propriedades: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var propriedade = data.rows.item(i);
            propriedades.push(propriedade);
          }
          return propriedades;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'propriedades.ts', 'getPropriedadesLocal', sql);
        console.error('Falha ao executar sql propriedade', err)
      });
    });
  }

  public getPropriedadesLocalWithChk(isCheck:boolean = false, isPeriodo:boolean = true){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql_group = '';

      if(isPeriodo){
        sql_group += '\t\tCASE WHEN (SELECT id FROM periodo_aplicacoes AS pap\n' +
          '\t\t\t\t\t\t\t\tWHERE NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE pap.id = chk_av.periodo_id\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)) \n' +
          '\t\t\t\t\t\t\t\t\tAND propriedade_id = prop.id\n' +
          '\t\t\t\t\t\t\t\t\tAND pap.vigente = "true"\n' +
          '\t\t\t\t\t\t\t\t\tAND pap.deleted_at is null limit 1) IS NULL\n' +
          '\t\t\t\t\tTHEN "Sem Checklist Pendente"\n' +
          '\t\t\t\t\tELSE "Com Checklist Pendente"\n' +
          '\t\tEND AS grupo_desc,\n' +
          '\t\tCASE WHEN (SELECT id FROM periodo_aplicacoes AS pap\n' +
          '\t\t\t\t\t\t\t\tWHERE NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE pap.id = chk_av.periodo_id\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at is null)\n' +
          '\t\t\t\t\t\t\t\t\tAND propriedade_id = prop.id\n' +
          '\t\t\t\t\t\t\t\t\tAND pap.vigente = "true"\n' +
          '\t\t\t\t\t\t\t\t\tAND pap.deleted_at is null limit 1) IS NULL\n' +
          '\t\t\t\t\tTHEN 1\n' +
          '\t\t\t\t\tELSE 0\n' +
          '\t\tEND AS grupo_id\n';
      }else{
        sql_group += '\t\tCASE WHEN (SELECT id FROM lotes AS lt\n' +
          '\t\t\t\t\t\t\t\tWHERE NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE lt.id = chk_av.lote_id\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND (chk_av.deleted_at == "null" OR chk_av.deleted_at IS NULL)) \n' +
          '\t\t\t\t\t\t\t\t\tAND propriedade_id = prop.id\n' +
          '\t\t\t\t\t\t\t\t\tAND lt.deleted_at is null limit 1) IS NULL\n' +
          '\t\t\t\t\tTHEN "Sem Checklist Pendente"\n' +
          '\t\t\t\t\tELSE "Com Checklist Pendente"\n' +
          '\t\tEND AS grupo_desc,\n' +
          '\t\tCASE WHEN (SELECT id FROM lotes AS lt\n' +
          '\t\t\t\t\t\t\t\tWHERE NOT EXISTS (SELECT id FROM checklists_avaliacoes AS chk_av\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tWHERE lt.id = chk_av.lote_id\n' +
          '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAND chk_av.deleted_at is null)\n' +
          '\t\t\t\t\t\t\t\t\tAND propriedade_id = prop.id\n' +
          '\t\t\t\t\t\t\t\t\tAND lt.deleted_at is null limit 1) IS NULL\n' +
          '\t\t\t\t\tTHEN 1\n' +
          '\t\t\t\t\tELSE 0\n' +
          '\t\tEND AS grupo_id\n';
      }

      let sql = '\tSELECT prop.id,\n' +
        '\t\tCOALESCE((p.nome || " - " || prop.descricao), prop.descricao) AS propriedade_concat_produtor,\n' +
        sql_group +
        '\tFROM propriedades AS prop\n' +
        '\t\tLEFT JOIN produtores AS p\n' +
        '\t\t\tON prop.produtor_id = p.id\n' +
        '\tWHERE (prop.ativo = 1\n' +
        '\t\tOR prop.ativo = "true")\n';

      /*Se a consulta for utilizada para o CHK verifica se existem lotes ou se é no periodo*/
      if (isCheck){
        sql += ' AND EXISTS (SELECT id FROM lotes AS l  ' +
          '  WHERE l.propriedade_id = prop.id  ' +
          '   AND lote_status_id != 3) OR (SELECT chk_periodo FROM empresa LIMIT 1) = 1 ';
      }

      sql += ' AND (prop.deleted_at == "null" OR prop.deleted_at IS NULL) ' +
        ' ORDER BY p.nome, prop.descricao';

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let propriedades: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var propriedade = data.rows.item(i);
            propriedades.push(propriedade);
          }
          return propriedades;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'propriedades.ts', 'getPropriedadesLocal', sql);
        console.error('Falha ao executar sql propriedade', err)
      });
    });
  }

  public getAllToSyncPropriedades(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM propriedades WHERE sincronizado = ? OR sincronizado = 0';
      let data = [false];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let propriedades: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              propriedades.push(data.rows.item(i));
            }
            return propriedades;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'getAllToSyncPropriedades', sql, data);
          console.error('Falha ao consultar propriedades para sincronizar', err)
        });
    });
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM propriedades';

      return db.executeSql(sql, [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let propriedades: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              propriedades.push(data.rows.item(i));
            }
            return propriedades;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'getAllBkp', sql);
          console.error('Falha ao consultar propriedades para sincronizar', err)
        });
    });
  }

  public insert(propriedade: Propriedade) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into propriedades (id, produtor_id, integradora_id, tipo_integracao_id, subtipo_integracao_id, estado_id, municipio_id, descricao, logradouro, complemento, ie, latitude, longitude, ativo, coderp, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        let data = [
          propriedade.id,
          propriedade.produtor_id,
          propriedade.integradora_id,
          propriedade.tipo_integracao_id,
          propriedade.subtipo_integracao_id,
          propriedade.estado_id,
          propriedade.municipio_id,
          propriedade.descricao,
          propriedade.logradouro,
          propriedade.complemento,
          propriedade.ie,
          propriedade.latitude,
          propriedade.longitude,
          propriedade.ativo,
          propriedade.codErp,
          propriedade.deleted_at,
        ];

        return db.executeSql(sql, data)
          .catch((err) => {
            this.exceptionProvider.insert(err, 'propriedades.ts', 'insert',sql,data);
            console.error('Falha ao executar sql propriedade', err)
          });

      })
      .catch((e) => console.error('Falha ao salvar propriedade', e));
  }

  public update(propriedade: Propriedade) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update propriedades set produtor_id=?, integradora_id=?, tipo_integracao_id=?, subtipo_integracao_id=?, estado_id=?, municipio_id=?, descricao=?, logradouro=?, complemento=?, ie=?, latitude=?, longitude=?, ativo=?, sincronizado=?, coderp=?, deleted_at=? where id=?';
        let data = [
          propriedade.produtor_id,
          propriedade.integradora_id,
          propriedade.tipo_integracao_id,
          propriedade.subtipo_integracao_id,
          propriedade.estado_id,
          propriedade.municipio_id,
          propriedade.descricao,
          propriedade.logradouro,
          propriedade.complemento,
          propriedade.ie,
          propriedade.latitude,
          propriedade.longitude,
          propriedade.ativo,
          propriedade.sincronizado,
          propriedade.codErp,
          propriedade.deleted_at,
          propriedade.id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'update',sql,data);
          console.error('Falha ao executar sql propriedade', err)
        });

      });
  }

  public updateSync(id_web) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update propriedades set sincronizado=? where id=?';
        let data = [
          1,
          id_web,
        ];
        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'updateSync',sql,data);
          console.error('Falha ao executar sql propriedade', err)
        });

      });
  }

  /*Fotos*/
  public getFotos(propriedade_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id, foto AS img, principal FROM propriedades_fotos WHERE propriedade_id = ? AND deleted_at is null';
      let data = [propriedade_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let fotos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              fotos.push(data.rows.item(i));
            }
            return fotos;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'getFotosPropriedade', sql, data);
          console.error('Falha ao consultar fotos da propriedade', err,sql, data)
        });
    });
  }

  public getAllFotosToSyncPropriedades(propriedade_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id, propriedade_id, foto AS img, mobile_key, deleted_at FROM propriedades_fotos WHERE propriedade_id = ? AND sincronizado = ?';
      let data = [propriedade_id, false];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let propriedades: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              propriedades.push(data.rows.item(i));
            }
            return propriedades;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'propriedades.ts', 'getAllToSyncPropriedades', sql, data);
          console.error('Falha ao consultar propriedades para sincronizar', err)
        });
    });
  }

  public insertFoto(foto){
    let sql = 'INSERT INTO propriedades_fotos (propriedade_id, mobile_key, foto, sincronizado) VALUES (?,?,?,?)';
    let params = [
      foto.propriedade_id,
      foto.mobile_key,
      foto.img,
      false
    ];

    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      return db.executeSql(sql, params).then((data)=>{
        return data;
      }).catch(err=>{
        console.error('falha salvar foto propriedade - provider', err);
      });
    });
  }

  public updateFotoPrincipal(foto_id, propriedade_id){
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        let sql_clear = 'UPDATE propriedades_fotos SET principal = ? WHERE propriedade_id = ?';
        let data_clear = [false, propriedade_id];

        let sql_set_principal = 'UPDATE propriedades_fotos SET principal = ? WHERE id = ?';
        let data_set_principal = [true, foto_id];

        db.executeSql(sql_clear, data_clear).then(()=>{
          db.executeSql(sql_set_principal, data_set_principal).then(()=>{
            resolve()
          }).catch(err=>{
            this.exceptionProvider.insert(err, 'fotos.ts', 'updateFotoPrincipalPropriedades - update', sql_set_principal, data_set_principal);
            console.error('erro ao setar principal',err);
            reject();
          });
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'fotos.ts', 'updateFotoPrincipalPropriedades - clear', sql_clear, data_clear);
          console.error('erro ao remover principais',err);
          reject();
        })
      })
    });

    return promise;

  }

  public updateSyncFoto(id_web, id_mobile, mobile_key){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'UPDATE propriedades_fotos SET sincronizado=?, id_web=? WHERE id=? AND mobile_key = ?';
      let data = [
        true,
        id_web,
        id_mobile,
        mobile_key,
      ];
      return db.executeSql(sql, data).catch((err) => {
        this.exceptionProvider.insert(err, 'propriedades.ts', 'updateSyncFoto', sql, data);
        console.error('Falha ao executar sql propriedade_fotos', err)
      });

    });
  }

  public deleteFoto(foto_id){
    let promise = new Promise( (resolve, reject)=>{
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        let sql = 'UPDATE propriedades_fotos SET deleted_at = ?, sincronizado = ? WHERE id = ?';
        let data = [moment().format('YYYY-MM-DD'),false,foto_id];
        db.executeSql(sql, data).then(()=>{
          resolve()
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'fotos.ts', 'deleteFotoPropriedades', sql, data);
          console.error('erro ao remover foto',err);
          reject();
        })
      })
    });

    return promise

  }

  public delete(propriedade_id){
    let exceptionProvider = this.exceptionProvider;
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          db.transaction(function (fx) {
            fx.executeSql('delete from propriedades where id = ?', [propriedade_id], function (fx) {
              fx.executeSql('delete from galpoes where propriedade_id = ?', [propriedade_id],function (res) {
                resolve();
              }, err =>{
                console.error('erro ao remover galpao', err);
                exceptionProvider.insert(err, 'propriedades.ts', 'delete','delete from galpoes where propriedade_id = ?', propriedade_id);
                reject();
              })
            }, err =>{
              console.error('erro ao remover propriedade', err);
              exceptionProvider.insert(err, 'propriedades.ts', 'delete','delete from propriedades where id = ?', propriedade_id);
              reject();
            })
          })
        });
    });
    return promise;
  }
  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaPropriedadesLocal(){
    /*Atualiza Propriedade*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/propriedades`).subscribe((results: any) => {

            let dados = '';
            let count = 0;

            if(results){

              count = results.length;

              for (let result of results) {
                dados += `(${result.id}, ${result.id}, ${result.produtor_id}, ${result.integradora_id}, ${result.tipo_integracao_id}, ${result.subtipo_integracao_id}, ${result.estado_id}, ${result.municipio_id}, "${result.descricao}", "${result.logradouro}", "${result.complemento}", "${result.ie}", ${result.latitude}, ${result.longitude}, ${result.codErp}, ${result.ativo}, '${result.deleted_at}')`;
                if (count-- > 1) {
                  dados += ','
                }
              }
              let sql = `REPLACE INTO propriedades (id, id_web, produtor_id, integradora_id, tipo_integracao_id, subtipo_integracao_id, estado_id, municipio_id, descricao, logradouro, complemento, ie, latitude, longitude, coderp, ativo, deleted_at) values ${dados}`;

              db.sqlBatch([
                [sql, []],
              ]).catch(err => {
                this.exceptionProvider.insert(err, 'propriedades.ts', 'atualizaPropriedadesLocal', sql);
                console.error('Erro ao incluir propriedades locais', err, sql)
                reject();
              });

            }
            resolve();
          });

        });
      }).catch(e => {
        console.error('Erro ao Abrir Base', e);
        reject();
      });
    });
    return promisse;
  }

  public atualizaPropriedadeApi(propriedade:Propriedade){
    let promisse = new Promise( (resolve, reject) => {
      this.update(propriedade).then(()=>{
        if(this.networkService.isConnected()){
          this.authProvider.checkLogin().then((data: any) => {

            if (data == "success") {
              this.storage.get('SERVER_URL').then((SERVER_URL:string) =>{
                this.http.patch(`${SERVER_URL}/propriedades/${propriedade.id}`, propriedade).timeout(10000).subscribe((results:any) =>{
                  results.ativo = results.ativo == 1;
                  results.sincronizado = true;

                  this.update(results);
                  this.enviaFotos(propriedade.id);
                  resolve(true);
                }, (err) => {
                  if(err.name == "TimeoutError"){
                    reject({'name': 'TimeoutError', 'msg':'Timeout ao enviar dados'});
                  }else{
                    reject(false);
                  }
                  // this.exceptionProvider.insert(err, 'propriedades.ts', 'atualizaPropriedadeApi', `${SERVER_URL}/propriedades/${propriedade.id}`, propriedade);
                  console.error('falha ao enviar dados', err);
                });
              });
            }
          });
        }else{
          resolve(true);

        }
      }).catch((err)=>{
        console.error('falha ao salvar dados do produtor no banco',err);
        reject(false);
      });
    });

    return promisse;
  }

  public sincAllPropriedades() {
    let promise = new Promise((resolve, reject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL: string) => {
        this.getAllToSyncPropriedades().then((propriedades: any[]) => {
          for(let propriedade of propriedades){
            this.http.patch(`${SERVER_URL}/propriedades/${propriedade.id}`, propriedade).subscribe((results: any) => {
              results.ativo = results.ativo == 1;
              results.sincronizado = true;
              this.update(results);
            }, (err) => {
              // this.exceptionProvider.insert(err, 'propriedades.ts', 'sincAllPropriedades', `${SERVER_URL}/propriedades/${propriedade.id}`, propriedade);
              console.error('falha ao enviar dados', err);
              reject(false);
            });
          }
          resolve();
        });
      });
    });
    return promise;
  }

  public enviaFotos(propriedade_id){
    this.getAllFotosToSyncPropriedades(propriedade_id).then((fotos:Array<any>)=>{
      for(let foto of fotos){
        this.storage.get('access_token').then((access_token) => {
          let options: FileUploadOptions = {
            fileKey: 'foto',
            fileName: `propriedade_${foto.id}_img.jpg`,
            chunkedMode: false,
            httpMethod: 'POST',
            mimeType: "image/jpeg",
            params: foto,
            headers: {
              'Authorization': `Bearer ${access_token}`,
            }
          };

          let fileTransfer = this.fileTransfer.create();
          let url = `${this.urlApi}/${foto.propriedade_id}/fotos`;

          fileTransfer.upload(foto.img, url, options).then((retorno_api: any)=>{
            let data_json = JSON.parse(retorno_api.response);
            this.updateSyncFoto(data_json.id, data_json.id_mobile, data_json.mobile_key);
          }).catch(err=>{
            console.error('Falha ao enviar foto da propriedade', err, url);
            // this.exceptionProvider.insert(err, 'propriedades.ts', 'enviaFoto', url, foto);
          })
        });
      }
    });

  }
}
