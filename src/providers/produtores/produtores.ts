import { Injectable } from '@angular/core';
import {Produtor} from "../../class/Produtor";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {CheckNetworkProvider} from "../check-network/check-network";
import {Events} from "ionic-angular";
import {PropriedadesProvider} from "./propriedades";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {AuthProvider} from "../auth/auth";

@Injectable()
export class ProdutoresProvider extends Produtor{
  private urlApi;

  constructor(
    private dbProvider: DatabaseProvider,
    private http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private networkService: CheckNetworkProvider,
    private events:Events,
    private propriedadeProvider:PropriedadesProvider,
    private authProvider: AuthProvider,
    private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/produtores`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/produtores`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/produtores`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiUpdate(produtor) {
    return this.http.patch(`${this.urlApi}/${produtor.id}`, produtor);
  }

  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/upload/bkp`, values);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(produtor: Produtor){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'insert into produtores (id, estado_id, municipio_id, cpf, nome, logradouro, complemento, latitude, longitude, ativo, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?)';
              let data = [
                  produtor.id,
                  produtor.estado_id,
                  produtor.municipio_id,
                  produtor.cpf,
                  produtor.nome,
                  produtor.logradouro,
                  produtor.complemento,
                  produtor.latitude,
                  produtor.longitude,
                  produtor.ativo,
                  produtor.deleted_at,
              ];

              return db.executeSql(sql, data).catch((err) => {
                this.exceptionProvider.insert(err, 'produtores.ts', 'insert',sql,data);
                console.error('Falha ao executar sql produtor', err)
              });

          });
  }

  public update(produtor: Produtor){
    return this.dbProvider.getDB()
        .then((db: SQLiteObject) =>{
          let sql = 'update produtores set estado_id=?, municipio_id=?, cpf=?, nome=?, matricula=?, logradouro=?, complemento=?, telefone=?, celular=?, email=?, foto=?, latitude=?, longitude=?, ativo=?, sincronizado=?, enviar_foto=?, deleted_at=? where id=?';
          let data = [
              produtor.estado_id,
              produtor.municipio_id,
              produtor.cpf,
              produtor.nome,
              produtor.matricula,
              produtor.logradouro,
              produtor.complemento,
              produtor.telefone,
              produtor.celular,
              produtor.email,
              produtor.foto,
              produtor.latitude,
              produtor.longitude,
              produtor.ativo,
              produtor.sincronizado,
              produtor.enviar_foto,
              produtor.deleted_at,
              produtor.id
          ];

          return db.executeSql(sql, data).catch((err) => {
            this.exceptionProvider.insert(err, 'produtores.ts', 'update',sql,data);
            console.error('Falha ao executar sql produtor', err)
          });
        });
  }

  protected updatePropriedade(produtor){

    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'UPDATE propriedades set logradouro=?, complemento=?, latitude=?, longitude=?, sincronizado=? where produtor_id=?;';
      let data = [
        produtor.logradouro,
        produtor.complemento,
        produtor.latitude,
        produtor.longitude,
        produtor.sincronizado,
        produtor.id,
      ];

      return db.executeSql(sql, data).catch((err) => {
        this.exceptionProvider.insert(err, 'produtores.ts', 'updatePropriedade',sql,data);
        console.error('Falha ao executar sql propriedade dentro do produtor', err)
      });
    });
  }

  public get(id: number) {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'SELECT prod.*, integ.nome AS integradora ' +
                'FROM produtores AS prod ' +
                ' LEFT JOIN propriedades AS prop ' +
                '  ON prod.id = prop.produtor_id ' +
                ' LEFT JOIN integradoras AS integ ' +
                '  ON prop.integradora_id = integ.id ' +
                'WHERE prod.id = ?  ' +
                ' AND (prod.deleted_at == "null" OR prod.deleted_at IS NULL)';
              let data = [id];

              return db.executeSql(sql, data)
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                        let produtor = new Produtor();
                        let item = data.rows.item(0);

                        produtor.id = item.id;
                        produtor.estado_id = item.estado_id;
                        produtor.municipio_id = item.municipio_id;
                        produtor.cpf = item.cpf;
                        produtor.nome = item.nome;
                        produtor.matricula = item.matricula;
                        produtor.celular = item.celular;
                        produtor.telefone = item.telefone;
                        produtor.email = item.email;
                        produtor.logradouro = item.logradouro;
                        produtor.complemento = item.complemento;
                        produtor.foto = item.foto;
                        produtor.latitude = item.latitude;
                        produtor.longitude = item.longitude;
                        produtor.sincronizado = item.sincronizado;
                        produtor.enviar_foto = item.enviar_foto;
                        produtor.ativo = item.ativo;
                        produtor.integradora = item.integradora;

                        return produtor;
                      }

                      return null;
                  }).catch((err) => {
                  this.exceptionProvider.insert(err, 'produtores.ts', 'get',sql,data);
                  console.error('Falha ao executar sql produtor', err)
                });
          });
  }

  public getAll() {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'SELECT  ' +
                ' prod.id,  ' +
                ' prod.nome,  ' +
                ' prod.foto,  ' +
                ' muni.nome AS municipio, ' +
                ' muni.uf ' +
                'FROM produtores as prod ' +
                ' LEFT JOIN municipios AS muni ' +
                '  on prod.municipio_id = muni.id ' +
                'WHERE (prod.deleted_at == "null" OR prod.deleted_at IS NULL) '+
                'ORDER BY prod.nome';

              return db.executeSql(sql, [])
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          let produtores: any[] = [];
                          for (var i = 0; i < data.rows.length; i++) {
                              var produtor = data.rows.item(i);

                              if(!produtor.foto){
                                produtor.foto = 'assets/imgs/user_default.png';
                              }else{
                                if(produtor.foto.toString().length == 0 || produtor.foto == "null"){
                                  produtor.foto = 'assets/imgs/user_default.png';
                                }
                              }
                              produtores.push(produtor);
                          }
                          return produtores;
                      } else {
                          return [];
                      }
                  }).catch((err) => {
                  this.exceptionProvider.insert(err, 'produtores.ts', 'getAll',sql);
                  console.error('Falha ao executar sql produtor', err)
                });
          });
  }

  public getAllToSyncProdutores(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM produtores WHERE sincronizado = ? OR sincronizado = 0';
      let data = [false];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let produtores: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              console.log('produtor',data.rows.item(i));
              produtores.push(data.rows.item(i));
            }
            return produtores;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getAllToSyncProdutores',sql, data);
          console.error('Falha ao consultar produtor para sincronizar', err)
        });
    });
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM produtores';

      return db.executeSql(sql, [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let produtores: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              produtores.push(data.rows.item(i));
            }
            return produtores;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getAllBkp',sql);
          console.error('Falha ao consultar produtor para sincronizar', err)
        });
    });
  }

  public findByPropriedade(propriedade_id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT prod.nome AS produtor, ' +
          'prop.descricao AS propriedade\n' +
          'FROM propriedades as prop\n' +
          '\tLEFT JOIN produtores as prod\n' +
          '\t\tON prop.produtor_id = prod.id\n' +
          'WHERE prop.id = ?';
        let data = [propriedade_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'produtores.ts', 'findByPropriedade()',sql,data);
            console.error('Falha ao executar sql findByPropriedade()', err)
          });
      });
    
  }

  public delete(produtor_id){
    let exceptionProvider = this.exceptionProvider;
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          db.transaction(function (fx) {
            fx.executeSql('delete from produtores where id = ?', [produtor_id], function (fx) {
              fx.executeSql('delete from propriedades where produtor_id = ?', [produtor_id], function (fx) {
                fx.executeSql('delete from galpoes where produtor_id = ?', [produtor_id],function (res) {
                  console.log('galpao delete', res);
                  resolve();
                }, err =>{
                  exceptionProvider.insert(err, 'produtores.ts','delete','delete from galpoes where produtor_id = ?', [produtor_id]);
                  console.log('erro ao remover galpao', err);
                  reject();
                })
              }, err =>{
                exceptionProvider.insert(err, 'produtores.ts','delete','delete from propriedades where produtor_id = ?', [produtor_id]);
                console.log('erro ao remover propriedade', err);
                reject();
              })
            }, err =>{
              exceptionProvider.insert(err, 'produtores.ts','delete','delete from produtores where id = ?', [produtor_id]);
              console.log('erro ao remover produtor', err);
              reject();
            })
          })
        });
    });
    return promise;
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaProdutores(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/produtores`).subscribe((results: any) => {
            let loading = this.messageService.showLoading('Atualizando Produtores...');
            if (results) {
              for (let result of results) {
                let sql_up = 'update produtores SET estado_id=?, municipio_id=?, nome=?, matricula=?, logradouro=?, complemento=?, telefone=?, celular=?, email=?, foto=?, latitude=?, longitude=?, ativo=?, deleted_at=? where id_web = ?;';
                let data_up = [result.estado_id, result.municipio_id, result.nome, result.matricula, result.logradouro, result.complemento, result.telefone, result.celular, result.email, result.foto_base_64, result.latitude, result.longitude, result.ativo, result.deleted_at, result.id];
                db.executeSql(sql_up, data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO produtores (id, id_web, estado_id, municipio_id, cpf, nome, matricula, logradouro, complemento, telefone, celular, email, foto, latitude, longitude, ativo, deleted_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                    let data_ins = [result.id, result.id, result.estado_id, result.municipio_id, result.cpf, result.nome, result.matricula, result.logradouro, result.complemento, result.telefone, result.celular, result.email, result.foto_base_64, result.latitude, result.longitude, result.ativo, result.deleted_at];
                    db.executeSql(sql_ins, data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'produtores.ts', 'atualizaProdutores',sql_ins, data_ins);
                      reject();
                    })
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'produtores.ts', 'atualizaProdutores',sql_up, data_up);
                  reject();
                })
              }
            }
            loading.dismiss();
            resolve();
          }, err=>{
            // this.exceptionProvider.insert(err, 'produtores.ts', 'atualizaProdutores',`${SERVER_URL}/produtores`);
            console.error('Erro ao Abrir Base', err);
            reject();
          });
        });
      });
    });
    return promisse;
  }

  public atualizaProdutorApi(produtor:Produtor, integracao_aves=false){
    var promisse = new Promise((resolve,reject) => {

      this.update(produtor).then(()=>{

        if(this.networkService.isConnected()){

          this.authProvider.checkLogin().then((data: any) => {

            if (data == "success") {
              this.storage.get('SERVER_URL').then((SERVER_URL:any) => {

                this.http.patch(`${SERVER_URL}/produtores/${produtor.id}`,produtor).timeout(10000).subscribe((results: any) => {

                  results.ativo = results.ativo == 1;
                  results.sincronizado = true;
                  results.enviar_foto = false;
                  results.id_web = results.id;
                  results.foto = results.foto_base_64;

                  this.update(results);
                  resolve(true);
                }, (err) => {
                  if(err.name == "TimeoutError"){
                    reject({'name': 'TimeoutError', 'msg':'Timeout ao enviar dados'});
                  }else{
                    reject(false);
                  }
                  // this.exceptionProvider.insert(err, 'produtores.ts', 'atualizaProdutores',`${SERVER_URL}/produtores/${produtor.id}`,produtor);
                  console.error('falha ao enviar dados', err);
                });
              });
            }
          })


        }else {
          resolve(true);
        }
      }).catch((err)=>{
        console.error('falha ao salvar dados do produtor no banco',err);
        reject(false);
      });

      if (integracao_aves){
        this.updatePropriedade(produtor).then((data=>{
          console.log('resultado atualização propriedades',data);
          if (this.networkService.isConnected()){
            this.propriedadeProvider.getAllToSyncPropriedades().then((propriedades:Array<any>)=>{
              console.log('propriedades',propriedades);

              for (let propriedade of propriedades){
                console.log('propriedade',propriedade);
                this.propriedadeProvider.atualizaPropriedadeApi(propriedade).then((data:any)=>{
                  console.log('result api prop',data);
                  this.propriedadeProvider.updateSync(data.id_mobile);
                });
              }

            })
          }

        }))
      }
    });

    return promisse;
  }

  public async sincAllProdutores() {
    let promise = new Promise((resolve, reject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL: any) => {
        this.getAllToSyncProdutores().then((produtores: any[]) => {
          for (let produtor of produtores) {
            this.http.patch(`${SERVER_URL}/produtores/${produtor.id}`, produtor).subscribe((results: any) => {
              results.ativo = results.ativo == 1;
              results.sincronizado = true;
              results.enviar_foto = false;
              results.id_web = results.id;
              results.foto = results.foto_base_64;
              this.update(results);
            }, (err) => {
              // this.exceptionProvider.insert(err, 'produtores.ts', 'sincAllProdutores', `${SERVER_URL}/produtores/${produtor.id}`, produtor);

              console.error('falha ao enviar dados', err);
              reject();
            });
          }
          resolve();
        });
      });
    });
    return promise;
  }

}
