import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {Galpao} from "../../class/Galpao";
import {CheckNetworkProvider} from "../check-network/check-network";
import {Events} from "ionic-angular";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {AuthProvider} from "../auth/auth";
import * as moment from "moment";

@Injectable()
export class GalpoesProvider {
  private urlApi;

  constructor(
    private dbProvider: DatabaseProvider,
    private http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private networkService: CheckNetworkProvider,
    private events:Events,
    private fileTransfer:FileTransfer,
    private authProvider: AuthProvider,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiGet(propriedade_id) {
    return this.http.get<any>(`${this.urlApi}/galpoes?propriedade_id=${propriedade_id}`);
  }

  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}/galpoes?last_update=${lastUpdate}`);
  }

  public apiGetTiposGalpoesAll(lastUpdate){
    return this.http.get(`${this.urlApi}/tipos/galpoes?last_update=${lastUpdate}`);
  }

  public apiUpdate(galpao){
    return this.http.patch(`${this.urlApi}/galpoes/${galpao.id}`, galpao);
  }

  public apiStoreBkp(value){
    return this.http.post(`${this.urlApi}/galpoes/upload/bkp`, value);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  /*Galpão*/
  public getGalpoesLocal(propriedade_id = null, considera_aplicao_chk = true){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT id, descricao, aplica_chk_lote FROM galpoes WHERE (deleted_at == "null" OR deleted_at IS NULL) ';

      if(considera_aplicao_chk){
        sql += 'AND (aplica_chk = 1 OR aplica_chk IS NULL)';
      }

      let params: any[] = [];

      if(propriedade_id){
        sql += ' and propriedade_id = ?';
        params.push(propriedade_id);
      }

      sql += ' order by descricao';

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let galpoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var galpao = data.rows.item(i);
            galpoes.push(galpao);
          }
          return galpoes;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'galpoes.ts', 'getGalpoesLocal',sql, params);
        console.error('Falha ao executar sql galpoes', err)
      });
    }).catch((e) => console.error('Falha ao listar galpoes', e));
  }

  public getAll(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT  ' +
        ' COALESCE((SELECT foto FROM galpoes_fotos where gp.id = galpao_id AND principal == "true" AND deleted_at IS NULL), "assets/imgs/alert_image.png") AS foto, ' +
        ' gp.id, ' +
        ' gp.descricao, ' +
        ' prod.nome AS produtor, ' +
        ' prop.descricao AS propriedade, ' +
        ' muni.nome AS municipio, ' +
        ' muni.uf ' +
        'FROM galpoes AS gp ' +
        ' LEFT JOIN propriedades AS prop ' +
        '  ON gp.propriedade_id = prop.id ' +
        ' LEFT JOIN produtores AS prod ' +
        '  ON gp.produtor_id = prod.id ' +
        ' LEFT JOIN municipios AS muni ' +
        '  ON prop.municipio_id = muni.id '+
        'WHERE (gp.deleted_at == "null" OR gp.deleted_at IS NULL)';

      return db.executeSql(sql, []).then((data:any)=>{
        if (data.rows.length > 0){
          let galpoes: any[] = [];
          for (let i = 0; i < data.rows.length; i++){
            galpoes.push(data.rows.item(i));
          }

          return galpoes;
        } else{
          return [];
        }
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'getAll',sql);
        console.error('falha ao consultar todos os galpoes', err);
      });
    });
  }

  public get(id:number){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT  ' +
        ' prod.nome AS produtor, ' +
        ' prop.descricao AS propriedade, ' +
        ' gp.*  ' +
        'FROM galpoes AS gp ' +
        ' LEFT JOIN propriedades AS prop ' +
        '  ON gp.propriedade_id = prop.id ' +
        ' LEFT JOIN produtores AS prod ' +
        '  ON gp.produtor_id = prod.id ' +
        'WHERE gp.id = ? AND (gp.deleted_at == "null" OR gp.deleted_at IS NULL)';
      let data = [id];

      return db.executeSql(sql, data).then((data:any)=>{
        if(data.rows.length > 0){
          let galpao = new Galpao();
          let item = data.rows.item(0);

          galpao.id = item.id;
          galpao.produtor_id = item.produtor_id;
          galpao.propriedade_id = item.propriedade_id;
          galpao.tipo_comedouro_id = item.tipo_comedouro_id;
          galpao.tipo_galpao_id = item.tipo_galpao_id;
          galpao.descricao = item.descricao;
          galpao.lotacao = item.lotacao;
          galpao.baias = item.baias;
          galpao.altura = item.altura;
          galpao.largura = item.largura;
          galpao.comprimento = item.comprimento;
          galpao.metros_quadrados = item.metros_quadrados;
          galpao.observacao = item.observacao;
          galpao.latitude = item.latitude;
          galpao.longitude = item.longitude;
          galpao.sincronizado = item.sincronizado;
          galpao.ativo = item.ativo;
          galpao.produtor = item.produtor;
          galpao.codErp = item.codErp;
          galpao.propriedade = item.propriedade;

          return galpao;
        }
        return null
      }).catch((err) =>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'get',sql,data);
        console.error('Falha ao buscar galão', err);
      });
    });
  }

  public getAllToSyncGalpoes(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM galpoes WHERE sincronizado = ?';
      let data = [false];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let galpoes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              galpoes.push(data.rows.item(i));
            }
            return galpoes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'galpoes.ts', 'getAllToSyncGalpoes',sql,data);
          console.error('Falha ao consultar galpões para sincronizar', err)
        });
    });
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM galpoes';

      return db.executeSql(sql, [])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let galpoes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              galpoes.push(data.rows.item(i));
            }
            return galpoes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'galpoes.ts', 'getAllBkp',sql);
          console.error('Falha ao consultar galpões para sincronizar', err)
        });
    });
  }

  public insert(galpao:Galpao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into galpoes (id, produtor_id, propriedade_id, tipo_comedouro_id, tipo_galpao_id, descricao, lotacao, baias, altura, largura, comprimento, metros_quadrados, observacao, latitude, longitude, aplica_chk, aplica_chk_lote, coderp, ativo, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
      let data = [
        galpao.id,
        galpao.produtor_id,
        galpao.propriedade_id,
        galpao.tipo_comedouro_id,
        galpao.tipo_galpao_id,
        galpao.descricao,
        galpao.lotacao,
        galpao.baias,
        galpao.altura,
        galpao.largura,
        galpao.comprimento,
        galpao.metros_quadrados,
        galpao.observacao,
        galpao.latitude,
        galpao.longitude,
        galpao.aplica_chk,
        galpao.aplica_chk_lote,
        galpao.codErp,
        galpao.ativo,
        galpao.deleted_at,
      ];

      return db.executeSql(sql, data).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'insert',sql,data);
        console.error('Falha ao inserir galpão', err, sql, data);
      });
    })
  }

  public update(galpao:Galpao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update galpoes SET produtor_id=?, propriedade_id=?, tipo_comedouro_id=?, tipo_galpao_id=?, descricao=?, lotacao=?, baias=?, altura=?, largura=?, comprimento=?, metros_quadrados=?, observacao=?, latitude=?, longitude=?, aplica_chk=?, aplica_chk_lote=?, ativo=?, sincronizado=?, coderp=?, deleted_at=? where id = ?;';
      let data = [
        galpao.produtor_id,
        galpao.propriedade_id,
        galpao.tipo_comedouro_id,
        galpao.tipo_galpao_id,
        galpao.descricao,
        galpao.lotacao,
        galpao.baias,
        galpao.altura,
        galpao.largura,
        galpao.comprimento,
        galpao.metros_quadrados,
        galpao.observacao,
        galpao.latitude,
        galpao.longitude,
        galpao.aplica_chk,
        galpao.aplica_chk_lote,
        galpao.ativo,
        galpao.sincronizado,
        galpao.codErp,
        galpao.deleted_at,
        galpao.id
      ];

      return db.executeSql(sql, data).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'update',sql,data);
        console.error('Falha ao atualizar galpão', err, sql, data);
      });
    });
  }

  /*Fotos*/
  public getFotosGalpao(galpao_id:number){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT foto AS img, id, galpao_id, principal FROM galpoes_fotos WHERE galpao_id = ? AND deleted_at is null';
      let data = [galpao_id];

      return db.executeSql(sql, data).then((data:any)=>{
        if (data.rows.length > 0) {
          let fotos: any[] = [];
          for (let i = 0; i < data.rows.length; i++){
            fotos.push(data.rows.item(i));
          }
          return fotos;
        }else{
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'galpoes.ts', 'getFotosGalpao', sql, data);
        console.error('falha ao consultar fotos dos galpoes', err);
      })
    })
  }

  public getFotosSincGalpao(galpao_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id, galpao_id, foto AS img, mobile_key, deleted_at FROM galpoes_fotos WHERE galpao_id = ? AND sincronizado = ?';
      let data = [galpao_id, false];

      return db.executeSql(sql, data).then((data:any)=>{
        if (data.rows.length > 0) {
          let fotos: any[] = [];
          for (let i = 0; i < data.rows.length; i++){
            fotos.push(data.rows.item(i));
          }
          return fotos;
        }else{
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'getFotosSincGalpoes', sql, data);
        console.error('falha ao consultar fotos dos galpoes', err);
      })
    })
  }

  public getAllFotosToSyncGalpoes(){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM galpoes_fotos WHERE sincronizado = ? OR sincronizado = 0';
      let data = [false];

      return db.executeSql(sql, data).then((data:any) =>{
        if (data.rows.length > 0) {
          let fotos: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            fotos.push(data.rows.item(i));
          }
          return fotos;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'galpoes.ts', 'getAllFotosToSyncGalpoes',sql, data);
        console.error('Falha ao consultar galpoes_fotos para sincronizar', err)
      });
    });
  }

  public insertFoto(foto){
    let sql = 'INSERT INTO galpoes_fotos (galpao_id, mobile_key, foto, sincronizado) VALUES (?,?,?,?)';
    let params = [
      foto.galpao_id,
      foto.mobile_key,
      foto.img,
      false
    ];

    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      return db.executeSql(sql, params).then((data)=>{
        console.log('foto da ação salva - provider', data, sql, params);
        return data;
      }).catch(err=>{
        console.error('falha salvar foto acao - provider', err);
      });
    });
  }

  public updateSyncFoto(id_web, id_mobile, mobile_key){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'UPDATE galpoes_fotos SET sincronizado=?, id_web=? WHERE id=? AND mobile_key = ?';
      let data = [
        true,
        id_web,
        id_mobile,
        mobile_key,
      ];
      return db.executeSql(sql, data)
        .catch((err) => {
          this.exceptionProvider.insert(err, 'galpoes.ts', 'updateSyncFoto',sql, data);
          console.error('Falha ao executar sql galpoes_fotos', err)
        });

    });
  }

  public updateFotoPrincipal(foto_id, galpao_id){
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        let sql_clear = 'UPDATE galpoes_fotos SET principal = ? WHERE galpao_id = ?';
        let data_clear = [false, galpao_id];

        let sql_set_principal = 'UPDATE galpoes_fotos SET principal = ? WHERE id = ?';
        let data_set_principal = [true, foto_id];

        db.executeSql(sql_clear, data_clear).then(()=>{
          db.executeSql(sql_set_principal, data_set_principal).then(()=>{
            resolve()
          }).catch(err=>{
            this.exceptionProvider.insert(err, 'fotos.ts', 'updateFotoPrincipalGalpoes - update', sql_set_principal, data_set_principal);
            console.error('erro ao setar principal',err);
            reject();
          });
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'fotos.ts', 'updateFotoPrincipalGalpoes - clear', sql_clear, data_clear);
          console.error('erro ao remover principais',err);
          reject();
        })
      })
    });

    return promise;

  }

  public deleteFoto(foto_id){
    let promise = new Promise( (resolve, reject)=>{
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        let sql = 'UPDATE galpoes_fotos SET deleted_at = ?, sincronizado = ? WHERE id = ?';
        let data = [moment().format('YYYY-MM-DD'),false,foto_id];
        db.executeSql(sql, data).then(()=>{
          resolve()
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'fotos.ts', 'deleteFotoGalpoes', sql, data);
          console.error('erro ao remover foto',err);
          reject();
        })
      })
    });

    return promise

  }

  public delete(galpao_id){
    let exceptionProvider = this.exceptionProvider;
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          db.transaction(function (fx) {
            fx.executeSql('delete from galpoes where id = ?', [galpao_id],function (res) {
              console.log('galpao delete', res);
              resolve();
            }, err =>{
              exceptionProvider.insert(err, 'galpoes.ts', 'delete','delete from galpoes where id = ?', [galpao_id]);
              console.log('erro ao remover galpao', err);
              reject();
            })
          })
        });
    });
    return promise;
  }

  /*Tipos de Galpão*/
  public getAllTiposGalpoesToSelect(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id, descricao FROM tipo_galpoes';

      return db.executeSql(sql,[]).then((data:any) =>{
        if(data.rows.length > 0){
          let tp_galpoes: any[] = [];
          for (let i = 0; i < data.rows.length; i++){
            tp_galpoes.push(data.rows.item(i));
          }
          return tp_galpoes;
        }else{
          return [];
        }
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'getAllTiposGalpoesToSelect',sql);
        console.error('falha ao consultar tipos galpoes',err, sql)
      })
    })
  }

  public insertTpGalpao(tipoGalpao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into tipo_galpoes (id, descricao) values (?,?);';
      let data = [
        tipoGalpao.id,
        tipoGalpao.descricao,
      ];

      return db.executeSql(sql, data).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'insertTpGalpao',sql,data);
        console.error('Falha ao inserir tipo_galpoes', err, sql, data);
      });
    });
  }

  public updateTpGalpao(tipoGalpao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update tipo_galpoes SET descricao=? where id = ?;';
      let data = [
        tipoGalpao.descricao,
        tipoGalpao.id
      ];

      return db.executeSql(sql, data).catch((err)=>{
        this.exceptionProvider.insert(err, 'galpoes.ts', 'updateTpGalpao',sql,data);
        console.error('Falha ao atualizar tipo_galpoes', err, sql, data);
      });
    });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaGalpoes(url){
    /*Atualiza Ações*/
    let promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/galpoes`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando Galpões...');
          if(results){
            for (let result of results){
              let sql_up = 'update galpoes SET produtor_id=?, propriedade_id=?, tipo_comedouro_id=?, descricao=?, ' +
                'lotacao=?, baias=?, altura=?, largura=?, comprimento=?, metros_quadrados=?, observacao=?, ' +
                'latitude=?, longitude=?, ativo=?, deleted_at=? where id = ?;';
              let data_up = [result.produtor_id, result.propriedade_id, result.tipo_comedouro_id, result.descricao,
                result.lotacao, result.baias, result.altura, result.largura, result.comprimento, result.metros_quadrados,
                result.observacao, result.latitude, result.longitude, result.ativo, result.deleted_at, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO galpoes (id, produtor_id, propriedade_id, tipo_comedouro_id, ' +
                    'descricao, lotacao, baias, altura, largura, comprimento, metros_quadrados, observacao, latitude, ' +
                    'longitude, ativo, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
                  let data_ins = [result.id, result.produtor_id, result.propriedade_id, result.tipo_comedouro_id,
                    result.descricao, result.lotacao, result.baias, result.altura, result.largura, result.comprimento,
                    result.metros_quadrados, result.observacao, result.latitude, result.longitude, result.ativo, result.deleted_at]
                  db.executeSql(sql_ins, data_ins).catch((err) => {
                    this.exceptionProvider.insert(err, 'galpoes.ts', 'atualizaGalpoes', sql_ins, data_ins);
                    console.error('falha ao adicionar galpao', err);
                    reject();
                  })
                }
              }).catch((err) => {
                this.exceptionProvider.insert(err, 'galpoes.ts', 'atualizaGalpoes',sql_up, data_up);
                console.error('falha ao adicionar galpao', err);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        });
      }).catch(e => {
        console.error('Erro ao Abrir Base', e);
        reject();
      });
    });
    return promisse;
  }

  public atualizaGalpaoApi(galpao:Galpao){
    let promisse = new Promise((resolve, reject)=>{
      this.update(galpao).then(()=>{
        resolve(true);
        if(this.networkService.isConnected()){
          this.authProvider.checkLogin().then((data: any) => {

            if (data == "success") {
              this.storage.get('SERVER_URL').then((SERVER_URL:string)=>{
                this.http.patch(`${SERVER_URL}/galpoes/${galpao.id}`, galpao).subscribe((result:any)=>{
                  result.ativo = result.ativo ? 1:0;
                  result.sincronizado = true;

                  this.update(result);
                  this.enviaFotos(galpao.id);
                });
              });
            }
          })

        }else{
          resolve(true);
        }
      }).catch((err)=>{
        console.error('falha ao atualizar produtor na base de dados', err);
        reject(false);
      });
    });

    return promisse;
  }

  public async sincAllGalpoes() {
    let promise = new Promise((resolve, reject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL: string) => {
        this.getAllToSyncGalpoes().then((galpoes: any[]) => {
          for(let galpao of galpoes){
            this.http.patch(`${SERVER_URL}/galpoes/${galpao.id}`, galpao).subscribe((result: any) => {
              result.ativo = result.ativo ? 1 : 0;
              result.sincronizado = true;

              this.update(result);
            }, (err) => {
              // this.exceptionProvider.insert(err, 'galpoes.ts', 'sincAllGalpoes',`${SERVER_URL}/galpoes/${galpao.id}`, galpao);
              console.error('falha ao atualiza galpao na API', err);
              reject(false);
            });
          }
          resolve();
        });
      });
    });
    return promise;
  }

  public atualizaTipoGalpoes(){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${this.urlApi}/tipos/galpoes`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Galpões...');
          if (results){
            for (let result of results){
              let sql_up = 'update tipo_galpoes SET descricao=? where id = ?;';
              let data_up = [result.descricao, result.id];
              db.executeSql(sql_up, data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO tipo_galpoes (id, descricao) values (?, ?)';
                  let data_ins = [result.id, result.descricao];
                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'galpoes.ts', 'atualizaGalpoes', sql_ins, data_ins);
                  });
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'galpoes.ts', 'atualizaGalpoes',sql_up, data_up);
                console.error('falha ao exectar sql ao inserir tipos de galpões', err);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar tipos de galpões', err);
          reject();
        })
      })
    });

    return promisse;
  }

  public enviaFotos(galao_id){
      this.getFotosSincGalpao(galao_id).then((fotos:Array<any>)=>{
        for(let foto of fotos){
          this.storage.get('access_token').then((access_token) => {
            let options: FileUploadOptions = {
              fileKey: 'foto',
              fileName: `galpao_${galao_id}_${foto.id}_img.jpg`,
              chunkedMode: false,
              httpMethod: 'POST',
              mimeType: "image/jpeg",
              params: foto,
              headers: {
                'Authorization': `Bearer ${access_token}`,
              }
            };

            let fileTransfer = this.fileTransfer.create();
            let url = `${this.urlApi}/galpoes/${foto.galpao_id}/fotos`;

            fileTransfer.upload(foto.img, url, options).then((retorno_api: any)=>{
              let data_json = JSON.parse(retorno_api.response);
              this.updateSyncFoto(data_json.id, data_json.id_mobile, data_json.mobile_key);
            }).catch(err=>{
              this.exceptionProvider.insert(err, 'galpoes.ts', 'enviaFoto',url, foto);
            })
          });
        }
      });
  }

}
