import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {SERVERS} from "../../config";

// We MUST import both the firebase AND firestore modules like so
import * as firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';
import {Storage} from "@ionic/storage";
import {UtilsService} from "../../services/utils.service";
import {Base64} from "@ionic-native/base64";
import {Device} from "@ionic-native/device";
import {AppVersion} from "@ionic-native/app-version";
import * as moment from "moment";

@Injectable()
export class FirestoreDatabaseProvider {
  protected now = moment().format('YYYY-MM-DD');
  /**
   * @name _DB
   * @type {object}
   * @private
   * @description Defines an object for handling interfacing with the Cloud Firestore database service
   */
  private _DB: any;

  /**
   * @name _COLL
   * @type {string}
   * @private
   * @description  Defines the name of the database collection
   */
  private _COLL: string = "";

  constructor(
    private http: HttpClient,
    private readonly storage: Storage,
    private utils: UtilsService,
    private base64: Base64,
    private device: Device,
    private appVersion: AppVersion,
  ) {

    this.storage.get('COD_EMPRESA').then((COD_EMPRESA: any) => {
      if (COD_EMPRESA == null) {
        this.storage.get('SERVER_URL').then(SERVER_URL => {
          let server = SERVERS.find(server => server.url === SERVER_URL);
          this.storage.set('COD_EMPRESA', server.codigo);
          this._COLL = `PA_${server.codigo}_NEW`;
        });
      } else {
        this._COLL = `PA_${COD_EMPRESA}_NEW`;
      }
    });

    firebase.firestore().enablePersistence();

    this._DB = firebase.firestore();
  }

  /**
   * Add a new document to a selected database collection
   *
   * @public
   * @method addDocument
   * @param  docObj           {Any}              The key/value object we want to add
   * @return {Promise}
   */
  private addDocument(dataObj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get('COD_EMPRESA').then((cod_empresa) => {

        if (cod_empresa.substring(0, 2) == 'LC') {
          this.getTokenFb('tecnico@example.com', '123456').then((user: any) => {
            dataObj.userUid = user.uid;
            this._DB.collection(this._COLL)
              .doc(user.uid)
              .collection(dataObj.type)
              .add(dataObj)
              .then((obj: any) => {
                resolve(obj);
              })
              .catch((error: any) => {
                reject(error);
              });
          });
        } else {

          this.storage.get('login').then((data) => {

            this.getTokenFb(data.email, data.password).then((user: any) => {
              dataObj.userUid = user.uid;
              this._DB.collection(this._COLL)
                .doc(user.uid)
                .collection(dataObj.type)
                .add(dataObj)
                .then((obj: any) => {
                  resolve(obj);
                })
                .catch((error: any) => {
                  reject(error);
                });
            });
          });
        }
      });

    });
  }

  /**
   * Adiciona nova ação
   *
   * @public
   * @method addAcao
   * @param  acaoObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addAcao(acaoObj: any) {
    acaoObj.type = "acao";
    if (acaoObj.com_foto) {

      this.base64.encodeFile(acaoObj.foto).then((base64File: string) => {

        acaoObj.foto = base64File;
        this.addDocument(acaoObj);
      }, (err) => {
        console.error(err);
      });
    } else {
      this.addDocument(acaoObj);
    }
  }

  /**
   * Adiciona recontagem de plantel
   *
   * @public
   * @method addRecontagem
   * @param  acaoObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addRecontagem(recontagemObj: any) {
    recontagemObj.type = "recontagemPlanteeis";
  	this.addDocument(recontagemObj);
  }

  /**
   * Adiciona nova Avaliacao Inicial (Parâmetro Avaliado)
   *
   * @public
   * @method addAvaliacaoInicial
   * @param  aviObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addAvaliacaoInicial(aviObj: any) {
    aviObj.type = "avaliacao_inicial";
    if (aviObj.com_foto) {

      this.base64.encodeFile(aviObj.foto).then((base64File: string) => {

        aviObj.foto = base64File;
        this.addDocument(aviObj).catch(err => console.error(err));
      }, (err) => {
        console.error(err);
      });
    } else {
      this.addDocument(aviObj).catch(err => console.error(err));
    }
  }

  /**
   * Adiciona nova Baixa da Ação
   *
   * @public
   * @method addBaixaAcao
   * @param  bxAcaoObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addBaixaAcao(bxAcaoObj: any) {
    bxAcaoObj.type = "baixa_acao";
    if (bxAcaoObj.com_foto) {

      this.base64.encodeFile(bxAcaoObj.foto).then((base64File: string) => {

        bxAcaoObj.foto = base64File;
        this.addDocument(bxAcaoObj);
      }, (err) => {
        console.error(err);
      });
    } else {
      this.addDocument(bxAcaoObj);
    }
  }

  /**
   * Adiciona Renovação da Ação
   *
   * @public
   * @method addRenovacaoAcao
   * @param  rnAcaoObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addRenovacaoAcao(rnAcaoObj: any) {
    rnAcaoObj.type = "renovacao_acao";
    if (rnAcaoObj.com_foto) {

      this.base64.encodeFile(rnAcaoObj.foto).then((base64File: string) => {

        rnAcaoObj.foto = base64File;
        this.addDocument(rnAcaoObj);
      }, (err) => {
        console.error(err);
      });
    } else {
      this.addDocument(rnAcaoObj);
    }
  }

  /**
   * Adiciona nova Avaliação do checklist
   *
   * @public
   * @method addAvalicaoChecklist
   * @param  avCheckObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addAvalicaoChecklist(avCheckObj: any, respostasObj: Array<any>) {
    avCheckObj.type = "avaliacao_checklist";

    this.addDocument(avCheckObj);

    for (let resposta of respostasObj) {
      if (resposta.com_foto == 'true' || resposta.com_foto == 1) {
        this.base64.encodeFile(resposta.foto).then((base64File: string) => {
          resposta.foto = base64File;
          resposta.type = "resposta_avaliacao_checklist";
          this.addDocument(resposta);

        }, (err) => {
          console.error(err);
        });
      } else {
        resposta.type = "resposta_avaliacao_checklist";
        this.addDocument(resposta);
      }
    }
  }

  /**
   * Adiciona Erros Applicação
   *
   * @public
   * @method addExceptionApp
   * @param  exAppObj {Any} The key/value object we want to add
   * @return {VoidFunction}
   */
  addExceptionApp(error: any, local: String, funcao: String, sql = null, data = null) {
    let exAppObj: any = {
      'type': "exception_app",
      'error': error,
      'local': local,
      'funcao': funcao,
      'sql': sql,
      'parametros': data,
    };


    this.appVersion.getVersionNumber().then((versionNumber) => {
      exAppObj.data = this.now;
      exAppObj.versao = versionNumber;
      exAppObj.mobile_info = `${this.device.model} - ${this.device.version}`;

      if (exAppObj.com_foto) {

        this.base64.encodeFile(exAppObj.foto).then((base64File: string) => {

          exAppObj.foto = base64File;
          this.addDocument(exAppObj);
        }, (err) => {
          console.error(err);
        });
      } else {
        this.addDocument(exAppObj);
      }
    });
  }

  private getTokenFb(email: string, password: string) {

    let promise = new Promise((resolve) => {
      let user = firebase.auth().currentUser;
      if (user == null) {
        firebase.auth().signInWithEmailAndPassword(email, password).then((response) => {
          resolve(response);
        }).catch((err) => {
          console.error('Falha no login fb', err);
          firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((response: any) => {
              resolve(response);
            })
            .catch((error: any) => console.error(error));
        })
      } else {
        resolve(user);
      }
    });

    return promise;
  }

}
