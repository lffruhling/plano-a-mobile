import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DateTime, Events} from "ionic-angular";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import * as moment from "moment";
import {DbExceptionProvider} from "../db-exception/db-exception";

/* Classe do Técnico */
export class Tecnico {
    id: number;
    name: string;
    email: string;
    foto: string;
    cpf: string;
    celular: string;
    password: string;
    token_fcm: string;
    ultima_sincronizacao: DateTime;
    forcar_atualizacao: any;
    respostas_automaticas_chk: any;
}

@Injectable()
export class TecnicoProvider {

  protected urlApi;

  constructor(
      public dbProvider: DatabaseProvider,
      private readonly http: HttpClient,
      public events: Events,
      private storage: Storage,
      private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * #################################################
  * API Connection
  * #################################################
  * */
  public apiUpdate(values) {
    let promisse  = new Promise((resolve, reject) =>{
        if(values){
          this.http.patch(`${this.urlApi}/tecnico/${values.id}`, values).timeout(10000).subscribe((success) =>{
            resolve(success)
          }, (err)=>{
            console.error('falha att tecnico', err);
            if(err.name == "TimeoutError") {
              reject({'name': 'TimeoutError', 'msg': 'Timeout ao enviar a foto'});
            }else{
              reject(err);
            }
          });
        }else{
          resolve(null)
        }

    });
    return promisse;
  }

  public enviaDados(dados){
    return this.http.patch(`${this.urlApi}/tecnico/${dados.id}`, dados);
  }

  public getDadosTecnicoApi(){
    return this.http.get(`${this.urlApi}/me`);
  }

  /*
  * #################################################
  * Database
  * #################################################
  * */
  public insert(tecnico: Tecnico){
    return this.dbProvider.getDB().then((db: SQLiteObject) =>{
      let sql = 'insert into tecnicos (id, name, email, foto, cpf, celular, forcar_atualizacao) values (?,?,?,?,?,?)';
      let data = [tecnico.id, tecnico.name, tecnico.email, tecnico.foto, tecnico.cpf, tecnico.celular, tecnico.forcar_atualizacao];

      return db.executeSql(sql, data).then(()=>{
        this.events.publish('tecnico:changed', tecnico);
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'tecnico provider', 'insert', sql, data);
        console.error('erro ao executar sql do técnico', err);
      })
    });
  }

  public update(tecnico: Tecnico){

    return this.dbProvider.getDB().then((db: SQLiteObject) =>{
      let sql = 'update tecnicos set name = ?, email = ?, foto = ?, cpf = ?, celular = ?, forcar_atualizacao = ?, respostas_automaticas_chk = ? where id = ?';
      let data = [tecnico.name, tecnico.email, tecnico.foto, tecnico.cpf, tecnico.celular, tecnico.forcar_atualizacao, tecnico.respostas_automaticas_chk, tecnico.id];

      db.executeSql(sql, data).then(()=>{
        this.events.publish('tecnico:changed', tecnico)
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'tecnico provider', 'update', sql, data);
        console.error('erro ao executar sql do update do técnico', err);
      })
    }).catch((err) => {
      // this.exceptionProvider.insert(err, 'tecnico provider', 'update');
        console.error('erro ao atualizar do técnico', err);
    })
  }

  public updateLastSync(now){
    return this.dbProvider.getDB().then((db: SQLiteObject) =>{
      let sql = 'update tecnicos set ultima_sincronizacao = ?';
      let data = [now];

      return db.executeSql(sql, data).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnico provider', 'updateLastSync', sql, data);
        console.error('falha atualizar tecnico ultima sinc', err, sql, data);
      })
    }).catch((err) => {
      // this.exceptionProvider.insert(err, 'tecnico provider', 'updateLastSync');
      console.error('erro ao atualizar do técnico ultima sinc', err);
    })
  }

  public getLastSync(){
    return this.dbProvider.getDB().then((db: SQLiteObject) =>{
      let sql = 'SELECT ultima_sincronizacao FROM tecnicos WHERE forcar_atualizacao != 1';
      let data = [];

      return db.executeSql(sql, data).then((data)=>{
        if(data.rows.item(0)){
          if(data.rows.item(0).ultima_sincronizacao){
            return data.rows.item(0).ultima_sincronizacao.toString().replace(" ", '_');
          }else{
            return null;
          }
        }else{
          return null;
        }
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnico provider', 'getLastSync', sql, data);
        console.error('falha atualizar tecnico ultima sinc', err, sql, data)
      })
    }).catch((err) => {
      // this.exceptionProvider.insert(err, 'tecnico provider', 'getLastSync');
      console.error('erro ao atualizar do técnico ultima sinc', err);
    })
  }

  public saveTokenFCM(token:string){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'UPDATE tecnicos SET token_fcm  = ?';
      let data= [token];
      return db.executeSql(sql,data).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnicos.ts', 'saveTokenFCM', sql, data);
        console.error('falha ao inserir tecnico token_fcm',err, sql, data)
      })
    })
  }

  public getTokenExists(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT token_fcm FROM tecnicos LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).token_fcm;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnicos.ts', 'getTokenExists', sql, null);
        console.error('falha ao consultar dados do tecnico token_fcm',err, sql)
      })
    })
  }

  public remove(id: number) {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'delete from tecnicos where id = ?';
              let data = [id];

              return db.executeSql(sql, data).catch((err) => {
                this.exceptionProvider.insert(err, 'tecnico provider', 'remove', sql, data);
                console.error('erro ao executar sql do remover do técnico', err);
              })
      }).catch((err) => {
          // this.exceptionProvider.insert(err, 'tecnico provider', 'remove');
          console.error('erro ao remover do técnico', err);
      })
  }

  public get() {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'select * from tecnicos limit 1';
              // let data = [id];

              return db.executeSql(sql, [])
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          let item = data.rows.item(0);
                          let tecnico = new Tecnico();
                          tecnico.id = item.id;
                          tecnico.name = item.name;
                          tecnico.email = item.email;
                          tecnico.password = item.password;
                          tecnico.foto = item.foto;
                          tecnico.cpf = item.cpf;
                          tecnico.celular = item.celular;
                          tecnico.forcar_atualizacao = item.forcar_atualizacao;
                          tecnico.respostas_automaticas_chk = item.respostas_automaticas_chk;
                          tecnico.token_fcm = item.token_fcm;
                          return tecnico;
                      }
                      return null;
                  }).catch((err) => {
                    this.exceptionProvider.insert(err, 'tecnico provider', 'get', sql, null);
                    console.error(err);
                  });
          }).catch((err) => {
            // this.exceptionProvider.insert(err, 'tecnico provider', 'get');
            console.error(err)
          });
  }

  public checkLogin(email, senha) {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'select id from tecnicos where email = ? and password = ? limit 1';
              let data = [email, senha];
              return db.executeSql(sql, data)
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          return true;
                      }

                      return false;
                  }).catch((err) => {
                    this.exceptionProvider.insert(err, 'tecnico provider', 'checkLogin', sql, data);
                    console.error(err)
                  });
          }).catch((err) => {
            // this.exceptionProvider.insert(err, 'tecnico provider', 'checkLogin');
            console.error(err)
          });
  }

  public atualizaDadosTecnicoDB(dados){
    let promisse = new Promise((resolve)=>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        let sql_up = 'update tecnicos SET name=?, email=?, foto=?, cpf=?, celular=?, forcar_atualizacao=? where id = ?;';
        let data_up =[dados.name, dados.email, dados.foto_base_64, dados.cpf, dados.celular, dados.forcar_atualizacao, dados.id];

        db.executeSql(sql_up,data_up).then((data:any) =>{
          if(data.rowsAffected < 1){
            let sql_ins = 'INSERT OR IGNORE INTO tecnicos (id, name, email, foto, cpf, celular, forcar_atualizacao) values (?,?,?,?,?,?,?)';
            let data_ins = [dados.id, dados.name, dados.email, dados.foto_base_64, dados.cpf, dados.celular, dados.forcar_atualizacao];
            db.executeSql(sql_ins, data_ins).then(()=>{
              resolve();
            }).catch((err)=>{
              console.error('tecnico error', err);
              this.exceptionProvider.insert(err, 'tecnico provider', 'atualizaDadosTecnicoDB ins', sql_ins, JSON.stringify(data_ins));
            })
          }else{
            resolve();
          }
        }).catch((err)=>{
          this.exceptionProvider.insert(err, 'tecnico provider', 'atualizaDadosTecnicoDB up', sql_up, JSON.stringify(data_up));
          console.error('falha ao exectar sql ao inserir dados do tecnico', err)
        });
      });
    });

    let tecnico = {
      id: dados.id,
      name: dados.name,
      foto: dados.foto_base_64,
      celular: dados.celular,
      email: dados.email,
    };

    this.events.publish('tecnico:changed', tecnico);

    return promisse;
  }

  public getParamForceUpdate(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT forcar_atualizacao FROM tecnicos LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).forcar_atualizacao == 1;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnico.ts', 'getParamForceUpdate', sql, null);
        console.error('falha ao consultar dados do tecnico forcar_atualizacao',err, sql)
      })
    })
  }

  public getParamRespostaAutoChk(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT respostas_automaticas_chk FROM tecnicos LIMIT 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        return data.rows.item(0).respostas_automaticas_chk == "true";
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'tecnico.ts', 'getParamRespostaAutoChk', sql, null);
        console.error('falha ao consultar dados do tecnico respostas_automaticas_chk',err, sql)
      })
    })
  }

}