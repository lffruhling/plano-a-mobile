import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {TipoInformacao} from "../../class/TipoInformacao";
import {HttpClient} from "@angular/common/http";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class TipoInformacaoAcaoProvider extends TipoInformacao{
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/informacoes/acoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/informacoes/acoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/informacoes/acoes`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(tpInformacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'INSERT INTO tipo_informacoes_acoes (id, descricao, tipo_campo) values (?, ?, ?)';
      let data = [
        tpInformacao.id,
        tpInformacao.descricao,
        tpInformacao.tipo_campo
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'tipoinformacaoacao.ts', 'insert',sql, data);
        console.log('Falha ao inserir tipo_informacoes_acoes', err, sql, data)
      })
    })
  }

  public update(tpInformacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update tipo_informacoes_acoes SET descricao=?, tipo_campo=? where id = ?;';
      let data = [
        tpInformacao.descricao,
        tpInformacao.tipo_campo,
        tpInformacao.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'tipoinformacaoacao.ts', 'update',sql, data);
        console.log('Falha ao atualizar tipo_informacoes_acoes', err, sql, data)
      })
    })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaTiposInformacaoAcao(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/informacoes/acoes`).subscribe((results:any) => {
            let loading = this.showLoading('Atualizando Informações das Ações...');
            if(results) {
              for (let result of results) {
                let sql_up = 'update tipo_informacoes_acoes SET descricao=?, tipo_campo=? where id = ?;';
                let data_up = [result.descricao, result.tipo_campo, result.id];
                db.executeSql(sql_up,data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO tipo_informacoes_acoes (descricao, tipo_campo, id) values (?, ?, ?)';
                    let data_ins = [result.descricao, result.tipo_campo, result.id];
                    db.executeSql(sql_ins,data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'tipoinformacaoacao.ts', 'update',sql_ins, data_ins);
                      reject();
                    });
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'tipoinformacaoacao.ts', 'update',sql_up, data_up);
                  reject();
                })
              }
            }
            loading.dismiss();
            resolve();
          });
        });

      })
    });
    return promisse;
  }

  showLoading(msg:string){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: msg
    });

    loading.present();

    return loading;
  }

}
