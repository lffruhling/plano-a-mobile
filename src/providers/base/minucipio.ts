import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Municipio} from "../../class/Municipio";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class MunicipioProvider extends Municipio{

    constructor(public dbProvider: DatabaseProvider,
                private exceptionProvider:DbExceptionProvider,
    ) {
        super();
    }

    public insert(municipio: Municipio){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'insert into municipios (id, codigo, nome, uf) values (?,?,?,?)';
                let data = [municipio.id, municipio.codigo, municipio.nome, municipio.uf];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'muncipio.ts', 'insert', sql, data);
                      console.error('Falha ao executar sql municipio', err)
                    });
            });
    }

    public getAll(nome: string = null){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) =>{
                let sql = 'select * from municipios order by nome';
                var data: any[] = [];

                // filtrando pelo nome
                if (nome) {
                    sql += ' where nome like ?'
                    data.push('%' + nome + '%');
                }

                return db.executeSql(sql,data)
                    .then((data:any)=>{
                        if(data.rows.length > 0){
                            let municipios : any[] = [];
                            for (var i =0; i < data.rows.length; i++){
                                var municipio = data.rows.item(i);
                                municipios.push(municipio);
                            }
                            return municipios;
                        }else{
                            return []
                        }
                    }).catch((err) => {
                    this.exceptionProvider.insert(err, 'muncipio.ts', 'getAll', sql, data);
                    console.error('Falha ao executar sql municipio', err)
                  });
            });
    }

}
