import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Estado} from "../../class/Estado";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class EstadoProvider extends Estado{

  constructor(public dbProvider: DatabaseProvider,
              private exceptionProvider:DbExceptionProvider) {
    super();
  }

  public insert(estado: Estado){
    return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'insert into estados (id, codigo, nome, uf, regiao) values (?,?,?,?,?)';
          let data = [estado.id, estado.codigo, estado.nome, estado.uf, estado.regiao];

          return db.executeSql(sql, data)
              .catch((err) => {
                this.exceptionProvider.insert(err, 'estado.ts', 'insert', sql, data);
                console.error('Falha ao executar sql estado', err)
              });

        });
  }

  public getAll(nome: string = null){
    return this.dbProvider.getDB()
        .then((db: SQLiteObject) =>{
          let sql = 'select * from estados order by nome';
          var data: any[] =[];

            // filtrando pelo nome
            if (nome) {
                sql += ' where nome like ?'
                data.push('%' + nome + '%');
            }

            return db.executeSql(sql,data)
                .then((data:any)=>{
                  if(data.rows.length > 0){
                    let estados : any[] = [];
                    for (var i =0; i < data.rows.length; i++){
                      var estado = data.rows.item(i);
                      estados.push(estado);
                    }
                    return estados;
                  }else{
                    return []
                  }
                }).catch((err) => {
                this.exceptionProvider.insert(err, 'estado.ts', 'getAll', sql, data);
                console.error('Falha ao executar sql estado', err)
              });
        });
  }

}
