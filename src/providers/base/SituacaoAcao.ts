import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {SituacaoAcao} from "../../class/SituacaoAcao";
import {HttpClient} from "@angular/common/http";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class SituacaoAcaoProvider extends SituacaoAcao{
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/situacoes/acoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/situacoes/acoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/situacoes/acoes`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(situacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'INSERT INTO situacao_acoes (id, descricao) values (?,?)';
      let data = [
        situacao.id,
        situacao.descricao,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'situacaoAcao.ts', 'insert', sql, data);
        console.log('Falha ao inserir situacao_acoes', err, sql, data)
      });
    });
  }

  public update(situacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update situacao_acoes SET descricao=? where id = ?;';
      let data = [
        situacao.descricao,
        situacao.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'situacaoAcao.ts', 'update', sql, data);
        console.log('Falha ao atualizar situacao_acoes', err, sql, data)
      })
    })
  }


  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaSituacaoAcao(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/situacoes/acoes`).subscribe((results:any) => {
            let loading = this.showLoading('Atualizando Informações das Ações...');
            if(results) {
              for (let result of results) {
                let sql_up = 'update situacao_acoes SET descricao=? where id = ?;';
                let data_up = [result.descricao, result.id];
                db.executeSql(sql_up,data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO situacao_acoes (descricao, id) values (?, ?, ?)';
                    let data_ins = [result.descricao, result.id];
                    db.executeSql(sql_ins,data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'situacaoAcao.ts', 'update', sql_ins, data_ins);
                      reject();
                    });
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'situacaoAcao.ts', 'update', sql_up, data_up);
                  reject();
                });
              }
            }
            loading.dismiss();
            resolve();
          });
        });

      });
    });
    return promisse;
  }

  showLoading(msg:string){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: msg
    });

    loading.present();

    return loading;
  }

}
