import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {TipoIntegracao} from "../../class/TipoIntegracao";
import {HttpClient} from "@angular/common/http";
import {Events} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class TipoIntegracaoProvider extends TipoIntegracao{
  private urlApi;

  constructor(
    private dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,

  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/integracoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/integracoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/integracoes`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  public insert(tipo_integracao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into tipo_integracoes (id, descricao, ativo) values (?,?,?)';
      let data = [
        tipo_integracao.id,
        tipo_integracao.descricao,
        tipo_integracao.ativo,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'tipointegracao.ts', 'insert',sql, data);
        console.log('Falha ao inserir tipo_integracao', err, sql, data)
      })
    })
  }

  public update(tipo_integracao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update tipo_integracoes set descricao=?, ativo=? where id =?';
      let data = [
        tipo_integracao.descricao,
        tipo_integracao.ativo,
        tipo_integracao.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'tipointegracao.ts', 'update',sql, data);
        console.log('Falha ao atualizar tipo_integracao', err, sql, data)
      })
    })
  }

  public getAllToSelect(){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT id, descricao FROM tipo_integracoes WHERE ativo = 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let tipo_integracoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                tipo_integracoes.push(data.rows.item(i));
              }
              return tipo_integracoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'tipointegracao.ts', 'getAllToSelect',sql, null);
            console.error('Falha ao executar sql tipo_integracoes', err)
          });
      });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaTiposIntegracoes(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/integracoes`).subscribe((results:any) => {
            let loading = this.messageService.showLoading('Atualizando Tipos de Integrações...');
            if(results) {
              for (let result of results) {
                let sql_up = 'update tipo_integracoes SET descricao=?, ativo=? where id = ?;';
                let data_up = [result.descricao, result.ativo, result.id];
                db.executeSql(sql_up,data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO tipo_integracoes (descricao, ativo, id) values (?, ?, ?)';
                    let data_ins = [result.descricao, result.ativo, result.id];
                    db.executeSql(sql_ins,data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'tipointegracao.ts', 'atualizaTiposIntegracoes',sql_ins,data_ins);
                      reject();
                    })
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'tipointegracao.ts', 'atualizaTiposIntegracoes',sql_up,data_up);
                  reject();

                })
              }
            }
            loading.dismiss();
            resolve();
          });
        });
      });
    });
    return promisse;
  }
}
