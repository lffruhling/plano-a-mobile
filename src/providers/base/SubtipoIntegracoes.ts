import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {SubtipoIntegracao} from "../../class/SubtipoIntegracao";
import {HttpClient} from "@angular/common/http";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class SubtipoIntegracoesProvider extends SubtipoIntegracao{
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/subintegracoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/subintegracoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/subintegracoes`;
      }
    });
  }
  /*
    * ############################
    * API - Métodos
    * ###############
    * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(subtipo_integracao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into subtipo_integracoes (id, tipo_integracao_id, descricao, animais_metros_quadrados_transporte, ativo) values (?,?,?,?,?)';
      let data = [
        subtipo_integracao.id,
        subtipo_integracao.tipo_integracao_id,
        subtipo_integracao.descricao,
        subtipo_integracao.animais_metros_quadrados_transporte,
        subtipo_integracao.ativo,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'subtipointegracoes.ts', 'insert',sql, data);
        console.log('Falha ao inserir subtipo_integracao', err, sql, data)
      })
    })
  }

  public update(subtipo_integracao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update subtipo_integracoes set tipo_integracao_id=?, descricao=?, animais_metros_quadrados_transporte=?, ativo=? where id =?';
      let data = [
        subtipo_integracao.tipo_integracao_id,
        subtipo_integracao.descricao,
        subtipo_integracao.animais_metros_quadrados_transporte,
        subtipo_integracao.ativo,
        subtipo_integracao.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'subtipointegracoes.ts', 'update',sql, data);
        console.log('Falha ao atualizar subtipo_integracao', err, sql, data)
      })
    })
  }

  public getAllToSelect(tipo_integracao_id:number){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT id, descricao FROM subtipo_integracoes where tipo_integracao_id = ? AND ativo = 1';
        let data = [tipo_integracao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let subtipo_integracoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                subtipo_integracoes.push(data.rows.item(i));
              }
              return subtipo_integracoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'subtipointegracoes.ts', 'getAllToSelect',sql, data);
            console.error('Falha ao executar sql subtipo_integracoes', err)
          });
      });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaSubTiposIntegracoes(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/subintegracoes`).subscribe((results:any) => {
            let loading = this.showLoading('Atualizando SubTipos de Integrações...');
            if(results) {
              for (let result of results) {
                let sql_up = 'update subtipo_integracoes SET descricao=?, tipo_integracao_id =?, ativo=? where id = ?;';
                let data_up = [result.descricao, result.tipo_integracao_id, result.ativo, result.id];
                db.executeSql(sql_up,data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO subtipo_integracoes (descricao,  tipo_integracao_id =?, ativo, id) values (?, ?, ?)';
                    let data_ins = [result.descricao, result.tipo_integracao_id, result.ativo, result.id];
                    db.executeSql(sql_ins, data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'subtipointegracoes.ts', 'atualizaSubTiposIntegracoes',sql_ins, data_ins);
                      reject();
                    });
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'subtipointegracoes.ts', 'atualizaSubTiposIntegracoes',sql_up, data_up);
                  reject();
                });
              }
            }
            loading.dismiss();
            resolve();
          });
        });

      });
    });
    return promisse;
  }

  showLoading(msg:string){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: msg
    });

    loading.present();

    return loading;
  }

}
