import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Integradora} from "../../class/Integradora";
import {HttpClient} from "@angular/common/http";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {Produtor} from "../../class/Produtor";

@Injectable()
export class IntegradoraProvider extends Integradora{
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private events: Events,
    private exceptionProvider:DbExceptionProvider
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/integradoras`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/integradoras`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/integradoras`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(integradora){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'INSERT INTO integradoras (id, nome, ativo, deleted_at) values (?, ?, ?, ?)';
      let data = [
        integradora.id,
        integradora.nome,
        integradora.ativo,
        integradora.deleted_at
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'integradora.ts', 'insert', sql, data);
        console.log('Falha ao inserir integradora', err, sql, data)
      });
    });
  }

  public update(integradora){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update integradoras SET nome=?, ativo=?, deleted_at=? where id = ?;';
      let data = [
        integradora.nome,
        integradora.ativo,
        integradora.deleted_at,
        integradora.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'integradora.ts', 'update', sql, data);
        console.log('Falha ao atualizar integradora', err, sql, data)
      })
    })
  }

  public getAllToSelect(){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT id, nome FROM integradoras WHERE deleted_at is null or  deleted_at == ""';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let tipo_integracoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                tipo_integracoes.push(data.rows.item(i));
              }
              return tipo_integracoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'integradora.ts', 'getAllToSelect',sql, null);
            console.error('Falha ao executar sql integradora', err)
          });
      });
  }

  public get(integradora_id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT id, nome FROM integradoras WHERE id = ?';
        let data = [integradora_id];
        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            } else {
              return null;
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'integradora.ts', 'get',sql, data);
            console.error('Falha ao executar sql integradora', err)
          });
      });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaIntegradoras(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/integradoras`).subscribe((results:any) => {
            let loading = this.showLoading('Atualizando Integradoras...');
            if(results) {

              for (let result of results) {
                let sql_up = 'update integradoras SET nome=?, ativo=?, deleted_at=? where id = ?;';
                let data_up = [result.nome, result.ativo, result.deleted_at, result.id];
                db.executeSql(sql_up,data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT INTO integradoras (id, nome, ativo, deleted_at) values (?, ?, ?, ?)';
                    let data_ins = [result.id, result.nome, result.ativo, result.deleted_at];
                    db.executeSql(sql_ins, data_ins).catch(err=>{
                      this.exceptionProvider.insert(err, 'integradora.ts', 'atualizaIntegradoras', sql_ins, data_ins);
                      reject();
                    })
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err, 'integradora.ts', 'atualizaIntegradoras', sql_up, data_up);
                  reject();
                })
              }
            }
            loading.dismiss();
            resolve();
          });
        });
      });
    });
    return promisse;
  }

  showLoading(msg:string){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: msg
    });

    loading.present();

    return loading;
  }

}
