import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class ComedourosProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/comedouros/tipos`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/comedouros/tipos`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/comedouros/tipos`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(comedouro){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into tipo_comedouros (id, descricao, ativo) values (?,?,?)';
      let data = [
        comedouro.id,
        comedouro.descricao,
        comedouro.ativo,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'comedouros.ts', 'insert', sql, data);
        console.log('Falha ao inserir comedouro', err, sql, data)
      })
    })
  }

  public update(comedouro){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update tipo_comedouros set descricao=?, ativo=? where id =?';
      let data = [
        comedouro.descricao,
        comedouro.ativo,
        comedouro.id,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'comedouros.ts', 'update', sql, data);
        console.log('Falha ao atualizar comedouro', err, sql, data)
      })
    })
  }

  public getAllToSelect(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT id, descricao FROM tipo_comedouros WHERE ativo = 1';

      return db.executeSql(sql,[]).then((data:any) =>{
        if(data.rows.length > 0){
          let comedouros: any[] = [];
          for (let i = 0; i < data.rows.length; i++){
            comedouros.push(data.rows.item(i));
          }
          return comedouros;
        }else{
          return [];
        }
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'comedouros.ts', 'getAllToSelect', sql, null);
        console.error('falha ao consultar comedouros',err, sql)
      })
    })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaTipoComedouros(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/comedouros/tipos`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Comedouros...');
          if (results){
            for (let result of results){
              let sql_up = 'update tipo_comedouros SET descricao=?, ativo=? where id = ?;';
              let data_up = [result.descricao, result.ativo, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO tipo_comedouros (id, descricao, ativo) values (?, ?, ?)';
                  let data_ins = [result.id, result.descricao, result.ativo];
                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'comedouros.ts', 'atualizaTipoComedouros', sql_ins,data_ins);
                    reject();
                  })
                }
              }).catch((err)=>{
                console.error('falha ao exectar sql ao inserir tipos de comedoros', err);
                this.exceptionProvider.insert(err, 'comedouros.ts', 'atualizaTipoComedouros', sql_up,data_up);
                reject();
              })
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar tipos de comedoros', err);
          // this.exceptionProvider.insert(err, 'comedouros.ts', 'atualizaTipoComedouros');
          reject();
        })
      })
    });

    return promisse;
  }

}
