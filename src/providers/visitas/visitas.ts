import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {SQLiteObject} from "@ionic-native/sqlite";

@Injectable()
export class VisitasProvider {
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    public http: HttpClient,
    private readonly messageService: AlertMessagesProvider,
    private storage: Storage,
    private events: Events,
    private exceptionProvider: DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/visitas/lotes`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/visitas/lotes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}/visitas/lotes`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetVisitasComPerguntas(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiGetAllTiposCampos(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/tipos/campos?last_update=${lastUpdate}`);
  }

  public apiGetLacamentosComRespostas(lastUpdate) {
    return this.http.get<any>(`${this.urlApi}/lancamentos?last_update=${lastUpdate}`);
  }

  public apiStore(lancamento) {
    // console.log(`${this.urlApi}/lancamentos/${lancamento.id}`, lancamento);
    return this.http.patch(`${this.urlApi}/lancamentos/${lancamento.id}`, lancamento);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  public getByDate(initialDate, finalDate){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = 'SELECT vtl.id,\n' +
        '\tvtl.lote_id,\n' +
        '\tvl.checklist_id,\n' +
        '\tprod.id AS propriedade_id,\n' +
        '\tgp.id AS galpao_id,\n' +
        '\tgp.descricao AS galpao,\n' +
        '\tgp.aplica_chk AS aplica_chk_gp,\n' +
        '\t("Lote - " || lt.nro_lote) AS lote,\n' +
        '\t"bookmarks" AS icon, \n' +
        '\t4 AS type,\n' +
        '\t"Visita Programada" as title,\n' +
        '\t(prod.nome || " - " || prop.descricao) AS location,\n' +
        '\tvl.descricao AS message,\n' +
        '\tvtl.data_previsao_visita AS date_limit\n' +
        '\tFROM visitas_lotes_lancamentos AS vtl\n' +
        '\t\tLEFT JOIN visitas_lotes AS vl\n' +
        '\t\t\tON vtl.visita_id = vl.id\n' +
        '\t\tLEFT JOIN lotes AS lt\n' +
        '\t\t\tON vtl.lote_id = lt.id\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON lt.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON lt.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\tWHERE vtl.data_previsao_visita BETWEEN ? AND ? \n' +
        '\t\tAND data_visita IS NULL\n' +
        '\t\tAND EXISTS (SELECT id FROM lotes WHERE id = vtl.lote_id ' +
        '\t\t\t\t\t\t\t\t\t\tAND (deleted_at IS NULL OR deleted_at == "null"))\n' +
        '\t\tAND (vtl.deleted_at IS NULL OR vtl.deleted_at == "null")\n';

      params = [initialDate, finalDate];

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var checklist = data.rows.item(i);
            checklists.push(checklist);
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'getByDate', sql, params);
        console.error('Falha ao executar sql consulta de visitas para a agenda', err, sql, params)
      });
    });
  }

  public getPerguntas(visita_id){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = '\tSELECT DISTINCT(perg.id) AS dist,\n' +
        '\t\tperg.*, \n' +
        '\t\tresp.resposta,\n' +
        '\t\ttp.tipo AS tipo_campo \n' +
        '\tFROM visitas_lotes_perguntas AS perg \n' +
        '\t\tLEFT JOIN tipos_campos AS tp\n' +
        '\t\t\tON perg.tipo_campo_id = tp.id\n' +
        '\t\tLEFT JOIN visitas_lotes_respostas AS resp\n' +
        '\t\t\tON (perg.id = resp.pergunta_id AND resp.lancamento_visita_id = ? AND resp.deleted_at IS NULL)\n' +
        '\tWHERE visita_id = (SELECT visita_id FROM visitas_lotes_lancamentos WHERE id = ?)';

      params = [visita_id,visita_id];

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let perguntas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var pergunta = data.rows.item(i);
            perguntas.push(pergunta);
          }
          return perguntas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'getPerguntas', sql, params);
        console.error('Falha ao executar sql consultar perguntas', err, sql, params)
      });
    });
  }

  public getSexoLotes(){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = 'SELECT id, descricao FROM lotes_sexos WHERE ativo = 1';

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let sexos: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            sexos.push(data.rows.item(i));
          }
          return sexos;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'getPerguntas', sql, params);
        console.error('Falha ao executar sql consultar perguntas', err, sql, params)
      });
    });
  }

  public getVisitas(){

    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = '\tSELECT vll.id,\n' +
        '\t\tvll.lote_id,\n' +
        '\t\tlt.propriedade_id,\n' +
        '\t\tlt.galpao_id,\n' +
        '\t\tvl.checklist_id,\n' +
        '\t\tvll.data_previsao_visita,\n' +
        '\t\tvll.sincronizado,\n' +
        '\t\tvll.data_visita,\n' +
        '\t\tvl.descricao,\n' +
        '\t\t(prod.nome || " - " || prop.descricao) AS produtor,\n' +
        '\t\tgp.descricao AS galpao,\n' +
        '\t\tgp.aplica_chk AS aplica_chk_gp,\n' +
        '\t\tlt.nro_lote AS lote\n' +
        '\tFROM visitas_lotes_lancamentos AS vll\n' +
        '\t\tLEFT JOIN visitas_lotes AS vl\n' +
        '\t\t\tON vl.id = vll.visita_id\n' +
        '\t\tLEFT JOIN lotes AS lt\n' +
        '\t\t\tON lt.id = vll.lote_id\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON gp.id = lt.galpao_id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON prop.id  = lt.propriedade_id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prod.id = prop.produtor_id\n' +
        '\tWHERE vll.deleted_at IS NULL\n' +
        '\tORDER BY data_visita DESC';

      return db.executeSql(sql, []).then( (data:any) =>{
        if (data.rows.length > 0) {
          let visitas: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            visitas.push(data.rows.item(i));
          }
          return visitas;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'getVisitas',sql,null);
        console.error('Falha ao executar sql getVisitas', err, sql)
      });
    });
  }

  public insertTipoCampo(campo) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'REPLACE INTO tipos_campos (id, descricao, tipo) values (?,?,?)';
      let data = [
        campo.id,
        campo.descricao,
        campo.tipo,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insert', sql, data);
        console.error('Falha ao inserir tipo campo', err, sql, data)
      })
    })
  }

  public insertVisitaLote(visita) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'INSERT INTO visitas_lotes (id, checklist_id, dias, descricao, deleted_at) values (?,?,?,?,?)';
      let data = [
        visita.id,
        visita.checklist_id,
        visita.dias,
        visita.descricao,
        visita.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insertVisitaLote', sql, data);
        console.error('Falha ao inserir insertVisitaLote', err, sql, data)
      })
    })
  }

  public updateVisitaLote(visita) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE visitas_lotes SET checklist_id=?, dias=?, descricao=?, deleted_at=? where id=?';
      let data = [
        visita.checklist_id,
        visita.dias,
        visita.descricao,
        visita.deleted_at,
        visita.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'updateVisitaLote', sql, data);
        console.error('Falha ao inserir updateVisitaLote', err, sql, data)
      })
    })
  }

  public insertPergunta(pergunta) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'INSERT INTO visitas_lotes_perguntas (id, visita_id, tipo_campo_id, descricao, opcao_campo, tabela_de_atualizacao, campo_de_atualizacao, deleted_at) values (?,?,?,?,?,?,?,?)';
      let data = [
        pergunta.id,
        pergunta.visita_id,
        pergunta.tipo_campo_id,
        pergunta.descricao,
        pergunta.opcao_campo,
        pergunta.tabela_de_atualizacao,
        pergunta.campo_de_atualizacao,
        pergunta.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insertPergunta', sql, data);
        console.error('Falha ao inserir insertPergunta', err, sql, data)
      })
    })
  }

  public updatePergunta(pergunta) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE visitas_lotes_perguntas SET visita_id=?, tipo_campo_id=?, descricao=?, opcao_campo=?, tabela_de_atualizacao=?, campo_de_atualizacao=?, deleted_at=? where id=?';
      let data = [
        pergunta.visita_id,
        pergunta.tipo_campo_id,
        pergunta.descricao,
        pergunta.opcao_campo,
        pergunta.tabela_de_atualizacao,
        pergunta.campo_de_atualizacao,
        pergunta.deleted_at,
        pergunta.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'updatePergunta', sql, data);
        console.error('Falha ao inserir updatePergunta', err, sql, data)
      })
    })
  }

  public insertVisitaLancamento(lancamento) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'INSERT INTO visitas_lotes_lancamentos (id, visita_id, lote_id, data_previsao_visita, data_visita, latitude, longitude, sincronizado, deleted_at) values (?,?,?,?,?,?,?,?,?)';
      let data = [
        lancamento.id,
        lancamento.visita_id,
        lancamento.lote_id,
        lancamento.data_previsao_visita,
        lancamento.data_visita,
        lancamento.latitude,
        lancamento.longitude,
        null,
        lancamento.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insertVisitaLancamento', sql, data);
        console.error('Falha ao inserir insertVisitaLancamento', err, sql, data)
      })
    })
  }

  public updateVisitaLancamento(lancamento) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE visitas_lotes_lancamentos SET visita_id=?, lote_id=?, data_previsao_visita=?, data_visita=?, latitude=?, longitude=?, deleted_at=? where id=?';
      let data = [
        lancamento.visita_id,
        lancamento.lote_id,
        lancamento.data_previsao_visita,
        lancamento.data_visita,
        lancamento.latitude,
        lancamento.longitude,
        lancamento.deleted_at,
        lancamento.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'updateVisitaLancamento', sql, data);
        console.error('Falha ao inserir updateVisitaLancamento', err, sql, data)
      })
    })
  }

  public storeLancamentoVisita(resposta){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE visitas_lotes_lancamentos SET data_visita=?, latitude=?, longitude=?, sincronizado=? where id=?';
      let data = [
        resposta.data_visita,
        resposta.latitude,
        resposta.longitude,
        false,
        resposta.id,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'storeLancamentoVisita', sql, data);
        console.error('Falha ao atualiza lancamento visita', err, sql, data)
      })
    })
  }

  public updateSinc(id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update visitas_lotes_lancamentos set sincronizado=? where id=?';
        let data = [
          true,
          id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'visitas.ts', 'updateSinc',sql,data);
          console.error('Falha ao executar sql lancamento visitas updateSync', err)
        });
      });
  }

  public insertResposta(resposta) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'INSERT INTO visitas_lotes_respostas (id, id_web, mobile_key, lancamento_visita_id, pergunta_id, resposta, deleted_at) values (?,?,?,?,?,?,?)';
      let data = [
        resposta.id,
        resposta.id_web,
        resposta.mobile_key,
        resposta.lancamento_visita_id,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.deleted_at,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insertResposta', sql, data);
        console.error('Falha ao inserir insertResposta', err, sql, data)
      })
    })
  }

  public storeResposta(resposta){
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'INSERT INTO visitas_lotes_respostas (mobile_key, lancamento_visita_id, pergunta_id, resposta) values (?,?,?,?)';
      let data = [
        resposta.mobile_key,
        resposta.lancamento_visita_id,
        resposta.pergunta_id,
        resposta.resposta,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'storeResposta', sql, data);
        console.error('Falha ao inserir storeResposta', err, sql, data)
      })
    })
  }

  public updateResposta(resposta) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE visitas_lotes_respostas SET id_web=?, lancamento_visita_id=?, pergunta_id=?, resposta=?, deleted_at=? where mobile_key=?';
      let data = [
        resposta.id_web,
        resposta.lancamento_visita_id,
        resposta.pergunta_id,
        resposta.resposta,
        resposta.deleted_at,
        resposta.mobile_key,
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'updateResposta', sql, data);
        console.error('Falha ao inserir updateResposta', err, sql, data)
      })
    })
  }

  public updateSyncRespostas(id_local, id_web){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update visitas_lotes_respostas set id_web=? where id=?';
        let data = [
          id_web,
          id_local
        ];

        return db.executeSql(sql, data).then((data)=>{
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'visitas.ts', 'updateSyncRespostas',sql,data);
          console.error('Falha ao executar sql updateSyncRespostas', err)
        });
      });
  }

  public updateDinamyc(tabela, campo, valor, id) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = `UPDATE ${tabela} set ${campo}=?, sincronizado=?  where id = ?`;
      let data = [
        valor,
        false,
        id
      ];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'visitas.ts', 'insertDinamyc', sql, data);
        console.error('Falha ao inserir insertDinamyc', err, sql, data)
      })
    })
  }

  public getAllLancamentosToSinc() {
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM visitas_lotes_lancamentos WHERE sincronizado = ? OR sincronizado = 0';
      let params = [false];

      return db.executeSql(sql, params)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let lancamentos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              lancamentos.push(data.rows.item(i));
            }
            return lancamentos;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'visitas.ts', 'getAllToSinc',sql, params);
          console.error('Falha ao consultar lancamentos de visitas para sincronizar', err)
        });
    });
  }

  public getAllRespostasLancamentoToSync(lancamento_visita_id) {
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM visitas_lotes_respostas WHERE lancamento_visita_id = ? AND id_web IS NULL';
      let data = [lancamento_visita_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let visitasRespostas: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              visitasRespostas.push(data.rows.item(i));
            }
            return visitasRespostas;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'visitas.ts', 'getAllRespostasLancamentoToSync',sql, data);
          console.error('Falha ao consultar visitasRespostas para sincronizar', err)
        });
    });
  }

}
