import { Injectable } from '@angular/core';
import {Network} from "@ionic-native/network";
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/timeout';
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";

@Injectable()
export class CheckNetworkProvider {
  private urlApi;

  constructor(private networkService: Network,
              private http: HttpClient,
              private storage: Storage,
              private events:Events,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  isConnected(){
    return (this.networkService.type !=  'unknown' && this.networkService.type !=  'none');
  }

  speed(){
    let promise = new Promise((resolve,reject) => {
      this.http.get(this.urlApi).timeout(2500).subscribe(response=>{
        resolve(response);
      }, err=>{
        reject(err);
      })

    });

    return promise;
  }

}
