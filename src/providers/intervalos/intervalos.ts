import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class IntervalosProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/intervalos`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/intervalos`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/intervalos`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(intervalo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'insert into intervalos (id, dias, descricao, ativo, somente_plantel, somente_checklist, deleted_at) values (?,?,?,?,?,?,?)';
      let data = [
        intervalo.id,
        intervalo.dias,
        intervalo.descricao,
        intervalo.ativo,
        intervalo.somente_plantel,
        intervalo.somente_checklist,
        intervalo.deleted_at,
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'intervalos.ts', 'insert', sql, data);
        console.log('Falha ao inserir intervalos', err, sql, data)
      })
    })
  }

  public update(intervalo){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update intervalos SET dias=?, descricao=?, ativo=?, somente_plantel=?, somente_checklist=?, deleted_at=? where id = ?;';
      let data = [
        intervalo.dias,
        intervalo.descricao,
        intervalo.ativo,
        intervalo.somente_plantel,
        intervalo.somente_checklist,
        intervalo.deleted_at,
        intervalo.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'intervalos.ts', 'update', sql, data);
        console.log('Falha ao atualizar intervalos', err, sql, data)
      })
    })
  }

  public getDiasIntervalo(id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT dias FROM intervalos WHERE id = ?';
      let data = [id];
      return db.executeSql(sql,data).then((data:any) =>{
        return data.rows.item(0).dias;
      }).catch((err)=>{
        this.exceptionProvider.insert(err, 'intervalos.ts', 'getDiasIntervalo', sql, data);
        console.error('falha ao consultar dados de intervalo dias',err, sql, data)
      })
    })
  }

  public getDiasIntervaloAplicaChk(){
	  return this.dbProvider.getDB().then((db:SQLiteObject) =>{
		  let sql = 'SELECT dias FROM intervalos WHERE deleted_at IS NULL AND somente_checklist = 1  ORDER BY dias DESC LIMIT 1';
		  let data = [];
		  return db.executeSql(sql,data).then((data:any) =>{
			  return data.rows.item(0).dias;
		  }).catch((err)=>{
			  this.exceptionProvider.insert(err, 'intervalos.ts', 'getDiasIntervalo', sql, data);
			  console.error('falha ao consultar dados de intervalo dias',err, sql, data)
		  })
	  })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaIntervalos(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/intervalos`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Intervalos de Aplicações...');
          if (results){
            for (let result of results){
              let sql_up = 'update intervalos SET dias=?, descricao=?, ativo=?, deleted_at=? where id = ?;';
              let data_up = [result.dias, result.descricao, result.ativo, result.deleted_at, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins ='INSERT OR IGNORE INTO intervalos (id, dias, descricao, ativo, deleted_at) values (?,?,?,?,?)';
                  let data_ins = [result.id, result.dias, result.descricao, result.ativo, result.deleted_at];
                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'intervalos.ts', 'getDiasIntervalo', sql_ins,data_ins);

                  })
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'intervalos.ts', 'getDiasIntervalo', sql_up,data_up);
                console.error('falha ao exectar sql ao inserir intervalos', err)
              })
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar intervalos', err);
          reject();
        })
      })
    });

    return promisse;
  }

}
