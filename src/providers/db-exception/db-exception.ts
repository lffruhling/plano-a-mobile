import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {DatabaseProvider} from "../database/database";
import {SQLiteObject} from "@ionic-native/sqlite";
import {AppVersion} from "@ionic-native/app-version";
import * as moment from "moment";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";

@Injectable()
export class DbExceptionProvider {
  protected now = moment().format('YYYY-MM-DD');
  private urlApi;

  constructor(
    private http: HttpClient,
    private dbProvider: DatabaseProvider,
    private appVersion: AppVersion,
    private storage: Storage,
    private events: Events,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/importar/erros`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/importar/erros`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/importar/erros`;
      }
    });
  }

  public storeApi(erro){
    return this.http.post(this.urlApi, erro);
  }

  public apiStoreBkp(values){
    return this.http.post(`${this.urlApi}/upload/bkp`, values);
  }

  public insert(error, local, funcao, sql_data = null, dados = null){
    if(dados){
      dados = JSON.stringify(dados)
    }

    this.appVersion.getVersionNumber().then((versionNumber) => {
      this.dbProvider.getDBException().then((db:SQLiteObject)=>{
        let sql = 'insert into exceptions (version_app, crash, local, funcao, sql, dados, created_at, sincronizado) values (?,?,?,?,?,?,?,?)';
        let data = [
          versionNumber,
          JSON.stringify(error),
          local,
          funcao,
          sql_data,
          dados,
          this.now,
          false
        ];

        db.executeSql(sql, data).catch(err=>{
          console.error('falah ao inserir', err, sql, data)
        });
      });
    });
  }

  public getAllSync(){
    return this.dbProvider.getDBException().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM exceptions WHERE sincronizado = ?';
      let data = [false];
      return db.executeSql(sql, data).then(data=>{
        if (data.rows.length > 0) {
          let erros: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            erros.push(data.rows.item(i));
          }
          return erros;
        } else {
          return [];
        }
      }).catch(err=>{
        console.error('falha ao consultar errors', err)
      })
    })
  }

  public updateSync(id){
    this.dbProvider.getDBException().then((db:SQLiteObject)=>{
      let sql = 'update exceptions set sincronizado=? where id=?';
      let data = [
        true,
        id
      ];

      db.executeSql(sql, data);
    });
  }

  public deleteAfterSync(){

    this.dbProvider.getDBException().then((db:SQLiteObject) =>{
      let sql = "delete from exceptions where id in (SELECT id from exceptions where created_at < datetime('now','-10 day'))";
      let data = [];
      db.executeSql(sql, data);
    })

  }

  public getAllBkp(){
    return this.dbProvider.getDBException().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM exceptions';

      return db.executeSql(sql, []).then(data=>{
        if (data.rows.length > 0) {
          let erros: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            erros.push(data.rows.item(i));
          }
          return erros;
        } else {
          return [];
        }
      }).catch(err=>{
        console.error('falha ao consultar errors', err)
      })
    })
  }

}
