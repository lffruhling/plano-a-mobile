import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {DatabaseProvider} from "../database/database";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class LiberacaoChkProvider {
  private urlApi;

  constructor(private http: HttpClient,
              private messageService: AlertMessagesProvider,
              private dbProvider: DatabaseProvider,
              private storage: Storage,
              private events:Events,
              private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/liberacoes`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/liberacoes`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/liberacoes`;
      }
    });
  }

  /*
 * ############################
 * API - Métodos
 * ###############
 * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiStore(values) {
    return this.http.post(this.urlApi, values)
  }

  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/upload/bkp`, values)
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(liberacao){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO checklist_libera_vigentes (id_web, periodo_id, justificativa, mobile_key, liberada, data_solicitada) values (?,?,?,?,?,?)';
        let data = [
          liberacao.id_web,
          liberacao.periodo_id,
          liberacao.justificativa,
          liberacao.mobile_key,
          liberacao.liberada,
          liberacao.data_solicitada,
        ];
        return db.executeSql(sql, data).catch((err)=>{
          this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'insert',sql, data);
          console.error('Falha ao inserir checklist_libera_vigentes', err);
        })
      });
  }

  public update(liberacao){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'update checklist_libera_vigentes SET id_web=?, periodo_id=?, justificativa=?, liberada=?, data_solicitada=?, data_liberada=? where mobile_key = ? OR id_web=?;';
      let data = [
        liberacao.periodo_id,
        liberacao.justificativa,
        liberacao.liberada,
        liberacao.data_solicitada,
        liberacao.data_liberada,
        liberacao.mobile_key,
        liberacao.id
      ];

      return db.executeSql(sql,data).catch(err=>{
        this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'update',sql, data);
        console.log('Falha ao atualizar checklist_libera_vigentes', err, sql, data)
      })
    })
  }

  public updateSinc(id_web: number, id_mobile:number, mobile_key:string){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update checklist_libera_vigentes set id_web = ? where id = ? and mobile_key = ?';

        let data = [id_web,id_mobile, mobile_key];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'updateSinc',sql, data);
          console.error('falha ao atualiza solicitação de liberação', err)
        })
      }).catch((err:any)=>{
        console.error('erro ao atualiza solicitação de liberação', err);
      })
  }

  public getLatestId() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select id from checklist_libera_vigentes order by id desc limit 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);

              return item.id;
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'getLatestId',sql);
            console.error('Falha ao executar sql checklist_libera_vigentes', err)
          });
      }).catch((e) => console.error('Falha ao salvar checklist_libera_vigentes', e));
  }

  public getSolicitacoesParaSincronizar(){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM checklist_libera_vigentes where id_web is null';

      return db.executeSql(sql,[]).then((data:any)=>{
        if (data.rows.length > 0) {
          let solicitacoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            solicitacoes.push(data.rows.item(i));
          }
          return solicitacoes;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'getSolicitacoesParaSincronizar',sql);
        console.error('falha ao exectar sql ao inserir liberaçõess', err)

      });
    });
  }

  public getAllBkp(){
    return this.dbProvider.getDB().then((db:SQLiteObject)=>{
      let sql = 'SELECT * FROM checklist_libera_vigentes';
      return db.executeSql(sql,[]).then((data:any)=>{
        if (data.rows.length > 0) {
          let solicitacoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            solicitacoes.push(data.rows.item(i));
          }
          return solicitacoes;
        } else {
          return [];
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'getAllBkp',sql);
        console.error('falha ao exectar sql ao consultar liberaçõess', err)

      });
    })
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaLiberacoes(url){
    let promisse = new Promise((resolve,reject) =>{
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${url}/liberacoes`).subscribe((results:any) =>{
          let loading = this.messageService.showLoading('Atualizando Solicitações de Liberações...');
          if (results){
            for (let result of results){
              let sql_up = 'update checklist_libera_vigentes SET id_web=?, periodo_id=?, justificativa=?, liberada=?, data_solicitada=?, data_liberada=? where mobile_key = ? OR id_web=?;';
              let data_up = [result.id, result.periodo_id, result.justificativa, result.liberada, result.data_solicitada, result.data_liberada, result.mobile_key, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO checklist_libera_vigentes (id_web, periodo_id, justificativa, liberada, data_solicitada, data_liberada) values (?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.periodo_id, result.justificativa, result.liberada, result.data_solicitada, result.data_liberada];
                  db.executeSql(sql_ins,data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'atualizaLiberacoes',sql_ins,data_ins);

                  })
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'liberacao-chk.ts', 'atualizaLiberacoes',sql_up,data_up);
              })
            }
          }
          loading.dismiss();
          resolve();
        }, (err) =>{
          console.error('falha ao consultar liberaçõess', err);
          reject();
        })
      })
    });

    return promisse;
  }

}
