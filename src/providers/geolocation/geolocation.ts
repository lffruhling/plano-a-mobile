import {Injectable} from '@angular/core';
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {DbExceptionProvider} from "../db-exception/db-exception";
import {FirestoreDatabaseProvider} from "../firestore-database/firestore-database";
import {Geolocation} from "@ionic-native/geolocation";
import {EmpresaProvider} from "../empresa/empresa";
import {Observable} from "rxjs";
import {Events, Platform} from "ionic-angular";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {UniqueIdProvider} from "../unique-id/unique-id";
import * as moment from "moment";
import {CheckNetworkProvider} from "../check-network/check-network";
import {AuthProvider} from "../auth/auth";
import {UtilsService} from "../../services/utils.service";
import {Storage} from "@ionic/storage";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class GeolocationProvider {
  private urlApi;

  constructor(
    private diagnostic: Diagnostic,
    private locationAccurace: LocationAccuracy,
    private exceptionProvider: DbExceptionProvider,
    private fbDbProvider: FirestoreDatabaseProvider,
    private geolocation: Geolocation,
    private empresaProvider: EmpresaProvider,
    private platform: Platform,
    private messageService: AlertMessagesProvider,
    private dbProvider: DatabaseProvider,
    private uniqueKeyService: UniqueIdProvider,
    private networkService: CheckNetworkProvider,
    private authProvider: AuthProvider,
    private utils: UtilsService,
    private storage: Storage,
    private events: Events,
    private readonly http: HttpClient,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}`;
      }
    });

    this.empresaProvider.getParamChkObrigaCapturaGeolocalizacao().then(data => {
      if (data) {
        this.diagnostic.isLocationEnabled().then((enabled) => {
          if (!enabled) {
            this.diagnostic.requestLocationAuthorization().then((data) => {
              if (data == 'GRANTED') {
                this.locationAccurace.request(this.locationAccurace.REQUEST_PRIORITY_HIGH_ACCURACY).then((data) => {
                  this.platform.ready().then(() => {
                    this.getGeolocation();
                  });
                }, (reject) => {
                  console.error('Request Reject', reject)
                }).catch((err) => {
                  console.error('Request error', err)
                })
              }
            }).catch(err => {
              this.exceptionProvider.insert(err, 'geolocation.ts', 'this.diagnostic.requestLocationAuthorization');
              this.fbDbProvider.addExceptionApp(err, 'geolocation.ts', 'this.diagnostic.requestLocationAuthorization');
              console.error('falha ao consultar requestLocationAuthorization', err)
            });
          } else {
            this.getGeolocation();
          }
        }).catch(err => {
          this.exceptionProvider.insert(err, 'geolocation.ts', 'this.diagnostic.isLocationEnabled');
          this.fbDbProvider.addExceptionApp(err, 'geolocation.ts', 'this.diagnostic.isLocationEnabled');
          console.error('Falha ao consultar isLocationEnabled', err);
        });
      }
    });

  }

  getGeolocation() {
    let promise = new Promise((resolve, reject) => {
      this.empresaProvider.getParamChkObrigaCapturaGeolocalizacao().then(data => {
        if (data) {
          let options = {
            enableHighAccuracy: true,
            timeout: 60000,
            maximumAge: 0
          };
          this.geolocation.getCurrentPosition(options)
            .then((resp) => {
              resolve(resp);
            }).catch((err) => {
            this.exceptionProvider.insert(err, 'geolocation.ts', 'getGeolocation');
            this.fbDbProvider.addExceptionApp(err, 'geolocation.ts', 'getGeolocation');
            console.error('Falha ao consultar isLocationEnabled', err);
            console.error('Falha ao consultar isLocationEnabled', err.code);
            console.error('Falha ao consultar isLocationEnabled', err.code == 3);
            reject(err);
          });

        } else {
          resolve(null);
        }
      }).catch((err) => {
        console.error('Falha ao consultar parametro de obrigao geolocation empresa', err);
        resolve(null);
      });
    });

    return promise;

  }

  watchGeolocation() {
    let observable = new Observable(observer => {
      let options = {
        enableHighAccuracy: true,
        // timeout: 300000,
        timeout: 300,
      };
      let obsGeolocation = this.geolocation.watchPosition(options).subscribe((result) => {
        observer.next(result);
      }, err => {
        console.error('err watch', err)
      });
      setTimeout(function () {
        obsGeolocation.unsubscribe();
      }, 60000);
    });

    return observable;
  }

  currentLocation() {
    let observable = new Observable(observer => {
      let alertShow = true;
      let diagnostic = this.diagnostic;
      let message = this.messageService;
      let locationAccurace = this.locationAccurace;
      setInterval(function () {
        diagnostic.isLocationEnabled().then((enabled) => {
          observer.next(enabled);
          if (!enabled) {
            if (alertShow) {
              alertShow = false;
              message.showAlert('Gps Desligado', 'Ligue o GPS para continuar a utilização do aplicativo.').then((data) => {
                diagnostic.requestLocationAuthorization().then((data) => {
                  if (data == 'GRANTED') {
                    locationAccurace.request(locationAccurace.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
                      alertShow = true;
                    })
                  }
                })
              })
            }

          }
        }).catch(err => {
          console.error('currentLocation falha ao habilitar gps', err)
        })
      }, 300000);
      /*esse timeout ajusta o tempo de captura das localizações do técnico*/

    });

    return observable;
  }

  saveGeolocation() {
    let now = moment().format('YYYY-MM-DD hh:mm:ss');
    let mobile_key = this.uniqueKeyService.generateKey();
    let options = {
      enableHighAccuracy: true,
      timeout: 30000,
      maximumAge: 0
    };
    this.geolocation.getCurrentPosition(options)
      .then((resp) => {
        this.dbProvider.getDBGeo().then((db: SQLiteObject) => {
          let sql = 'insert into localizacao_tecnicos (mobile_key, latitude, longitude, velocidade, precisao, data_hora) values (?,?,?,?,?,?)';
          let localizacao = [
            mobile_key,
            resp.coords.latitude,
            resp.coords.longitude,
            resp.coords.accuracy,
            resp.coords.speed,
            now
          ];

          return db.executeSql(sql, localizacao).then(() => {
            if (this.networkService.isConnected()) {
              this.authProvider.checkLogin().then((data: any) => {
                if (data == "success") {
                  this.utils.retornaUUID().then((uuid: any) => {
                    let dados = {
                      device_uuid: uuid,
                      mobile_key: mobile_key,
                      latitude: resp.coords.latitude,
                      longitude: resp.coords.longitude,
                      velocidade: resp.coords.speed,
                      data_hora: now
                    };

                    this.http.post(`${this.urlApi}/localizacao`, dados).subscribe((result:any) => {
                      this.deleteLocalizacao(result);
                    })
                  })
                }
              })
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'intervalos.ts', 'insert', sql, localizacao);
            console.error('Falha ao inserir localizacoes', err, sql, localizacao)
          })
        })
      }).catch((err) => {
      this.exceptionProvider.insert(err, 'geolocation.ts', 'getGeolocation');
      this.fbDbProvider.addExceptionApp(err, 'geolocation.ts', 'getGeolocation');

    });

  }

  public getAllSync() {
    return this.dbProvider.getDBGeo().then((db: SQLiteObject) => {
      let sql = 'SELECT * FROM localizacao_tecnicos LIMIT 1000';
      let data = [];
      return db.executeSql(sql, data).then(data => {
        if (data.rows.length > 0) {
          let localizacoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            localizacoes.push(data.rows.item(i));
          }
          return localizacoes;
        } else {
          return [];
        }
      }).catch(err => {
        console.error('falha ao consultar localizacao_tecnicos', err)
      })
    })
  }

  public deleteLocalizacao(mobile_key) {
    this.dbProvider.getDBGeo().then((db: SQLiteObject) => {
      let sql = 'DELETE FROM localizacao_tecnicos WHERE mobile_key =?';
      let param = [
        mobile_key
      ];

      db.executeSql(sql, param).catch(err => {
        console.error('falha ao remover localizaçao', err)
      })
    })
  }

  public enviaLocalizacoesOffline(data){
    this.utils.retornaUUID().then((uuid: any) => {
      let localizacoes = [];
      for (let localizacao of data) {
        localizacoes.push({
          device_uuid: uuid,
          mobile_key: localizacao.mobile_key,
          latitude: localizacao.latitude,
          longitude: localizacao.longitude,
          velocidade: localizacao.velocidade,
          data_hora: localizacao.data_hora
        });
      }

      this.http.post(`${this.urlApi}/localizacoes`, localizacoes).subscribe((result:any[]) => {
        for (let mobile_key of result){
          this.deleteLocalizacao(mobile_key);
        }
      }, err => {
        console.error('falha ao enviar localizações', err);
      });


    })
  }

}
