import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {HttpClient} from "@angular/common/http";
import {AppVersion} from "@ionic-native/app-version";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {UniqueIdProvider} from "../unique-id/unique-id";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import * as moment from "moment";

@Injectable()
export class DatabaseProvider {
  private urlApi;

  constructor(private sqlite: SQLite,
              private http: HttpClient,
              private appVersion: AppVersion,
              private storage: Storage,
              private events: Events,
              private uniqueKeyService: UniqueIdProvider,
              private messagesService: AlertMessagesProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  public exportDB(sql){
    return this.http.post(`${this.urlApi}/bkp/db`, sql);
  }

  /**
   * Cria um banco caso não exista ou pega um banco existente com o nome no parametro
   */
  public getDB() {
    return this.sqlite.create({
      name: 'planoa.db',
      location: 'default'
    });
  }

  /**
   * Cria/Usa base de dados para armazenar crash do app
   */
  public getDBException() {
    return this.sqlite.create({
      name: 'planoa_crash.db',
      location: 'default'
    });
  }

  /**
   * Cria/Usa base de dados para armazenar localizacoes do app
   */
  public getDBGeo() {
    return this.sqlite.create({
      name: 'planoa_geo.db',
      location: 'default'
    });
  }

  /**
   * Cria a estrutura inicial do banco de dados
   */
  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        // Criando as tabelas
        this.createTables(db);

        // Inserindo dados padrão
        // this.insertDefaultItems(db);
      })
      .catch(e => console.error('erro na base aqui', e));
  }

  public createDatabaseCrash(){
    return this.getDBException().then((db:SQLiteObject) =>{
      db.sqlBatch([
        ['CREATE TABLE IF NOT EXISTS exceptions (id integer primary key AUTOINCREMENT NOT NULL, version_app TEXT, crash TEXT, local TEXT, funcao TEXT, sql TEXT, dados TEXT, created_at DATE, sincronizado boolean)']
      ]).then((res)=>{
        // console.log('tabela de crash criada');
      }).catch(err=>console.error('erro ao criar tabela de crash', err));
    });
  }

  public createDatabaseGeo(){
    return this.getDBGeo().then((db:SQLiteObject) =>{
      db.sqlBatch([
        ['CREATE TABLE IF NOT EXISTS localizacao_tecnicos (mobile_key TEXT, latitude double, longitude double, velocidade double, precisao double, data_hora date)'],
      ]).then((res)=>{
        // console.log('tabela de crash criada');
      }).catch(err=>console.error('erro ao criar tabela de crash', err));
    });

  }

  public updateDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS db_version (id integer primary key AUTOINCREMENT NOT NULL, version TEXT)'],
          ['CREATE TABLE IF NOT EXISTS migrations (id integer primary key AUTOINCREMENT NOT NULL, version_app integer, migration TEXT)'],
        ]).catch(e => console.error('Erro ao criar as tabelas', e));

        this.appVersion.getVersionNumber().then((versionNumber) => {
          this.updateTables(db, versionNumber);
        }, (error) => {
          console.error(error, 'Erro ao pegar a versão do app');
        });
      }).catch(e => console.error('erro na base aqui', e));
  }

  /**
   * Criando as tabelas no banco de dados
   * @param db
   */
  private createTables(db: SQLiteObject) {
    db.sqlBatch([
      ['DROP TABLE IF EXISTS localizacao_tecnicos']]).catch(err => {
      console.error('falha ao remover TB localizacao_tecnicos');
    });
    // Criando as tabelas
    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS db_version (id integer primary key AUTOINCREMENT NOT NULL, version TEXT)'],
      ['CREATE TABLE IF NOT EXISTS tecnicos (id integer primary key AUTOINCREMENT NOT NULL, name TEXT, email TEXT, foto TEXT, foto_thumb TEXT, ultima_sincronizacao DATETIME)'],
      ['CREATE TABLE IF NOT EXISTS estados (id integer primary key AUTOINCREMENT  NOT NULL, codigo INTEGER NOT NULL, nome TEXT NOT NULL, uf CHAR(2) NOT NULL, regiao integer NOT NULL )'],
      ['CREATE TABLE IF NOT EXISTS municipios (id integer primary key AUTOINCREMENT NOT NULL, codigo INTEGER NOT NULL, nome TEXT NOT NULL, uf CHAR(2) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS produtores (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, estado_id integer, municipio_id integer, cpf TEXT, nome TEXT, foto TEXT, foto_thumb TEXT, latitude double(18,15), longitude double(18,15), ativo BOOLEAN, FOREIGN KEY(estado_id) REFERENCES estados(id),FOREIGN KEY(municipio_id) REFERENCES municipios(id))'],
      ['CREATE TABLE IF NOT EXISTS integradoras (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS tipo_integracoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS subtipo_integracoes (id integer primary key AUTOINCREMENT NOT NULL, tipo_integracao_id integer, descricao TEXT, animais_metros_quadrados_transporte integer,ativo BOOLEAN, FOREIGN KEY(tipo_integracao_id) REFERENCES tipo_integracoes(id))'],

      ['CREATE TABLE IF NOT EXISTS propriedades (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, produtor_id integer, integradora_id integer, tipo_integracao_id integer, subtipo_integracao_id integer, descricao TEXT, latitude double(18,15), longitude double(18,15), ativo BOOLEAN, FOREIGN KEY(produtor_id) REFERENCES produtores(id), FOREIGN KEY(integradora_id) REFERENCES integradoras(id), FOREIGN KEY(tipo_integracao_id) REFERENCES tipo_integracoes(id), FOREIGN KEY(subtipo_integracao_id) REFERENCES subtipo_integracoes(id))'],
      ['CREATE TABLE IF NOT EXISTS propriedades_fotos (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, propriedade_id integer, mobile_key TEXT, foto TEXT, foto_thumb TEXT, sincronizado BOOLEAN, FOREIGN KEY(propriedade_id) REFERENCES propriedades(id))'],

      ['CREATE TABLE IF NOT EXISTS tipo_informacoes_acoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, tipo_campo TEXT)'],
      ['CREATE TABLE IF NOT EXISTS situacao_acoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT)'],
      ['CREATE TABLE IF NOT EXISTS grupo_acoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS ponto_acoes (id integer primary key AUTOINCREMENT NOT NULL, grupo_acao_id integer, descricao TEXT, FOREIGN KEY(grupo_acao_id) REFERENCES grupo_acoes(id))'],
      ['CREATE TABLE IF NOT EXISTS parametro_pagamento_acoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT,  amplitude_maxima double, amplitude_minima double, valor double, valor_percentual BOOLEAN, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS acao_parametros (id integer primary key AUTOINCREMENT NOT NULL, parametro_pag_id integer, grupo_acao_id integer, ponto_acao_id integer, tipo_integracao_id integer, tipo_info_id integer, informacao_padrao TEXT, descricao TEXT, FOREIGN KEY(parametro_pag_id) REFERENCES parametro_pagamento_acoes(id), FOREIGN KEY(grupo_acao_id) REFERENCES grupo_acoes(id), FOREIGN KEY(ponto_acao_id) REFERENCES ponto_acoes(id), FOREIGN KEY(tipo_integracao_id) REFERENCES tipo_integracoes(id), FOREIGN KEY(tipo_info_id) REFERENCES tipo_informacoes_acoes(id) )'],
      ['CREATE TABLE IF NOT EXISTS avaliacoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, propriedade_id integer, parametro_id integer, tecnico_id integer, data_visita DATE, gerou_acao BOOLEAN, observacoes TEXT, FOREIGN KEY(propriedade_id) REFERENCES propriedades(id), FOREIGN KEY(parametro_id) REFERENCES acao_parametros(id), FOREIGN KEY(tecnico_id) REFERENCES tecnicos(id))'],
      ['CREATE TABLE IF NOT EXISTS acoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer,  propriedade_id integer, tipo_integracao_id integer, grupo_acao_id integer, ponto_acao_id integer, avaliacao_inicial integer, parametro_acao_id integer, situacao_id integer, o_que TEXT, como TEXT, quem TEXT, quando DATE, considerar_claculos BOOLEAN, com_baixa BOOLEAN, FOREIGN KEY(propriedade_id) REFERENCES propriedades(id), FOREIGN KEY(tipo_integracao_id) REFERENCES tipo_integracoes(id), FOREIGN KEY(grupo_acao_id) REFERENCES grupo_acoes(id), FOREIGN KEY(ponto_acao_id) REFERENCES ponto_acoes(id), FOREIGN KEY(avaliacao_inicial) REFERENCES avaliacoes(id), FOREIGN KEY(parametro_acao_id) REFERENCES acao_parametros(id), FOREIGN KEY(situacao_id) REFERENCES situacao_acoes(id) )'],
      ['CREATE TABLE IF NOT EXISTS baixa_acoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, acao_id integer, resolvido BOOLEAN, data DATE, resposta TEXT, FOREIGN KEY(acao_id) REFERENCES acoes(id))'],

      ['CREATE TABLE IF NOT EXISTS galpoes (id integer primary key AUTOINCREMENT NOT NULL, produtor_id integer, propriedade_id integer, descricao TEXT, aplica_chk boolean, aplica_chk_lote boolean, FOREIGN KEY(produtor_id) REFERENCES produtores(id), FOREIGN KEY(propriedade_id) REFERENCES propriedades(id))'],
      ['CREATE TABLE IF NOT EXISTS galpoes_fotos (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, galpao_id integer, mobile_key TEXT, foto TEXT, foto_thumb TEXT, sincronizado BOOLEAN, FOREIGN KEY(galpao_id) REFERENCES galpoes(id))'],

      ['CREATE TABLE IF NOT EXISTS lotes_status (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT)'],
      ['CREATE TABLE IF NOT EXISTS lotes (id integer primary key AUTOINCREMENT NOT NULL, propriedade_id integer, galpao_id integer, lote_status_id integer, nro_lote integer, FOREIGN KEY(propriedade_id) REFERENCES propriedades(id), FOREIGN KEY(galpao_id) REFERENCES galpoes(id), FOREIGN KEY(lote_status_id) REFERENCES lotes_status(id))'],
      ['CREATE TABLE IF NOT EXISTS checklists_fases (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT)'],
      ['CREATE TABLE IF NOT EXISTS checklists (id integer primary key AUTOINCREMENT NOT NULL, subtipo_integracao_id integer, fase_checklist_id integer, descricao TEXT, FOREIGN KEY(subtipo_integracao_id) REFERENCES subtipo_integracoes(id), FOREIGN KEY(fase_checklist_id) REFERENCES checklists_fases(id))'],
      ['CREATE TABLE IF NOT EXISTS checklists_perguntas (id integer primary key AUTOINCREMENT NOT NULL, checklist_id integer, descricao TEXT, valor_min double, valor_max double, data_validade DATE, FOREIGN KEY(checklist_id) REFERENCES checklists(id))'],
      // ['CREATE TABLE IF NOT EXISTS checklists_respostas (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, lote_id integer, checklist_id integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, data_resposta DATE, FOREIGN KEY(checklist_id) REFERENCES checklists(id))'],
      ['CREATE TABLE IF NOT EXISTS checklists_respostas (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, avaliacao_id integer, pergunta_id integer, acao_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, com_acao boolean, data_resposta date, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS lotes_sexos (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS fabricas (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, cnpj TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS checklists_perguntas_acoes (pergunta_id integer, lote_id integer, acao_id integer, sincronizada BOOLEAN, mobile_key TEXT, FOREIGN KEY(pergunta_id) REFERENCES checklists_perguntas(id), FOREIGN KEY(lote_id) REFERENCES lotes(id), FOREIGN KEY(acao_id) REFERENCES acoes(id))'],
      ['CREATE TABLE IF NOT EXISTS tipo_comedouros (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, ativo BOOLEAN)'],

      ['CREATE TABLE IF NOT EXISTS empresa (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, razao_social TEXT, cnpj TEXT, logo_64 TEXT, chk_periodo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS cotacoes (id integer primary key AUTOINCREMENT NOT NULL, tipo_integracao_id integer, valor double, data_inicio date, data_final date, considerar BOOLEAN, vigente BOOLEAN, FOREIGN KEY(tipo_integracao_id) REFERENCES tipo_integracoes(id))'],
      ['CREATE TABLE IF NOT EXISTS intervalos (id integer primary key AUTOINCREMENT NOT NULL, dias integer, descricao TEXT, ativo BOOLEAN)'],
      ['CREATE TABLE IF NOT EXISTS periodo_aplicacoes (id integer primary key AUTOINCREMENT NOT NULL, propriedade_id integer, galpao_id integer, lote_id integer, intervalo_id integer, checklist_id integer, data_inicio date, data_final date, vigente BOOLEAN, FOREIGN KEY(propriedade_id) REFERENCES propriedades(id), FOREIGN KEY(galpao_id) REFERENCES galpoes(id), FOREIGN KEY(lote_id) REFERENCES lotes(id), FOREIGN KEY(intervalo_id) REFERENCES intervalos(id), FOREIGN KEY(checklist_id) REFERENCES checklists(id))'],

      ['CREATE TABLE IF NOT EXISTS checklist_libera_vigentes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, periodo_id integer, justificativa TEXT, liberada boolean, data_solicitada date, data_liberada date, FOREIGN KEY(periodo_id) REFERENCES periodo_aplicacoes(id))'],

      ['CREATE TABLE IF NOT EXISTS versoes (id integer primary key AUTOINCREMENT NOT NULL, versao TEXT, melhorias TEXT, funcionalidades TEXT,  bugs TEXT, data_liberacao date)'],

      // ['CREATE TABLE IF NOT EXISTS checklists_respostas_aux (id integer primary key AUTOINCREMENT NOT NULL, resposta_id integer, lote_id integer, checklist_id integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta DATE, mobile_key TEXT)'],
      ['CREATE TABLE IF NOT EXISTS checklists_respostas_aux (id integer primary key AUTOINCREMENT NOT NULL, mobile_key TEXT, resposta_id integer, avaliacao_id_aux integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta date)'],
      // ['CREATE TABLE IF NOT EXISTS checklists_respostas_assinaturas (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, lote_id integer, periodo_id integer, checklist_id integer, assinatura BLOB, data_assinatura date)'],

      ['CREATE TABLE IF NOT EXISTS checklists_avaliacoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, checklist_id integer, lote_id integer, periodo_id integer, mobile_key TEXT, data_aplicacao date, assinatura BLOB)'],
      ['CREATE TABLE IF NOT EXISTS checklists_avaliacoes_aux (id integer primary key AUTOINCREMENT NOT NULL, avaliacao_id integer, checklist_id integer, lote_id integer, periodo_id integer, mobile_key TEXT, data_aplicacao date, assinatura BLOB, salva boolean)'],

      ['CREATE TABLE IF NOT EXISTS tipo_galpoes (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT)'],
      ['CREATE TABLE IF NOT EXISTS linhagens (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT, descricao TEXT)'],

      ['CREATE TABLE IF NOT EXISTS checklist_subtipo_integracao (checklist_id integer, subtipo_integracao_id integer)'],

      ['CREATE TABLE IF NOT EXISTS foto_acoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, acao_id integer, mobile_key TEXT, foto TEXT, data date, observacoes TEXT)'],
      ['CREATE TABLE IF NOT EXISTS foto_baixa_acoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, baixa_acao_id integer, mobile_key TEXT, foto TEXT, data date, observacoes TEXT)'],
      ['CREATE TABLE IF NOT EXISTS fotos_avaliacoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, avaliacao_id integer, mobile_key TEXT, foto TEXT, data date, observacoes TEXT)'],

      ['CREATE TABLE IF NOT EXISTS acoes_renovacoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, acao_id integer, data_prevista DATE, data_renovacao DATE, justificativa TEXT, assinatura_digital BLOB, mobile_key TEXT, deleted_at date, FOREIGN KEY(acao_id) REFERENCES acoes(id))'],
      ['CREATE TABLE IF NOT EXISTS fotos_acoes_renovacoes (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, renovacao_id integer, mobile_key TEXT, foto TEXT, data date, deleted_at date, sincronizado boolean)'],

      ['CREATE TABLE IF NOT EXISTS intervalo_celas_uteis (id integer primary key AUTOINCREMENT NOT NULL, dias integer, descricao TEXT, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS recontagem_planteis (id integer primary key AUTOINCREMENT NOT NULL, intervalo_id integer, galpao_id integer, data_prevista date, data_recontagem date, leitoas_nc integer, leitoas_c integer, matrizes_desc integer, machos_desc integer, machos_inteiros integer, machos_vasec integer, qtd_ags_lnc integer, qtd_ags_lc integer, qtd_ags_mat_desc integer, qtd_ags_mac_desc integer, qtd_ags_mac_int integer, qtd_ags_mac_vas integer, qtd_contrato_femeas integer, qtd_contrato_leitoes integer, qtd_lao_vigente integer, assinatura_produtor TEXT, sincronizado boolean, deleted_at date )'],
      ['CREATE TABLE IF NOT EXISTS recontagem_plantel_celas_uteis (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, recontagem_plantel_id integer, celas_uteis_id integer, mobile_key TEXT, quantidade integer, deleted_at date)'],

      ['CREATE TABLE IF NOT EXISTS tipos_campos (id integer primary key AUTOINCREMENT NOT NULL, descricao TEXT, tipo TEXT)'],
      ['CREATE TABLE IF NOT EXISTS visitas_lotes (id integer primary key AUTOINCREMENT NOT NULL, checklist_id integer, dias integer, descricao TEXT, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS visitas_lotes_perguntas (id integer primary key AUTOINCREMENT NOT NULL, visita_id integer, tipo_campo_id integer, descricao TEXT, opcao_campo TEXT, tabela_de_atualizacao TEXT, campo_de_atualizacao TEXT, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS visitas_lotes_lancamentos (id integer primary key AUTOINCREMENT NOT NULL, visita_id integer, lote_id integer, data_previsao_visita date, data_visita date, latitude double(18,15), longitude double(18,15), sincronizado boolean, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS visitas_lotes_respostas (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, lancamento_visita_id integer, pergunta_id integer, resposta TEXT, deleted_at date)'],
      ['CREATE TABLE IF NOT EXISTS checklists_perguntas_old_thumbs (id integer primary key AUTOINCREMENT NOT NULL, pergunta_id integer, galpao_id integer, foto64 TEXT)'],

    ]).then(() => {
      db.executeSql('SELECT lote_id FROM checklists_respostas LIMIT 1',[]).then(()=>{
        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS checklists_respostas_mig (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, avaliacao_id integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta date)'],
          ['CREATE TABLE IF NOT EXISTS checklists_respostas_aux_mig (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, avaliacao_id integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta date)'],
        ]).then(()=>{

          this.geraAvaliacoes(db);
          let consulta_respostas = 'SELECT * FROM checklists_respostas';
          db.executeSql(consulta_respostas, []).then((result) => {

            let count_resp = result.rows.length;
            if (result.rows.length > 0) {
              for (var i = 0; i < result.rows.length; i++) {
                let resposta = result.rows.item(i);

                let sql_consulta_avaliacoes = 'SELECT * FROM checklists_avaliacoes WHERE (lote_id = ? OR lote_id IS NULL) AND (periodo_id = ? OR periodo_id IS NULL) AND checklist_id = ?';
                let params = [
                  resposta.lote_id,
                  resposta.periodo_id,
                  resposta.checklist_id,
                ];

                db.executeSql(sql_consulta_avaliacoes, params).then((retorno) => {

                  if (retorno.rows.length > 0) {
                    let insere_resposta = 'INSERT INTO checklists_respostas_mig (id_web, mobile_key, avaliacao_id, pergunta_id, resposta, valor, foto, com_foto, data_resposta) VALUES (?,?,?,?,?,?,?,?,?)'
                    let params_rep = [
                      resposta.id_web,
                      resposta.mobile_key,
                      retorno.rows.item(0).id,
                      resposta.pergunta_id,
                      resposta.resposta,
                      resposta.valor,
                      resposta.foto,
                      resposta.com_foto,
                      resposta.data_resposta,
                    ];

                    db.executeSql(insere_resposta, params_rep).then(() => {

                      count_resp--;
                      if (count_resp == 0) {

                        db.sqlBatch(['DROP TABLE IF EXISTS checklists_respostas']).then((result_drop) => {

                          db.sqlBatch(['CREATE TABLE IF NOT EXISTS checklists_respostas (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, avaliacao_id integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta date, deleted_at date)']).then(result_create => {

                            let sql_consulta_mig = ' SELECT * FROM checklists_respostas_mig';
                            let param_cons_mig = [];
                            db.executeSql(sql_consulta_mig, param_cons_mig).then((consult_mig) => {

                              if (consult_mig.rows.length > 0) {
                                for (var i = 0; i < consult_mig.rows.length; i++) {

                                  let resposta_mig = consult_mig.rows.item(i);
                                  let sql_insert_resp = 'INSERT INTO checklists_respostas (id_web, mobile_key, avaliacao_id, pergunta_id, resposta, valor, foto, com_foto, data_resposta) VALUES (?,?,?,?,?,?,?,?,?)'
                                  let param_insert_resp = [
                                    resposta_mig.id_web,
                                    resposta_mig.mobile_key,
                                    resposta_mig.avaliacao_id,
                                    resposta_mig.pergunta_id,
                                    resposta_mig.resposta,
                                    resposta_mig.valor,
                                    resposta_mig.foto,
                                    resposta_mig.com_foto,
                                    resposta_mig.data_resposta,
                                  ];

                                  db.executeSql(sql_insert_resp, param_insert_resp).catch((err) => {
                                    console.error('Falha ao inserir/migrar da mig para resp', err, sql_insert_resp, param_insert_resp);
                                  })
                                }

                                db.sqlBatch([
                                  ['DROP TABLE IF EXISTS checklists_respostas_mig'],
                                  ['DROP TABLE IF EXISTS checklists_respostas_aux_mig'],
                                  ['DROP TABLE IF EXISTS checklists_respostas_assinaturas']]).catch(err => {
                                  console.error('falha ao remover TB checklists_respostas_mig');
                                });
                              }
                            }).catch((err) => {
                              console.error('Falha ao migrar dados da resp_mig', err, sql_consulta_mig);
                            })
                          })
                        });

                        db.sqlBatch(['DROP TABLE checklists_respostas_aux']).then((result_drop_aux) => {

                          db.sqlBatch(['CREATE TABLE IF NOT EXISTS checklists_respostas_aux (id integer primary key AUTOINCREMENT NOT NULL, id_web integer, mobile_key TEXT, resposta_id integer, avaliacao_id_aux integer, pergunta_id integer, resposta boolean, valor double, foto TEXT, com_foto boolean, data_resposta date)']).then(result_create => {
                            console.error('TB_respostas_aux criada', result_create);
                          })
                        })
                      }
                    }).catch((err) => {
                      console.error('falha ao inserir resposta', err);
                    })

                  }
                }).catch(err=>{
                });

              }
            }
          }).catch((err) => {
            console.error('falha ao consultar respostas', err);
          })
        });

      }).catch((err)=>{
        console.error('Não precisa executar alterações', err);
      });
    }).catch(e => console.error('Erro ao criar as tabelas', e));

  }

  private criaAssinatura(db, assinatura) {
    let cria_assinatura = 'INSERT INTO checklists_avaliacoes (mobile_key, lote_id, periodo_id, checklist_id, data_aplicacao, assinatura) VALUES (?,?,?,?,?,?)';
    let params_cria_ass = [
      this.uniqueKeyService.generateKey(),
      assinatura.lote_id,
      assinatura.periodo_id,
      assinatura.checklist_id,
      assinatura.data_assinatura,
      assinatura.assinatura,
    ];

    db.executeSql(cria_assinatura, params_cria_ass).catch((err) => {
      console.error('falha ao criar avaliacao a partir da assinatura', err);
    })
  }

  private atualizaAssinatura(db, assinatura) {
    let atualiza_assinatura = `UPDATE checklists_avaliacoes SET assinatura = ? WHERE (lote_id = ? OR lote_id IS NULL) AND (periodo_id = ? OR periodo_id IS NULL) AND checklist_id = ?`;
    let params_assinatura = [
      assinatura.assinatura,
      assinatura.lote_id,
      assinatura.periodo_id,
      assinatura.checklist_id,
    ];

    db.executeSql(atualiza_assinatura, params_assinatura).catch((err) => {
      console.error('Falha ao atualizar a assinatura', err);
    });
  }

  private consultaAvaliacaoes(db, assinatura) {
    let sql_consulta_avaliacoes = 'SELECT * FROM checklists_avaliacoes WHERE (lote_id = ? OR lote_id IS NULL) AND (periodo_id = ? OR periodo_id IS NULL) AND checklist_id = ?';
    let params = [
      assinatura.lote_id,
      assinatura.periodo_id,
      assinatura.checklist_id,
    ];

    db.executeSql(sql_consulta_avaliacoes, params).then((result) => {

      if (result.rows.length > 0) {
        this.atualizaAssinatura(db, assinatura);
      } else {
        this.criaAssinatura(db, assinatura);
      }
    }).catch(err=>{

    });
  }

  private geraAvaliacoes(db) {
    var sql = 'SELECT lote_id, periodo_id, checklist_id, data_resposta FROM checklists_respostas group by lote_id, periodo_id, checklist_id, data_resposta';
    db.executeSql(sql, []).then((data: any) => {
      let dados = '';
      let count = data.rows.length;

      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let resposta = data.rows.item(i);
          let mobile_key = this.uniqueKeyService.generateKey();

          dados += `('${mobile_key}',${resposta.lote_id}, ${resposta.periodo_id}, ${resposta.checklist_id}, '${resposta.data_resposta}')`;
          if (count-- > 1) {
            dados += ','
          }
        }

        let sql = `REPLACE INTO checklists_avaliacoes (mobile_key, lote_id, periodo_id, checklist_id, data_aplicacao) values ${dados}`;
        if (data.rows.length > 0) {
          db.sqlBatch([
            [sql, []],
          ]).then(() => {
            var sql = 'SELECT lote_id, periodo_id, checklist_id, data_assinatura, assinatura FROM checklists_respostas_assinaturas';
            db.executeSql(sql, []).then((data: any) => {

              if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                  let assinatura = data.rows.item(i);
                  this.consultaAvaliacaoes(db, assinatura)
                }
              }

            }).catch((err) => {
              console.error('Falha ao executar sql para consultar tabelas antigas', err)
            });
          }).catch(err => {

            console.error('Erro ao incluir dados respostas', err, sql)
          });
        }

      }

    }).catch((err) =>{
      console.error('Falha ao executar sql para consultar tabelas antigas', err, sql)
    });
  }

  private updateTables(db: SQLiteObject, version: string) {
    var versionNumber = parseInt(version.replace('.', '').replace('.', ''));

    db.sqlBatch([
      ['REPLACE INTO db_version (version) values (?)',
        [version]
      ],
    ]).catch(e => console.error('Erro ao incluir versão', e));

    db.executeSql('SELECT * FROM migrations', []).then((data: any) => {
      this.migrations(db, versionNumber);
    }).catch((err) => {
      console.error('tabela de migrations não encontradas', err)
    });
  }

  private migrations(db: SQLiteObject, appVersionNumber) {
    this.atualizaTabela(db, 'tecnicos', 'cpf', 'TEXT', appVersionNumber, 'alter_tecnico_table_adiciona_campo_cpf_tecnico');
    this.atualizaTabela(db, 'tecnicos', 'celular', 'TEXT', appVersionNumber, 'alter_tecnico_table_adiciona_campo_celular_tecnico');
    this.atualizaTabela(db, 'tecnicos', 'password', 'TEXT', appVersionNumber, 'alter_tecnico_table_adiciona_campo_password_tecnico');
    this.atualizaTabela(db, 'acoes', 'produtor_concat_propriedade', 'TEXT', appVersionNumber, 'alter_acoes_table_add_column_produtor_concat_propriedade');

    /* Adiciona Campos para as Fotos */
    this.atualizaTabela(db, 'acoes', 'foto', 'TEXT', appVersionNumber, 'alter_acoes_table_add_column_fotos');
    this.atualizaTabela(db, 'acoes', 'com_foto', 'boolean', appVersionNumber, 'alter_acoes_table_add_column_com_fotos');
    this.atualizaTabela(db, 'avaliacoes', 'foto', 'TEXT', appVersionNumber, 'alter_avaliacoes_table_add_column_fotos');
    this.atualizaTabela(db, 'avaliacoes', 'com_foto', 'boolean', appVersionNumber, 'alter_avaliacoes_table_add_column_com_fotos');
    this.atualizaTabela(db, 'baixa_acoes', 'foto', 'TEXT', appVersionNumber, 'alter_baixa_acoes_table_add_column_fotos');
    this.atualizaTabela(db, 'baixa_acoes', 'com_foto', 'boolean', appVersionNumber, 'alter_baixa_acoes_table_add_column_com_fotos');
    this.atualizaTabela(db, 'checklists_respostas', 'com_foto', 'boolean', appVersionNumber, 'alter_checklists_respostas_table_add_column_com_fotos');

    /* Adiciona Campos para as Keys-to-sync */
    this.atualizaTabela(db, 'acoes', 'mobile_key', 'TEXT', appVersionNumber, 'alter_acoes_table_add_column_mobile_key');
    this.atualizaTabela(db, 'avaliacoes', 'mobile_key', 'TEXT', appVersionNumber, 'alter_avaliacoes_table_add_column_mobile_key');
    this.atualizaTabela(db, 'baixa_acoes', 'mobile_key', 'TEXT', appVersionNumber, 'alter_baixa_acoes_table_add_column_mobile_key');
    this.atualizaTabela(db, 'checklists_respostas', 'mobile_key', 'TEXT', appVersionNumber, 'alter_checklists_respostas_table_add_column_mobile_key');

    /* Adiciona Campos para as os relatórios do checklist */
    this.atualizaTabela(db, 'lotes', 'fabrica_id', 'integer', appVersionNumber, 'alter_lotes_table_add_column_fabrica_id', true, 'fabricas');
    this.atualizaTabela(db, 'lotes', 'sexo_lote_id', 'integer', appVersionNumber, 'alter_lotes_table_add_column_sexo_lote_id', true, 'lotes_sexos');
    this.atualizaTabela(db, 'lotes', 'nro_animais', 'integer', appVersionNumber, 'alter_lotes_table_add_column_nro_animais');
    this.atualizaTabela(db, 'lotes', 'data_alojamento', 'DATE', appVersionNumber, 'alter_lotes_table_add_column_data_alojamento');
    this.atualizaTabela(db, 'lotes', 'dias_alojamento', 'integer', appVersionNumber, 'alter_lotes_table_add_column_dias_alojamento');
    this.atualizaTabela(db, 'lotes', 'data_previsao_carregamento', 'DATE', appVersionNumber, 'alter_lotes_table_add_column_data_previsao_carregamento');

    this.atualizaTabela(db, 'checklists', 'valor_total', 'double', appVersionNumber, 'alter_checklists_table_add_column_valor_total');
    this.atualizaTabela(db, 'checklists', 'obrigatorio', 'boolean', appVersionNumber, 'alter_checklists_table_add_column_obrigatorio');

    /* Adiciona Campo para gerar ações a partir de uma pergunta do checklist */
    this.atualizaTabela(db, 'acoes', 'pergunta_id', 'integer', appVersionNumber, 'alter_acoes_table_add_column_pergunta_id', true, 'checklists_perguntas');

    /* Adiciona Campos para atualizar produtores */
    this.atualizaTabela(db, 'produtores', 'matricula', 'string', appVersionNumber, 'alter_produtores_table_add_column_matricula');
    this.atualizaTabela(db, 'produtores', 'logradouro', 'string', appVersionNumber, 'alter_produtores_table_add_column_logradouro');
    this.atualizaTabela(db, 'produtores', 'complemento', 'string', appVersionNumber, 'alter_produtores_table_add_column_complemento');
    this.atualizaTabela(db, 'produtores', 'telefone', 'string', appVersionNumber, 'alter_produtores_table_add_column_telefone');
    this.atualizaTabela(db, 'produtores', 'celular', 'string', appVersionNumber, 'alter_produtores_table_add_column_celular');
    this.atualizaTabela(db, 'produtores', 'email', 'string', appVersionNumber, 'alter_produtores_table_add_column_email');
    this.atualizaTabela(db, 'produtores', 'sincronizado', 'boolean', appVersionNumber, 'alter_produtores_table_add_column_sincronizado');
    this.atualizaTabela(db, 'produtores', 'enviar_foto', 'boolean', appVersionNumber, 'alter_produtores_table_add_column_enviar_foto');

    /* Adiciona Campos para atualizar propriedades */
    this.atualizaTabela(db, 'propriedades', 'municipio_id', 'integer', appVersionNumber, 'alter_propriedades_table_add_column_municipio_id', true, 'municipios');
    this.atualizaTabela(db, 'propriedades', 'estado_id', 'integer', appVersionNumber, 'estado_id', true, 'estados');
    this.atualizaTabela(db, 'propriedades', 'ie', 'string', appVersionNumber, 'alter_propriedades_table_add_column_ie');
    this.atualizaTabela(db, 'propriedades', 'logradouro', 'string', appVersionNumber, 'alter_propriedades_table_add_column_logradouro');
    this.atualizaTabela(db, 'propriedades', 'complemento', 'string', appVersionNumber, 'alter_propriedades_table_add_column_complemento');
    this.atualizaTabela(db, 'propriedades', 'sincronizado', 'boolean', appVersionNumber, 'alter_propriedades_table_add_column_sincronizado');

    /* Adiciona Campos para atualizar galpoes */
    this.atualizaTabela(db, 'galpoes', 'tipo_comedouro_id', 'integer', appVersionNumber, 'alter_galpoes_table_add_column_tipo_comedouro_id', true, 'tipo_comedouros');
    this.atualizaTabela(db, 'galpoes', 'lotacao', 'integer', appVersionNumber, 'alter_galpoes_table_add_column_lotacao');
    this.atualizaTabela(db, 'galpoes', 'baias', 'integer', appVersionNumber, 'alter_galpoes_table_add_column_baias');
    this.atualizaTabela(db, 'galpoes', 'altura', 'double', appVersionNumber, 'alter_galpoes_table_add_column_altura');
    this.atualizaTabela(db, 'galpoes', 'largura', 'double', appVersionNumber, 'alter_galpoes_table_add_column_largura');
    this.atualizaTabela(db, 'galpoes', 'comprimento', 'double', appVersionNumber, 'alter_galpoes_table_add_column_comprimento');
    this.atualizaTabela(db, 'galpoes', 'metros_quadrados', 'double', appVersionNumber, 'alter_galpoes_table_add_column_metros_quadrados');
    this.atualizaTabela(db, 'galpoes', 'observacao', 'text', appVersionNumber, 'alter_galpoes_table_add_column_observacao');
    this.atualizaTabela(db, 'galpoes', 'foto', 'text', appVersionNumber, 'alter_galpoes_table_add_column_foto');
    this.atualizaTabela(db, 'galpoes', 'latitude', 'double', appVersionNumber, 'alter_galpoes_table_add_column_latitude');
    this.atualizaTabela(db, 'galpoes', 'longitude', 'double', appVersionNumber, 'alter_galpoes_table_add_column_longitude');
    this.atualizaTabela(db, 'galpoes', 'ativo', 'boolean', appVersionNumber, 'alter_galpoes_table_add_column_ativo');
    this.atualizaTabela(db, 'galpoes', 'sincronizado', 'boolean', appVersionNumber, 'alter_galpoes_table_add_column_sincronizado');

    // this.atualizaTabela(db, 'checklists_respostas', 'periodo_id', 'integer', appVersionNumber, 'alter_checklists_respostas_table_add_column_periodo_id', true, 'periodo_aplicacoes');

    this.atualizaTabela(db, 'baixa_acoes', 'acao_id_web', 'integer', appVersionNumber, 'alter_baixa_acoes_table_add_column_acao_id_web');

    this.atualizaTabela(db, 'empresa', 'dias_tolerancia_resposta_chk', 'integer', appVersionNumber, 'alter_empresa_table_add_column_dias_tolerancia_resposta_chk');

    /*Adiciona campos para marcar dados como deletados*/
    this.atualizaTabela(db, 'produtores', 'deleted_at', 'DATE', appVersionNumber, 'alter_produtores_table_add_column_deleted_at');
    this.atualizaTabela(db, 'integradoras', 'deleted_at', 'DATE', appVersionNumber, 'alter_integradoras_table_add_column_deleted_at');
    this.atualizaTabela(db, 'propriedades', 'deleted_at', 'DATE', appVersionNumber, 'alter_propriedades_table_add_column_deleted_at');
    this.atualizaTabela(db, 'grupo_acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_grupo_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'ponto_acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_ponto_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'parametro_pagamento_acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_parametro_pagamento_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'acao_parametros', 'deleted_at', 'DATE', appVersionNumber, 'alter_acao_parametros_table_add_column_deleted_at');
    this.atualizaTabela(db, 'avaliacoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_avaliacoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'baixa_acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_baixa_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'galpoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_galpoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'lotes', 'deleted_at', 'DATE', appVersionNumber, 'alter_lotes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'checklists_fases', 'deleted_at', 'DATE', appVersionNumber, 'alter_checklists_fases_table_add_column_deleted_at');
    this.atualizaTabela(db, 'checklists', 'deleted_at', 'DATE', appVersionNumber, 'alter_checklists_table_add_column_deleted_at');
    this.atualizaTabela(db, 'checklists_perguntas', 'deleted_at', 'DATE', appVersionNumber, 'alter_checklists_perguntas_table_add_column_deleted_at');
    // this.atualizaTabela(db, 'checklists_respostas', 'deleted_at', 'DATE', appVersionNumber, 'alter_checklists_respostas_table_add_column_deleted_at');
    this.atualizaTabela(db, 'fabricas', 'deleted_at', 'DATE', appVersionNumber, 'alter_fabricas_table_add_column_deleted_at');
    this.atualizaTabela(db, 'checklists_perguntas_acoes', 'deleted_at', 'DATE', appVersionNumber, 'alter_checklists_perguntas_acoes_table_add_column_deleted_at');

    // this.atualizaTabela(db, 'checklists_respostas_aux', 'periodo_id', 'integer', appVersionNumber, 'alter_checklists_respostas_aux_table_add_column_periodo_id');

    this.atualizaTabela(db, 'empresa', 'capturar_assinatura', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_capturar_assinatura');
    this.atualizaTabela(db, 'empresa', 'obriga_captura_assinatura', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_obriga_captura_assinatura');
    this.atualizaTabela(db, 'empresa', 'considerar_cotacao', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_considerar_cotacao');

    this.atualizaTabela(db, 'checklists_perguntas_acoes', 'periodo_id', 'integer', appVersionNumber, 'alter_checklists_perguntas_acoes_table_add_column_periodo_id', true,'periodos_aplicacoes');

    this.atualizaTabela(db, 'empresa', 'respostas_nulas', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_respostas_nulas');
    this.atualizaTabela(db, 'empresa', 'integracao_aves', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_integracao_aves');

    this.atualizaTabela(db, 'galpoes', 'tipo_galpao_id', 'integer', appVersionNumber, 'alter_galpoes_table_add_column_tipo_galpao_id', true, 'tipo_galpoes');

    this.atualizaTabela(db, 'lotes', 'linhagem_id', 'integer', appVersionNumber, 'alter_lotes_table_add_column_linhagem_id', true, 'linhagens');

    this.atualizaTabela(db, 'checklists_respostas', 'nao_se_aplica', 'boolean', appVersionNumber, 'alter_checklists_respostas_table_add_column_nao_se_aplica');
    this.atualizaTabela(db, 'checklists_respostas_aux', 'nao_se_aplica', 'boolean', appVersionNumber, 'alter_checklists_respostas_table_add_column_nao_se_aplica');
    this.atualizaTabela(db, 'checklists_respostas', 'avaliacao_id_web', 'integer', appVersionNumber, 'alter_checklists_respostas_table_add_column_avaliacao_id_web');

    this.atualizaTabela(db, 'propriedades_fotos', 'principal', 'boolean', appVersionNumber, 'alter_propriedades_fotos_table_add_column_principal');
    this.atualizaTabela(db, 'galpoes_fotos', 'principal', 'boolean', appVersionNumber, 'alter_galpoes_fotos_table_add_column_principal');

    this.atualizaTabela(db, 'propriedades_fotos', 'deleted_at', 'date', appVersionNumber, 'alter_propriedades_fotos_table_add_column_deleted_at');
    this.atualizaTabela(db, 'galpoes_fotos', 'deleted_at', 'date', appVersionNumber, 'alter_galpoes_fotos_table_add_column_deleted_at');

    this.atualizaTabela(db, 'empresa', 'repetir_chk', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_repetir_chk');

    this.atualizaTabela(db, 'tecnicos', 'token_fcm', 'boolean', appVersionNumber, 'alter_tecnicos_table_add_column_token_fcm');

    this.atualizaTabela(db, 'checklists_avaliacoes', 'deleted_at', 'boolean', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_deleted_at');

    this.atualizaTabela(db, 'acoes', 'data_criacao', 'date', appVersionNumber, 'alter_acoes_table_add_column_data_criacao');

    this.atualizaTabela(db, 'periodo_aplicacoes', 'deleted_at', 'date', appVersionNumber, 'alter_periodo_aplicacoes_table_add_column_deleted_at');

    this.atualizaTabela(db, 'tecnicos', 'forcar_atualizacao', 'boolean', appVersionNumber, 'alter_tecnicos_table_add_column_forcar_atualizacao');

    this.atualizaTabela(db, 'checklists_respostas_aux', 'com_acao', 'boolean', appVersionNumber, 'alter_checklists_respostas_aux_table_add_column_com_acao');
    this.atualizaTabela(db, 'checklists_respostas_aux', 'acao_id', 'number', appVersionNumber, 'alter_checklists_respostas_aux_table_add_column_acao_id');

    this.atualizaTabela(db, 'tecnicos', 'respostas_automaticas_chk', 'boolean', appVersionNumber, 'alter_tecnicos_table_add_column_respostas_automaticas_chk');

    this.atualizaTabela(db, 'empresa', 'chk_pgto_kg', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_chk_pgto_kg');
    this.atualizaTabela(db, 'empresa', 'agrupar_checklists', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_agrupar_checklists');

    this.atualizaTabela(db, 'periodo_aplicacoes', 'grupo_aplicacao_id', 'integer', appVersionNumber, 'alter_periodo_aplicacoes_table_add_column_grupo_aplicacao_id');
    this.atualizaTabela(db, 'checklists_avaliacoes', 'grupo_aplicacao_id', 'integer', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_grupo_aplicacao_id');
    this.atualizaTabela(db, 'checklists_avaliacoes_aux', 'grupo_aplicacao_id', 'integer', appVersionNumber, 'alter_checklists_avaliacoes_aux_table_add_column_grupo_aplicacao_id');

    this.atualizaTabela(db, 'checklists_avaliacoes', 'valor_chk', 'double', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_valor_chk');
    this.atualizaTabela(db, 'checklists_avaliacoes', 'peso_chk', 'double', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_peso_chk');
    this.atualizaTabela(db, 'checklists_avaliacoes_aux', 'valor_chk', 'double', appVersionNumber, 'alter_checklists_avaliacoes_aux_table_add_column_valor_chk');
    this.atualizaTabela(db, 'checklists_avaliacoes_aux', 'peso_chk', 'double', appVersionNumber, 'alter_checklists_avaliacoes_aux_table_add_column_peso_chk');

    this.atualizaTabela(db, 'acoes', 'sincronizado', 'boolean', appVersionNumber, 'alter_acoes_table_add_column_sincronizado');
    this.atualizaTabela(db, 'baixa_acoes', 'sincronizado', 'boolean', appVersionNumber, 'alter_baixa_acoes_table_add_column_sincronizado');
    this.atualizaTabela(db, 'foto_acoes', 'deleted_at', 'date', appVersionNumber, 'alter_foto_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'foto_acoes', 'sincronizado', 'boolean', appVersionNumber, 'alter_foto_acoes_table_add_column_sincronizado');
    this.atualizaTabela(db, 'foto_baixa_acoes', 'deleted_at', 'date', appVersionNumber, 'alter_foto_baixa_acoes_table_add_column_deleted_at');
    this.atualizaTabela(db, 'foto_baixa_acoes', 'sincronizado', 'boolean', appVersionNumber, 'alter_foto_baixa_acoes_table_add_column_sincronizado');
    this.atualizaTabela(db, 'foto_baixa_acoes', 'deleted_at', 'date', appVersionNumber, 'alter_foto_baixa_acoes_table_add_column_deleted_at');

    this.atualizaTabela(db, 'empresa', 'considerar_ordem_aplicacao_chk', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_considerar_ordem_aplicacao_chk');

    this.atualizaTabela(db, 'checklists', 'ordem_aplicacao', 'integer', appVersionNumber, 'alter_checklists_table_add_column_ordem_aplicacao');
    this.atualizaTabela(db, 'galpoes', 'aplica_chk', 'boolean', appVersionNumber, 'alter_galpoes_table_add_column_aplica_chk');
    this.atualizaTabela(db, 'checklists', 'obriga_ordenacao', 'boolean', appVersionNumber, 'alter_checklists_table_add_column_obriga_ordenacao');

    this.atualizaTabela(db, 'intervalos', 'deleted_at', 'date', appVersionNumber, 'alter_intervalos_table_add_column_deleted_at');

    this.atualizaTabela(db, 'checklists_perguntas', 'ordem_aplicacao', 'integer', appVersionNumber, 'alter_checklists_perguntas_table_add_column_ordem_aplicacao');

    this.atualizaTabela(db, 'checklists_respostas_aux', 'thumb_foto', 'BLOB', appVersionNumber, 'alter_checklists_respostas_aux_table_add_column_thumb_foto');
    this.atualizaTabela(db, 'checklists_respostas', 'thumb_foto', 'BLOB', appVersionNumber, 'alter_checklists_respostas_table_add_column_thumb_foto');

    this.atualizaTabela(db, 'intervalos', 'somente_plantel', 'boolean', appVersionNumber, 'alter_intervalos_table_add_column_somente_plantel');
    this.atualizaTabela(db, 'intervalos', 'somente_checklist', 'boolean', appVersionNumber, 'alter_intervalos_table_add_column_somente_checklist');

    this.atualizaTabela(db, 'empresa', 'obriga_geolocalizacao', 'boolean', appVersionNumber, 'alter_empresa_table_add_column_obriga_geolocalizacao');
    this.atualizaTabela(db, 'acoes', 'latitude', 'double', appVersionNumber, 'alter_acoes_table_add_column_latitude');
    this.atualizaTabela(db, 'acoes', 'longitude', 'double', appVersionNumber, 'alter_acoes_table_add_column_longitude');
    this.atualizaTabela(db, 'baixa_acoes', 'latitude', 'double', appVersionNumber, 'alter_baixa_acoes_table_add_column_latitude');
    this.atualizaTabela(db, 'baixa_acoes', 'longitude', 'double', appVersionNumber, 'alter_baixa_acoes_table_add_column_longitude');
    this.atualizaTabela(db, 'acoes_renovacoes', 'latitude', 'double', appVersionNumber, 'alter_acoes_renovacoes_table_add_column_latitude');
    this.atualizaTabela(db, 'acoes_renovacoes', 'longitude', 'double', appVersionNumber, 'alter_acoes_renovacoes_table_add_column_longitude');
    this.atualizaTabela(db, 'checklists_avaliacoes', 'latitude', 'double', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_latitude');
    this.atualizaTabela(db, 'checklists_avaliacoes', 'longitude', 'double', appVersionNumber, 'alter_checklists_avaliacoes_table_add_column_longitude');
    this.atualizaTabela(db, 'recontagem_planteis', 'latitude', 'double', appVersionNumber, 'alter_recontagem_planteis_table_add_column_latitude');
    this.atualizaTabela(db, 'recontagem_planteis', 'longitude', 'double', appVersionNumber, 'alter_recontagem_planteis_table_add_column_longitude');

    this.atualizaTabela(db, 'checklists_respostas', 'foto_base64', 'BLOB', appVersionNumber, 'alter_checklists_respostas_table_add_column_foto_base64');
    this.atualizaTabela(db, 'checklists_respostas_aux', 'foto_base64', 'BLOB', appVersionNumber, 'alter_checklists_respostas_aux_table_add_column_foto_base64');

    this.atualizaTabela(db, 'lotes', 'nro_machos', 'integer', appVersionNumber, 'alter_lotes_table_add_column_nro_machos');
    this.atualizaTabela(db, 'lotes', 'nro_machos_inteiros', 'integer', appVersionNumber, 'alter_lotes_table_add_column_nro_machos_inteiros');

    this.atualizaTabela(db, 'checklists_perguntas', 'parametro_id', 'integer', appVersionNumber, 'alter_checklists_perguntas_table_add_column_parametro_id');

    this.atualizaTabela(db, 'recontagem_planteis', 'plantel_produtivo', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_plantel_produtivo');
    this.atualizaTabela(db, 'recontagem_planteis', 'celas_maternidades', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_celas_maternidades');
    this.atualizaTabela(db, 'recontagem_planteis', 'box_gestacao', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_box_gestacao');
    this.atualizaTabela(db, 'recontagem_planteis', 'gestacao_coletiva', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_gestacao_coletiva');

    this.atualizaTabela(db, 'lotes', 'nro_animais_mortos', 'integer', appVersionNumber, 'alter_lotes_table_add_column_nro_animais_mortos');
    this.atualizaTabela(db, 'lotes', 'nro_animais_entregues', 'integer', appVersionNumber, 'alter_lotes_table_add_column_nro_animais_entregues');
    this.atualizaTabela(db, 'lotes', 'sincronizado', 'boolean', appVersionNumber, 'alter_lotes_table_add_column_sincronizado');

    this.atualizaTabela(db, 'galpoes', 'aplica_chk_lote', 'boolean', appVersionNumber, 'alter_galpoes_table_add_column_aplica_chk_lote');

    this.atualizaTabela(db, 'acoes', 'assinatura_produtor', 'TEXT', appVersionNumber, 'alter_acoes_table_add_column_assinatura_produtor');

    this.atualizaTabela(db, 'recontagem_planteis', 'observacoes', 'TEXT', appVersionNumber, 'alter_recontagem_planteis_table_add_column_observacoes');

	this.atualizaTabela(db, 'propriedades', 'codErp', 'integer', appVersionNumber, 'alter_propriedades_table_add_column_codErp');
	this.atualizaTabela(db, 'galpoes', 'codErp', 'integer', appVersionNumber, 'alter_galpoes_table_add_column_codErp');

	this.atualizaTabela(db, 'recontagem_planteis', 'saldo_cidasc_femeas', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_saldo_cidasc_femeas');
	this.atualizaTabela(db, 'recontagem_planteis', 'saldo_cidasc_machos', 'integer', appVersionNumber, 'alter_recontagem_planteis_table_add_column_saldo_cidasc_machos');

  }

  private adicionaMigration(db: SQLiteObject, appVersionNumber: number, migration: string) {

    db.executeSql('SELECT * FROM migrations WHERE (version_app = ? AND migration = ?) OR migration = ?', [appVersionNumber, migration, migration]).then((data) => {
      if (data.rows.length == 0) {
        db.sqlBatch([
          ['INSERT INTO migrations (version_app, migration) values (?,?)',
            [appVersionNumber, migration]
          ],
        ]).catch((err) => {
          console.error('Falha ao inserir migration', err)
        })
      }
    })
  }

  private adicionaColuna(db: SQLiteObject, tabela: string, campo: string, tipoCampo: string, isFK: boolean = false, tabela_referencia: string = '') {
    return new Promise((resolve, reject) => {
      let alters: Array<any> = [];

      if (isFK) {
        alters.push([`ALTER TABLE ${tabela} ADD COLUMN ${campo} ${tipoCampo} REFERENCES ${tabela_referencia}(id);`]);
      } else {
        alters.push([`ALTER TABLE ${tabela} ADD COLUMN ${campo} ${tipoCampo};`]);
      }

      db.sqlBatch(alters)
        .then((res) => {
          resolve(res)
        }).catch((err) => {
        reject(err);
        console.error(`Falha ao criar campo ${campo} na tabela ${tabela}`, err);
      });


    });
  }

  private atualizaTabela(db: SQLiteObject, tabela: string, campo: string, tipoCampo: string, appVersionNumber: number, migration: string, isFK: boolean = false, tabela_referencia: string = '') {
    db.executeSql(`SELECT ${campo} FROM ${tabela}`, []).then(() => {
      this.adicionaMigration(db, appVersionNumber, migration);
    }).catch(() => {
      this.adicionaColuna(db, tabela, campo, tipoCampo, isFK, tabela_referencia).then(() => {
        this.adicionaMigration(db, appVersionNumber, migration);
      }).catch((err) => {
        console.error(`Falha ao criar campo ${campo} na tabela ${tabela}`, err);
      })

    });
  }

  public compactDatabase(db: SQLiteObject, showToast:boolean = false){
    let promise = new Promise(resolve => {
        db.transaction(function (tx){
          tx.executeSql('select dias from intervalos order by dias desc limit 1',[], function(tx1, data) {
            let data_limit
            if(data.rows.item(0).dias > 0 ){
              data_limit = moment().subtract(data.rows.item(0).dias + 30, 'days').format('YYYY-MM-DD');
            }else{
              data_limit = moment().subtract(120, 'days').format('YYYY-MM-DD');
            }

		  let data_limit_sign = moment().subtract(30, 'days').format('YYYY-MM-DD');
            tx1.executeSql('delete from checklists_avaliacoes_aux where avaliacao_id is not null; ', [], function (tx2){
              tx2.executeSql('delete from checklists_respostas_aux where resposta_id is not null;', [], function (tx3) {
                tx3.executeSql('delete from checklists_avaliacoes where id_web is not null and data_aplicacao <= ?;', [data_limit], function (tx4) {
                  tx4.executeSql('delete from checklists_respostas where id_web is not null and data_resposta <= ?;', [data_limit], function (tx5) {
                    tx5.executeSql('update checklists_avaliacoes set assinatura = null where id_web is not null and data_aplicacao <= ?;', [data_limit_sign], function (tx6) {
                      tx6.executeSql('update checklists_respostas set foto_base64 = null, thumb_foto = null where id_web is not null;', [],function (tx7) {
                        /*Remove ações duplicadas - Remover depois de normalizar*/
                        tx7.executeSql('PRAGMA foreign_keys = OFF;', [], function(tx8) {
                          tx8.executeSql('delete from acoes where id in (select id from (SELECT id, id_web, COUNT(*) c FROM acoes GROUP BY id_web HAVING c > 1));', [], function (tx9) {
                            tx9.executeSql('PRAGMA foreign_keys = ON;',[]);
                          })
                        });
                      })
                    })
                  })
                })
              })
            })
          })

        }).then(()=>{
          db.executeSql(' VACUUM;',[]).then(() =>{
            resolve();
            if(showToast){
              this.messagesService.showToastSuccess('Limpeza Concluída.');
            }
          }).catch((err)=>{
            resolve();
            console.error('vacuum falhou',err)
          });

        }).catch(e => {
          console.error('Erro ao criar as tabelas', e)
          resolve();
        });
    });

    return promise;

  }
}
