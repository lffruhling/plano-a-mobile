import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {ParametroPagamentoAcao} from "../../class/ParametroPagamentoAcao";
import {HttpClient} from "@angular/common/http";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class ParametroPagamentoAcoesProvider extends ParametroPagamentoAcao{
  private urlApi;

  constructor(
    public dbProvider: DatabaseProvider,
    public http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private events:Events,
    private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}/parametros/pagamentos/acoes?last_update=${lastUpdate}`);
  }

    public insert(parametro: ParametroPagamentoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'insert into parametro_pagamento_acoes (id, descricao, amplitude_maxima, amplitude_minima, valor,  valor_percentual, ativo) values (?,?,?,?,?,?,?)';
                let data = [
                    parametro.id,
                    parametro.descricao,
                    parametro.amplitude_maxima,
                    parametro.amplitude_minima,
                    parametro.valor,
                    parametro.valor_percentual,
                    parametro.ativo,
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'insert', sql, data);
                      console.error('Falha ao executar sql parametro_pagamento_acoes', err)
                    });

            });
    }

    public update(parametro: ParametroPagamentoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) =>{
                let sql = 'update parametro_pagamento_acoes set descricao = ?, amplitude_maxima = ?, amplitude_minima = ?, valor = ?,  valor_percentual = ?, ativo = ? where id = ?';
                let data = [
                    parametro.descricao,
                    parametro.amplitude_maxima,
                    parametro.amplitude_minima,
                    parametro.valor,
                    parametro.valor_percentual,
                    parametro.ativo,
                    parametro.id
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'update', sql, data);
                      console.error('Falha ao executar sql parametro_pagamento_acoes', err)
                    });
            });
    }

    public get(id: number) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'select * from parametro_pagamento_acoes where id = ?';
                let data = [id];

                return db.executeSql(sql, data)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let item = data.rows.item(0);
                            let parametro = new ParametroPagamentoAcao();
                            parametro.id = item.id;
                            parametro.descricao = item.descricao;
                            parametro.amplitude_maxima = item.amplitude_maxima;
                            parametro.amplitude_minima = item.amplitude_minima;
                            parametro.valor = item.valor;
                            parametro.valor_percentual = item.valor_percentual;
                            parametro.ativo = item.ativo;

                            return parametro;
                        }

                        return null;
                    }).catch((err) => {
                    this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'get', sql, data);
                    console.error('Falha ao executar sql parametro_pagamento_acoes', err)
                  });
            });
    }

    public getAll(ativo: boolean, descricao: string = null) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'SELECT * FROM parametro_pagamento_acoes where ativo = ?';
                var data: any[] = [ativo ? 1 : 0];

                // filtrando pelo nome
                if (descricao) {
                    sql += ' and descricao like ?';
                    data.push('%' + descricao + '%');
                }

                return db.executeSql(sql, data)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let pontoAcoes: any[] = [];
                            for (var i = 0; i < data.rows.length; i++) {
                                var parametro = data.rows.item(i);
                                pontoAcoes.push(parametro);
                            }
                            return pontoAcoes;
                        } else {
                            return [];
                        }
                    }).catch((err) => {
                    this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'getAll', sql, data);
                    console.error('Falha ao executar sql parametro_pagamento_acoes', err)
                  });
            });
    }

  atualizaParametroPgtoLocal(){
    /*Atualiza Pontos das Ações*/
    this.dbProvider.getDB().then((db:SQLiteObject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
        this.http.get(`${SERVER_URL}/parametros/pagamentos/acoes`).subscribe((results:any) => {
          if(results) {
            let dados = '';
            let count = results.length;

            for (let result of results) {
              dados += `(${result.id}, "${result.descricao}", ${result.amplitude_maxima}, ${result.amplitude_minima}, ${result.valor}, ${result.valor_percentual}, ${result.ativo}, "${result.deleted_at}")`;
              if (count-- > 1) {
                dados += ','
              }
            }
            let sql = `REPLACE INTO parametro_pagamento_acoes (id, descricao, amplitude_maxima, amplitude_minima, valor, valor_percentual, ativo, deleted_at) values ${dados}`;

            db.sqlBatch([
              [sql, []],
            ]).catch(err => {
              this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'atualizaParametroPgtoLocal', sql, null);
              console.error('Erro ao incluir parametro_pagamento_acoes', err,sql)
            });
          }
        });
      });

    });
  }

}
