import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {AvaliacaoInicial} from "../../class/AvaliacaoInicial";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {AcoesProvider} from "./acoes";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";
import * as moment from "moment";

@Injectable()
export class AvaliacoesProvider extends AvaliacaoInicial{
  private urlApi;
  protected now = moment().format('YYYY-MM-DD');

  constructor(
      private dbProvider: DatabaseProvider,
      private readonly http: HttpClient,
      private storage: Storage,
      private fileTransfer: FileTransfer,
      private messageService: AlertMessagesProvider,
      private acaoService: AcoesProvider,
      private events: Events,
      private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/avaliacaoinicial`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/avaliacaoinicial`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}/avaliacaoinicial`;
      }
    });

  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate) {
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`)
  }

  public apiGetParametros(propriedade_id) {
      return this.http.get<any>(this.urlApi + '?propriedade_id=' + propriedade_id);
  }

  public apiResolver(propriedade_id, parametro_id) {
      return this.http.post<any>(this.urlApi, {propriedade_id: propriedade_id, parametro_id: parametro_id});
  }

  public apiStore(values) {
    return this.http.post(this.urlApi, values)
  }

  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/upload/bkp`, values)
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(avaliacao: AvaliacaoInicial){
    return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'insert into avaliacoes (id_web, propriedade_id, parametro_id, tecnico_id, data_visita, gerou_acao, observacoes, foto, com_foto, mobile_key) values (?,?,?,?,?,?,?,?,?,?)';
              let data = [
                  avaliacao.id_web,
                  avaliacao.propriedade_id,
                  avaliacao.parametro_id,
                  avaliacao.tecnico_id,
                  avaliacao.data_visita,
                  avaliacao.gerou_acao,
                  avaliacao.observacoes,
                  avaliacao.foto,
                  avaliacao.com_foto,
                  avaliacao.mobile_key
              ];

              return db.executeSql(sql, data).then(data=>{
                return data;
              }).catch((err) => {
                this.exceptionProvider.insert(err, 'avaliacoes.ts', 'insert', sql, data);
                console.error('Falha ao executar sql avaliacoes', err)
              });

          });
  }

  public update(avaliacao: AvaliacaoInicial){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) =>{
              let sql = 'update avaliacoes set id_web = ?, propriedade_id = ?, parametro_id = ?, tecnico_id = ?, data_visita = ?, gerou_acao = ?, observacoes = ?, foto = ?, com_foto = ? where id = ?';
              let data = [
                avaliacao.id_web,
                avaliacao.propriedade_id,
                avaliacao.parametro_id,
                avaliacao.tecnico_id,
                avaliacao.data_visita,
                avaliacao.gerou_acao,
                avaliacao.observacoes,
                avaliacao.foto,
                avaliacao.com_foto,
                avaliacao.id,
              ];

              return db.executeSql(sql, data)
                  .catch((err) => {
                    this.exceptionProvider.insert(err, 'avaliacoes.ts', 'update', sql, data);
                    console.error('Falha ao executar sql avaliacoes', err)
                  });
          });
  }

  public updateGerouAcao(avaliacao_id: number, gerou_acao:boolean = true){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) =>{
              let sql = 'update avaliacoes set gerou_acao = ? where id = ?';
              let data = [gerou_acao,avaliacao_id];

              return db.executeSql(sql, data).catch((err) => {
                this.exceptionProvider.insert(err, 'avaliacoes.ts', 'updateGerouAcao', sql, data);

                console.error('Falha ao executar sql avaliacoes', err)
              });
          });
  }

  public updateSinc(id_web: number, id_mobile:number){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) =>{
              let sql = 'update avaliacoes set id_web = ? where id = ?';

              let data = [id_web,id_mobile];

              return db.executeSql(sql, data)
                  .catch((err) => {
                    this.exceptionProvider.insert(err, 'avaliacoes.ts', 'updateSinc', sql, data);
                    console.error('Falha ao executar sql avaliacoes', err)
                  });
          });
  }

  public get(id: number) {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'select * from avaliacoes where id = ?';
              let data = [id];

              return db.executeSql(sql, data)
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          let item = data.rows.item(0);
                          let avaliacao = new AvaliacaoInicial();
                          avaliacao.id = item.id;
                          avaliacao.id_web = item.id_web;
                          avaliacao.id_mobile = item.id_mobile;
                          avaliacao.propriedade_id = item.propriedade_id;
                          avaliacao.parametro_id = item.parametro_id;
                          avaliacao.tecnico_id = item.tecnico_id;
                          avaliacao.data_visita = item.data_visita;
                          avaliacao.gerou_acao = item.gerou_acao;
                          avaliacao.observacoes = item.observacoes;
                          avaliacao.foto = item.foto;
                          avaliacao.com_foto = item.com_foto;

                          return avaliacao;
                      }

                      return null;
                  }).catch((err) => {
                    this.exceptionProvider.insert(err, 'avaliacoes.ts', 'get', sql, data);
                    console.error('Falha ao executar sql avaliacoes', err);
                });
          });
  }

  public getAll() {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'SELECT * FROM avaliacoes';
              var data: any[] = [];

              return db.executeSql(sql, data)
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          let avaliacoes: any[] = [];
                          for (var i = 0; i < data.rows.length; i++) {
                              var avaliacao = data.rows.item(i);
                              avaliacoes.push(avaliacao);
                          }
                          return avaliacoes;
                      } else {
                          return [];
                      }
                  })
                  .catch((err) => {
                    this.exceptionProvider.insert(err, 'avaliacoes.ts', 'getAll', sql, data);
                    console.error('Falha ao executar sql avaliacoes', err)
                  });
          });
  }

  public getAllToSinc() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM avaliacoes where id_web is null';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let avaliacoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var avaliacao = data.rows.item(i);
                avaliacoes.push(avaliacao);
              }
              return avaliacoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'avaliacoes.ts', 'getAllToSinc', sql, data);
            console.error('Falha ao executar sql sinc avaliacoes', err)
          });
      });
  }

  public getLatestId() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select id from avaliacoes order by id desc limit 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);


              return item.id;
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'avaliacoes.ts', 'getLatestId', sql, null);
            console.error('Falha ao executar sql avaliacoes', err)
          });
      });
  }

  /*Fotos*/
  public insertFoto(foto){
    let sql = 'INSERT INTO fotos_avaliacoes (avaliacao_id, mobile_key, foto, data) VALUES (?,?,?,?)';
    let params = [
      foto.avaliacao_id,
      foto.mobile_key,
      foto.img,
      this.now
    ];

    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      return db.executeSql(sql, params).then((data)=>{
        console.log('foto da fotos_avaliacoes salva - provider', data, sql, params);
        return data;
      }).catch(err=>{
        console.error('falha salvar fotos_avaliacoes - provider', err);
      });
    });
  }

  public updateSincFoto(id_web: number, id_mobile:number){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update fotos_avaliacoes set id_web = ? where id = ?';

        let data = [id_web,id_mobile];

        return db.executeSql(sql, data).then((data) =>{
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'avaliacoes.ts', 'updateSincFoto', sql, data)
        });
      });
  }

  public getAllFotoToSinc(avaliacao_id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM fotos_avaliacoes where id_web is null and avaliacao_id = ?';
        var data= [avaliacao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let fotos: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                fotos.push(data.rows.item(i));
              }
              return fotos;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'avaliacoes.ts', 'getAllFotoToSinc', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public deleteFoto(foto_id){
    let promise = new Promise( (resolve, reject)=>{
      this.dbProvider.getDB().then((db:SQLiteObject)=>{
        let sql = 'DELETE FROM fotos_avaliacoes WHERE id = ?';
        let data = [foto_id];
        db.executeSql(sql, data).then(()=>{
          resolve()
        }).catch(err=>{
          this.exceptionProvider.insert(err, 'fotos.ts', 'deleteFotoAvaliacao', sql, data);
          console.error('erro ao remover foto',err);
          reject();
        })
      })
    });

    return promise
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaAvaliacoesLocal(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get(`${this.urlApi}`).subscribe((results:any) => {
          let loading = this.messageService.showLoading('Atualizando Avaliações Iniciais...');
          if(results){
            for (let result of results) {
              let sql_up = 'update avaliacoes SET propriedade_id = ?, parametro_id = ?, tecnico_id = ?, data_visita = ?, gerou_acao = ?, observacoes = ? where id_web = ?;';
              let data_up = [result.propriedade_id, result.parametro_id, result.tecnico_id, result.data_visita, result.gerou_acao, result.observacoes, result.id];
              db.executeSql(sql_up,data_up).then((data: any) => {
                if (data.rowsAffected < 1) {
                  let sql_ins = 'INSERT OR IGNORE INTO avaliacoes (id_web, propriedade_id, parametro_id, tecnico_id, data_visita, gerou_acao, observacoes) values (?, ?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.propriedade_id, result.parametro_id, result.tecnico_id, result.data_visita, result.gerou_acao, result.observacoes];
                  db.executeSql(sql_ins, data_ins).catch(err=>{
                    this.exceptionProvider.insert(err, 'avaliacoes.ts', 'atualizaAvaliacoesLocal', sql_ins, data_ins);
                    reject();
                  });
                }
              }).catch(err=>{
                reject();
                this.exceptionProvider.insert(err, 'avaliacoes.ts', 'atualizaAvaliacoesLocal', sql_up, data_up);
              })
            }
          }
          loading.dismiss();
          resolve();
        });
      });
    });
    return promisse;
  }

  public enviaFotos(avaliacao_id, avaliacao_id_web){
    var self = this;

    this.getAllFotoToSinc(avaliacao_id).then((fotos:Array<any>)=>{
      fotos.forEach(function (foto) {

        self.storage.get('access_token').then((access_token) => {
          let options: FileUploadOptions = {
            fileKey: 'foto',
            fileName: `avaliacao_${avaliacao_id_web}_foto_${foto.id}_img.jpg`,
            chunkedMode: false,
            httpMethod: 'POST',
            mimeType: "image/jpeg",
            params: {},
            headers: {
              'Authorization': `Bearer ${access_token}`,
            }
          };

          self.storage.get('SERVER_URL').then((SERVER_URL:any) => {

            let url = `${SERVER_URL}/avaliacaoinicial/foto/${avaliacao_id_web}`;
            let fileTransfer = self.fileTransfer.create();

            fileTransfer.upload(foto.foto, url, options).then((success: any) => {
              let response = JSON.parse(success.response);
              self.updateSincFoto(response.id, foto.id)
            });
          })
        });
      })
    });
  }
}
