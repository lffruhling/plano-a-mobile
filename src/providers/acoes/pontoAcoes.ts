import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {PontoAcao} from "../../class/PontoAcao";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class PontoAcoesProvider extends PontoAcao{
    private urlApi;

    constructor(
      public dbProvider: DatabaseProvider,
      public http: HttpClient,
      private storage: Storage,
      private events:Events,
      private exceptionProvider:DbExceptionProvider,
    ) {
      super();
      this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
        this.urlApi = `${SERVER_URL}`;
        if(SERVER_URL == null){
          this.events.subscribe('url:changed', url => {
            if(url != null){
              this.urlApi = `${url}`;

            }
          });
        }
      });

      this.events.subscribe('url:changed', url => {
        if(url){
          this.urlApi = `${url}`;
        }
      });
    }

    /*
    * ############################
    * API - Métodos
    * ###############
    * */
    public apiGetAll(lastUpdate){
      return this.http.get(`${this.urlApi}/pontos?last_update=${lastUpdate}`);
    }

    getPontosPorGrupo(grupo_id) {
        return this.http.get(`${this.urlApi}/acao/pontos?grupo_id=${grupo_id}`);
    }

    atualizaPontosLocal(){
      /*Atualiza Pontos das Ações*/
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/pontos`).subscribe((results:any) => {
            if(results) {

              let dados = '';
              let count = results.length;

              for (let result of results) {
                let descricao = result.descricao.replace(/'/g,'"');

                dados += `(${result.id}, ${result.grupo_acao_id}, '${descricao}', '${result.deleted_at}')`;
                if (count-- > 1) {
                  dados += ','
                }
              }
              let sql = `REPLACE INTO ponto_acoes (id, grupo_acao_id, descricao, deleted_at) values ${dados}`;

              db.sqlBatch([
                [sql, []],
              ]).catch(err => {
                this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'atualizaPontosLocal', sql, null);
                console.error('Erro ao incluir ponto_acoes', err, sql)
              });
            }
          });
        });

      });
    }

    public insert(pontoAcao: PontoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'insert into ponto_acoes (id, grupo_acao_id, descricao, deleted_at) values (?,?,?,?)';
                let data = [
                    pontoAcao.id,
                    pontoAcao.grupo_acao_id,
                    pontoAcao.descricao,
                    pontoAcao.deleted_at,
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'insert', sql, data);
                      console.error('Falha ao executar sql ponto_acoes', err)
                    });

            });
    }

    public update(pontoAcao: PontoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) =>{
                let sql = 'update ponto_acoes set grupo_acao_id = ?, descricao = ?, deleted_at = ? where id = ?';
                let data = [
                    pontoAcao.grupo_acao_id,
                    pontoAcao.descricao,
                    pontoAcao.deleted_at,
                    pontoAcao.id
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'update', sql, data);
                      console.error('Falha ao executar sql ponto_acoes', err)
                    });
            });
    }

  public find(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from ponto_acoes where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }
            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'get', sql, data);
            console.error('Falha ao executar sql ponto_acoes', err)
          });
      });
  }

    public getAll(descricao: string = null) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'SELECT * FROM ponto_acoes where (deleted_at == "null" OR deleted_at IS NULL)';
                var data: any[] = [];

                // filtrando pelo nome
                if (descricao) {
                    sql += ' and descricao like ?'
                    data.push('%' + descricao + '%');
                }

                return db.executeSql(sql, data)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let pontoAcoes: any[] = [];
                            for (var i = 0; i < data.rows.length; i++) {
                                var pontoAcao = data.rows.item(i);
                                pontoAcoes.push(pontoAcao);
                            }
                            return pontoAcoes;
                        } else {
                            return [];
                        }
                    }).catch((err) => {
                    this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'getAll', sql, null);
                    console.error('Falha ao executar sql ponto_acoes', err)
                  });
            });
    }

  public getPontosPorGrupoLocal(grupo_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'select id, descricao from ponto_acoes where grupo_acao_id = ? and (deleted_at == "null" OR deleted_at IS NULL)';
      var data: any[] = [grupo_id];

      return db.executeSql(sql, data).then( (data:any) =>{
        if (data.rows.length > 0) {
          let pontoAcoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var pontoAcao = data.rows.item(i);
            pontoAcoes.push(pontoAcao);
          }
          return pontoAcoes;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'parametroPagamentoAcoes', 'getPontosPorGrupoLocal', sql, data);
        console.error('Falha ao executar sql pontos_acoes', err)
      });
    });
  }

}
