import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {BaixaAcao} from "../../class/BaixaAcao";
import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {DbExceptionProvider} from "../db-exception/db-exception";
import * as moment from "moment";

@Injectable()
export class BaixaAcoesProvider extends BaixaAcao {
  private urlApi;
  protected now = moment().format('YYYY-MM-DD');

  constructor(
    private dbProvider: DatabaseProvider,
    private http: HttpClient,
    private readonly loadingCtrl: LoadingController,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private fileTransfer: FileTransfer,
    private events: Events,
    private exceptionProvider: DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/baixaacao`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/baixaacao`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}/baixaacao`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate) {
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`)
  }

  public apiGetAllRenovacoes(lastUpdate) {
    return this.http.get(`${this.urlApi}/renovacoes?last_update=${lastUpdate}`)
  }

  public apiBaixaAcao(values) {

    return this.http.post(`${this.urlApi}`, values)
  }


  public apiStore(values) {
    return this.http.post(this.urlApi, values)
  }

  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/upload/bkp`, values)
  }

  public apiUpdate(baixa_acao_id, values) {
    return this.http.patch(this.urlApi + '/' + baixa_acao_id, values)
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */

  public insert(baixa: BaixaAcao) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into baixa_acoes (id_web, acao_id, acao_id_web, resolvido, data, resposta, foto, com_foto, mobile_key, deleted_at) values (?,?,?,?,?,?,?,?,?,?)';
        let data = [
          baixa.id_web,
          baixa.acao_id,
          baixa.acao_id_web,
          baixa.resolvido,
          baixa.data,
          baixa.resposta,
          baixa.foto,
          baixa.com_foto,
          baixa.mobile_key,
          baixa.deleted_at,
        ];

        return db.executeSql(sql, data).then(data => {
          return data;
        }).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'insert', sql, data);
        })

      });
  }

  public insertRenovacaoAcao(dados: any) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into acoes_renovacoes (id_web, acao_id, data_prevista, data_renovacao, justificativa, assinatura_digital, mobile_key, deleted_at) values (?,?,?,?,?,?,?,?)';
        let data = [
          dados.id_web,
          dados.acao_id,
          dados.data_prevista,
          dados.data_renovacao,
          dados.resposta, //nesse caso vira uma justificativa
          dados.assinatura,
          dados.mobile_key,
          dados.deleted_at,
        ];

        return db.executeSql(sql, data).then(data => {
          return data;
        }).catch(err => {
          console.error('insertRenovacaoAcao', err, dados)
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'insertRenovacaoAcao', sql, data);
        })

      });
  }

  public updateRenovacaoAcao(dados: any) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes_renovacoes set acao_id=?, data_prevista=?, data_renovacao=?, justificativa=?, assinatura_digital=?, mobile_key=?, deleted_at=? where id_web=?';
        let data = [
          dados.acao_id,
          dados.data_prevista,
          dados.data_renovacao,
          dados.justificativa, //nesse caso vira uma justificativa
          dados.assinatura,
          dados.mobile_key,
          dados.deleted_at,
          dados.id_web,
        ];

        return db.executeSql(sql, data).then(data => {
          return data;
        }).catch(err => {
          console.error('updateRenovacaoAcao', err, dados);
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'updateRenovacaoAcao', sql, data);
        })

      });
  }

  public updateGeolocationRenovacao(lat, long, id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes_renovacoes set latitude=?, longitude=?, where id = ?';
        let data = [
          lat,
          long,
          id
        ];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'update', sql, data);
        });
      });
  }

  public update(baixa: BaixaAcao) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update baixa_acoes set acao_id = ?,acao_id_web = ?, resolvido = ?, data = ?, resposta = ?, deleted_at = ? where id_web = ?';
        let data = [
          baixa.acao_id,
          baixa.acao_id_web,
          baixa.resolvido,
          baixa.data,
          baixa.resposta,
          baixa.deleted_at,
          baixa.id,
        ];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'update', sql, data);
        });
      });
  }

  public updateGeolocationBaixa(lat, long, id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update baixa_acoes set latitude=?, longitude=?, where id = ?';
        let data = [
          lat,
          long,
          id
        ];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'update', sql, data);
        });
      });
  }

  public editaBaixa(baixa: BaixaAcao) {

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update baixa_acoes set resolvido=?, resposta=?, sincronizado=? where id=?';

        let data = [
          baixa.resolvido,
          baixa.resposta,
          false, // Sempre será false para sinalizar que deve ser atualizado em caso de edição
          baixa.id
        ];

        return db.executeSql(sql, data).catch(err => {
          console.error('falha ao editar baixa', err);
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'editaBaixa', sql, data)
        });
      })
  }

  public updateSinc(id_web: number, id_mobile: number, acao_id_web: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update baixa_acoes set sincronizado = ?, id_web = ?, acao_id_web = ? where id = ?';

        let data = [true, id_web, acao_id_web, id_mobile];
        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'updateSinc', sql, data);
          console.error('falaha ao atualiza baixa', err)
        });
      });
  }

  public updateSincRenovacaoAcao(id_web: number, id_mobile: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes_renovacoes set id_web = ? where id = ?';

        let data = [id_web, id_mobile];
        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'updateSincRenovacaoAcao', sql, data);
          console.error('falaha ao atualiza renovacao', err, data, sql)
        });
      });
  }

  public updateSincFoto(id_web: number, id_mobile: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update foto_baixa_acoes set sincronizado = ?, id_web = ? where id = ?';

        let data = [true, id_web, id_mobile];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'updateSincFoto', sql, data)
        });
      });
  }

  public updateSincFotoRenovacao(id_web: number, id_mobile: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update fotos_acoes_renovacoes set sincronizado = ?, id_web = ? where id = ?';

        let data = [true, id_web, id_mobile];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'updateSincFotoRenovacao', sql, data)
        });
      });
  }

  public find(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = '\tSELECT bxa.id,\n' +
          '\t\tbxa.acao_id_web,\n' +
          '\t\tbxa.mobile_key,\n' +
          '\t\t(prod.nome || prop.descricao) AS produtor,\n' +
          '\t\tgp.descricao AS grupo,\n' +
          '\t\tpt.descricao AS ponto,\n' +
          '\t\tpa.descricao AS parametro,\n' +
          '\t\tac.o_que,\n' +
          '\t\tac.como,\n' +
          '\t\tac.quem,\n' +
          '\t\tac.quando,\n' +
          '\t\tbxa.data,\n' +
          '\t\tbxa.resolvido,\n' +
          '\t\tbxa.resposta\n' +
          '\tFROM baixa_acoes AS bxa\n' +
          '\t\tLEFT JOIN acoes AS ac\n' +
          '\t\t\tON ac.id = bxa.acao_id\n' +
          '\t\tLEFT JOIN grupo_acoes AS gp\n' +
          '\t\t\tON gp.id = ac.grupo_acao_id\n' +
          '\t\tLEFT JOIN ponto_acoes AS pt\n' +
          '\t\t\tON pt.id = ac.ponto_acao_id\n' +
          '\t\tLEFT JOIN acao_parametros AS pa\n' +
          '\t\t\tON pa.id = ac.parametro_acao_id\n' +
          '\t\tLEFT JOIN propriedades AS prop\n' +
          '\t\t\tON prop.id = ac.propriedade_id\n' +
          '\t\tLEFT JOIN produtores AS prod\n' +
          '\t\t\tON prod.id = prop.produtor_id\n' +
          '\tWHERE bxa.id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data) {
              return data.rows.item(0);
            }

            return null;
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'get', sql, data);
          });
      })
  }

  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT  ' +
          '    ba.id, ' +
          '    ba.mobile_key, ' +
          '    ba.id_web, ' +
          '    ba.acao_id,  ' +
          '    ba.acao_id_web,  ' +
          '    ba.data,  ' +
          '    ba.resolvido,   ' +
          '    ba.resposta,  ' +
          '    (prod.nome || " - " || prop.descricao) as produtor_concat_propriedade,    ' +
          '    a.o_que    ' +
          'FROM baixa_acoes AS ba    ' +
          '    LEFT JOIN acoes AS a    ' +
          '        ON a.id = ba.acao_id ' +
          '    LEFT JOIN propriedades AS prop    ' +
          '        ON prop.id = a.propriedade_id    ' +
          '    LEFT JOIN produtores AS prod    ' +
          '        ON prod.id = prop.produtor_id  ' +
          ' WHERE (ba.deleted_at == "null" OR ba.deleted_at IS NULL) ' +
          '   AND ba.id_web IS NULL' +
          ' ORDER BY ba.id DESC';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let baixas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var baixa = data.rows.item(i);
                baixas.push(baixa);
              }
              return baixas;
            } else {
              return [];
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAll', sql, data);
          });
      });
  }

  public getAllSincronizadas() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT  ' +
          '    ba.id, ' +
          '    ba.mobile_key, ' +
          '    ba.id_web, ' +
          '    ba.acao_id,  ' +
          '    ba.acao_id_web,  ' +
          '    ba.data,  ' +
          '    ba.resolvido,   ' +
          '    ba.resposta,  ' +
          '    (prod.nome || " - " || prop.descricao) as produtor_concat_propriedade,    ' +
          '    a.o_que    ' +
          'FROM baixa_acoes AS ba    ' +
          '    LEFT JOIN acoes AS a    ' +
          '        ON a.id_web = ba.acao_id_web ' +
          '    LEFT JOIN propriedades AS prop    ' +
          '        ON prop.id = a.propriedade_id    ' +
          '    LEFT JOIN produtores AS prod    ' +
          '        ON prod.id = prop.produtor_id  ' +
          ' WHERE (ba.deleted_at == "null" OR ba.deleted_at IS NULL)' +
          '   AND ba.id_web IS NOT NULL' +
          ' ORDER BY ba.id DESC';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let baixas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var baixa = data.rows.item(i);
                baixas.push(baixa);
              }
              return baixas;
            } else {
              return [];
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllSincronizadas', sql, data);
          });
      });
  }

  public getAllToSinc() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT ba.*, coalesce(ac.id_web, ba.acao_id_web) as acao_id_web, ac.mobile_key as acao_mobile_key FROM baixa_acoes as ba LEFT JOIN acoes as ac ON ac.id = ba.acao_id WHERE ba.id_web IS NULL OR ba.sincronizado = "false"';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let baixas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var baixa = data.rows.item(i);
                baixas.push(baixa);
              }
              return baixas;
            } else {
              return [];
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllToSinc', sql, data);

          });
      });
  }

  public getAllRenovadasToSinc() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = ' SELECT ar.*, \n' +
			' \tac.id_web AS acao_id_web,\n' +
			' \t0 AS resolvido,\n' +
			' \tjustificativa AS resposta\n' +
			' FROM acoes_renovacoes AS ar\n' +
			' \tLEFT JOIN acoes AS ac\n' +
			' \t\tON ar.acao_id = CASE WHEN ac.id_web IS NULL \n' +
			' \t\t\t\t\t\t\tTHEN ac.id\n' +
			' \t\t\t\t\t\t\tELSE ac.id_web\n' +
			' \t\t\t\t\t\t\tEND \n' +
			' WHERE ar.id_web IS NULL';

        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let renovacoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                renovacoes.push(data.rows.item(i));
              }
              return renovacoes;
            } else {
              return [];
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllRenovadasToSinc', sql, data);

          });
      });
  }

  public getLatestId() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select id from baixa_acoes order by id desc limit 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);
              return item.id;
            }

            return null;
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getLatestId', sql, null);
          })
      })
  }

  public getAllBkp() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM baixa_acoes WHERE id_web IS NULL';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let baixas: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var baixa = data.rows.item(i);
                baixas.push(baixa);
              }
              return baixas;
            } else {
              return [];
            }
          }).catch(err => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllBkp', sql);
          });
      });
  }

  /*Fotos*/
  public getAllFotoToSinc(baixa_acao_id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM foto_baixa_acoes where (id_web is null OR sincronizado = "false") and baixa_acao_id = ?';
        var data = [baixa_acao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let fotos: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                fotos.push(data.rows.item(i));
              }
              return fotos;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllFotoToSinc', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public getAllFotoRenovacaoToSinc(renovacao_id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM fotos_acoes_renovacoes where (id_web is null OR sincronizado = ?) and renovacao_id = ?';
        var data = [false, renovacao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let fotos: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                fotos.push(data.rows.item(i));
              }
              return fotos;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getAllFotoRenovacaoToSinc', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public getFotos(baixa_acao_id) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT id, foto AS img FROM foto_baixa_acoes WHERE baixa_acao_id = ? AND deleted_at is null';
      let data = [baixa_acao_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let fotos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              fotos.push(data.rows.item(i));
            }
            return fotos;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'getFotos', sql, data);
          console.error('Falha ao consultar fotos da acao baixada', err, sql, data)
        });
    });
  }

  public insertFoto(foto) {
    let sql = 'INSERT INTO foto_baixa_acoes (mobile_key, foto, data, baixa_acao_id, sincronizado) VALUES (?,?,?,?,?)';
    let params = [
      foto.mobile_key,
      foto.img,
      this.now,
      foto.baixa_acao_id,
      false
    ];

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      return db.executeSql(sql, params).then((data) => {
        return data;
      }).catch(err => {
        console.error('falha salvar foto foto_baixa_acoes - provider', err);
      });
    });
  }

  public insertFotoRenovacao(foto) {
    let sql = 'INSERT INTO fotos_acoes_renovacoes (mobile_key, foto, data, renovacao_id, sincronizado) VALUES (?,?,?,?,?)';
    let params = [
      foto.mobile_key,
      foto.img,
      this.now,
      foto.baixa_acao_id,
      false
    ];

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      return db.executeSql(sql, params).then((data) => {
        return data;
      }).catch(err => {
        console.error('falha salvar foto foto_baixa_acoes - provider', err);
      });
    });
  }

  public ligaFotos(mobile_key, baixa_acao_id) {
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE foto_baixa_acoes SET baixa_acao_id = ? WHERE mobile_key = ?';
      let params = [baixa_acao_id, mobile_key];
      db.executeSql(sql, params).catch(err => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'ligaFotoBaixaAcao', sql, params);
        console.error('erro ao remover foto', err);
      })
    });

    /*
    *  Ao salvar a foto da renovação da ação, ainda não se sabe se é uma baixa ou uma renovação,
    *  desta forma fica mais facil adiciona nas duas e posteriosmente remover o caminho da foto não utilizada
    * */
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'DELETE FROM fotos_acoes_renovacoes WHERE mobile_key = ?';
      let params = [mobile_key];
      db.executeSql(sql, params).catch(err => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'ligaFotosRenovacao - remove foto da tablela de baixas', sql, params);
        console.error('erro ao remover foto', err);
      })
    })
  }

  public ligaFotosRenovacao(mobile_key, renovacao_id) {
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE fotos_acoes_renovacoes SET renovacao_id = ? WHERE mobile_key = ?';
      let params = [renovacao_id, mobile_key];
      db.executeSql(sql, params).catch(err => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'ligaFotosRenovacao', sql, params);
        console.error('erro ao remover foto', err);
      })
    });

    /*
    *  Ao salvar a foto da baixa, ainda não se sabe se é uma baixa ou uma renovação,
    *  desta forma fica mais facil adiciona nas duas e posteriosmente remover o caminho da foto não utilizada
    * */
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'DELETE FROM foto_baixa_acoes WHERE mobile_key = ?';
      let params = [mobile_key];
      db.executeSql(sql, params).catch(err => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'ligaFotosRenovacao - remove foto da tablela de baixas', sql, params);
        console.error('erro ao remover foto', err);
      })
    })
  }

  public deleteFoto(foto_id) {
    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        let sql = 'UPDATE foto_baixa_acoes SET deleted_at = ?, sincronizado = ? WHERE id = ?';
        let data = [moment().format('YYYY-MM-DD'), false, foto_id];
        db.executeSql(sql, data).then(() => {
          resolve()
        }).catch(err => {
          this.exceptionProvider.insert(err, 'fotos.ts', 'deleteFotoBaixaAcao', sql, data);
          console.error('erro ao remover foto', err);
          reject();
        })
      })
    });

    return promise
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaAcoesBaixadasLocal() {
    /*Atualiza Ações*/
    var promisse = new Promise((resolve, reject) => {
      let loading = this.messageService.showLoading('Atualizando Ações Baixadas...');
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL: any) => {
          this.http.get(`${SERVER_URL}/baixaacao`).subscribe((results: any) => {
            if (results) {
              for (let result of results) {
                let sql_up = 'update baixa_acoes SET acao_id_web = ?, resolvido = ?, data = ?, resposta = ? where id_web = ?;';
                let data_up = [result.acao_id, result.resolvido, result.data, result.resposta_formatada, result.id];
                db.executeSql(sql_up, data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO baixa_acoes (id_web, acao_id_web, resolvido, data, resposta) values (?, ?, ?, ?, ?)';
                    let data_ins = [result.id, result.acao_id, result.resolvido, result.data, result.resposta_formatada];
                    db.executeSql(sql_ins, data_ins).catch(err => {
                      this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'atualizaAcoesBaixadasLocal', sql_ins, data_ins);
                      reject();
                    });
                  }
                }).catch(err => {
                  this.exceptionProvider.insert(err, 'baixaAcoes.ts', 'atualizaAcoesBaixadasLocal', sql_up, data_up);
                  reject();
                })
              }
            }
            loading.dismiss();
            resolve();
          });
        });
      })
    });
    return promisse;
  }

  public enviaFotos(baixa_acao_id, baixa_acao_id_web) {
    var self = this;

    this.getAllFotoToSinc(baixa_acao_id).then((fotos: Array<any>) => {
      fotos.forEach(function (foto) {

        self.storage.get('access_token').then((access_token) => {
          let options: FileUploadOptions = {
            fileKey: 'foto',
            fileName: `baixa_acao_${baixa_acao_id_web}_foto_${foto.id}_img.jpg`,
            chunkedMode: false,
            httpMethod: 'POST',
            mimeType: "image/jpeg",
            params: foto,
            headers: {
              'Authorization': `Bearer ${access_token}`,
            }
          };

          self.storage.get('SERVER_URL').then((SERVER_URL: any) => {

            let url = `${SERVER_URL}/baixaacao/foto/${baixa_acao_id_web}`;
            let fileTransfer = self.fileTransfer.create();

            fileTransfer.upload(foto.foto, url, options).then((success: any) => {
              let response = JSON.parse(success.response);
              self.updateSincFoto(response.id, foto.id)
            });
          })
        });
      })
    });
  }

  public enviaFotosRenovacao(renovacao_id, renovacao_id_web) {
    var self = this;

    this.getAllFotoRenovacaoToSinc(renovacao_id).then((fotos: Array<any>) => {
      fotos.forEach(function (foto) {

        self.storage.get('access_token').then((access_token) => {
          let options: FileUploadOptions = {
            fileKey: 'foto',
            fileName: `renovacao_${renovacao_id_web}_foto_${foto.id}_img.jpg`,
            chunkedMode: false,
            httpMethod: 'POST',
            mimeType: "image/jpeg",
            params: foto,
            headers: {
              'Authorization': `Bearer ${access_token}`,
            }
          };

          self.storage.get('SERVER_URL').then((SERVER_URL: any) => {

            let url = `${SERVER_URL}/baixaacao/foto/renovacao/${renovacao_id_web}`;
            let fileTransfer = self.fileTransfer.create();

            fileTransfer.upload(foto.foto, url, options).then((success: any) => {
              let response = JSON.parse(success.response);
              self.updateSincFotoRenovacao(response.id, foto.id)
            }).catch(err => {
              console.error('falha ao enviar fotos da ação', err)
            });
          });
        });
      })
    });
  }

}
