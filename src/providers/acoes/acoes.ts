import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {Acao} from "../../class/Acao";
import {Storage} from "@ionic/storage";
import {AlertMessagesProvider} from "../alert-messages/alert-messages";
import {FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";
import * as moment from "moment";

@Injectable()
export class AcoesProvider extends Acao {
  private urlApi;
  protected now = moment().format('YYYY-MM-DD');

  constructor(
    public dbProvider: DatabaseProvider,
    private readonly http: HttpClient,
    private storage: Storage,
    private messageService: AlertMessagesProvider,
    private fileTransfer: FileTransfer,
    private events: Events,
    private exceptionProvider: DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL: any = null) => {
      this.urlApi = `${SERVER_URL}/acao`;
      if (SERVER_URL == null) {
        this.events.subscribe('url:changed', url => {
          if (url != null) {
            this.urlApi = `${url}/acao`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if (url) {
        this.urlApi = `${url}/acao`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiGetAll(lastUpdate) {
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiStore(values) {
    return this.http.post(this.urlApi, values)
  }

  public apiStoreBkp(values) {
    return this.http.post(`${this.urlApi}/upload/bkp`, values)
  }

  public apiUpdate(acao_id, values) {
    return this.http.patch(this.urlApi + '/' + acao_id, values)
  }

  public exportaDados(dados) {
    let url = this.urlApi;
    url = url.replace('acao', 'importacao');
    return this.http.post(url, dados);
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(acao: Acao, assinaturaProdutor) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'insert into acoes (id_web, propriedade_id, tipo_integracao_id, grupo_acao_id, ponto_acao_id, avaliacao_inicial, parametro_acao_id, situacao_id, o_que, como, quem, quando, considerar_claculos, com_baixa, foto, com_foto, mobile_key, pergunta_id, deleted_at, data_criacao, assinatura_produtor) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      let data = [
        acao.id_web,
        acao.propriedade_id,
        acao.tipo_integracao_id,
        acao.grupo_acao_id,
        acao.ponto_acao_id,
        acao.avaliacao_inicial,
        acao.parametro_acao_id,
        acao.situacao_id,
        acao.o_que,
        acao.como,
        acao.quem,
        acao.quando,
        acao.considerar_calculos ? 1 : 0,
        acao.com_baixa ? 1 : 0,
        acao.foto,
        acao.com_foto ? 1 : 0,
        acao.mobile_key,
        acao.pergunta_id,
        acao.deleted_at,
        acao.data_criacao,
        assinaturaProdutor,
      ];

      return db.executeSql(sql, data).then(data => {
        return data;
      }).catch(err => {
        console.error('falha ao salvar acao', err, sql, data);
        this.exceptionProvider.insert(err, 'acoes.ts', 'insert', sql, data)
      });
    })
  }

  public update(acao: Acao) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes set propriedade_id = ?, tipo_integracao_id = ?, grupo_acao_id = ?, ponto_acao_id = ?, avaliacao_inicial = ?, parametro_acao_id = ?, situacao_id = ?, o_que = ?, como = ?, quem = ?, quando = ?, com_baixa = ?, foto = ?, com_foto = ?, deleted_at = ?, data_criacao = ?, assinatura_produtor = ? where id_web = ?';

        let data = [
          acao.propriedade_id,
          acao.tipo_integracao_id,
          acao.grupo_acao_id,
          acao.ponto_acao_id,
          acao.avaliacao_inicial,
          acao.parametro_acao_id,
          acao.situacao_id,
          acao.o_que,
          acao.como,
          acao.quem,
          acao.quando,
          acao.com_baixa ? 1 : 0,
          acao.foto,
          acao.com_foto ? 1 : 0,
          acao.deleted_at,
          acao.data_criacao,
          acao.assinatura_produtor,
          acao.id //quando vem da web o id = id_web do mobile
        ];

        return db.executeSql(sql, data).catch(err => {
          console.error('falha ao atualizar ação', err);
          console.error('falha ao atualizar ação - 2 ', sql, data);
          this.exceptionProvider.insert(err, 'acoes.ts', 'update', sql, data)
        });
      })
  }

  public updateGeolocation(lat, long, id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes set latitude=?, longitude=? where id = ?';

        let data = [
          lat,
          long,
          id,
        ];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'updateGeolocation', sql, data)
        })
      })
  }

  public updateAssinatura(assinatura, propriedade_id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = "update acoes set assinatura_produtor=? WHERE assinatura_produtor IS NULL " +
			" AND data_criacao BETWEEN date('now','-30 days') AND date('now') " +
			" AND propriedade_id = ?";

        let data = [
			assinatura,
			propriedade_id,
        ];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'updateAssinatura', sql, data)
        })
      })
  }

  public editaAcao(acao: Acao) {

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes set grupo_acao_id=?, ponto_acao_id=?, parametro_acao_id=?, o_que=?, como=?, quem=?, quando=?, sincronizado=? where id=?';

        let data = [
          acao.grupo_acao_id,
          acao.ponto_acao_id,
          acao.parametro_acao_id,
          acao.o_que,
          acao.como,
          acao.quem,
          acao.quando,
          false, // Sempre será false para sinalizar que deve ser atualizado em caso de edição
          acao.id //quando vem da web o id = id_web do mobile
        ];

        return db.executeSql(sql, data).catch(err => {
          console.error('falha ao editar acao', err);
          this.exceptionProvider.insert(err, 'acoes.ts', 'editaAcao', sql, data)
        });
      })
  }

  public baixaAcao(id_acao) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'update acoes set com_baixa = ? where id = ?';

      let data = [1, id_acao];

      return db.executeSql(sql, data).catch(err => {
        this.exceptionProvider.insert(err, 'acoes.ts', 'baixaAcao', sql, data)
      });
    });
  }

  public updateSinc(id_web: number, id_mobile: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update acoes set sincronizado=?, id_web=? where id=?';

        let data = [true, id_web, id_mobile];

        return db.executeSql(sql, data).catch(err => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'updateSinc', sql, data)
        });
      });
  }

  public updateSincFoto(id_web: number, id_mobile: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'update foto_acoes set sincronizado=?, id_web=? where id=?';

        let data = [true, id_web, id_mobile];

        return db.executeSql(sql, data).then((data) => {
        }).catch(err => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'updateSincFoto', sql, data)
        });
      });
  }

  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = '\tSELECT ac.id,\n' +
          '\t\tac.id_web,\n' +
          '\t\tac.propriedade_id,\n' +
          '\t\tac.tipo_integracao_id,\n' +
          '\t\tac.grupo_acao_id,\n' +
          '\t\tac.ponto_acao_id,\n' +
          '\t\tac.avaliacao_inicial,\n' +
          '\t\tac.parametro_acao_id,\n' +
          '\t\tac.situacao_id,\n' +
          '\t\tac.o_que,\n' +
          '\t\tac.como,\n' +
          '\t\tac.quem,\n' +
          '\t\tCOALESCE((select data_prevista from acoes_renovacoes where (acao_id = ac.id or acao_id = ac.id_web) order by data_prevista desc limit 1), ac.quando) AS quando,\n' +
          '\t\tac.com_baixa,\n' +
          '\t\tac.produtor_concat_propriedade,\n' +
          '\t\tCOALESCE(produtor_concat_propriedade,(prod.nome || " - " || prop.descricao)) AS produtor_propriedade ,\n' +
          '\t\tCASE WHEN (select data_prevista from acoes_renovacoes where acao_id = ac.id order by data_prevista desc limit 1) > 0 THEN 1 ELSE 0 END AS renovada\n' +
          '\tFROM acoes AS ac\n' +
          '\t\tLEFT JOIN propriedades AS prop\n' +
          '\t\t\tON prop.id = ac.propriedade_id\n' +
          '\t\tLEFT JOIN produtores AS prod\n' +
          '\t\t\tON prod.id = prop.produtor_id\n' +
          '\tWHERE ac.id = ?\n' +
          '\tAND (ac.deleted_at == "null" OR ac.deleted_at IS NULL);';

        let data = [id];

        return db.executeSql(sql, data).then((data: any) => {
          if (data.rows.length > 0) {
            return data.rows.item(0);
          }

          return null;
        }).catch(err => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'get', sql, data)
        });
      })
  }

  public existIdWeb(id_web: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from acoes where id_web = ?';
        let data = [id_web];

        return db.executeSql(sql, data)
          .then((data: any) => {
            return data.rows.length > 0;
          }).catch(err => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'existIdWeb', sql, data)
          });
      })
  }

  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = '\tSELECT ac.id,\n' +
          '\t\tac.id_web,\n' +
          '\t\tac.propriedade_id,\n' +
          '\t\tac.tipo_integracao_id,\n' +
          '\t\tac.grupo_acao_id,\n' +
          '\t\tac.ponto_acao_id,\n' +
          '\t\tac.avaliacao_inicial,\n' +
          '\t\tac.parametro_acao_id,\n' +
          '\t\tac.situacao_id,\n' +
          '\t\tac.o_que,\n' +
          '\t\tac.como,\n' +
          '\t\tac.quem,\n' +
          '\t\tCOALESCE((SELECT data_prevista \n' +
          '\t\t\tFROM acoes_renovacoes \n' +
          '\t\t\tWHERE acao_id = CASE WHEN ac.id_web IS NULL \n' +
          '\t\t\t\t\t\t\tTHEN ac.id \n' +
          '\t\t\t\t\t\t\tELSE ac.id_web \n' +
          '\t\t\t\t\t\t\tEND \n' +
          '\t\t\tORDER BY data_prevista DESC \n' +
          '\t\t\tLIMIT 1),\n' +
          '\t\tac.quando\n' +
          '\t\t) AS quando,\n' +
          '\t\tac.com_baixa,\n' +
          '\t\tac.produtor_concat_propriedade,\n' +
          '\t\tCOALESCE(produtor_concat_propriedade,(prod.nome || " - " || prop.descricao)) AS produtor_propriedade ,\n' +
          '\t\tCASE WHEN (SELECT data_prevista \n' +
          '\t\t\t\tFROM acoes_renovacoes \n' +
          '\t\t\t\tWHERE acao_id = CASE WHEN ac.id_web IS NULL \n' +
          '\t\t\t\t\t\t\t\tTHEN ac.id \n' +
          '\t\t\t\t\t\t\t\tELSE ac.id_web \n' +
          '\t\t\t\t\t\t\t\tEND \n' +
          '\t\t\t\tORDER BY data_prevista DESC \n' +
          '\t\t\t\tLIMIT 1) > 0 \n' +
          '\t\t\t\tTHEN 1 \n' +
          '\t\t\t\tELSE 0 \n' +
          '\t\t\t\tEND AS renovada\n' +
          '\tFROM acoes AS ac\n' +
          '\t\tLEFT JOIN propriedades AS prop\n' +
          '\t\t\tON prop.id = ac.propriedade_id\n' +
          '\t\tLEFT JOIN produtores AS prod\n' +
          '\t\t\tON prod.id = prop.produtor_id\n' +
          '\tWHERE com_baixa = 0\n' +
          '\t\tAND (ac.deleted_at == "null" OR ac.deleted_at IS NULL)\n' +
          '\tORDER BY ac.id DESC';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let acoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var acao = data.rows.item(i);
                acoes.push(acao);
              }
              return acoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getAll', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      })
  }

  public getAllToSinc() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT ac.*, ' +
          '  ac.avaliacao_inicial as avaliacao_inicial_id_mobile, ' +
          '  av.id_web as avaliacao_id_web, ' +
          '  av.mobile_key as avaliacao_mobile_key ' +
          ' FROM acoes as ac ' +
          '  LEFT JOIN avaliacoes as av ' +
          '   ON av.id = ac.avaliacao_inicial ' +
          ' WHERE ac.id_web IS NULL OR ac.sincronizado == "false"';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let acoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var acao = data.rows.item(i);
                acoes.push(acao);
              }
              return acoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getAllToSinc', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public getAllFotoToSinc(acao_id) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM foto_acoes where (id_web is null OR sincronizado = "false") and acao_id = ?';
        var data = [acao_id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let fotos: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                fotos.push(data.rows.item(i));
              }
              return fotos;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getAllFotoToSinc', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public getAllAcaoComPerguntaToSinc() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM acoes WHERE id_web is null AND pergunta_id is not null ';
        var data: any[] = [];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let acoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var acao = data.rows.item(i);
                acoes.push(acao);
              }
              return acoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getAllAcaoComPerguntaToSinc', sql, null);
            console.error('Falha ao executar sql acoes com perguntas', err)
          });
      });
  }

  public getLatestId() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select id from acoes order by id desc limit 1';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);


              return item.id;
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getLatestId', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public findAcoes(datas: any = null, produtor: string = null) {
    let filtro_data = '';
    let filtro_nome = '';
    if (datas) {
      let from = datas.from.string;
      let to = datas.to.string;
      filtro_data = ` AND (quando BETWEEN '${from}' AND '${to}') `;
    }

    if (produtor == null) {
      produtor = '';
    }

    if (produtor.trim() != '') {
      filtro_nome = ` AND (produtor_propriedade like '%${produtor}%') `;
    }

    let ordena = ` ORDER BY produtor_propriedade `;

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT ac.*,  ' +
          ' COALESCE(produtor_concat_propriedade,(prod.nome || " - " || prop.descricao)) AS produtor_propriedade ' +
          'FROM acoes AS ac   ' +
          ' LEFT JOIN propriedades AS prop ' +
          '  ON prop.id = ac.propriedade_id ' +
          ' LEFT JOIN produtores AS prod ' +
          '  ON prod.id = prop.produtor_id ' +
          'WHERE com_baixa = 0  ' +
          ' AND (ac.deleted_at == "null" OR ac.deleted_at IS NULL) ' +
          filtro_data + filtro_nome + ordena;

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let acoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var acao = data.rows.item(i);
                acoes.push(acao);
              }
              return acoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'findAcoes', sql, null);
            console.error('Falha ao executar sql acoes', err)
          });
      });
  }

  public getAllBkp() {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT * FROM acoes WHERE id_web IS NULL';
      return db.executeSql(sql, []).then((data: any) => {
        if (data.rows.length > 0) {
          let acoes: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var acao = data.rows.item(i);
            acoes.push(acao);
          }
          return acoes;
        } else {
          return [];
        }
      }).catch(err => {
        this.exceptionProvider.insert(err, 'acoes.ts', 'getAllBkp', sql, null);
        console.error('Erro no sql ao consultar ações para bkp', err, sql);
      })
    })
  }

  public getAllPerguntaAcaobkp() {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT * FROM checklists_perguntas_acoes';

      return db.executeSql(sql, []).then((data) => {
        if (data.rows.length > 0) {
          let pergunta_acao: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var resposta = data.rows.item(i);
            pergunta_acao.push(resposta);
          }
          return pergunta_acao;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'acoes.ts', 'getAllPerguntaAcaobkp', sql);
        console.error('Falha ao executar sql perguntas acoes', err)
      });
    });
  }

  public getByDate(initialDate, finalDate) {

    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT ac.id, prop.id AS propriedade_id, "construct" AS icon, 1 AS type, "Ação" as title, ' +
          '  COALESCE(produtor_concat_propriedade,(prod.nome || " - " || prop.descricao)) AS location,  ' +
          '  ac.o_que AS message,  ' +
          '  COALESCE((select data_prevista from acoes_renovacoes where (acao_id = ac.id or acao_id = ac.id_web) order by data_prevista desc limit 1), ac.quando) AS date_limit ' +
          ' FROM acoes AS ac  ' +
          '  LEFT JOIN propriedades AS prop  ' +
          '   ON prop.id = ac.propriedade_id  ' +
          '  LEFT JOIN produtores AS prod  ' +
          '   ON prod.id = prop.produtor_id  ' +
          ' WHERE date_limit BETWEEN ? AND ? ' +
          '  AND (com_baixa == 0 OR com_baixa == "false")  ' +
          '  AND (ac.deleted_at == "null" OR ac.deleted_at IS NULL)';

        let params = [initialDate, finalDate];

        return db.executeSql(sql, params)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let acoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var acao = data.rows.item(i);
                acoes.push(acao);
              }
              return acoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'acoes.ts', 'getByDate', sql, null);
            console.error('Falha ao executar sql acoes getByDate', err)
          });
      })
  }

  public getAllByPropriedade(propriedade_id) {
    let sql = '\tSELECT prod.nome AS produtor,\n' +
		'\t\tgpa.descricao AS grupo, \n' +
		'\t\tpta.descricao AS ponto, \n' +
		'\t\tac.o_que, \n' +
		'\t\tac.como, \n' +
		'\t\tac.quem, \n' +
		'\t\tac.quando, \n' +
		'\t\tac.assinatura_produtor, \n' +
		'\t\tsa.descricao AS situacao\n' +
		'\tFROM acoes AS ac \n' +
		'\t\tLEFT JOIN situacao_acoes as sa \n' +
		'\t\t\tON ac.situacao_id = sa.id\n' +
		'\t\tLEFT JOIN propriedades as prop\n' +
		'\t\t\tON ac.propriedade_id = prop.id\n' +
		'\t\tLEFT JOIN produtores as prod\n' +
		'\t\t\tON prop.produtor_id = prod.id\n' +
		'\t\tLEFT JOIN grupo_acoes as gpa\n' +
		'\t\t\tON gpa.id = ac.grupo_acao_id\n' +
		'\t\tLEFT JOIN ponto_acoes as pta\n' +
		'\t\t\tON pta.id = ac.ponto_acao_id\n' +
		'\tWHERE ac.propriedade_id = ? \n' +
		'\t\tAND ac.com_baixa = 0\n' +
		'\t\tAND ac.deleted_at IS NULL';
    let params = [propriedade_id];

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      return db.executeSql(sql, params).then((data: any) => {
        let acoes: any[] = [];
        for (var i = 0; i < data.rows.length; i++) {
          var acao = data.rows.item(i);
          acoes.push(acao);
        }
        return acoes;
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'acoes.ts', 'getAllByPropriedade', sql, null);
        console.error('Falha ao executar sql acoes getAllByPropriedade', err)
      });
    })
  }

  /*Fotos*/
  public insertFoto(foto) {
    let sql = 'INSERT INTO foto_acoes (mobile_key, foto, data, acao_id, sincronizado) VALUES (?,?,?,?,?)';
    let params = [
      foto.mobile_key,
      foto.img,
      this.now,
      foto.acao_id,
      false
    ];

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      return db.executeSql(sql, params).then((data) => {
        return data;
      }).catch(err => {
        console.error('falha salvar foto acao - provider', err);
      });
    });
  }

  public deleteFoto(foto_id) {

    let promise = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        let sql = 'UPDATE foto_acoes SET deleted_at = ?, sincronizado = ? WHERE id = ?';
        let data = [moment().format('YYYY-MM-DD'), false, foto_id];
        db.executeSql(sql, data).then(() => {
          resolve()
        }).catch(err => {
          this.exceptionProvider.insert(err, 'fotos.ts', 'deleteFotoAcao', sql, data);
          console.error('erro ao remover foto', err);
          reject();
        })
      })
    });

    return promise
  }

  public ligaFotos(mobile_key, acao_id) {
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'UPDATE foto_acoes SET acao_id = ? WHERE mobile_key = ?';
      let params = [acao_id, mobile_key];
      db.executeSql(sql, params).catch(err => {
        this.exceptionProvider.insert(err, 'fotos.ts', 'ligaFotoAcao', sql, params);
        console.error('erro ao remover foto', err);
      })
    })
  }

  /*Fotos*/
  public getFotos(acao_id) {
    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let sql = 'SELECT id, foto AS img FROM foto_acoes WHERE acao_id = ? AND deleted_at is null';
      let data = [acao_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let fotos: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              fotos.push(data.rows.item(i));
            }
            return fotos;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'acoes.ts', 'getFotos', sql, data);
          console.error('Falha ao consultar fotos da propriedade', err, sql, data)
        });
    });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaAcoesLocal() {
    /*Atualiza Ações*/
    var promisse = new Promise((resolve, reject) => {
      this.dbProvider.getDB().then((db: SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL: any) => {
          this.http.get(`${SERVER_URL}/acao`).subscribe((results: any) => {
            let loading = this.messageService.showLoading('Atualizando Ações...');
            if (results) {
              for (let result of results) {
                let sql_up = 'update acoes SET propriedade_id = ?, tipo_integracao_id = ?, grupo_acao_id = ?, ponto_acao_id = ?, avaliacao_inicial = ?, parametro_acao_id = ?, situacao_id = ?, o_que = ?, como = ?, quem = ?, quando = ?, considerar_claculos = ?, com_baixa = ?, produtor_concat_propriedade = ?, deleted_at = ? where id_web = ?;';
                let data_up = [result.propriedade_id, result.tipo_integracao_id, result.grupo_acao_id, result.ponto_acao_id, result.avaliacao_inicial_id, result.parametro_acao_id, result.situacao_id, result.o_que, result.como, result.quem, result.quando, result.considerar_calculos, result.com_baixa, result.propriedade.produtor_concat_propriedade, result.deleted_at, result.id];
                db.executeSql(sql_up, data_up).then((data: any) => {
                  if (data.rowsAffected < 1) {
                    let sql_ins = 'INSERT OR IGNORE INTO acoes (id_web, propriedade_id, tipo_integracao_id, grupo_acao_id, ponto_acao_id, avaliacao_inicial, parametro_acao_id, situacao_id, o_que, como, quem, quando, considerar_claculos, com_baixa, produtor_concat_propriedade, deleted_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                    let data_ins = [result.id, result.propriedade_id, result.tipo_integracao_id, result.grupo_acao_id, result.ponto_acao_id, result.avaliacao_inicial_id, result.parametro_acao_id, result.situacao_id, result.o_que, result.como, result.quem, result.quando, result.considerar_calculos, result.com_baixa, result.propriedade.produtor_concat_propriedade, result.deleted_at];
                    db.executeSql(sql_ins, data_ins).catch(err => {
                      this.exceptionProvider.insert(err, 'acoes.ts', 'findAcoes', sql_ins, data_ins);
                      reject();
                    });
                  }
                }).catch(err => {
                  this.exceptionProvider.insert(err, 'acoes.ts', 'findAcoes', sql_up, data_up);
                  reject();
                });
              }
            }
            loading.dismiss();
            resolve();
          });
        });

      })
    });
    return promisse;
  }

  public enviaFotos(acao_id, acao_id_web) {
    var self = this;

    this.getAllFotoToSinc(acao_id).then((fotos: Array<any>) => {
      fotos.forEach(function (foto) {

        self.storage.get('access_token').then((access_token) => {
          let options: FileUploadOptions = {
            fileKey: 'foto',
            fileName: `acao_${foto.acao_id_web}_foto_${foto.id}_img.jpg`,
            chunkedMode: false,
            httpMethod: 'POST',
            mimeType: "image/jpeg",
            params: foto,
            headers: {
              'Authorization': `Bearer ${access_token}`,
            }
          };

          self.storage.get('SERVER_URL').then((SERVER_URL: any) => {

            let url = `${SERVER_URL}/acao/foto/${acao_id_web}`;
            let fileTransfer = self.fileTransfer.create();

            fileTransfer.upload(foto.foto, url, options).then((success: any) => {
              let response = JSON.parse(success.response);

              self.updateSincFoto(response.id, foto.id)
            });
          })
        });
      })
    });
  }

}
