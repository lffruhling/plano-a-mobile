import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {GrupoAcao} from "../../class/GrupoAcao";
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class GrupoAcoesProvider extends GrupoAcao{
  private urlApi;

  constructor( public dbProvider: DatabaseProvider,
               public http: HttpClient,
               private storage: Storage,
               private events: Events,
               private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}/grupos?last_update=${lastUpdate}`);
  }

  atualizaGruposLocal(){

    /*Atualiza Grupos das Ações*/
    this.dbProvider.getDB().then((db:SQLiteObject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
        this.http.get(`${SERVER_URL}/grupos`).subscribe((results:any) => {
          if(results) {

            let dados = '';
            let count = results.length;

            for (let result of results) {
              dados += `(${result.id}, "${result.descricao}", ${result.ativo}, "${result.deleted_at}")`;
              if (count-- > 1) {
                dados += ','
              }
            }
            let sql = `REPLACE INTO grupo_acoes (id, descricao, ativo, deleted_at) values ${dados}`;

            db.sqlBatch([
              [sql, []],
            ]).catch(err => {
              this.exceptionProvider.insert(err, 'grupoAcoes.ts','atualizaGruposLocal', sql, null);
              console.error('Erro ao incluir grupo_acoes', err, sql)
            });
          }
        });
      });
    });
  }

    getGruposPorIntegracao(tipo_integracao_id) {
        return this.http.get(`${this.urlApi}/acao/grupos?tipo_integracao_id=${tipo_integracao_id}`);
    }

    public insert(grupoAcao: GrupoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'insert into grupo_acoes (id, descricao, ativo, deleted_at) values (?,?,?,?)';
                let data = [
                    grupoAcao.id,
                    grupoAcao.descricao,
                    grupoAcao.ativo,
                    grupoAcao.deleted_at
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'grupoAcoes.ts','getGruposPorIntegracao', sql, data);
                      console.error('Falha ao executar sql grupo_acoes', err)
                    });

            });
    }

    public update(grupoAcao: GrupoAcao){
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) =>{
                let sql = 'update grupo_acoes set descricao = ?, ativo = ?, deleted_at = ? where id = ?';
                let data = [
                    grupoAcao.descricao,
                    grupoAcao.ativo,
                    grupoAcao.deleted_at,
                    grupoAcao.id
                ];

                return db.executeSql(sql, data)
                    .catch((err) => {
                      this.exceptionProvider.insert(err, 'grupoAcoes.ts','update', sql, data);
                      console.error('Falha ao executar sql grupo_acoes', err)
                    });
            });
    }

  public find(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from grupo_acoes where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              return data.rows.item(0);
            }

            return null;
          })
          .catch((err) => {
            this.exceptionProvider.insert(err, 'grupoAcoes.ts', 'find', sql, data);
            console.error('Falha ao executar sql grupo_acoes', err)
          });
      });
  }

    public getAll(ativo: boolean, descricao: string = null) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'SELECT * FROM grupo_acoes where ativo = ? AND (deleted_at == "null" OR deleted_at IS NULL)';
                var data: any[] = [ativo ? 1 : 0];

                // filtrando pelo nome
                if (descricao) {
                    sql += ' AND descricao like ?'
                    data.push('%' + descricao + '%');
                }

                return db.executeSql(sql, data)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let grupoAcoes: any[] = [];
                            for (var i = 0; i < data.rows.length; i++) {
                                var grupoAcao = data.rows.item(i);
                                grupoAcoes.push(grupoAcao);
                            }
                            return grupoAcoes;
                        } else {
                            return [];
                        }
                    }).catch((err) => {
                      this.exceptionProvider.insert(err, 'grupoAcoes.ts','getAll', sql, data);
                      console.error('Falha ao executar sql grupo_acoes', err);
                  });
            });
    }

    public getGruposPorIntegracaoLocal(integracao_id){
      return this.dbProvider.getDB().then((db:SQLiteObject) => {
        let sql = 'SELECT DISTINCT(ga.id),  ' +
          ' ga.descricao  ' +
          'FROM acao_parametros AS ap  ' +
          ' LEFT JOIN grupo_acoes AS ga  ' +
          '  ON ga.id = ap.grupo_acao_id  ' +
          'WHERE (ap.tipo_integracao_id = ?  OR ap.tipo_integracao_id IS NULL)' +
          ' AND ga.ativo = 1  ' +
          ' AND (ga.deleted_at == "null" OR ga.deleted_at IS NULL);';

        var data: any[] = [integracao_id];

        return db.executeSql(sql, data).then( (data:any) =>{
          if (data.rows.length > 0) {
            let grupoAcoes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var grupoAcao = data.rows.item(i);
              grupoAcoes.push(grupoAcao);
            }
            return grupoAcoes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'grupoAcoes.ts','getGruposPorIntegracaoLocal', sql, data);
          console.error('Falha ao executar sql grupo_acoes', err)
        });
      });
    }

}
