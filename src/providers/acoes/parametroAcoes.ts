import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {ParametroAcao} from "../../class/ParametroAcao";

import {Events, LoadingController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {DbExceptionProvider} from "../db-exception/db-exception";

@Injectable()
export class ParametroAcoesProvider extends ParametroAcao{
  private urlApi;

  constructor(
      public dbProvider: DatabaseProvider,
      public http: HttpClient,
      private readonly loadingCtrl: LoadingController,
      private storage: Storage,
      private events:Events,
      private exceptionProvider:DbExceptionProvider,
  ) {
    super();
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAll(lastUpdate){
    return this.http.get(`${this.urlApi}/parametros?last_update=${lastUpdate}`);
  }

  getParametrosPorPonto(ponto_id) {
      return this.http.get(`${this.urlApi}/parametros?ponto_id=${ponto_id}`);
  }

  atualizaParametroAcoesLocal(){
    /*Atualiza Pontos das Ações*/
    this.dbProvider.getDB().then((db:SQLiteObject) => {
      this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
        this.http.get(`${SERVER_URL}/parametros`).subscribe((results:any) => {
          if(results) {
            let dados = '';
            let count = results.length;

            for (let result of results) {
              let descricao = result.descricao.replace(/'/g,'"');
              let valor_padrao_formatado = result.valor_padrao_formatado.replace(/'/g,'"');

              dados += `(${result.id}, ${result.parametro_pag_id}, ${result.grupo_acao_id}, ${result.ponto_acao_id}, ${result.tipo_integracao_id}, ${result.tipo_info_id}, '${valor_padrao_formatado}', '${descricao}', '${result.deleted_at}')`;
              if (count-- > 1) {
                dados += ','
              }
            }
            let sql = `REPLACE INTO acao_parametros (id, parametro_pag_id, grupo_acao_id, ponto_acao_id, tipo_integracao_id, tipo_info_id, informacao_padrao, descricao, deleted_at) values ${dados}`;

            db.sqlBatch([
              [sql, []],
            ]).catch(err => {
              this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'atualizaParametroAcoesLocal', sql, null);
              console.error('Erro ao incluir parametro_pagamento_acoes', err,sql)
            });
          }
        });
      });

    });
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public insert(parametroAcao: ParametroAcao){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'insert into acao_parametros (id, parametro_pag_id, grupo_acao_id, ponto_acao_id, tipo_integracao_id, tipo_info_id, informacao_padrao, descricao, deleted_at) values (?,?,?,?,?,?,?,?,?)';
              let data = [
                  parametroAcao.id,
                  parametroAcao.parametro_pag_id,
                  parametroAcao.grupo_acao_id,
                  parametroAcao.ponto_acao_id,
                  parametroAcao.tipo_integracao_id,
                  parametroAcao.tipo_info_id,
                  parametroAcao.informacao_padrao,
                  parametroAcao.descricao,
                  parametroAcao.deleted_at,
              ];

              return db.executeSql(sql, data)
                  .catch((err) => {
                    this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'insert', sql, data);
                    console.error('Falha ao executar sql acao_parametros', err)
                  });

          });
  }

  public update(parametroAcao: ParametroAcao){
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) =>{
              let sql = 'update acao_parametros set parametro_pag_id = ?, grupo_acao_id = ?, ponto_acao_id = ?, tipo_integracao_id = ?, tipo_info_id = ?, informacao_padrao = ?, descricao = ?, deleted_at = ? where id = ?';
              let data = [
                  parametroAcao.parametro_pag_id,
                  parametroAcao.grupo_acao_id,
                  parametroAcao.ponto_acao_id,
                  parametroAcao.tipo_integracao_id,
                  parametroAcao.tipo_info_id,
                  parametroAcao.informacao_padrao,
                  parametroAcao.descricao,
                  parametroAcao.deleted_at,
                  parametroAcao.id
              ];

              return db.executeSql(sql, data)
                  .catch((err) => {
                    this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'update', sql, data);
                    console.error('Falha ao executar sql acao_parametros', err)
                  });
          });
  }

  public find(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from acao_parametros where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {

              return data.rows.item(0);
            }

            return null;
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'parametrosAcoes.ts', 'get', sql, data);
            console.error('Falha ao executar sql acao_parametros', err)
          });
      });
  }

  public getAll(propriedade_id:string, integracao_id:string = null) {
      return this.dbProvider.getDB()
          .then((db: SQLiteObject) => {
              let sql = 'SELECT ap.*, ' +
                ' pa.descricao AS ponto_descricao,  ' +
                ' ga.descricao AS grupo_descricao  ' +
                'FROM acao_parametros AS ap  ' +
                ' LEFT JOIN ponto_acoes AS pa  ' +
                '  ON pa.id = ap.ponto_acao_id  ' +
                ' LEFT JOIN grupo_acoes AS ga  ' +
                '  ON ga.id = ap.grupo_acao_id  ' +
                'WHERE (tipo_integracao_id = ? OR tipo_integracao_id IS NULL)  ' +
                ' AND NOT EXISTS (SELECT parametro_id FROM avaliacoes AS av  ' +
                '      WHERE ap.id = av.parametro_id AND av.propriedade_id == ?) ' +
                ' AND (ap.deleted_at == "null" OR ap.deleted_at IS NULL);';

              var parameters: any[] = [];

              parameters.push(integracao_id, propriedade_id);

              return db.executeSql(sql, parameters)
                  .then((data: any) => {
                      if (data.rows.length > 0) {
                          let parametrosAcoes: any[] = [];
                          for (var i = 0; i < data.rows.length; i++) {
                              var parametroAcao = data.rows.item(i);
                              parametrosAcoes.push(parametroAcao);
                          }
                          return parametrosAcoes;
                      } else {
                          return [];
                      }
                  }).catch((err) => {
                    this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'getAll', sql, null);
                    console.error('Falha ao executar sql acao_parametros', err)
                });
          });
  }

  public getParametrosPorPontoLocal(ponto_id) {
      return this.dbProvider.getDB().then((db:SQLiteObject) => {
        let sql = 'select id, descricao, informacao_padrao from acao_parametros where ponto_acao_id = ? AND (deleted_at == "null" OR deleted_at IS NULL)';
        var data: any[] = [ponto_id];

        return db.executeSql(sql, data).then( (data:any) =>{
          if (data.rows.length > 0) {
            let parametrosAcoes: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              var parametroAcao = data.rows.item(i);
              parametrosAcoes.push(parametroAcao);
            }
            return parametrosAcoes;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'getParametrosPorPontoLocal', sql, data);
          console.error('Falha ao executar sql pontos_acoes', err)
        });
      });
    }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaParametrosLocal(){
    /*Atualiza Parametros Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.storage.get('SERVER_URL').then((SERVER_URL:any) => {
          this.http.get(`${SERVER_URL}/parametros`).subscribe((results:any) => {
            let loading = this.showLoading('Atualizando Parâmetros das Ações...');
            if(results){
              for (let result of results){
                let sql_up = 'update acao_parametros SET parametro_pag_id = ?, grupo_acao_id= ?, ponto_acao_id = ?, tipo_integracao_id = ?, tipo_info_id = ?, informacao_padrao = ?, descricao = ? where id = ?;';
                let data_up = [result.parametro_pag_id, result.grupo_acao_id, result.ponto_acao_id, result.tipo_integracao_id, result.tipo_info_id, result.valor_padrao_formatado, result.descricao, result.id];
                db.executeSql(sql_up,data_up).then((data:any) =>{
                  if(data.rowsAffected < 1){
                    let sql_ins = 'INSERT OR IGNORE INTO acao_parametros (id, parametro_pag_id, grupo_acao_id, ponto_acao_id, tipo_integracao_id, tipo_info_id, informacao_padrao, descricao) values (?, ?, ?, ?, ?, ?, ?, ?)';
                    let data_ins = [result.id, result.parametro_pag_id, result.grupo_acao_id, result.ponto_acao_id, result.tipo_integracao_id, result.tipo_info_id, result.valor_padrao_formatado, result.descricao];
                    db.executeSql(sql_ins,data_ins).catch(err=>{
                      this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'atualizaParametrosLocal', sql_ins, data_ins);
                    })
                  }
                }).catch(err=>{
                  this.exceptionProvider.insert(err,'parametrosAcoes.ts', 'atualizaParametrosLocal', sql_up, data_up);
                  reject();
                });
              }
            }
            loading.dismiss();
            resolve();
          });
        });
      });
    });
    return promisse;
  }

  showLoading(msg:string){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: msg
    });

    loading.present();

    return loading;
  }

}
