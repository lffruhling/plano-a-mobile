import { Injectable } from '@angular/core';
import {Platform} from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class CameraProvider {

  constructor(private camera: Camera, public platform: Platform) {}

  public capturarFoto(isGallery:boolean = false, width:number = 1100, height:number = 900, frontCamera:boolean = false, isBase64:boolean = false){
    let sourceType, direcition, destinationType;
    let options:CameraOptions;

    if(isGallery){
      sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }else{
      sourceType = this.camera.PictureSourceType.CAMERA;
    }

    if(frontCamera){
      direcition = this.camera.Direction.FRONT
    }else{
      direcition = this.camera.Direction.BACK
    }

    if(isBase64){
      destinationType = this.camera.DestinationType.DATA_URL;
    }else{
      destinationType = this.camera.DestinationType.FILE_URI;
    }

    options = {
      quality: 75,
      targetWidth:width,
      targetHeight:height,
      destinationType: destinationType,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      cameraDirection: direcition,
    };

    console.log('options', options);

    return this.camera.getPicture(options)
  }

}
