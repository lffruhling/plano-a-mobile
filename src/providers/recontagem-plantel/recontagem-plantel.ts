import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Storage} from "@ionic/storage";
import {Events} from "ionic-angular";
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {DbExceptionProvider} from "../db-exception/db-exception";


@Injectable()
export class RecontagemPlantelProvider {
  private urlApi;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private events:Events,
    private dbProvider: DatabaseProvider,
    private exceptionProvider:DbExceptionProvider,
  ) {
    this.storage.get('SERVER_URL').then((SERVER_URL:any = null) => {
      this.urlApi = `${SERVER_URL}/recontagem/plantel`;
      if(SERVER_URL == null){
        this.events.subscribe('url:changed', url => {
          if(url != null){
            this.urlApi = `${url}/recontagem/plantel`;

          }
        });
      }
    });

    this.events.subscribe('url:changed', url => {
      if(url){
        this.urlApi = `${url}/recontagem/plantel`;
      }
    });
  }

  /*
  * ############################
  * API - Métodos
  * ###############
  * */
  public apiGetAllIntervalosCelasUteis(lastUpdate){
    return this.http.get(`${this.urlApi}/intervalos/celas/uteis?last_update=${lastUpdate}`);
  }

  public apiGetAllRecontagemPlantel(lastUpdate){
    return this.http.get(`${this.urlApi}?last_update=${lastUpdate}`);
  }

  public apiStore(recontagem) {
    return this.http.patch(`${this.urlApi}/${recontagem.id}`, recontagem);
  }


  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public getAll() {
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT rcp.id, \n' +
        '\trcp.data_recontagem,\n' +
        '\t(prod.nome || " - " || prop.descricao) as produtor,\n' +
        '\tgp.descricao AS galpao\n' +
        '\tFROM recontagem_planteis AS rcp\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON rcp.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON gp.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\tWHERE data_recontagem IS NOT NULL\n' +
        '\t\tAND rcp.deleted_at IS NULL \n' +
        '\tORDER BY rcp.data_recontagem DESC, prod.nome';
      let params = [];

      return db.executeSql(sql, params)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let recontagens: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              recontagens.push(data.rows.item(i));
            }
            return recontagens;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getAll',sql, params);
          console.error('Falha ao consultar recontagens para relatorios', err)
        });
    });
  }

  public getAllToSinc() {
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM recontagem_planteis WHERE sincronizado = ? OR sincronizado = 0';
      let params = [false];

      return db.executeSql(sql, params)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let recontagens: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              recontagens.push(data.rows.item(i));
            }
            return recontagens;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getAllToSinc',sql, params);
          console.error('Falha ao consultar recontagens para sincronizar', err)
        });
    });
  }

  public find(recontagem_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT rcp.*,\n' +
        '\tprod.nome as produtor,\n' +
        '\tprop.descricao as propriedade,\n' +
        '\tgp.descricao AS galpao\n' +
        '\tFROM recontagem_planteis AS rcp\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON rcp.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON gp.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\tWHERE rcp.id = ?';
      let params = [recontagem_id];

      return db.executeSql(sql, params)
        .then((data: any) => {
          return data.rows.item(0);
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'find',sql, params);
          console.error('Falha ao consultar recontagem find', err)
        });
    });
  }

  public getCelasUteisRecontagem(recontagem_id){
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT \n' +
        '\t\t(int_cel_ut.dias || " " || int_cel_ut.descricao) AS descricao,\n' +
        '\t\trcp_cel_ut.quantidade\n' +
        '\tFROM recontagem_plantel_celas_uteis AS rcp_cel_ut\n' +
        '\t\tLEFT JOIN intervalo_celas_uteis AS int_cel_ut\n' +
        '\t\t\tON rcp_cel_ut.celas_uteis_id = int_cel_ut.id\n' +
        '\tWHERE recontagem_plantel_id = ?';
      let data = [recontagem_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let celasUteis: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              celasUteis.push(data.rows.item(i));
            }
            return celasUteis;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getCelasUteisRecontagem',sql, data);
          console.error('Falha ao consultar celasUteis para getCelasUteisRecontagem', err)
        });
    });
  }

  public getAllToSyncCelasUteis(recontagem_id) {
    return this.dbProvider.getDB().then((db:SQLiteObject) =>{
      let sql = 'SELECT * FROM recontagem_plantel_celas_uteis WHERE recontagem_plantel_id = ? AND id_web IS NULL';
      let data = [recontagem_id];

      return db.executeSql(sql, data)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let celasUteis: any[] = [];
            for (var i = 0; i < data.rows.length; i++) {
              celasUteis.push(data.rows.item(i));
            }
            return celasUteis;
          } else {
            return [];
          }
        }).catch((err) => {
          this.exceptionProvider.insert(err, 'produtores.ts', 'getAllToCelasUteis',sql, data);
          console.error('Falha ao consultar celasUteis para sincronizar', err)
        });
    });
  }


  public getRecontagens(galpao_id, intervalo_id, repetir_contagem = false){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT rcp.id, \n' +
        '\trcp.data_prevista,\n' +
        '\trcp.qtd_contrato_femeas,\n' +
        '\trcp.qtd_contrato_leitoes,\n' +
        '\trcp.qtd_lao_vigente,\n' +
        '\trcp.celas_maternidades,\n' +
        '\trcp.box_gestacao,\n' +
        '\trcp.gestacao_coletiva,\n' +
        '\t(prod.nome || " - " || prop.descricao) as produtor,\n' +
        '\tCast ((JulianDay(rcp.data_prevista) - JulianDay("now")) As Integer) as dias_vencimento,\n' +
        '\tgp.descricao AS galpao\n' +
        '\tFROM recontagem_planteis AS rcp\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON rcp.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON gp.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON prop.produtor_id = prod.id\n' +
        '\tWHERE rcp.galpao_id = ? \n' +
        '\tAND rcp.intervalo_id = ?\n' +
        '\tAND rcp.deleted_at IS NULL';

      if(!repetir_contagem){
        sql +='\tAND rcp.data_recontagem IS NULL \n';
      }

      let params = [galpao_id, intervalo_id];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let recontagens: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            recontagens.push(data.rows.item(i));
          }
          return recontagens;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'getRecontagens', sql);
        console.error('Falha ao executar sql getRecontagens', err)
      });
    });
  }

  public getCelasUteis(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT id,\n' +
        '\t ("cela_util_" ||id) as cela_util,\n' +
        '\t (dias || " " || descricao) as descricao\n' +
        '\tFROM intervalo_celas_uteis \n' +
        '\tWHERE deleted_at IS NULL';

      let params = [];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let recontagens: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            recontagens.push(data.rows.item(i));
          }
          return recontagens;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'getCelasUteis', sql);
        console.error('Falha ao executar sql getCelasUteis', err)
      });
    });
  }

  public getIntervalos(){
    return this.dbProvider.getDB().then((db:SQLiteObject) => {
      let sql = 'SELECT id, ' +
        ' (descricao || " - " || dias || " Dias") as descricao_com_dias' +
        ' FROM intervalos ' +
        ' WHERE deleted_at IS NULL ' +
        '  AND somente_plantel = ?';

      let params = [1];

      return db.executeSql(sql, params).then( (data:any) =>{
        if (data.rows.length > 0) {
          let intervalos: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            intervalos.push(data.rows.item(i));
          }
          return intervalos;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'periodos.ts', 'getIntervalosDeAplicacoesLocal',sql, params);
        console.error('Falha ao executar sql periodos', err)
      });
    });
  }

  public getByDate(initialDate, finalDate){

    return this.dbProvider.getDB().then((db: SQLiteObject) => {
      let params: any[];
      let sql = 'SELECT rp.id,\n' +
        '\tgp.propriedade_id AS propriedade_id,\n' +
        '\tgp.id AS galpao_id,\n' +
        '\trp.intervalo_id,\n' +
        '\trp.intervalo_id,\n' +
        '\tprod.nome AS produtor,\n' +
        '\tgp.descricao AS galpao,\n' +
        '\t"calculator" AS icon, 3 AS type,\n' +
        '\t"Recontagem de Plantel" as title,\n' +
        '\t(prod.nome || " - " || prop.descricao) AS location,\n' +
        '\t"Recontagem de Plantel" AS message,\n' +
        '\trp.data_prevista AS date_limit,\n' +
        '\trp.data_prevista\n' +
        '\tFROM recontagem_planteis AS rp\n' +
        '\t\tLEFT JOIN galpoes AS gp\n' +
        '\t\t\tON rp.galpao_id = gp.id\n' +
        '\t\tLEFT JOIN propriedades AS prop\n' +
        '\t\t\tON gp.propriedade_id = prop.id\n' +
        '\t\tLEFT JOIN produtores AS prod\n' +
        '\t\t\tON gp.produtor_id = prod.id\n' +
        '\tWHERE rp.data_prevista BETWEEN ? AND ? \n' +
        '\t\tAND data_recontagem IS NULL\n' +
        '\t\tAND (rp.deleted_at IS NULL OR rp.deleted_at == "null")\n';

      params = [initialDate, finalDate];

      return db.executeSql(sql, params).then((data: any) => {
        if (data.rows.length > 0) {
          let checklists: any[] = [];
          for (var i = 0; i < data.rows.length; i++) {
            var checklist = data.rows.item(i);
            checklists.push(checklist);
          }
          return checklists;
        } else {
          return [];
        }
      }).catch((err) => {
        this.exceptionProvider.insert(err, 'checklists.ts', 'getByPeriodoAndDate', sql, params);
        console.error('Falha ao executar sql checklists', err, sql, params)
      });
    });
  }

  public insert(recontagem){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into recontagem_planteis (id, intervalo_id, galpao_id, data_prevista, data_recontagem, leitoas_nc, leitoas_c, matrizes_desc, machos_desc, machos_inteiros, machos_vasec, saldo_cidasc_femeas, saldo_cidasc_machos, qtd_ags_lnc, qtd_ags_lc, qtd_ags_mat_desc, qtd_ags_mac_desc, qtd_ags_mac_int, qtd_ags_mac_vas, qtd_contrato_femeas, qtd_contrato_leitoes, qtd_lao_vigente, plantel_produtivo, celas_maternidades, box_gestacao, gestacao_coletiva, assinatura_produtor, observacoes, sincronizado, deleted_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		  let data = [
			  recontagem.id,
			  recontagem.intervalo_id,
			  recontagem.galpao_id,
			  recontagem.data_prevista,
			  recontagem.data_recontagem,
			  recontagem.leitoas_nc,
			  recontagem.leitoas_c,
			  recontagem.matrizes_desc,
			  recontagem.machos_desc,
			  recontagem.machos_inteiros,
			  recontagem.machos_vasec,
			  recontagem.saldo_cidasc_femeas,
			  recontagem.saldo_cidasc_machos,
			  recontagem.qtd_ags_lnc,
			  recontagem.qtd_ags_lc,
			  recontagem.qtd_ags_mat_desc,
			  recontagem.qtd_ags_mac_desc,
			  recontagem.qtd_ags_mac_int,
			  recontagem.qtd_ags_mac_vas,
			  recontagem.qtd_contrato_femeas,
			  recontagem.qtd_contrato_leitoes,
			  recontagem.qtd_lao_vigente,
			  recontagem.plantel_produtivo,
			  recontagem.celas_maternidades,
			  recontagem.box_gestacao,
			  recontagem.gestacao_coletiva,
			  recontagem.assinatura_produtor,
			  recontagem.observacoes,
			  true,
			  recontagem.deleted_at
		  ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'insert',sql,data);
          console.error('Falha ao executar sql recontagem de plantel insert', err)
        });

      });
  }

  public update(recontagem, sincronizado = true){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update recontagem_planteis set intervalo_id=?, galpao_id=?, data_prevista=?, data_recontagem=?, leitoas_nc=?, leitoas_c=?, matrizes_desc=?, machos_desc=?, machos_inteiros=?, machos_vasec=?, saldo_cidasc_femeas=?,saldo_cidasc_machos=?, qtd_ags_lnc=?, qtd_ags_lc=?, qtd_ags_mat_desc=?, qtd_ags_mac_desc=?, qtd_ags_mac_int=?, qtd_ags_mac_vas=?, qtd_contrato_femeas=?, qtd_contrato_leitoes=?, qtd_lao_vigente=?,  plantel_produtivo=?, celas_maternidades=?, box_gestacao=?, gestacao_coletiva=?, assinatura_produtor=?, observacoes=?, sincronizado=?, deleted_at=? where id=?';
        let data = [
          recontagem.intervalo_id,
          recontagem.galpao_id,
          recontagem.data_prevista,
          recontagem.data_recontagem,
          recontagem.leitoas_nc,
          recontagem.leitoas_c,
          recontagem.matrizes_desc,
          recontagem.machos_desc,
          recontagem.machos_inteiros,
          recontagem.machos_vasec,
          recontagem.saldo_cidasc_femeas,
          recontagem.saldo_cidasc_machos,
          recontagem.qtd_ags_lnc,
          recontagem.qtd_ags_lc,
          recontagem.qtd_ags_mat_desc,
          recontagem.qtd_ags_mac_desc,
          recontagem.qtd_ags_mac_int,
          recontagem.qtd_ags_mac_vas,
          recontagem.qtd_contrato_femeas,
          recontagem.qtd_contrato_leitoes,
          recontagem.qtd_lao_vigente,
          recontagem.plantel_produtivo,
          recontagem.celas_maternidades,
          recontagem.box_gestacao,
          recontagem.gestacao_coletiva,
          recontagem.assinatura_produtor,
          recontagem.observacoes,
          sincronizado,
          recontagem.deleted_at,
          recontagem.id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'update',sql,data);
          console.error('Falha ao executar sql recontagem de plantel update', err)
        });
      });
  }

  public updateSinc(id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update recontagem_planteis set sincronizado=? where id=?';
        let data = [
          true,
          id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'updateSinc',sql,data);
          console.error('Falha ao executar sql recontagem de plantel updateSync', err)
        });
      });
  }

  public updateGeolocation(latitude,longitude,id){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update recontagem_planteis set latitude=?, longitude=? where id=?';
        let data = [
          latitude,
          longitude,
          id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'updateGeolocation',sql,data);
          console.error('Falha ao executar sql recontagem de plantel updateGeolocation', err)
        });
      });
  }

  public updateSyncTotalCelasUteis(id_local, id_web){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update recontagem_plantel_celas_uteis set id_web=? where id=?';
        let data = [
          id_web,
          id_local
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'updateSyncTotalCelasUteis',sql,data);
          console.error('Falha ao executar sql updateSyncTotalCelasUteis', err)
        });
      });
  }

  public insertIntervalosCelasuteis(intervalo){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into intervalo_celas_uteis (id, dias, descricao, deleted_at) values (?,?,?,?)';
        let data = [
          intervalo.id,
          intervalo.dias,
          intervalo.descricao,
          intervalo.deleted_at,
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'insertIntervalosCelasuteis',sql,data);
          console.error('Falha ao executar sql intervalo celas uteis', err)
        });

      });
  }

  public updateIntervalosCelasuteis(intervalo){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update intervalo_celas_uteis set dias=?, descricao=?, deleted_at=? where id=?';
        let data = [
          intervalo.dias,
          intervalo.descricao,
          intervalo.deleted_at,
          intervalo.id
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'updateIntervalosCelasuteis',sql,data);
          console.error('Falha ao executar sql intervalo celas uteis', err)
        });
      });
  }

  public insertTotalCela(cela){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into recontagem_plantel_celas_uteis (id_web, recontagem_plantel_id, celas_uteis_id, mobile_key, quantidade, deleted_at) values (?,?,?,?,?,?)';
        let data = [
          cela.id_web,
          cela.recontagem_plantel_id,
          cela.celas_uteis_id,
          cela.mobile_key,
          cela.quantidade,
          cela.deleted_at,
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'insertTotalCela',sql,data);
          console.error('Falha ao executar sql total celas uteis', err)
        });

      });
  }

  public updateTotalCela(cela){
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) =>{
        let sql = 'update recontagem_plantel_celas_uteis set id_web=?, recontagem_plantel_id=?, celas_uteis_id=?, quantidade=?, deleted_at=? where mobile_key=?';
        let data = [
          cela.id_web,
          cela.recontagem_plantel_id,
          cela.celas_uteis_id,
          cela.quantidade,
          cela.deleted_at,
          cela.mobile_key,
        ];

        return db.executeSql(sql, data).catch((err) => {
          this.exceptionProvider.insert(err, 'recontagem-plantel.ts', 'updateTotalCela',sql,data);
          console.error('Falha ao executar sql total celas uteis', err)
        });
      });
  }

}
