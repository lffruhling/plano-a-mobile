import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "../database/database";
import {DbExceptionProvider} from "../db-exception/db-exception";

/*
  Generated class for the VersoesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VersoesProvider {

  constructor(
    private http: HttpClient,
    private dbProvider: DatabaseProvider,
    private exceptionProvider:DbExceptionProvider,
  ) {
  }


  /*
  * ############################
  * API - Métodos
  * ###############
  * */

  public apiGetVersion() {
    return this.http.get('http://versionador.sistemaplanoa.com.br/api/versao/1/APP/ultima/versao');
    // return this.http.get('http://192.168.1.109:8009/api/versao/1/APP/ultima/versao');
  }

  /*
  * ############################
  * BASE DE DADOS - Métodos
  * ###############
  * */
  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM versoes order by id desc ';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let versoes: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                versoes.push(data.rows.item(i));
              }
              return versoes;
            } else {
              return [];
            }
          }).catch((err) => {
            this.exceptionProvider.insert(err, 'versoes.ts', 'getAll',sql);
            console.error('Falha ao executar sql consulta versoes', err)
          });
      });
  }

  /*
  * ############################
  * SINCRONIZAÇÃO - Métodos
  * ###############
  * */
  public atualizaVersoes(){
    /*Atualiza Ações*/
    var promisse = new Promise((resolve,reject) => {
      this.dbProvider.getDB().then((db:SQLiteObject) => {
        this.http.get('http://versionador.sistemaplanoa.com.br/api/versao/1/APP').subscribe((results:any) => {
          if(results){
            for (let result of results){
              let sql_up = 'update versoes SET versao=?, melhorias=?, funcionalidades=?, bugs=?, data_liberacao=? where id = ?;';
              let data_up = [result.versao, result.melhorias, result.funcionalidades, result.bugs, result.data_liberacao, result.id];
              db.executeSql(sql_up,data_up).then((data:any) =>{
                if(data.rowsAffected < 1){
                  let sql_ins = 'INSERT OR IGNORE INTO versoes (id, versao, melhorias, funcionalidades, bugs, data_liberacao) values (?, ?, ?, ?, ?, ?)';
                  let data_ins = [result.id, result.versao, result.melhorias, result.funcionalidades, result.bugs, result.data_liberacao];
                  db.executeSql(sql_ins,data_ins).catch((err)=>{
                    this.exceptionProvider.insert(err, 'versoes.ts', 'atualizaVersoes',sql_ins,data_ins);
                    console.error('Falha ao atualizar versao', err);
                    reject();
                  });
                }
              }).catch((err)=>{
                this.exceptionProvider.insert(err, 'versoes.ts', 'atualizaVersoes',sql_up,data_up);
                console.error('Falha ao atualizar versao', err)
                reject();
              });
            }
          }
          resolve();
        }, err=>{
          // this.exceptionProvider.insert(err, 'versoes.ts', 'atualizaVersoes','http://versionador.sistemaplanoa.com.br/api/versao/1/APP');
          console.error('Erro ao Abrir Base', err);
          reject();
        });
      });
    });
    return promisse;
  }

}
