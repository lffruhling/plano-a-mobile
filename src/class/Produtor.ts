export class Produtor {
    id: number;
    id_web: number;
    estado_id: number;
    municipio_id: number;
    cpf: string;
    nome: string;
    foto: string;
    foto_thumb: string;
    latitude: number;
    longitude:number;
    ativo: boolean;
    matricula:string;
    logradouro:string;
    complemento:string;
    telefone:string;
    celular:string;
    email:string;
    sincronizado:boolean;
    enviar_foto:boolean;
    deleted_at:Date;
    integradora:string;

}