export class BaixaAcao {
    id: number;
    id_web: number;
    id_mobile: number;
    acao_id: number;
    acao_id_web: number;
    resolvido: boolean;
    data: Date;
    resposta: String;
    foto: String;
    com_foto: boolean;
    mobile_key:string;
    deleted_at:Date;
}