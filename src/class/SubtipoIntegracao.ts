export class SubtipoIntegracao {
    id: number;
    tipo_integracao_id: number;
    descricao: string;
    animais_metros_quadrados_transporte: number;
    ativo:boolean;
}