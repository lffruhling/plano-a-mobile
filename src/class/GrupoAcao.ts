export class GrupoAcao {
    id: number;
    descricao: string;
    ativo: boolean;
    deleted_at: Date;
}