export class Municipio {
    id: number;
    codigo: number;
    nome: string;
    uf: string;
}