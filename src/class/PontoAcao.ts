export class PontoAcao {
    id: number;
    grupo_acao_id: number;
    descricao: string;
    deleted_at: Date
}