export class Propriedade {
  id: number;
  id_web: number;
  produtor_id: number;
  integradora_id: number;
  tipo_integracao_id: number;
  subtipo_integracao_id: number;
  estado_id: number;
  municipio_id: number;
  descricao: string;
  logradouro: string;
  complemento: string;
  ie: string;
  latitude: number;
  longitude: number;
  ativo: boolean;
  codErp: number;

  propriedade_concat_produtor?: string;
  integradora?: string;
  produtor?: string;
  sincronizado: boolean;
  deleted_at: Date;

}
