export class Resposta {
  id: number;
  id_web: number;
  id_mobile: number;
  avaliacao_id: number;
  acao_id: number;
  pergunta_id: number;
  valor: number;
  resposta: boolean;
  nao_se_aplica: boolean;
  foto: string;
  foto_base64: string;
  thumb_foto: string;
  data_resposta: Date;
  com_foto: boolean;
  com_acao: boolean;
  mobile_key :string;
  resposta_aux_id:number;
}