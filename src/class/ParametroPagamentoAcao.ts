export class ParametroPagamentoAcao {
    id: number;
    descricao: string;
    amplitude_maxima: number;
    amplitude_minima: number;
    valor: number;
    valor_percentual: boolean;
    ativo: boolean;
}