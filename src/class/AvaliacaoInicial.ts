export class AvaliacaoInicial {
    id: number;
    id_web: number;
    id_mobile: number;
    propriedade_id: number;
    parametro_id: number;
    tecnico_id: number;
    data_visita: Date;
    gerou_acao: boolean;
    observacoes: string;
    foto: string;
    com_foto: boolean;
    mobile_key :string;
}