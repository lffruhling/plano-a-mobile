export class Acao {
    id: number;
    id_web: number;
    id_mobile: number;
    propriedade_id: number;
    tipo_integracao_id: number;
    grupo_acao_id: number;
    ponto_acao_id: number;
    avaliacao_inicial: number;
    parametro_acao_id: number;
    situacao_id: number;
    o_que: string;
    como: string;
    quem: string;
    quando: Date;
    considerar_calculos: boolean;
    com_baixa: boolean;
    foto: string;
    com_foto: boolean;
    mobile_key:string;
    pergunta_id:number;
    deleted_at:Date;
    data_criacao:Date;
	assinatura_produtor:string;
}
