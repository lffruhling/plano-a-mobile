export class Galpao {
  id: number;
  produtor_id: number;
  propriedade_id: number;
  tipo_comedouro_id: number;
  tipo_galpao_id: number;
  descricao: string;
  lotacao: number;
  baias: number;
  altura: number;
  largura: number;
  comprimento: number;
  metros_quadrados: number;
  observacao: string;
  latitude: number;
  longitude: number;
  sincronizado: boolean;
  aplica_chk: boolean;
  aplica_chk_lote: boolean;
  codErp: number;
  ativo: boolean;

  produtor?:string;
  propriedade?:string;
  deleted_at:Date;
}
