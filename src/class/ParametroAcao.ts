export class ParametroAcao {
    id: number;
    parametro_pag_id: number;
    grupo_acao_id: number;
    ponto_acao_id: number;
    tipo_integracao_id: number;
    tipo_info_id: number;
    informacao_padrao: string;
    descricao: string;
    deleted_at: Date;
}