export const SERVERS = [
  {
    nome:'Local11',
    codigo:'LC11',
    url: "http://192.168.0.11:8000/api/v1",
  },{
    nome:'Local12',
    codigo:'LC12',
    url: "http://192.168.0.12:8000/api/v1",
  },{
    nome:'Local13',
    codigo:'LC13',
    url: "http://192.168.0.13:8000/api/v1",
  },{
    nome:'Local100',
    codigo:'LC100',
    url: "http://192.168.1.100:8000/api/v1",
  },{
    nome:'Local100-0',
    codigo:'LC100-0',
    url: "http://192.168.0.100:8000/api/v1",
  },{
    nome:'Local101',
    codigo:'LC101',
    url: "http://192.168.1.101:8000/api/v1",
  },{
    nome:'Local102',
    codigo:'LC102',
    url: "http://192.168.1.102:8000/api/v1",
  },{
    nome:'Local103',
    codigo:'LC103',
    url: "http://192.168.1.103:8000/api/v1",
  },{
    nome:'Local104',
    codigo:'LC104',
    url: "http://192.168.1.104:8000/api/v1",
  },{
    nome:'Local104-0',
    codigo:'LC104-0',
    url: "http://192.168.0.104:8000/api/v1",
  },{
    nome:'Local105',
    codigo:'LC105',
    url: "http://192.168.1.105:8000/api/v1",
  },{
    nome:'Local107',
    codigo:'LC107',
    url: "http://192.168.1.107:8000/api/v1",
  },{
    nome:'Local108',
    codigo:'LC108',
    url: "http://192.168.1.108:8000/api/v1",
  },{
    nome:'Local109',
    codigo:'LC109',
    url: "http://192.168.1.109:8000/api/v1",
  },{
    nome:'Local111',
    codigo:'LC111',
    url: "http://192.168.1.111:8000/api/v1",
  },{
    nome:'Local110',
    codigo:'LC110',
    url: "http://192.168.1.110:8000/api/v1",
  },{
    nome:'Local110-0',
    codigo:'LC110-0',
    url: "http://192.168.0.110:8000/api/v1",
  },{
    nome:'Local115',
    codigo:'LC115',
    url: "http://192.168.0.115:8000/api/v1",
  },{
    nome:'Desenvolvimento',
    codigo:'DE01',
    url: "https://desenvolvimento.sistemaplanoa.com.br/api/v1",
  },{
    nome: 'Demo',
    codigo:'DM01',
    url: "https://demo.sistemaplanoa.com.br/api/v1",
  },{
    nome:'Majestade',
    codigo:'MA01SC',
    url: "https://majestade.sistemaplanoa.com.br/api/v1",
  },{
    nome:'Salus',
    codigo:'SL01',
    url: "https://salus.sistemaplanoa.com.br/api/v1",
  },{
    nome:'CooperA1',
    codigo:'CA1',
    url: "https://coopera1.sistemaplanoa.com.br:3443/api/v1",
  },{
    nome:'CooperAlfa',
    codigo:'CAF',
    url: "https://planoasistemas.cooperalfa.com.br/api/v1",
  },{
    nome:'CALFA',
    codigo:'CAF1',
    url: "https://planoasistemas.cooperalfa.com.br/api/v1",
  },{
    nome:'Friella',
    codigo:'FRI01',
    url: "https://friella.sistemaplanoa.com.br/api/v1",
  },{
    nome:'SSA',
    codigo:'SSA01',
    url: "https://ssa.sistemaplanoa.com.br/api/v1",
  },{
    nome:'Flamboia',
    codigo:'FLA01',
    url: "https://flamboia.sistemaplanoa.com.br/api/v1",
  }
];

//Enum variable for tab
export var EN_TAB_PAGES = {
  EN_TP_ACAO: 0,
  EN_TP_CHECKLIST:1,
  EN_TP_PRODUTOR: 2,
  EN_TP_LENGTH: 3,
};

//A global variable
export var Globals = {//Nav ctrl of each tab page
  navCtrls : new Array(EN_TAB_PAGES.EN_TP_LENGTH),tabIndex:0, //Index of the current tab
  tabs: <any>{}, //Hook to the tab object
};
