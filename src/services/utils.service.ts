import { Injectable } from "@angular/core";
import {File} from "@ionic-native/file";
import {AlertMessagesProvider} from "../providers/alert-messages/alert-messages";
import {DbExceptionProvider} from "../providers/db-exception/db-exception";
import {AcoesProvider} from "../providers/acoes/acoes";
import {AvaliacoesProvider} from "../providers/acoes/avaliacoes";
import {BaixaAcoesProvider} from "../providers/acoes/baixaAcoes";
import {LiberacaoChkProvider} from "../providers/liberacao-chk/liberacao-chk";
import {AvaliacoesChecklistsProvider} from "../providers/checklist/avaliacoes-checklists";
import {RespostasProvider} from "../providers/checklist/respostas";
import {GalpoesProvider} from "../providers/produtores/galpoes";
import {PropriedadesProvider} from "../providers/produtores/propriedades";
import {ProdutoresProvider} from "../providers/produtores/produtores";
import {AuthProvider} from "../providers/auth/auth";
import {LotesProvider} from "../providers/produtores/lotes";
import {PeriodosProvider} from "../providers/periodos/periodos";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";
import {Device} from "@ionic-native/device";
import {LocalNotifications} from "@ionic-native/local-notifications";

@Injectable()
export class UtilsService {

  constructor(
  	private file: File,
    private messageService:AlertMessagesProvider,
    private exceptionProvider:DbExceptionProvider,
    private acaoProvider: AcoesProvider,
    private aviProvider: AvaliacoesProvider,
    private bxAcaoProvider: BaixaAcoesProvider,
    private liberaVigenteProvider: LiberacaoChkProvider,
    private avChkProvider: AvaliacoesChecklistsProvider,
    private respostasChkProvider: RespostasProvider,
    private galpoesProvider: GalpoesProvider,
    private lotesProvider: LotesProvider,
    private propriedadeProvider: PropriedadesProvider,
    private produtorProvider: ProdutoresProvider,
    private periodoAplicacaoProvider: PeriodosProvider,
    private erroProvider: DbExceptionProvider,
    private authProvider: AuthProvider,
    private uniqueDeviceID: UniqueDeviceID,
    private device: Device,
    private localNotifications:LocalNotifications
  ) {}

  checkDisck(){

    let promisse = new Promise((resolve, reject) => {

      this.file.getFreeDiskSpace().then((freeDiskSpace)=>{

        if((freeDiskSpace/1024) < 100){

          this.messageService.showAlert(
            'Armazenamento Baixo!',
            'O armazenamento do seu dispositivo está abaixo do necessário para a captura de imagens. Libere espaço e tente novamente!'
          );

          resolve(false);
        }else{

          resolve(true);
        }
      }).catch(err=>{
        this.exceptionProvider.insert(err, 'utils.ts', 'checkDisck');
        console.log('erro ao consultar espaço do dispositivo',err)
      });
    });

    return promisse;
  }

  validaAcao(acao:any):boolean{

    if(!(acao.propriedade_id > 0)){
      return false;
    }

    if(!(acao.tipo_integracao_id > 0)){
      return false;
    }

    if(!(acao.grupo_acao_id > 0)){
      return false;
    }

    if(!(acao.ponto_acao_id > 0)){
      return false;
    }

    if(!(acao.parametro_acao_id > 0)){
      return false;
    }

    return true;
  }

  clonaObjeto(obj){
    return JSON.parse(JSON.stringify(obj));
  }

  retornaUUID(){
    let promise = new Promise((resolve)=>{
      this.uniqueDeviceID.get().then((uuid: any) => {

        resolve(uuid);
      }).catch(()=>{
        resolve(this.device.uuid);
      })
    });

    return promise;
  }

  bkpDadosColetados(){
    let promise = new Promise((resolve, reject) => {
      this.authProvider.checkLogin().then((result)=>{
        console.log('result', result);
        if(result == 'success'){
          let loading = this.messageService.showLoading('Gerando backup dos dados');
          this.geraBkpAcoes().then(()=>{
            this.geraBkpAvaliacoesIniciais().then(()=>{
              this.geraBkpBaixasAcoes().then(()=>{
                this.geraBkpAvaliacoesChk().then(()=>{
                  this.geraBkpAvaliacoesAuxChk().then(()=>{
                    this.geraBkpRespostasChk().then(()=>{
                      this.geraBkpRespostasAuxChk().then(()=>{
                        this.geraBkpRespostasGerouAcaoChk().then(()=>{
                          this.geraBkpLotes().then(()=>{
                            this.geraBkpPeriodoAplicacoes().then(()=>{
                              this.geraBkpGalpoes().then(()=>{
                                this.geraBkpPropriedades().then(()=>{
                                  this.geraBkpProdutores().then(()=>{
                                    loading.dismiss();
                                    resolve();
                                  });
                                });
                              });
                            })
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
          // this.geraBkpErros();
        }else{
          reject();
        }
      });
    });
    return promise;
  }

  private geraBkpAcoes(){
    let promise = new Promise(resolve=> {
      this.acaoProvider.getAllBkp().then((acoes:Array<any>)=>{
        if(acoes.length > 0){
          this.acaoProvider.apiStoreBkp(acoes).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar acoes', err);
            resolve();
          })
        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('erro ao gerar bkp de ações', err);
        resolve();
      });
    });

    return promise;
  }

  private geraBkpAvaliacoesIniciais(){
    let promise = new Promise(resolve => {
      this.aviProvider.getAllToSinc().then((avaliacoes:Array<any>)=>{
        if(avaliacoes.length > 0){
          this.aviProvider.apiStoreBkp(avaliacoes).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar avaliacoes iniciais', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar avaliacoes iniciais', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpBaixasAcoes(){
    let promise = new Promise(resolve => {
      this.bxAcaoProvider.getAllBkp().then((baixas:Array<any>)=>{
        if(baixas.length > 0){
          this.bxAcaoProvider.apiStoreBkp(baixas).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar baixas acoes', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar baixais acoes', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpAvaliacoesChk(){
    let promise = new Promise(resolve => {
      this.avChkProvider.getAllBkp().then((avaliacoesChk:Array<any>)=>{
        if(avaliacoesChk.length > 0){
          this.avChkProvider.apiStoreAvaliacoesChkBkp(avaliacoesChk).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar avaliacoesChk chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar avaliacoesChk chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpAvaliacoesAuxChk(){
    let promise = new Promise(resolve => {
      this.avChkProvider.getAllAuxBkp().then((avaliacoesAuxChk:Array<any>)=>{
        if(avaliacoesAuxChk.length > 0){
          this.avChkProvider.apiStoreAvaliacoesChkAuxBkp(avaliacoesAuxChk).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar geraBkpAvaliacoesAuxChk chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar avaliacoesChk chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpRespostasChk(){
    let promise = new Promise(resolve => {
      this.respostasChkProvider.getAllBkp().then((respostas:Array<any>)=>{
        if(respostas.length > 0){
          this.respostasChkProvider.apiStoreBkp(respostas).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar respostas chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar respostas chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpRespostasAuxChk(){
    let promise = new Promise(resolve => {
      this.respostasChkProvider.getAllAuxBkp().then((respostas:Array<any>)=>{
        if(respostas.length > 0){
          this.respostasChkProvider.apiStoreAuxBkp(respostas).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar respostas chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar respostas chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpRespostasGerouAcaoChk(){
    let promise = new Promise(resolve => {
      this.acaoProvider.getAllPerguntaAcaobkp().then((respostaAcao:Array<any>)=>{
        if(respostaAcao.length > 0){
          this.respostasChkProvider.apiStoreRespostaAcoesBkp(respostaAcao).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar respostaAcao chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar respostaAcao chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpLotes(){
    let promise = new Promise(resolve => {
      this.lotesProvider.getAllBkp().then((lotes:Array<any>)=>{
        if(lotes.length > 0){
          this.lotesProvider.apiStoreBkp(lotes).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar galpoes chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar lotes chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpPeriodoAplicacoes(){
    let promise = new Promise(resolve => {
      this.periodoAplicacaoProvider.getAllBkp().then((periodos_aplicacoes:Array<any>)=>{
        if(periodos_aplicacoes.length > 0){
          this.periodoAplicacaoProvider.apiStoreBkp(periodos_aplicacoes).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar galpoes chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar periodos_aplicacoes chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpGalpoes(){
    let promise = new Promise(resolve => {
      this.galpoesProvider.getAllBkp().then((galpoes:Array<any>)=>{
        if(galpoes.length > 0){
          this.galpoesProvider.apiStoreBkp(galpoes).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar galpoes chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar galpoes chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpPropriedades(){
    let promise = new Promise(resolve => {
      this.propriedadeProvider.getAllBkp().then((propriedades:Array<any>)=>{
        if(propriedades.length > 0){
          this.propriedadeProvider.apiStoreBkp(propriedades).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar propriedades chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar propriedades chk', err);
        resolve();
      })
    });

    return promise;
  }

  private geraBkpProdutores(){
    let promise = new Promise(resolve => {
      this.produtorProvider.getAllBkp().then((produtores:Array<any>)=>{
        if(produtores.length > 0){
          this.produtorProvider.apiStoreBkp(produtores).subscribe(()=>{
            resolve();
          },err=>{
            console.error('erro ao enviar produtores chk', err);
            resolve();
          })

        }else{
          resolve();
        }
      }).catch(err=>{
        console.error('falha ao enviar produtores chk', err);
        resolve();
      })
    });

    return promise;
  }

  // private geraBkpErros(){
  //   let promise = new Promise(resolve => {
  //     this.erroProvider.getAllBkp().then((erros:Array<any>)=>{
  //       if(erros.length > 0){
  //         this.erroProvider.apiStoreBkp(erros).subscribe(()=>{
  //           resolve();
  //         },err=>{
  //           console.error('erro ao enviar erros chk', err);
  //           resolve();
  //         })
  //
  //       }else{
  //         resolve();
  //       }
  //     }).catch(err=>{
  //       console.error('falha ao enviar erros chk', err);
  //       resolve();
  //     })
  //   });
  //
  //   return promise;
  // }

  /*Notificações locais*/
  async sendNotification(id, title, text, value) {
    await this.localNotifications.schedule({
      id: id,
      title: title,
      text: text,
      progressBar: {
        value: value,
      },
      sticky: true,
      vibrate: false,
      sound: null,
      icon: 'res://notification_icon.png',
      smallIcon: 'res://notification_icon.png',
    });

  }

  async updateNotificationProgress(id:number, progressValue:number, text:string)
  {
    await this.localNotifications.update({
      id:id,
      progressBar:{value:progressValue},
      text:text
    });
  }

  async cancelNotification(id:number)
  {
    await this.localNotifications.cancel(id);
  }
}
