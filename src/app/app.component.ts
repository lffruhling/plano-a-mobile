import {Component, ViewChild} from '@angular/core';
import {AlertController, Events, IonicApp, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {MenuController} from 'ionic-angular';
import {LoginPage} from "../pages/login/login";
import {AuthProvider} from "../providers/auth/auth";
import {DatabaseProvider} from "../providers/database/database";
import localePtBr from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';
import {Tecnico, TecnicoProvider} from "../providers/tecnico/tecnico";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {AlertMessagesProvider} from "../providers/alert-messages/alert-messages";
import {CheckNetworkProvider} from "../providers/check-network/check-network";
import {SobrePage} from "../pages/sobre/sobre";
import {Storage} from "@ionic/storage";
import {Globals, SERVERS} from "../config";
import {BackbuttonService} from "../services/backbutton.service";
import {TabsPage} from "../pages/tabs/tabs";
import {PlanoaPage} from "../pages/planoa/planoa";
import {PerfilPage} from "../pages/perfil/perfil";
import {FileTransfer} from "@ionic-native/file-transfer";
import {SqliteDbCopy} from "@ionic-native/sqlite-db-copy";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {Device} from '@ionic-native/device';
import {SincronizacaoPage} from "../pages/sincronizacao/sincronizacao";
import {UtilsService} from "../services/utils.service";
import {ENV} from '../config/env';
import firebase from 'firebase';
import {SocialSharing} from "@ionic-native/social-sharing";
import {File} from "@ionic-native/file";
import {AppVersion} from "@ionic-native/app-version";
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import {storage} from "firebase/app";
import TaskEvent = firebase.storage.TaskEvent;

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}

@Component({
  templateUrl: 'app.html',
  providers: [
    ScreenOrientation,
    SocialSharing
  ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  tecnico: Tecnico;
  empresa: '';
  img: String = 'assets/imgs/user_default.png';
  pages: PageInterface[];
  alert;
  limpaAlert: boolean = false;

  rootPage: any = 'TabsPage';

  protected app_version: string;
  dadosEnvio: any = {};
  private _COLL: string = "";

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private authProvider: AuthProvider,
              private dbProvider: DatabaseProvider,
              private tecnicoProvider: TecnicoProvider,
              private screenOrientation: ScreenOrientation,
              private sqlite: SQLite,
              private alertCtrl: AlertController,
              private networkService: CheckNetworkProvider,
              private messagesService: AlertMessagesProvider,
              private storage: Storage,
              private backbuttonService: BackbuttonService,
              public menu: MenuController,
              public events: Events,
              private sqliteDbCopy: SqliteDbCopy,
              private fileTransfer: FileTransfer,
              private androidPermissions: AndroidPermissions,
              private device: Device,
              private utils: UtilsService,
              private ionicApp: IonicApp,
              private socialSharing: SocialSharing,
              private file: File,
              private appVersion: AppVersion,
              private sqlitePorter: SQLitePorter,
  ) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    registerLocaleData(localePtBr);

    this.initializeApp();

    this.pages = [
      {title: 'Agenda', pageName: 'TabsPage', tabComponent: 'AgendaPage', index: 0, icon: 'calendar'},
      {title: 'Ações', pageName: 'TabsPage', tabComponent: 'PlanoaPage', index: 1, icon: 'construct'},
      {title: 'Checklist', pageName: 'TabsPage', tabComponent: 'ChecklistPage', index: 2, icon: 'clipboard'},
      {title: 'Produtores', pageName: 'TabsPage', tabComponent: 'ProdutoresPage', index: 3, icon: 'people'},
    ];

    this.registerBackButton();

    this.storage.get('SERVER_URL').then((SERVER_URL: any) => {
      if (SERVER_URL) {
        // this.rootPage = HomePage;
        this.rootPage = 'TabsPage';
        this.dbProvider.createDatabaseCrash();
        this.dbProvider.createDatabaseGeo();

        this.appVersion.getVersionNumber().then(versionNumber => {
          if (versionNumber != null) {
            let local_version = versionNumber.split(".");
            let sum_version = parseInt(local_version[1]) + (parseInt(local_version[2]) / 10);

            // ultima atualização da base de dados foi a 1.20.0 os dois ultimos numeros da versão são somados
            // para testar, quando for gerar uma alteração na base, corrigir o bug de travamento em base acima
            // de 100mb, quando vai atualiza a base trava todo o app
			  console.log('sum_version', sum_version)
            if (sum_version <= 23.7) {
              this.dbProvider.createDatabase()
                .then(() => {
                  this.dbProvider.updateDatabase();
                });
            }

          }
        });


      } else {
        this.rootPage = LoginPage;
        //Criando o banco de dados
        this.dbProvider.createDatabaseCrash();
        this.dbProvider.createDatabaseGeo();
        this.dbProvider.createDatabase()
          .then(() => {
            // fechando a SplashScreen somente quando o banco for criado
            this.dbProvider.updateDatabase();
            this.splashScreen.hide();
          })
          .catch(() => {
            // ou se houver erro na criação do banco
            this.splashScreen.hide();
          });

      }
    });

    authProvider.checkLogin();

    //Altera dados do menu em tempo real
    this.tecnicoProvider.events.subscribe('tecnico:changed', tecnico => {
      if (tecnico != null) {
        this.tecnico = tecnico;
        if (tecnico.foto) {
          this.img = tecnico.foto;
        }
      }
    });

    /*Altera Versão do app em tempo real*/
    this.events.subscribe('version_app:changed', versionNumber => {
      if (versionNumber != null) {
        this.app_version = versionNumber;
      }
    });

    this.events.subscribe('empresa:changed', empresa => {
      if (empresa != null) {
        this.empresa = empresa;
      }
    })
  }

  initializeApp() {
    this.storage.get('NOME_EMPRESA').then((NOME_EMPRESA: any) => {
      this.empresa = NOME_EMPRESA;
    });

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });

    firebase.initializeApp(ENV.firebase);

    this.storage.get('COD_EMPRESA').then((COD_EMPRESA: any) => {
      if (COD_EMPRESA == null) {
        this.storage.get('SERVER_URL').then(SERVER_URL => {
          let server = SERVERS.find(server => server.url === SERVER_URL);
          this.storage.set('COD_EMPRESA', server.codigo);
          this._COLL = `PA_${server.codigo}_NEW`;
        });
      } else {
        this._COLL = `PA_${COD_EMPRESA}_NEW`;
      }
    });

    this.platform.registerBackButtonAction(() => {
      /*Função utitlizada para fechar modais com o botão físico*/
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();
      if (activePortal) {
        activePortal.dismiss();
        return;
      }

      if (this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      } else {
        if (this.nav.canGoBack()) {
          this.nav.pop();
        } else {
          this.platform.exitApp();
        }
      }
    });
  }

  openPage(page: PageInterface) {
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Navega entre as abas e seta como root caso a página a ser navegada não seja uma aba
     */

    let params = {};

    if (page.index) {
      params = {tabIndex: page.index};
    }

    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
    } else {
      this.nav.setRoot(page.pageName, params);
    }
  }

  openAccount() {
    let loading = this.messagesService.showLoading('Carregando dados do técnico... Aguarde!');
    this.tecnicoProvider.get().then((result: any) => {
      if (result != null) {
        loading.dismiss();
        this.nav.push(PerfilPage);
      } else {
        this.events.subscribe('tecnico:changed', tecnico => {
          if (tecnico != null) {
            if (this.nav.getActive().name !== 'PerfilPage') {
              loading.dismiss();
              this.nav.push(PerfilPage);
            }
          }
        });
      }
    }).catch((err) => {
      this.messagesService.showToast('Aguarde os dados do técnico serem baixados!')
      this.events.subscribe('tecnico:changed', tecnico => {
        if (tecnico != null) {

          if (this.nav.getActive().name !== 'PerfilPage') {
            loading.dismiss();
            this.nav.push(PerfilPage);
          }
        }
      });
      console.error('falha ao buscar dados do tecnico', err);
    });
  }

  openPageAbout() {
    this.nav.push(SobrePage);
  }

  openPageSync() {
    this.nav.push(SincronizacaoPage, {automatico: false});
  }

  openPageAccount() {
    this.nav.push(PerfilPage);
  }

  isActive(page: PageInterface) {
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * Verifica se a aba ou página é ativa
    */
    let childNav = this.nav.getActiveChildNav();

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary'
    }
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Sair do Aplicativo',
      message: 'Você deseja realmente sair?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Sim, Sair',
          handler: () => {
            this.authProvider.logout();
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();

  }

  registerBackButton() {
    this.platform.registerBackButtonAction(() => {

      /*Função utitlizada para fechar modais com o botão físico*/
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();
      if (activePortal) {
        activePortal.dismiss();
        return;
      }

      if (this.menu.isOpen()) {
        this.menu.close();
        return;
      }

      let checkHomePage = true;
      for (let i = 0; i < Globals.navCtrls.length; i++) {
        let n = Globals.navCtrls[i];
        if (n) {
          if (n.canGoBack()) {
            let navParams = n.getActive().getNavParams();
            if (navParams) {
              let resolve = navParams.get("resolve");
              if (resolve) {
                n.pop().then(() => resolve({}));
              } else {
                n.pop();
              }
            } else {
              n.pop();
            }
            checkHomePage = false;
            return;
          }
        }
      }

      if (this.nav.getActive().instance instanceof TabsPage && !this.nav.canGoBack()) {
        let popPageVal = this.backbuttonService.popPage();
        if (popPageVal >= 0) {
          this.switchTab(popPageVal);
        } else {
          if (this.limpaAlert) {
            this.alert = null;
          }

          if (!this.alert) {
            this.limpaAlert = false;
            this.alert = this.messagesService.showConfirmationAlert('Sair?', 'Deseja realmente fechar a aplicação?', 'Sair Agora!', 'Cancelar')
              .then((data) => {
                if (data) {
                  this.platform.exitApp();
                } else {
                  this.limpaAlert = true;
                }
              }).catch(err => {
                this.limpaAlert = true;
              });
          }
        }
      } else {
        if (this.nav.canGoBack()) {
          this.nav.pop();
        }
      }
    });
  }

  switchTab(tabIndex) {
    if (Globals.tabs && tabIndex >= 0) {
      Globals.tabIndex = tabIndex;
      Globals.tabs.select(tabIndex);
      Globals.tabs.selectedIndex = tabIndex;
    }
  }

  compact(showToast: boolean) {
    this.dbProvider.getDB().then((db: SQLiteObject) => {
      this.dbProvider.compactDatabase(db, showToast);

    })

  }

  logoutAndDestroy() {
    let alert = this.alertCtrl.create({
      title: 'Sair e Remover Dados',
      message: 'Você deseja realmente sair e remover todos os seus dados?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Sim, Apagar e Sair',
          handler: () => {
            this.extractAndSendDb().then(() => {
              this.authProvider.logout();
              this.nav.setRoot(LoginPage);
              this.sqlite.deleteDatabase({
                name: 'planoa.db',
                location: 'default'
              });
              this.sqlite.deleteDatabase({
                name: 'planoa_crash.db',
                location: 'default'
              });

              this.sqlite.deleteDatabase({
                name: 'planoa_geo.db',
                location: 'default'
              });
            }).catch(err=>{
              console.error('pq não saiu?', err);
            });
          }
        }
      ]
    });
    alert.present();
  }

  exportaBase() {

    let alert = this.alertCtrl.create({
      title: 'Exportar Base de dados',
      message: 'Você deseja realmente exportar os dados?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Sim, Exportar dados',
          handler: () => {

            this.extractAndSendDb();

          }
        }
      ]
    });
    alert.present();
  }

  private extractAndSendDb() {
    let promise = new Promise(resolve => {
      let loading = this.messagesService.showLoading('Exportando base de dados...');
      this.authProvider.checkLogin().then((data: any) => {
        this.dbProvider.getDB().then((db: SQLiteObject) => {
          this.dbProvider.compactDatabase(db).then(() => {
            this.dbProvider.getDB().then((db: SQLiteObject) => {
              this.sqlitePorter.exportDbToSql(db).then((result) => {
                this.file.createFile(this.file.dataDirectory, 'planoa.sql', true).then((retorno) => {
                  this.file.writeFile(this.file.dataDirectory, 'planoa.sql', result, {append: true}).then((sqlFile) => {
                    this.saveBase().then((deu) => {
                      loading.dismiss();
                      resolve();
                    }).catch((nd) => {
                      loading.dismiss();
                      resolve();
                      console.error('não deu ', nd)
                    });
                  }).catch((err) => {
                    console.error('falha ao gerar arquivo', err)
                    loading.dismiss();
                    resolve();
                  });
                }).catch((err) => {
                  console.error('falha ao criar arquivo', err)
                  loading.dismiss();
                  resolve();
                });


              }).catch((err) => {
                console.error('nao exportou?', err)
                loading.dismiss();
                resolve();
              })
            }).catch((err) => {
              console.error('nao exportou?', err)
              loading.dismiss();
              resolve();
            })
          }).catch((err) => {
            console.error('nao exportou?', err)
            loading.dismiss();
            resolve();
          })
        }).catch((err) => {
          console.error('nao exportou?', err)
          loading.dismiss();
          resolve();
        })
      }).catch((err) => {
        console.error('nao exportou?', err)
        loading.dismiss();
        resolve();
      })
    });

    return promise;

  }

  private saveBase() {
    return new Promise((resolve, reject) => {
      this.file.readAsArrayBuffer(this.file.dataDirectory, 'planoa.sql').then((arrFile) => {
        this.storage.get('COD_EMPRESA').then((cod_empresa) => {
          if (cod_empresa.substring(0, 2) == 'LC') {
            this.getTokenFb('tecnico@example.com', '123456').then((user: any) => {
              this.enviaBase(user.uid, arrFile);
              resolve();
            });
          } else {
            this.storage.get('login').then((data) => {
              this.getTokenFb(data.email, data.password).then((user: any) => {
                this.enviaBase(user.uid, arrFile);
                resolve();
              });
            });
          }

        })

      }).catch(err => {
        console.error('falha arr file', err)
      });

    })
  }

  private enviaBase(uidUser: string, file: ArrayBuffer) {
    let storageProgress = storage().ref()
      .child(this._COLL)
      .child(uidUser)
      .child('dumpDataBase')
      .child('planoa.sql')
      .put(file);
    this.utils.sendNotification(1, "Plano A - Enviando...", "Exportando Base de Dados", 0);

    storageProgress.on(TaskEvent.STATE_CHANGED, (snap) => {
      var perc = Math.floor((storageProgress.snapshot.bytesTransferred / storageProgress.snapshot.totalBytes) * 100);
      this.utils.updateNotificationProgress(1, perc, "Exportando Base de Dados " + perc + " %");
    });

    storageProgress.then((obj: any) => {
      this.utils.cancelNotification(1);
    }).catch((error: any) => {
      console.error('terminou?', error)
      this.utils.cancelNotification(1);
    });
  }

  private getTokenFb(email: string, password: string) {

    let promise = new Promise((resolve) => {
      let user = firebase.auth().currentUser;
      if (user == null) {
        firebase.auth().signInWithEmailAndPassword(email, password).then((response) => {
          resolve(response);
        }).catch((err) => {
          console.error('Falha no login fb', err);
          firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((response: any) => {
              resolve(response);
            })
            .catch((error: any) => console.error(error));
        })
      } else {
        resolve(user);
      }
    });

    return promise;
  }


}
