import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ErrorHandler, LOCALE_ID} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {Storage, IonicStorageModule} from "@ionic/storage";
import {AuthProvider} from "../providers/auth/auth";
import {HttpClientModule} from "@angular/common/http";
import {JWT_OPTIONS, JwtModule} from '@auth0/angular-jwt';
import { CalendarModule } from 'ion2-calendar';
import { SQLite } from '@ionic-native/sqlite'
import { DatabaseProvider } from '../providers/database/database';
import { TecnicoProvider } from '../providers/tecnico/tecnico';
import { EstadoProvider } from '../providers/base/estado';
import { MunicipioProvider } from "../providers/base/minucipio";
import {TipoIntegracaoProvider} from "../providers/base/tipoIntegracao";
import {SubtipoIntegracoesProvider} from "../providers/base/SubtipoIntegracoes";
import {TipoInformacaoAcaoProvider} from "../providers/base/TipoInformacaoAcao";
import {SituacaoAcaoProvider} from "../providers/base/SituacaoAcao";
import { ProdutoresProvider } from '../providers/produtores/produtores';
import {PropriedadesProvider} from "../providers/produtores/propriedades";
import {PontoAcoesProvider} from "../providers/acoes/pontoAcoes";
import {ParametroPagamentoAcoesProvider} from "../providers/acoes/parametroPagamentoAcoes";
import {ParametroAcoesProvider} from "../providers/acoes/parametroAcoes";
import {AvaliacoesProvider} from "../providers/acoes/avaliacoes";
import {AcoesProvider} from "../providers/acoes/acoes";
import {BaixaAcoesProvider} from "../providers/acoes/baixaAcoes";
import {GrupoAcoesProvider} from "../providers/acoes/grupoAcoes";
import { AcoesCreatePage } from "../pages/acoes/acoes/create/acoes-create";
import { AcoesEditPage } from "../pages/acoes/acoes/edit/acoes-edit";
import { ModalBaixaPage } from "../pages/acoes/modal-baixa/modal-baixa";
import { ModalAvaliacaoPage } from "../pages/acoes/modal-avaliacao/modal-avaliacao";
import {AppVersion} from "@ionic-native/app-version";
import {Network} from "@ionic-native/network";
import {FileTransfer} from "@ionic-native/file-transfer";
import {File} from "@ionic-native/file";
import {IntegradoraProvider} from "../providers/base/integradora";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";
import { GalpoesProvider } from '../providers/produtores/galpoes';
import { LotesProvider } from '../providers/produtores/lotes';
import { PerguntasProvider } from '../providers/checklist/perguntas';
import { ChecklistProvider } from '../providers/checklist/checklist';
import {PerguntasPage} from "../pages/checklist/perguntas/perguntas";
import {IntroPageModule} from "../pages/intro/intro.module";
import { SlideProvider } from '../providers/slide/slide';
import {TabsPageModule} from "../pages/tabs/tabs.module";
import { AlertMessagesProvider } from '../providers/alert-messages/alert-messages';
import { CheckNetworkProvider } from '../providers/check-network/check-network';
import {SobrePageModule} from "../pages/sobre/sobre.module";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import { RespostasProvider } from '../providers/checklist/respostas';
import {ReactiveFormsModule} from "@angular/forms";
import { UniqueIdProvider } from '../providers/unique-id/unique-id';
import {ModalGeraAcaoPage} from "../pages/modal-gera-acao/modal-gera-acao";
import {ProdutorEditPage} from "../pages/produtores/produtor-edit/produtor-edit";
import {BrMaskerModule} from "brmasker-ionic-3";
import { Geolocation } from '@ionic-native/geolocation';
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {PropriedadesEditPage} from "../pages/produtores/propriedades-edit/propriedades-edit";
import {GalpoesEditPage} from "../pages/produtores/galpoes-edit/galpoes-edit";
import { ComedourosProvider } from '../providers/comedouros/comedouros';
import { BackbuttonService } from "../services/backbutton.service";
import { CotacaoProvider } from '../providers/cotacao/cotacao';
import { IntervalosProvider } from '../providers/intervalos/intervalos';
import { PeriodosProvider } from '../providers/periodos/periodos';
import { EmpresaProvider } from '../providers/empresa/empresa';
import {PerfilPage} from "../pages/perfil/perfil";
import { LiberacaoChkProvider } from '../providers/liberacao-chk/liberacao-chk';
import {ModalSolicitarLiberacaoPage} from "../pages/modal-solicitar-liberacao/modal-solicitar-liberacao";
import { VersoesProvider } from '../providers/versoes/versoes';
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {IonicSelectableModule} from "ionic-selectable";
import {ModalSignPage} from "../pages/modal-sign/modal-sign";
import { AssinaturaProvider } from '../providers/assinatura/assinatura';
import { AvaliacoesChecklistsProvider } from '../providers/checklist/avaliacoes-checklists';
import {UtilsService} from "../services/utils.service";
import {SqliteDbCopy} from "@ionic-native/sqlite-db-copy";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import { Device } from '@ionic-native/device';
import {SincronizacaoPage} from "../pages/sincronizacao/sincronizacao";
import {ProgressBarModule} from "angular-progress-bar";
import {Market} from "@ionic-native/market";
import { DbExceptionProvider } from '../providers/db-exception/db-exception';
import {Push} from "@ionic-native/push";
import { FirestoreDatabaseProvider } from '../providers/firestore-database/firestore-database';
import {Base64} from "@ionic-native/base64";
import {Calendar} from "@ionic-native/calendar";
import {FotosPlanoAcaoPage} from "../pages/fotos-plano-acao/fotos-plano-acao";
import {EditarBaixasPage} from "../pages/baixadas/editar-baixas/editar-baixas";
import {AcoesShowPage} from "../pages/acoes/acoes/show/acoes-show";
import {FileOpener} from "@ionic-native/file-opener";
import {ImageResizer} from "@ionic-native/image-resizer";
import {PlantelContagemPage} from "../pages/produtores/recontagem-plantel/plantel-contagem/plantel-contagem";
import { RecontagemPlantelProvider } from '../providers/recontagem-plantel/recontagem-plantel';
import { GeolocationProvider } from '../providers/geolocation/geolocation';
import {Camera} from "@ionic-native/camera";
import {CameraProvider} from "../providers/camera/camera";
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import {LocalNotifications} from "@ionic-native/local-notifications";
import { VisitasProvider } from '../providers/visitas/visitas';
import {VisitaLotePage} from "../pages/visita-lote/visita-lote";

export function jwtOptionsFactory(storage: Storage) {
  return {
    tokenGetter: () => storage.get('access_token'),
    whitelistedDomains: [
      'localhost:8000',
      '192.168.0.11:8000',
      '192.168.0.12:8000',
      '192.168.0.13:8000',
	  '192.168.0.100:8000',
	  '192.168.1.100:8000',
      '192.168.1.101:8000',
      '192.168.1.102:8000',
      '192.168.1.103:8000',
      '192.168.1.104:8000',
      '192.168.0.104:8000',
      '192.168.1.105:8000',
      '192.168.1.107:8000',
      '192.168.1.108:8000',
      '192.168.1.109:8000',
      '192.168.1.109:8009',
	  '192.168.1.110:8000',
      '192.168.1.111:8000',
      '192.168.0.115:8000',
      'desenvolvimento.sistemaplanoa.com.br',
      'demo.sistemaplanoa.com.br',
      'majestade.sistemaplanoa.com.br',
      'salus.sistemaplanoa.com.br',
      'coopera1.sistemaplanoa.com.br:3443',
      'planoasistemas.cooperalfa.coop.br:50443',
      'planoasistemas.cooperalfa.com.br',
      'friella.sistemaplanoa.com.br',
      'ssa.sistemaplanoa.com.br',
      'flamboia.sistemaplanoa.com.br',
    ]
  }
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    AcoesEditPage,
    AcoesCreatePage,
    ModalBaixaPage,
    ModalAvaliacaoPage,
    ModalGeraAcaoPage,
    ProdutorEditPage,
    PropriedadesEditPage,
    GalpoesEditPage,
    PerguntasPage,
    FotosPlanoAcaoPage,
    PerfilPage,
    ModalSolicitarLiberacaoPage,
    ModalSignPage,
    SincronizacaoPage,
    EditarBaixasPage,
    AcoesShowPage,
    PlantelContagemPage,
    VisitaLotePage,
  ],
  imports: [
    IonicStorageModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage]
      }
    }),
    IonicModule.forRoot(MyApp,{
      tabsHideOnSubPages: true,
      scrollAssist: true,
      autoFocusAssist: true
    }),
    CalendarModule,
    IntroPageModule,
    TabsPageModule,
    SobrePageModule,
    ReactiveFormsModule,
    BrMaskerModule,
    IonicSelectableModule,
    ProgressBarModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    AcoesEditPage,
    AcoesCreatePage,
    ModalBaixaPage,
    ModalAvaliacaoPage,
    ModalGeraAcaoPage,
    ProdutorEditPage,
    PropriedadesEditPage,
    GalpoesEditPage,
    PerguntasPage,
    FotosPlanoAcaoPage,
    PerfilPage,
    ModalSolicitarLiberacaoPage,
    ModalSignPage,
    SincronizacaoPage,
    EditarBaixasPage,
    AcoesShowPage,
    PlantelContagemPage,
    VisitaLotePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: LOCALE_ID, useValue: 'pt-BR'},// Grande sacada para formatar numeros e datas no formato brasileiro
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    SQLite,
    DatabaseProvider,
    TecnicoProvider,
    EstadoProvider,
    MunicipioProvider,
    TipoIntegracaoProvider,
    SubtipoIntegracoesProvider,
    TipoInformacaoAcaoProvider,
    SituacaoAcaoProvider,
    ProdutoresProvider,
    PropriedadesProvider,
    IntegradoraProvider,
    PontoAcoesProvider,
    ParametroPagamentoAcoesProvider,
    ParametroAcoesProvider,
    AvaliacoesProvider,
    AcoesProvider,
    BaixaAcoesProvider,
    GrupoAcoesProvider,
    AppVersion,
    Network,
    Camera,
    FileTransfer,
    File,
    FileOpener,
    UniqueDeviceID,
    GalpoesProvider,
    LotesProvider,
    PerguntasProvider,
    ChecklistProvider,
    SlideProvider,
    AlertMessagesProvider,
    CheckNetworkProvider,
    InAppBrowser,
    RespostasProvider,
    CameraProvider,
    UniqueIdProvider,
    Geolocation,
    Diagnostic,
    LocationAccuracy,
    ComedourosProvider,
    BackbuttonService,
    CotacaoProvider,
    IntervalosProvider,
    PeriodosProvider,
    EmpresaProvider,
    LiberacaoChkProvider,
    VersoesProvider,
    ScreenOrientation,
    AssinaturaProvider,
    AvaliacoesChecklistsProvider,
    UtilsService,
    SqliteDbCopy,
    AndroidPermissions,
    Device,
    Market,
    DbExceptionProvider,
    Push,
    FirestoreDatabaseProvider,
    Base64,
    Calendar,
    ImageResizer,
    RecontagemPlantelProvider,
    GeolocationProvider,
    SQLitePorter,
    LocalNotifications,
    VisitasProvider,
  ]
})
export class AppModule {
}
